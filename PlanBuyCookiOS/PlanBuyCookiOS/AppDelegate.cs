using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using Foundation;
using UIKit;
using EventKit;
using MessageUI;
using Parse;
using AudioToolbox;


namespace PlanBuyCookiOS
{
	// The UIApplicationDelegate for the application. This class is responsible for launching the
	// User Interface of the application, as well as listening (and optionally responding) to
	// application events from iOS.
	[Register ("AppDelegate")]
	public partial class AppDelegate : UIApplicationDelegate
	{
        public static readonly string sPBCWebPage = "http://www.planbuycook.com.au/tutorial";
		static Settings mSettings = new Settings();
		static RecipeManager mRecipeManager;
		static PBCCalendar mPBCCalender = new PBCCalendar ();
        static MeasurementUnits mUnits = new MeasurementUnits(bXmlInitialise:false);

		// class-level declarations
		static UIWindow window;
		static CookPageViewController mCookPageViewController;
		static PlanPageViewController mPlanPageViewController;
		static BuyPageViewController mBuyPageViewController;

        static UIPopoverController sSettingsController;

        static bool sbRunTestFunctions = false;
        public static bool sbWebRecipesEnabled = false;

        static UIPopoverController SettingsController
        {
            get
            {
                if (sSettingsController == null)
                {
                    sSettingsController = new UIPopoverController(new SettingsPopoverViewController());
                    sSettingsController.PopoverContentSize = sSettingsController.ContentViewController.View.Frame.Size;
                    sSettingsController.BackgroundColor = UIColor.White;
					sSettingsController.DidDismiss += (object dismissSender, EventArgs e) => (sSettingsController.ContentViewController as SettingsPopoverViewController).UpdateSettings();
                }
                return sSettingsController;
            }
        }

		public static Settings Settings
		{
			get {
				return mSettings;
			}
		}

        public static void OpenSettings(UIView reference, UIButton settingsButton)
        {
            SettingsController.PresentFromRect(settingsButton.Frame, reference, UIPopoverArrowDirection.Up,true);
        }



//        public static async void InitialiseBuddyAnalytics()
//        {
//            Buddy.Init("bbbbbc.zqfbbcGnfJPp", "8DB63F65-7302-4D0B-A160-473AB1322D44");
//
//            if (mSettings.EMail != Settings.cDefaultEmail)
//            {
//                var searchUser = await Buddy.Users.FindAsync("");
//
//                bool bFoundUser = false;
//                if (searchUser.IsSuccess)
//                    bFoundUser = searchUser.PageResults.Where(u => u.Username == mSettings.EMail).Count() > 0;
//
//                if (bFoundUser)
//                {
//                    Console.WriteLine("Buddy User {0} found. ", mSettings.EMail);
//
//                    var loginTask = await Buddy.LoginUserAsync(mSettings.EMail, "pass123");
//
//                    if (loginTask.IsSuccess)
//                    {
//                        Console.WriteLine("Buddy User {0} logged in. ", mSettings.EMail);
//                    }
//                    else
//                    {
//                        Console.WriteLine("Failed to login Buddy User {0}. ", mSettings.EMail);
//                    }
//                }
//                else
//                {
//                    Console.WriteLine("Creating new Buddy User");
//
//                    if (mSettings.EMail != "")
//                    {
//                        var userTask = await Buddy.CreateUserAsync(mSettings.EMail, "pass123");
//
//                        if (userTask.IsSuccess)
//                        {
//                            Console.WriteLine("Successfully created new Buddy User " + mSettings.EMail);
//
//                            var loginTask = await Buddy.LoginUserAsync(mSettings.EMail, "pass123");
//
//                            if (loginTask.IsSuccess)
//                            {
//                                Console.WriteLine("Buddy User {0} logged in. ", mSettings.EMail);
//                            }
//
//                        }
//                        else
//                        {
//                            Console.Error.WriteLine("Failed to create Buddy user " + mSettings.EMail);
//                        }
//                    }
//                }
//            }
//        }

        //return true or false is access granted.  This is a blocking call
        public static void RequestCalendarAccess(Action onCalendarAccessGranted, Action onCalendarAccessDenied)
        {
            PBCCalendar.EventStore.RequestAccess (EKEntityType.Event, 
                (bool granted, NSError e) => 
            {
                if (granted)
                {
                    PBCCalendar.AccessToCalendars = true;
                    PBCCalendar.EnumerateCalendars();

                    if (PBCCalendar.GetPBCCalendar() == null)
                    {
                        PBCCalendar.AccessToCalendars = false;

                        if (onCalendarAccessDenied != null)
                            onCalendarAccessDenied();

                        return;
                    }                        

                    PBCCalendar.EnumerateAllEvents();
                    PBCCalendar.PopulatePlannerFromCalendar();

                    if (onCalendarAccessGranted != null)
                        onCalendarAccessGranted();
                }
                else
                {
                    PBCCalendar.AccessToCalendars = false;

                    if (onCalendarAccessDenied != null)
                        onCalendarAccessDenied();

                    return;
                }

            } );
        }

        public void CalendarAccessAccepted()
        {
            InvokeOnMainThread( 
                ()=> 
                {
                    Console.WriteLine("Calendar access accepted");
                    mPlanPageViewController.ResetCalendarViewPosition(); mPlanPageViewController.RefreshCalendarView();
                });
        }

        public void CalendarAccessDenied()
        {
            InvokeOnMainThread(
                () =>
                {
                    new UIAlertView("PlanBuyCook Calender Access", 
                        "PlanBuyCook relies on access to your Calendar to save its meal planning data. The app maintains its own calendar and will not affect your existing calendars.  Not giving access to the calendar will severely diminish PlanBuyCook's utilty. If you have already denied permission you can reset this by opening Settings, and resetting General >Reset >Location & Privacy, followed by restarting PlanBuyCook.", null,
                        "OK", null).Show();
                });
        }



		//
		// This method is invoked when the application has loaded and is ready to run. In this
		// method you should instantiate the window, load the UI into it and then make the window
		// visible.
		//
		// You have 17 seconds to return from this method, or iOS will terminate your application.
		//
		public override bool FinishedLaunching (UIApplication app, NSDictionary options)
		{
            AudioSession.Initialize();

			mSettings.Initialise ();

            //InitialiseBuddyAnalytics();

            mRecipeManager = new RecipeManager();

			// create a new window instance based on the screen size
			window = new UIWindow (UIScreen.MainScreen.Bounds);
			
			// If you have defined a root view controller, set it here:
			mCookPageViewController = new CookPageViewController ();
			mPlanPageViewController = new PlanPageViewController ();
			mBuyPageViewController = new BuyPageViewController ();

			//window.RootViewController = mCookPageViewController;
			window.RootViewController = mPlanPageViewController;
            window.MakeKeyAndVisible ();

            if (!AppDelegate.Settings.SkipIntro)
                mPlanPageViewController.PlayMovie();
            else
                mPlanPageViewController.PresentBusyIndicator();

            AsyncInitialise();

            // make the window visible
            mRecipeManager.mOnFinishedLoadingRecipes = () =>
            {
                RequestCalendarAccess (() => CalendarAccessAccepted(), () => CalendarAccessDenied() );

                mPlanPageViewController.CreateMosaicLayout();
                mPlanPageViewController.RefreshMosaicView();
                    
                if (AppDelegate.Settings.SkipIntro)
                {
                    mPlanPageViewController.HideBusyIndicator();
                    mPlanPageViewController.HideMovie();
                }
                    
                if (sbRunTestFunctions)
                {
                    RecipeManager.Instance.TestMasterShoppingList();
                    RecipeManager.Instance.TestRecipeUnits();
                }
            };
			return true;
		}

        public async void AsyncInitialise()
        {
            await MasterIngredientsList.Instance.Initialise();
            await mRecipeManager.Initialise();
        }

		public static UIView PlanPage
		{
			get
			{
				return mPlanPageViewController.View;
			}
		}
		public static UIView CookPage
		{
			get
			{
				return mCookPageViewController.View;
			}
		}
		public static PlanPageViewController PlanPageViewController
		{
			get
			{
				return mPlanPageViewController;
			}
		}
		public static CookPageViewController CookPageViewController
		{
			get
			{
				return mCookPageViewController;
			}
		}
		public static BuyPageViewController BuyPageViewController
		{
			get
			{
				return mBuyPageViewController;
			}
		}
          
		static public void SwitchToPlanPage()
		{
			window.RootViewController = mPlanPageViewController;

            mPlanPageViewController.ResetCalendarViewPosition();
			mPlanPageViewController.RefreshCalendarView ();
            mPlanPageViewController.RefreshMosaicView();

			window.MakeKeyAndVisible ();
		}

        public static bool IsOnCookPage()
        {
            return window.RootViewController == mCookPageViewController;
        }

		static public void SwitchToCookPage()
		{
			window.RootViewController = mCookPageViewController;
			window.MakeKeyAndVisible ();
			mCookPageViewController.UpdateRecipe (mPlanPageViewController.GetSelectedRecipe ());
		}

        static public void SwitchToCookPage(IRecipeImport recipe)
        {
            window.RootViewController = mCookPageViewController;
            window.MakeKeyAndVisible ();
            mCookPageViewController.UpdateRecipe (recipe);
        }

        static public void SwitchToCookPage(CalenderDay.Meal meal)
        {
            window.RootViewController = mCookPageViewController;
            window.MakeKeyAndVisible ();
            mCookPageViewController.UpdateRecipe (meal);
        }

		static public void SwitchToBuyPage()
		{
			window.RootViewController = mBuyPageViewController;
			mBuyPageViewController.SetMealPlannerDayList (mPlanPageViewController.GetMealPlanForShoppingList ());

			window.MakeKeyAndVisible ();
		}


        static MFMailComposeViewController mMailController;

        static public void EmailShoppingList(string htmlShoppingList)
        {
            mMailController = new MFMailComposeViewController ();

            //Leave an emty recipients list if the user hasn't edited settings
            if (Settings.EMail == Settings.cDefaultEmail)
                mMailController.SetToRecipients (new string[]{""});
            else
                mMailController.SetToRecipients (new string[]{Settings.EMail});

            mMailController.SetSubject ("PlanBuyCook Shopping List");
            mMailController.SetMessageBody (htmlShoppingList, true);

            mMailController.Finished += async ( object s, MFComposeResultEventArgs args) => {

                Console.WriteLine (args.Result.ToString ());

                args.Controller.DismissViewController (true, null);

                if (args.Result == MFMailComposeResult.Sent)
                {
                    mBuyPageViewController.GetMealPlannerSource.FlagRecipesAsShopped();
  				    mBuyPageViewController.RefreshShoppingList();
                }
            };

            window.RootViewController.PresentViewController (mMailController, true, null);
        }
	}
}



