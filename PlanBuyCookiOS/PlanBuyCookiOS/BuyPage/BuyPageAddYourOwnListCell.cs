using System;
using System.Collections.Generic;
using CoreGraphics;
using Foundation;
using UIKit;

namespace PlanBuyCookiOS
{
    public partial class BuyPageAddYourOwnListCell : UITableViewCell
    {
        public readonly float IngredientsListTitleFontSize = 11.0f;

        public static readonly UINib Nib = UINib.FromName("BuyPageAddYourOwnListCell", NSBundle.MainBundle);
        public static readonly NSString Key = new NSString("BuyPageAddYourOwnListCell");

        public BuyPageAddYourOwnListCell(IntPtr handle) : base(handle)
        {
        }

        public static BuyPageAddYourOwnListCell Create()
        {
            var cell = (BuyPageAddYourOwnListCell)Nib.Instantiate(null, null)[0];

            cell.InitialiseFonts();

            return cell;
        }

        public void InitialiseFonts()
        {
            FoodName.Font = UIFont.FromName ("SceneStd-Regular", IngredientsListTitleFontSize);
            QuantityTextField.Font = UIFont.FromName ("SceneStd-Regular", IngredientsListTitleFontSize);
            UnitsTextField.Font = UIFont.FromName ("SceneStd-Regular", IngredientsListTitleFontSize);
        }

        public void SetFood(string mFood)
        {
            QuantityTextField.Text = "";
            UnitsTextField.Text = "";

            FoodName.Text = mFood;
            FoodName.LineBreakMode = UILineBreakMode.WordWrap;
        }

        public float GetFoodNameWidth()
        {
			return (float)FoodName.Frame.Width;
        }

        partial void AddToMasterShoppingList(NSObject sender)
        {
            //This food SHOULD be in the list otherwise it shouldn't even exist
            MasterIngredientsList.Ingredient  masterIngredient = MasterIngredientsList.FindIngredient(FoodName.Text);

            string food = FoodName.Text;
            string unit = UnitsTextField.Text;

            float quantity;

            if (QuantityTextField.Text=="")
                quantity = 1;
            else
                float.TryParse(QuantityTextField.Text, out quantity);

            if (unit == "unit" || unit=="")
                unit = "number";

            if (masterIngredient != null)
            {
                MasterShoppingList.AddIngredient(masterIngredient.Section, new ShoppingListIngredient(food, quantity, unit, masterIngredient.Aisle));
                AppDelegate.BuyPageViewController.RemoveFromAddYourOwn(food);
                AppDelegate.BuyPageViewController.RefreshEditedLists(masterIngredient.Section,"AddYourOwn");
            }
            else
            {
                MasterShoppingList.AddIngredient("household", new ShoppingListIngredient(food, quantity, unit,""));
                AppDelegate.BuyPageViewController.RemoveFromAddYourOwn(food);
                AppDelegate.BuyPageViewController.RefreshEditedLists("household","AddYourOwn");
            }
        }
    }
}

