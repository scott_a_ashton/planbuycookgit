// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace PlanBuyCookiOS
{
	[Register ("BuyPageAddYourOwnListCell")]
	partial class BuyPageAddYourOwnListCell
	{
		[Outlet]
		UIKit.UILabel FoodName { get; set; }

		[Outlet]
		UIKit.UITextField QuantityTextField { get; set; }

		[Outlet]
		UIKit.UITextField UnitsTextField { get; set; }

		[Action ("AddToMasterShoppingList:")]
		partial void AddToMasterShoppingList (Foundation.NSObject sender);
		
		void ReleaseDesignerOutlets ()
		{
			if (FoodName != null) {
				FoodName.Dispose ();
				FoodName = null;
			}

			if (QuantityTextField != null) {
				QuantityTextField.Dispose ();
				QuantityTextField = null;
			}

			if (UnitsTextField != null) {
				UnitsTextField.Dispose ();
				UnitsTextField = null;
			}
		}
	}
}
