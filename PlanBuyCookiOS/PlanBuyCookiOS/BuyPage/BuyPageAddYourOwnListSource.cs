using System;
using System.Collections.Generic;
using CoreGraphics;
using System.Linq;
using Foundation;
using UIKit;


namespace PlanBuyCookiOS
{
    public class BuyPageAddYourOwnListSource : UITableViewSource
    {
        static List<ShoppingListIngredient> mAddYourOwnList = new List<ShoppingListIngredient>();

        public BuyPageAddYourOwnListSource ()
        {
        }
            
        public override nint RowsInSection (UITableView tableview, nint section)
        {
            return mAddYourOwnList.Count;
        }

        public override nint NumberOfSections (UITableView tableView)
        {
            // TODO: return the actual number of sections
            return 1;
        }

        public override UITableViewCell GetCell (UITableView tableView, NSIndexPath indexPath)
        {
            var cell = tableView.DequeueReusableCell (BuyPageAddYourOwnListCell.Key) as BuyPageAddYourOwnListCell;

            if (cell == null)
                cell = BuyPageAddYourOwnListCell.Create();
                
            cell.SetFood(mAddYourOwnList[indexPath.Row].mFood);
            cell.SizeToFit();

            return cell;
        }

        public override nfloat GetHeightForRow (UITableView tableView, NSIndexPath indexPath)
        {
            List<ShoppingListIngredient> shoppingList = mAddYourOwnList;
            NSString methodText = new NSString(shoppingList[indexPath.Row].mFood);
            CGSize stringSize;

            stringSize = methodText.StringSize (UIFont.FromName ("SceneStd-Regular", BuyPageShoppingListIngredientCell.cIngredientFontSize), new CGSize (86, 99999), UILineBreakMode.WordWrap);

            if (stringSize.Height > 36.0)
                return 20.0f * stringSize.Height/BuyPageShoppingListIngredientCell.cIngredientFontSize;
            else
                return 36;
        }

        public void SetIngredientsList(List<MasterIngredientsList.Ingredient> suggestions)
        {
            mAddYourOwnList = suggestions.Select(suggest => new ShoppingListIngredient(suggest.Name, 1.0f, "unit","")).ToList();           
        }
        public void AddToIngredientsList(List<MasterIngredientsList.Ingredient> suggestions)
        {
            mAddYourOwnList = mAddYourOwnList.Concat(suggestions.Select(suggest => new ShoppingListIngredient(suggest.Name, 1.0f, "unit",""))).ToList();           
        }

        public override void RowUnhighlighted (UITableView tableView, NSIndexPath rowIndexPath)
        {
        }

        public override void RowHighlighted (UITableView tableView, NSIndexPath rowIndexPath)
        {
        }

        public override void RowDeselected (UITableView tableView, NSIndexPath indexPath)
        {

        }

        public override void RowSelected (UITableView tableView, NSIndexPath indexPath)
        {
        }
            
        public void RemoveIngredient(UITableView tableView, string ingredient)
        {
            int index = mAddYourOwnList.FindIndex(ing => ing.mFood.ToLower() == ingredient.ToLower());

            if (index != -1)
            {
                NSIndexPath[] rows = new NSIndexPath[]{ NSIndexPath.FromItemSection(index, 0) };
                tableView.ReloadRows(rows, UITableViewRowAnimation.Automatic);

                mAddYourOwnList.RemoveAt(index);
            }
        }           
    }
}

