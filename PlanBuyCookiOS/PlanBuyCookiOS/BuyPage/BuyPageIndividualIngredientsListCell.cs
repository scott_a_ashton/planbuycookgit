using System;
using System.Collections.Generic;
using CoreGraphics;
using System.Linq;
using Foundation;
using UIKit;



namespace PlanBuyCookiOS
{
	public partial class BuyPageIndividualIngredientsListCell : UICollectionViewCell
	{
        public static UIPopoverController sSearchResultsPopover;
        public static BuyPageSearchItemsResultsController sSearchResultsController;
        static BuyPageIndividualIngredientsListCell sCurrentPopoverIngredientsListCell;

        public readonly float IngredientsListTitleFontSize = 13.0f;

		public static readonly UINib Nib = UINib.FromName ("BuyPageIndividualIngredientsListCell", NSBundle.MainBundle);
		public static readonly NSString Key = new NSString ("BuyPageIndividualIngredientsListCell");

		public BuyPageIndividualIngredientsListCell (IntPtr handle) : base (handle)
		{
		}

		public static BuyPageIndividualIngredientsListCell Create ()
		{
			return (BuyPageIndividualIngredientsListCell)Nib.Instantiate (null, null) [0];
		}

        public UITableView GetIngredientsList()
        {
            return IngredientsList;
        }

        public void UpdateContentViewIndicatorIsTooLarge()
        {
            ScrollArrow.Hidden = !(IngredientsList.ContentSize.Height >= BuyPageMasterIngredientsListLayout.cHeightOfShoppingList);
        }

        static UIPopoverController SearchResultsPopover(BuyPageIndividualIngredientsListCell cell)
        {
            sCurrentPopoverIngredientsListCell = cell;

            if (sSearchResultsPopover == null)
            {
                sSearchResultsPopover = new UIPopoverController(new BuyPageSearchItemsResultsController());
                sSearchResultsPopover.BackgroundColor = UIColor.White;
                sSearchResultsPopover.DidDismiss += (object dismissSender, EventArgs e) => cell.UpdateAddYourOwnFromSearchResults(); cell.ClearSearchResultsPopover();
            }
            return sSearchResultsPopover;
        }
        //Hacky: If we don't know the cell, access with the current recently accessed cell.
        public static void DismissSearchResultsPopover()
        {
            if (sCurrentPopoverIngredientsListCell != null)
            {
                sCurrentPopoverIngredientsListCell.UpdateAddYourOwnFromSearchResults(); 
                sCurrentPopoverIngredientsListCell.ClearSearchResultsPopover();
                SearchResultsPopover(sCurrentPopoverIngredientsListCell).Dismiss(true);
            }
        }

        public void SetEditMode(bool bIsBeingEdited)
        {
            EditAmountsButton.Hidden = bIsBeingEdited;
            FinishEditingButton.Hidden = !bIsBeingEdited;
        }

        public void InitAddYourOwn()
        {
            IngredientsListTitle.Hidden = true;
            SearchIngredientsField.Hidden = false;
            SearchIngredientsField.Text = "Search items to add";
            EditAmountsButton.Hidden = true;          
            IngredientsList.Hidden = true;
            FinishEditingButton.Hidden = true;

            SearchIngredientsField.Font = UIFont.FromName ("SceneStd-Regular", IngredientsListTitleFontSize);

            AddYourOwnList.Source = new BuyPageAddYourOwnListSource();
            AddYourOwnList.ReloadData();
            AddYourOwnList.Hidden = false;

            if (sSearchResultsController == null)
                sSearchResultsController = new BuyPageSearchItemsResultsController();
        }

		public void SetIngredientsListTitle(string title)
		{
            IngredientsListTitle.Hidden = false;
            SearchIngredientsField.Hidden = true;
            EditAmountsButton.Hidden = false;
            AddYourOwnList.Hidden = true;
            //FinishEditingButton.Hidden = true;

            IngredientsList.Hidden = false;
            IngredientsListTitle.Font = UIFont.FromName ("SceneStd-Black", IngredientsListTitleFontSize);
            IngredientsListTitle.Text = title.ToUpper();
		    IngredientsList.Source = new BuyPageIndividualShoppingListSource(title);
		}

        partial void ClickEditAmountsButton(NSObject sender)
        {
            string shoppingListName = (IngredientsList.Source as BuyPageIndividualShoppingListSource).mShoppingListSectionName;

            string oldShoppingListname = MasterShoppingList.EditedShoppingListName;
            MasterShoppingList.EditedShoppingListName = shoppingListName;

            SetEditMode(true);

            AppDelegate.BuyPageViewController.RefreshEditedLists(newListName:shoppingListName, oldListName:oldShoppingListname);
        }

        partial void ClickFinishEditingButton(NSObject sender)
        {
            string shoppingListName = (IngredientsList.Source as BuyPageIndividualShoppingListSource).mShoppingListSectionName;

            string oldShoppingListname = MasterShoppingList.EditedShoppingListName;
            MasterShoppingList.EditedShoppingListName = shoppingListName;

            SetEditMode(false);

            AppDelegate.BuyPageViewController.RefreshEditedLists(newListName:shoppingListName, oldListName:oldShoppingListname);
        }


        public void UpdateSearchResultsPopover(List<MasterIngredientsList.Ingredient> results)
        {
            SearchResultsPopover(this).PresentFromRect(SearchIngredientsField.Frame, this, UIPopoverArrowDirection.Up , true);
            sSearchResultsPopover.PopoverContentSize = new CGSize(SearchIngredientsField.Frame.Width + 50.0f, 500.0f);

            string searchItem = SearchIngredientsField.Text.ToLower().Trim();

            if (results.Count() == 0 || results.Count() > 0 && !results[0].Name.StartsWith(searchItem))
            {
                MasterIngredientsList.Ingredient missingItem = new MasterIngredientsList.Ingredient(){ Name = searchItem, Section = "Household" };
                results.Insert(0, missingItem);

                ((SearchResultsPopover(this).ContentViewController as BuyPageSearchItemsResultsController).TableView.Source as BuyPageSearchItemsResultsSource).UpdateResults(results, bNoDirectHits: true);
            }
            else
            {
                ((SearchResultsPopover(this).ContentViewController as BuyPageSearchItemsResultsController).TableView.Source as BuyPageSearchItemsResultsSource).UpdateResults(results, bNoDirectHits: false);
            }


            (SearchResultsPopover(this).ContentViewController as BuyPageSearchItemsResultsController).TableView.ReloadData();
        }
        public void ClearSearchResultsPopover()
        {
            SearchResultsPopover(this).PresentFromRect(SearchIngredientsField.Frame, this, UIPopoverArrowDirection.Up , true);
            sSearchResultsPopover.PopoverContentSize = new CGSize(SearchIngredientsField.Frame.Width + 50.0f, 100.0f);

            ((SearchResultsPopover(this).ContentViewController as BuyPageSearchItemsResultsController).TableView.Source as BuyPageSearchItemsResultsSource).Reset();
            (SearchResultsPopover(this).ContentViewController as BuyPageSearchItemsResultsController).TableView.ReloadData();
        }

        public void UpdateAddYourOwnFromSearchResults()
        {               
            SearchIngredientsField.ResignFirstResponder();

            (AddYourOwnList.Source as BuyPageAddYourOwnListSource).SetIngredientsList(((SearchResultsPopover(this).ContentViewController as BuyPageSearchItemsResultsController).TableView.Source as BuyPageSearchItemsResultsSource).SelectedIngredients);
            AddYourOwnList.ReloadData();
        }

        public void RemoveIngredient(string ingredient)
        {
            (AddYourOwnList.Source as BuyPageAddYourOwnListSource).RemoveIngredient(AddYourOwnList, ingredient);
            AddYourOwnList.ReloadData();
        }

        partial void SearchIngredientsKeyPressed(NSObject sender)
        {
            string searchString  = (sender as SearchIngredientsTextField).Text.ToLower().Trim();

            string[] words = searchString.Split(' ');

            //The first search is the whole word
            List<MasterIngredientsList.Ingredient> suggestions = new List<MasterIngredientsList.Ingredient>();

            suggestions = MasterIngredientsList.GetSuggestions(searchString.Trim());

            foreach (string word in words)
            {
                if (word.Trim()!="")
                    suggestions = suggestions.Concat(MasterIngredientsList.GetSuggestions(word)).Distinct().ToList();
            }

            //Update even if the list is empty
            UpdateSearchResultsPopover(suggestions);
        }
	}
}

