// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace PlanBuyCookiOS
{
	[Register ("BuyPageIndividualIngredientsListCell")]
	partial class BuyPageIndividualIngredientsListCell
	{
		[Outlet]
		UIKit.UITableView AddYourOwnList { get; set; }

		[Outlet]
		UIKit.UIButton EditAmountsButton { get; set; }

		[Outlet]
		UIKit.UIButton FinishEditingButton { get; set; }

		[Outlet]
		UIKit.UITableView IngredientsList { get; set; }

		[Outlet]
		UIKit.UILabel IngredientsListTitle { get; set; }

		[Outlet]
		UIKit.UIImageView ScrollArrow { get; set; }

		[Outlet]
		UIKit.UITextField SearchIngredientsField { get; set; }

		[Action ("ClickEditAmountsButton:")]
		partial void ClickEditAmountsButton (Foundation.NSObject sender);

		[Action ("ClickFinishEditingButton:")]
		partial void ClickFinishEditingButton (Foundation.NSObject sender);

		[Action ("SearchIngredientsKeyPressed:")]
		partial void SearchIngredientsKeyPressed (Foundation.NSObject sender);
		
		void ReleaseDesignerOutlets ()
		{
			if (ScrollArrow != null) {
				ScrollArrow.Dispose ();
				ScrollArrow = null;
			}

			if (AddYourOwnList != null) {
				AddYourOwnList.Dispose ();
				AddYourOwnList = null;
			}

			if (EditAmountsButton != null) {
				EditAmountsButton.Dispose ();
				EditAmountsButton = null;
			}

			if (FinishEditingButton != null) {
				FinishEditingButton.Dispose ();
				FinishEditingButton = null;
			}

			if (IngredientsList != null) {
				IngredientsList.Dispose ();
				IngredientsList = null;
			}

			if (IngredientsListTitle != null) {
				IngredientsListTitle.Dispose ();
				IngredientsListTitle = null;
			}

			if (SearchIngredientsField != null) {
				SearchIngredientsField.Dispose ();
				SearchIngredientsField = null;
			}
		}
	}
}
