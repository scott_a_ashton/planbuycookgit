using System;
using System.Collections.Generic;
using CoreGraphics;
using Foundation;
using UIKit;


namespace PlanBuyCookiOS
{
    public class BuyPageIndividualShoppingListSource : UITableViewSource
    {
        public string mShoppingListSectionName;

        public BuyPageIndividualShoppingListSource (string sectionName)
        {
            mShoppingListSectionName = sectionName;
        }

        public override nint RowsInSection (UITableView tableview, nint section)
        {
            List<ShoppingListIngredient> shoppingList = MasterShoppingList.GetShoppingList(mShoppingListSectionName);

            if (shoppingList != null)
                return shoppingList.Count;

            return 0;
        }
            
        public override nint NumberOfSections (UITableView tableView)
        {
            // TODO: return the actual number of sections
            return 1;
        }

        public override UITableViewCell GetCell (UITableView tableView, NSIndexPath indexPath)
        {
            var cell = tableView.DequeueReusableCell (BuyPageShoppingListIngredientCell.Key) as BuyPageShoppingListIngredientCell;

            if (cell == null)
                cell = BuyPageShoppingListIngredientCell.Create();

            List<ShoppingListIngredient> shoppingList = MasterShoppingList.GetShoppingList(mShoppingListSectionName);

            if (shoppingList != null && indexPath.Row < shoppingList.Count)
            {
                if (MasterShoppingList.GetEditMode(mShoppingListSectionName))
                {
                    if (!shoppingList[indexPath.Row].mbIgnore)
                        cell.SetEditIngredient(shoppingList[indexPath.Row]);
                    else
                        cell.ResetIngredient();
                }
                else
                    cell.SetIngredient(shoppingList[indexPath.Row]);
            }
            else
                cell.ResetIngredient();

            cell.SizeToFit();

            return cell;
        }

        public override nfloat GetHeightForRow (UITableView tableView, NSIndexPath indexPath)
        {
            List<ShoppingListIngredient> shoppingList = MasterShoppingList.GetShoppingList(mShoppingListSectionName);
            NSString methodText = new NSString(FormattedIngredientString.IngredientsString(shoppingList[indexPath.Row]));
            CGSize stringSize;

            if (MasterShoppingList.GetEditMode(mShoppingListSectionName))
            {
                stringSize = methodText.StringSize (UIFont.FromName ("SceneStd-Black", BuyPageShoppingListIngredientCell.cIngredientFontSize), new CGSize (105.0f, 99999), UILineBreakMode.WordWrap);

                if (stringSize.Height > 36.0)
                    return 20.0f * stringSize.Height/BuyPageShoppingListIngredientCell.cIngredientFontSize;
                else
                    return 36;
            }

            stringSize = methodText.StringSize (UIFont.FromName ("SceneStd-Black", BuyPageShoppingListIngredientCell.cIngredientFontSize), new CGSize (189.0f, 99999), UILineBreakMode.WordWrap);

            return stringSize.Height < 26.0f ? 26.0f : 26.0f * stringSize.Height/BuyPageShoppingListIngredientCell.cIngredientFontSize;
        }

        public override void RowUnhighlighted (UITableView tableView, NSIndexPath rowIndexPath)
        {
        }

        public override void RowHighlighted (UITableView tableView, NSIndexPath rowIndexPath)
        {
        }

        public override void RowDeselected (UITableView tableView, NSIndexPath indexPath)
        {

        }

        public override void RowSelected (UITableView tableView, NSIndexPath indexPath)
        {
            MasterShoppingList.MasterShoppingListItem masterShoppingListItem = MasterShoppingList.GetMasterShoppingListItem(mShoppingListSectionName);

            //Only be able to "strikethrough" when we are not in edit mode already
            if (!masterShoppingListItem.mbEditMode)
            {
                masterShoppingListItem.mShoppingList[indexPath.Row].mbIgnore = !masterShoppingListItem.mShoppingList[indexPath.Row].mbIgnore;

                //Move the struck out items to the end of the list
                if (masterShoppingListItem.mShoppingList[indexPath.Row].mbIgnore)
                    masterShoppingListItem.PushIngredientToEndOfList(indexPath.Row);
                else
                    masterShoppingListItem.InsertIngredientBeforeFirstIgnoredItem(indexPath.Row);

                //NSIndexPath[] rows = new NSIndexPath[]{ indexPath };
                tableView.ReloadData();
            }
        }

        public override bool CanEditRow(UITableView tableView, NSIndexPath indexPath)
        {
            return MasterShoppingList.GetEditMode(mShoppingListSectionName);
        }
    }
}

