using System;
using System.Collections.Generic;
using CoreGraphics;
using System.Linq;
using Foundation;
using UIKit;
using ObjCRuntime;

namespace PlanBuyCookiOS
{
	[Register("BuyPageMasterIngredientsListController")]
	public class BuyPageMasterIngredientsListController : UICollectionViewController
	{
		UIView mContainer;

		public BuyPageMasterIngredientsListController (UICollectionViewLayout layout, UIView container) : base (layout)
		{
			mContainer = container;

			OnLoadMasterIngredientsPopulateBuyPage ();
		}

		void OnLoadMasterIngredientsPopulateBuyPage()
		{
			Console.WriteLine ("Populating the buy page from master ingredients list");

			int titleIndex = 0;
			foreach (string title in MasterIngredientsList.Sections) 
            {
				AddShoppingList (title, titleIndex++);
			}
		}
			
		public void Refresh()
		{
			CollectionView.ReloadData ();

            //For some reason not working correctly in ViewDidLoad()
            //Turn off scrolling for the collection view (will stop interfering with the individual lists)
            CollectionView.ScrollEnabled = false;
		}

		public void AddShoppingList(string name, int listIndex)
		{
			List<ShoppingListIngredient> shoppingList = new List<ShoppingListIngredient> ();

            MasterShoppingList.AddShoppingList (name, shoppingList, listIndex);
		}

		public override void DidReceiveMemoryWarning ()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning ();

			// Release any cached data, images, etc that aren't in use.
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			CollectionView.RegisterNibForCell (BuyPageIndividualIngredientsListCell.Nib, BuyPageIndividualIngredientsListCell.Key);

			CollectionView.Superview.UserInteractionEnabled = false;
			CollectionView.UserInteractionEnabled = true;
			CollectionView.Frame = mContainer.Frame;

			CollectionView.ReloadData ();
		}


		public override nint NumberOfSections (UICollectionView collectionView)
		{
			return 1;
		}

		public override nint GetItemsCount (UICollectionView collectionView, nint section)
		{
            //The +1 is because the first list is "Add you own"
            return MasterIngredientsList.Sections.Count() + 1;
		}
            

		public override void ItemDeselected (UICollectionView collectionView, NSIndexPath indexPath)
		{
		}

		public override UICollectionViewCell GetCell(UICollectionView collectionView, NSIndexPath indexPath)
		{
			var cell = collectionView.DequeueReusableCell (BuyPageIndividualIngredientsListCell.Key, indexPath) as BuyPageIndividualIngredientsListCell;

            if (cell == null)
                cell = BuyPageIndividualIngredientsListCell.Create();

            //The first list is the "Add your own"
            if (indexPath.Item == 0)
            {
                cell.InitAddYourOwn();
            }
            else
            {
                cell.SetEditMode(MasterIngredientsList.Sections[indexPath.Item - 1] == MasterShoppingList.EditedShoppingListName);
                cell.SetIngredientsListTitle(MasterIngredientsList.Sections[indexPath.Item-1]);
                cell.GetIngredientsList().ReloadData();
            }
            //Display an indicator that the list is scrollable
            cell.UpdateContentViewIndicatorIsTooLarge();

			return cell;
		}
	}
}

