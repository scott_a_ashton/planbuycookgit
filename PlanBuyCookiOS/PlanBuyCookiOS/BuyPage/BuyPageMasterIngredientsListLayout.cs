using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.IO;
using System.Diagnostics;
using CoreGraphics;
using Foundation;
using UIKit;

namespace PlanBuyCookiOS
{
	[Register("BuyPageMasterIngredientsListLayout")]
	public class BuyPageMasterIngredientsListLayout : UICollectionViewLayout
	{
		UIView mContainerView;

		List<CGRect> mLayoutRects = new List<CGRect>();

		List<List<ShoppingListIngredient>> mMasterIngredientsColumn = new List<List<ShoppingListIngredient>> ();

        public const float cHeightOfShoppingListCell = 292.0f;
        public const float cHeightOfShoppingList = 233;//cHeightOfShoppingListCell - 40.0f;

		//The rect for the entire layout
		CGRect mSinglePageLayoutRect;

		public BuyPageMasterIngredientsListLayout(IntPtr handle) : base (handle)
		{
		}

		public BuyPageMasterIngredientsListLayout (UIView containerView)
		{
			mContainerView = containerView;
		}

		public override CGSize CollectionViewContentSize 
		{
			get 
			{
				CGSize size = mContainerView.Frame.Size;
                //However many rows, we add one so we have the option of scrolling up a row when editing a quantity
                size.Height = ((CollectionView.NumberOfItemsInSection (0) / 3) + 1) * cHeightOfShoppingListCell; 

				return size;
			}
		}

		public override void PrepareLayout ()
		{
			base.PrepareLayout ();
		}


		public override bool ShouldInvalidateLayoutForBoundsChange (CGRect newBounds)
		{
			return true;
		}

		public override UICollectionViewLayoutAttributes LayoutAttributesForItem (NSIndexPath path)
		{
			UICollectionViewLayoutAttributes attributes = UICollectionViewLayoutAttributes.CreateForCell (path);

			nint column = path.Item % 3;
			nint row = path.Item / 3;

            attributes.Size = new CGSize (210, cHeightOfShoppingListCell);
            attributes.Center = new CGPoint (((210 + 20)*column) + 105, (cHeightOfShoppingListCell * row) + cHeightOfShoppingListCell/2.0f);
//
//			int rectIndex = path.Item % mLayoutRects.Count;
//			int layoutPage = path.Item / mLayoutRects.Count;
//
//			RectangleF rect = mLayoutRects [rectIndex];
//			attributes.Size = new SizeF(rect.Size.Width, rect.Size.Height);
//
//			PointF rectOrigin = new PointF (rect.X, rect.Y + mSinglePageLayoutRect.Height * layoutPage);
//			attributes.Center = new PointF((rectOrigin.X + rect.Width/2), (rectOrigin.Y + rect.Height/2));

			return attributes;
		}

		public override UICollectionViewLayoutAttributes LayoutAttributesForDecorationView (NSString kind, NSIndexPath indexPath)
		{
			return base.LayoutAttributesForDecorationView (kind, indexPath);
		}

		public override UICollectionViewLayoutAttributes[] LayoutAttributesForElementsInRect (CGRect rect)
		{
			//Create enough rects for all the recipes in memory at the moment.
			var attributes = new UICollectionViewLayoutAttributes [CollectionView.NumberOfItemsInSection(0)];

			for (int i = 0; i < attributes.Count(); i++) 
			{
				NSIndexPath indexPath = NSIndexPath.FromItemSection (i, 0);
				attributes [i] = LayoutAttributesForItem (indexPath);
			}

			//var decorationAttribs = UICollectionViewLayoutAttributes.CreateForDecorationView (myDecorationViewId, NSIndexPath.FromItemSection (0, 0));
			//decorationAttribs.Size = CollectionView.Frame.Size;
			//decorationAttribs.Center = CollectionView.Center;
			//decorationAttribs.ZIndex = -1;
			//attributes [cellCount] = decorationAttribs;

			return attributes;
		}

	}
}

