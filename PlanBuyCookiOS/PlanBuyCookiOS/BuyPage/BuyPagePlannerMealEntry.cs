using System;
using CoreGraphics;
using Foundation;
using UIKit;

namespace PlanBuyCookiOS
{
	public partial class BuyPagePlannerMealEntry : UITableViewCell
	{
		public static readonly UINib Nib = UINib.FromName ("BuyPagePlannerMealEntry", NSBundle.MainBundle);
		public static readonly NSString Key = new NSString ("BuyPagePlannerMealEntry");

		ShoppingListMeal mMeal;

		readonly float cDayTitleFontSize = 12.0f;
		readonly float cRecipeNameFontSize = 12.0f;
		readonly float cNumServesFontSize = 12.0f;

		public BuyPagePlannerMealEntry (IntPtr handle) : base (handle)
		{

		}

		public static BuyPagePlannerMealEntry Create ()
		{
			return (BuyPagePlannerMealEntry)Nib.Instantiate (null, null) [0];
		}


        public UIImageView GetShoppedForIcon()
        {
            return ShoppedFor;
        }

		string CreateDateString(DateTime date)
		{
			string[] months = new string[]{"JAN","FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"};

			return date.DayOfWeek.ToString ().ToUpper () + " " + date.Day + " " + months[date.Month-1];
		}

		public void SetRecipe(ShoppingListMeal meal)
		{
			mMeal = meal;

			RecipeName.Text = meal.Recipe.Name.ToUpper();
			RecipeName.Font = UIFont.FromName ("SceneStd-Regular", cRecipeNameFontSize);

			//Set the day string
			DateLabel.Text = CreateDateString (meal.Date);
			DateLabel.Font = UIFont.FromName ("SceneStd-Black", cDayTitleFontSize);

			NumServesLabel.Text = meal.NumServes.ToString();
			NumServesLabel.Font = UIFont.FromName ("SceneStd-Black", cNumServesFontSize);

			//Now have to use SetImage since 8.3
			//RecipeImagePortrait.ImageView.Image = PlanPagePlannerViewCell.GetCachedRecipePortrait (meal.Recipe);
			RecipeImagePortrait.SetImage(PlanPagePlannerViewCell.GetCachedRecipePortrait (meal.Recipe), UIControlState.Normal);
		}

		new public void Select()
		{
			RecipePortraitSelectionRing.Hidden = false;
			MealBox.Selected = true;
		}

		public void Deselect()
		{
			RecipePortraitSelectionRing.Hidden = true;
			MealBox.Selected = false;
		}

        partial void IncrementNumServes(NSObject sender)
        {
            if (mMeal.IsPartOfShoppingList)
                RemoveMealFromShoppingList();

            mMeal.IncrementNumServes();
            NumServesLabel.Text = mMeal.NumServes.ToString();

            if (mMeal.IsPartOfShoppingList)
                AddMealToShoppingList();
        }

        partial void HandleResetNumServes(NSObject sender)
        {
            if (mMeal.IsPartOfShoppingList)
                RemoveMealFromShoppingList();

            mMeal.ResetNumServes();
            NumServesLabel.Text = mMeal.NumServes.ToString();

            if (mMeal.IsPartOfShoppingList)
                AddMealToShoppingList();
        }

        void RemoveMealFromShoppingList()
        {
            ((Superview.Superview as UITableView).Source as BuyPagePlannerMealsSource).RemoveMealFromShoppingList(mMeal);
        }

        void AddMealToShoppingList()
        {
            ((Superview.Superview as UITableView).Source as BuyPagePlannerMealsSource).AddMealToShoppingList(mMeal);
        }

	}
}

