// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace PlanBuyCookiOS
{
	[Register ("BuyPagePlannerMealEntry")]
	partial class BuyPagePlannerMealEntry
	{
		[Outlet]
		UIKit.UILabel DateLabel { get; set; }

		[Outlet]
		UIKit.UIButton IncrementServesButton { get; set; }

		[Outlet]
		UIKit.UIButton MealBox { get; set; }

		[Outlet]
		UIKit.UILabel NumServesLabel { get; set; }

		[Outlet]
		UIKit.UIButton RecipeImagePortrait { get; set; }

		[Outlet]
		UIKit.UILabel RecipeName { get; set; }

		[Outlet]
		UIKit.UIImageView RecipePortraitSelectionRing { get; set; }

		[Outlet]
		UIKit.UIImageView ShoppedFor { get; set; }

		[Action ("HandleResetNumServes:")]
		partial void HandleResetNumServes (Foundation.NSObject sender);

		[Action ("IncrementNumServes:")]
		partial void IncrementNumServes (Foundation.NSObject sender);
		
		void ReleaseDesignerOutlets ()
		{
			if (DateLabel != null) {
				DateLabel.Dispose ();
				DateLabel = null;
			}

			if (IncrementServesButton != null) {
				IncrementServesButton.Dispose ();
				IncrementServesButton = null;
			}

			if (MealBox != null) {
				MealBox.Dispose ();
				MealBox = null;
			}

			if (NumServesLabel != null) {
				NumServesLabel.Dispose ();
				NumServesLabel = null;
			}

			if (RecipeImagePortrait != null) {
				RecipeImagePortrait.Dispose ();
				RecipeImagePortrait = null;
			}

			if (RecipeName != null) {
				RecipeName.Dispose ();
				RecipeName = null;
			}

			if (RecipePortraitSelectionRing != null) {
				RecipePortraitSelectionRing.Dispose ();
				RecipePortraitSelectionRing = null;
			}

			if (ShoppedFor != null) {
				ShoppedFor.Dispose ();
				ShoppedFor = null;
			}
		}
	}
}
