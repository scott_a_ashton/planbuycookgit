using System;
using System.Collections.Generic;
using CoreGraphics;
using System.Linq;
using System.Threading.Tasks;
using Foundation;
using UIKit;

namespace PlanBuyCookiOS
{
	public class BuyPagePlannerMealsSource : UITableViewSource
	{
        BuyPageMasterIngredientsListController mMasterIngredientsListController;
		public static List<ShoppingListMeal> mPossibleShoppingListMeals = new List<ShoppingListMeal>();

        public BuyPagePlannerMealsSource (BuyPageMasterIngredientsListController masterIngredientsListController)
		{
            mMasterIngredientsListController = masterIngredientsListController;
            IsFirstViewing = true;
		}

		public override nint RowsInSection (UITableView tableview, nint section)
		{
			return mPossibleShoppingListMeals.Count;
		}
		public override nint NumberOfSections (UITableView tableView)
		{
			// TODO: return the actual number of sections
			return 1;
		}

//		public override string TitleForHeader (UITableView tableView, int section)
//		{
//			return "Header";
//		}

//		public override string TitleForFooter (UITableView tableView, int section)
//		{
//			return "Footer";
//		}

        //We only need to populate the list once from the recipe list
        public bool IsFirstViewing { set; get; }

		public void SetShoppingListMeals(List<ShoppingListMeal> shoppingListMeals)
		{
            if (shoppingListMeals == null)
                return;

            //mPossibleShoppingListMeals = shoppingListMeals;

            for (int mealIndex = 0; mealIndex < shoppingListMeals.Count; mealIndex++)
            {
                if (mPossibleShoppingListMeals.Find((x) => ShoppingListMeal.IsEqualTo(x, shoppingListMeals[mealIndex])) == null)
                {
                    mPossibleShoppingListMeals.Add(shoppingListMeals[mealIndex]);
                }
            }

            mPossibleShoppingListMeals.Sort(ShoppingListMeal.Comparer);
		}

		public override UITableViewCell GetCell (UITableView tableView, NSIndexPath indexPath)
		{
			var cell = tableView.DequeueReusableCell (BuyPagePlannerMealEntry.Key) as BuyPagePlannerMealEntry;

			if (cell == null) {
				cell = BuyPagePlannerMealEntry.Create ();
			}

			cell.SetRecipe (mPossibleShoppingListMeals[indexPath.Row]);

			if (mPossibleShoppingListMeals [indexPath.Row].IsPartOfShoppingList)
				cell.Select ();
			else
				cell.Deselect ();

            cell.GetShoppedForIcon().Hidden = !mPossibleShoppingListMeals[indexPath.Row].IsShoppedFor;

			return cell;
		}

		public override nfloat GetHeightForRow (UITableView tableView, NSIndexPath indexPath)
		{
			return 82;
		}

		public override void RowUnhighlighted (UITableView tableView, NSIndexPath rowIndexPath)
		{
		}

		public override void RowHighlighted (UITableView tableView, NSIndexPath rowIndexPath)
		{
		}

		public override void RowDeselected (UITableView tableView, NSIndexPath indexPath)
		{
			mPossibleShoppingListMeals [indexPath.Row].IsPartOfShoppingList = false;

			NSIndexPath[] rows = new NSIndexPath[]{ indexPath };
			tableView.ReloadRows (rows, UITableViewRowAnimation.Automatic);
		}

		public override void RowSelected (UITableView tableView, NSIndexPath indexPath)
		{
			//if (mPossibleShoppingListMeals [indexPath.Row].mRecipe is RecipeSQL) 
			{
				if (mPossibleShoppingListMeals [indexPath.Row].IsPartOfShoppingList) {
					MasterShoppingList.RemoveIngredients (mPossibleShoppingListMeals [indexPath.Row].mRecipe.IngredientsList, mPossibleShoppingListMeals [indexPath.Row].ServesMulitplier);
				} else {
					MasterShoppingList.AddIngredients (mPossibleShoppingListMeals [indexPath.Row].mRecipe.IngredientsList, mPossibleShoppingListMeals [indexPath.Row].ServesMulitplier);
				}
				mMasterIngredientsListController.Refresh ();

				mPossibleShoppingListMeals [indexPath.Row].IsPartOfShoppingList = !mPossibleShoppingListMeals [indexPath.Row].IsPartOfShoppingList;

				NSIndexPath[] rows = new NSIndexPath[]{ indexPath };
				tableView.ReloadRows (rows, UITableViewRowAnimation.Automatic);
			}
		}

        public static void UpdateServesOnShoppingList(CalenderDay.Meal meal)
        {
            Console.WriteLine("Start modify serves");
            //Find the meal in the shopping list
            ShoppingListMeal mealToChange = mPossibleShoppingListMeals.Where(m => m.CalenderDayMeal == meal).FirstOrDefault();

            if (mealToChange != null)
            {
                if (mealToChange.NumServes != meal.Serves)
                {
                    if (mealToChange.IsPartOfShoppingList)
                        MasterShoppingList.RemoveIngredients(mealToChange.mRecipe.IngredientsList, mealToChange.ServesMulitplier);

                    mealToChange.NumServes = meal.Serves;

                    if (mealToChange.IsPartOfShoppingList)
                        MasterShoppingList.AddIngredients(mealToChange.mRecipe.IngredientsList, mealToChange.ServesMulitplier);
                }
            }
            Console.WriteLine("End modify serves");
        }

        //In case the meal is deleted in the planner
        public static void RemoveMealFromPossibleShoppingList(CalenderDay.Meal meal)
        {
            //Find the meal in the shopping list
            ShoppingListMeal mealToDelete = mPossibleShoppingListMeals.Where(m => m.CalenderDayMeal == meal).FirstOrDefault();

            if (mealToDelete != null)
            {
                if (mealToDelete.IsPartOfShoppingList)
                    MasterShoppingList.RemoveIngredients (mealToDelete.mRecipe.IngredientsList, mealToDelete.ServesMulitplier);;

                mPossibleShoppingListMeals.Remove(mealToDelete);
            }
        }

        public void RemoveMealFromShoppingList(ShoppingListMeal meal)
        {
            if (meal.IsPartOfShoppingList)
			    MasterShoppingList.RemoveIngredients (meal.mRecipe.IngredientsList, meal.ServesMulitplier);

			mMasterIngredientsListController.Refresh ();
        }

        public void AddMealToShoppingList(ShoppingListMeal meal)
        {
			MasterShoppingList.AddIngredients (meal.mRecipe.IngredientsList, meal.ServesMulitplier);
			mMasterIngredientsListController.Refresh ();
        }

        //When we've send our shopping list by e-mail flag recipes as being shopped - so they will represented as so visually
        public void FlagRecipesAsShopped()
        {
            IEnumerable<ShoppingListMeal> partOfShoppingList = mPossibleShoppingListMeals.Where(meal => meal.IsPartOfShoppingList);
            List<CalenderDay> days = new List<CalenderDay>();

            foreach (ShoppingListMeal meal in partOfShoppingList)
            {
                meal.IsShoppedFor = true;

                days.Add(meal.CalenderDay);
            }

            var uniqueDays = days.Distinct();

            foreach (var day in uniqueDays)
            {
                for (int mealIndex = 0; mealIndex < day.MealCount; mealIndex++)
                    PBCCalendar.UpdateCalendar(day, mealIndex);
            }
        }

		public void ClearMealsFromShoppingList()
		{
			List<ShoppingListMeal> partOfShoppingList = mPossibleShoppingListMeals.Where(meal => meal.IsPartOfShoppingList).ToList();

			foreach (ShoppingListMeal meal in partOfShoppingList)
			{
				mPossibleShoppingListMeals.Remove (meal);
			}
		}
	}
}

