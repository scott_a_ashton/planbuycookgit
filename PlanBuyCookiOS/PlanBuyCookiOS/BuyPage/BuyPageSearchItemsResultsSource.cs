using System;
using System.Collections.Generic;
using CoreGraphics;
using Foundation;
using UIKit;

namespace PlanBuyCookiOS
{
    public class BuyPageSearchItemsResultsSource : UITableViewSource
    {
        bool mbNorDirectHits = false;
        List<MasterIngredientsList.Ingredient> mResults;
        List<MasterIngredientsList.Ingredient> mSelected = new List<MasterIngredientsList.Ingredient>();

        public BuyPageSearchItemsResultsSource()
        {
            mResults = new List<MasterIngredientsList.Ingredient>();
        }

        public List<MasterIngredientsList.Ingredient> SelectedIngredients
        {
            get
            {
                return mSelected;
            }
        }

        public override nint NumberOfSections(UITableView tableView)
        {
            // TODO: return the actual number of sections
            return 1;
        }

        public override nint RowsInSection(UITableView tableview, nint section)
        {
            // TODO: return the actual number of items in the section
            return mResults.Count;
        }

        public override string TitleForHeader(UITableView tableView, nint section)
        {
            return "Item Search Results";
        }

//        public override string TitleForFooter(UITableView tableView, int section)
//        {
//            return "Footer";
//        }

        public override void RowSelected (UITableView tableView, NSIndexPath indexPath)
        {
            mSelected.Add(mResults[indexPath.Row]);

            mResults.RemoveAt(indexPath.Row);
            tableView.ReloadData();

            BuyPageIndividualIngredientsListCell.DismissSearchResultsPopover();
        }

        public void Reset()
        {
            mSelected.Clear();
            mResults.Clear();
        }


        //public override row
        public void UpdateResults(List<MasterIngredientsList.Ingredient> results, bool bNoDirectHits)
        {            
            mbNorDirectHits = bNoDirectHits;
            mResults = results;
        }

        public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
        {
            var cell = tableView.DequeueReusableCell(BuyPageSearchItemsResultsCell.Key) as BuyPageSearchItemsResultsCell;
            if (cell == null)
                cell = new BuyPageSearchItemsResultsCell();
			
            if (mbNorDirectHits && indexPath.Row == 0)
                cell.TextLabel.TextColor = UIColor.Red;
            else
                cell.TextLabel.TextColor = UIColor.Black;

            cell.TextLabel.Text = mResults[indexPath.Row].Name;

            return cell;
        }
    }
}

