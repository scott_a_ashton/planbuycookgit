using System;
using CoreGraphics;
using Foundation;
using UIKit;

namespace PlanBuyCookiOS
{
    public partial class BuyPageShoppingListIngredientCell : UITableViewCell
    {
        public static readonly float cIngredientFontSize = 13.0f;
        public static readonly float cIngredientsListTitleFontSize = 14.0f;

        public static readonly UINib Nib = UINib.FromName("BuyPageShoppingListIngredientCell", NSBundle.MainBundle);
        public static readonly NSString Key = new NSString("BuyPageShoppingListIngredientCell");

        ShoppingListIngredient mIngredient;
        string mDisplayedUnit = "";

        public BuyPageShoppingListIngredientCell(IntPtr handle) : base(handle)
        {
        }

        public static BuyPageShoppingListIngredientCell Create()
        {
            var cell = (BuyPageShoppingListIngredientCell)Nib.Instantiate(null, null)[0];

            cell.InitialiseFonts();

            return cell;
        }

        public void ResetIngredient()
        {
            mIngredient = null;
            ShoppingIngredient.Text = "";
        }

        public void SetIngredient(ShoppingListIngredient ingredient)
        {
            EditMode = false;

            ShoppingIngredient.AttributedText = AttributedIngredientsString(ingredient);
            //ShoppingIngredient.Text = FormattedIngredientString.IngredientsString(ingredient);
            //ShoppingIngredient.SizeToFit();

            mIngredient = ingredient;
        }
            
        public void SetEditIngredient(ShoppingListIngredient ingredient)
        {
            EditMode = true;

            MasterIngredientsList.Ingredient masterIngredient = MasterIngredientsList.FindIngredient(ingredient.mFood);

            if (masterIngredient != null)
            {
                RoundingRules.RoundedAmount rounded = RoundingRules.RoundingRule(masterIngredient.RoundingRule, ingredient);

                EditQuantity.Text = rounded.mQuantity.ToString();
                mDisplayedUnit = rounded.mUnit;
            }
            else
            {
                EditQuantity.Text = ingredient.Quantity.ToString();
                mDisplayedUnit = ingredient.mUnit;
            }

            if (masterIngredient != null)
                EditIngedientLabel.Text = FormattedIngredientString.IngredientsStringNoQuantity(ingredient);
            else
                EditIngedientLabel.Text = ingredient.mFood;

            mIngredient = ingredient;
        }

        bool mbEditMode = false;

        public bool EditMode
        {
            set
            {
                EditDeleteIngredientButton.Hidden = !value;
                EditQuantity.Hidden = !value;
                EditIngedientLabel.Hidden = !value;
                ShoppingIngredient.Hidden = value;

                mbEditMode = value;
            }
        }

        public void InitialiseFonts()
        {
            EditQuantity.Font = UIFont.FromName ("SceneStd-Regular", cIngredientFontSize);
            EditQuantity.TextColor = UIColor.Red;
            EditIngedientLabel.Font = UIFont.FromName ("SceneStd-Regular", cIngredientFontSize);
        }

        public static NSMutableAttributedString AttributedIngredientsString(ShoppingListIngredient ingredient)
        {
            NSMutableAttributedString regular = new NSMutableAttributedString("");
            UIStringAttributes ingredientRegular;
            UIStringAttributes ingredientBold;

            //The ingredient has been selected which means it has a strikethrough visually
            if (ingredient.mbIgnore)
            {
                ingredientRegular = new UIStringAttributes {Font = UIFont.FromName("SceneStd-Regular", cIngredientFontSize), ForegroundColor = UIColor.Black, StrikethroughColor = UIColor.Black, StrikethroughStyle = NSUnderlineStyle.Single};
                ingredientBold = new UIStringAttributes {Font = UIFont.FromName("SceneStd-Black", cIngredientFontSize), ForegroundColor = UIColor.Black, StrikethroughColor = UIColor.Black, StrikethroughStyle = NSUnderlineStyle.Single};
            }
            else
            {
                ingredientRegular = new UIStringAttributes {Font = UIFont.FromName("SceneStd-Regular", cIngredientFontSize), ForegroundColor = UIColor.Black};
                ingredientBold = new UIStringAttributes {Font = UIFont.FromName("SceneStd-Black", cIngredientFontSize), ForegroundColor = UIColor.Black};
            }

            //Lookup the master ingredient
            MasterIngredientsList.Ingredient masterIngredient = MasterIngredientsList.FindIngredient(ingredient.mFood);
            //Lookup the rounding rule for this ingredient
            RoundingRules.RoundedAmount roundedAmount = RoundingRules.RoundingRule ((masterIngredient==null)?0:masterIngredient.RoundingRule, ingredient);

            float ingredientQuantity = roundedAmount.mQuantity;
            string ingredientUnit = roundedAmount.mUnit;
            string quantityString = ingredientQuantity == 0 ? "" : FormattedIngredientString.CreateNumericalString(roundedAmount);
            string unitString = "";
            string foodString = ingredient.mFood;
            string ingredientString = "";

            RecipeUnit recipeUnit = MeasurementUnits.GetRecipeUnit(ingredientUnit);

            if (recipeUnit == null)
                recipeUnit = MeasurementUnits.GetRecipeUnit("");

            if (ingredientQuantity == 1 && ingredientUnit != "number" && recipeUnit.Unit == "")
                unitString = ingredientUnit + " of";
            else if (ingredientQuantity == 1 && ingredientUnit!= "number" && recipeUnit.Unit == "")
                unitString = ingredientUnit + " of";
            else if (ingredientQuantity > 1 && ingredientUnit != "number" && recipeUnit.Unit != "")
                unitString = recipeUnit.Unit + " of";
            else if (ingredientQuantity > 1 && ingredientUnit!= "number" && recipeUnit.Plural != "")
                unitString = recipeUnit.Plural + " of";
            else if (ingredientQuantity > 1 && ingredientUnit != "number" && recipeUnit.Plural=="")
                unitString = ingredientUnit + " of";
            else if (ingredientQuantity >= 0.0f && ingredientUnit == "number")
                unitString = "";
            else
                unitString = ingredientUnit;

            if (quantityString != "")
                ingredientString += quantityString + " ";

            if (unitString != "")
                ingredientString += unitString + " ";

            if (ingredientQuantity > 1 && masterIngredient != null && masterIngredient.Plural != "")
                foodString = masterIngredient.Plural;

            ingredientString += foodString;

            nint positionOfIngredient = regular.Length;

            regular.Append (new NSMutableAttributedString (ingredientString));

            nint positionOfFood = regular.Length - foodString.Length;

            regular.AddAttributes (ingredientBold.Dictionary, new NSRange (positionOfIngredient, ingredientString.Length));
            regular.AddAttributes(ingredientRegular.Dictionary, new NSRange(positionOfFood, foodString.Length));

            return regular;
        }

        partial void EditingQuantityEnded(NSObject sender)
        {
            float value = mIngredient.Quantity;

            if (float.TryParse( (sender as UITextField).Text, out value))
            {
                mIngredient.Modify(mDisplayedUnit, value);
            }
            AppDelegate.BuyPageViewController.EditingQuantityEnded();
        }

        partial void EditingBegins(NSObject sender)
        {
            AppDelegate.BuyPageViewController.EditingQuantityStarted();
        }

        partial void DeleteIngredient(NSObject sender)
        {
            MasterShoppingList.EditedShoppingListItem.DeleteEntry(mIngredient);
            AppDelegate.BuyPageViewController.RefreshEditedLists(MasterShoppingList.EditedShoppingListName,"");
        }
    }
}

