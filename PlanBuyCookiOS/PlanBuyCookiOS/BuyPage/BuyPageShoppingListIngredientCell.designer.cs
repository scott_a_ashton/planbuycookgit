// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace PlanBuyCookiOS
{
	[Register ("BuyPageShoppingListIngredientCell")]
	partial class BuyPageShoppingListIngredientCell
	{
		[Outlet]
		UIKit.UIButton EditDeleteIngredientButton { get; set; }

		[Outlet]
		UIKit.UILabel EditIngedientLabel { get; set; }

		[Outlet]
		UIKit.UITextField EditQuantity { get; set; }

		[Outlet]
		UIKit.UILabel ShoppingIngredient { get; set; }

		[Action ("DeleteIngredient:")]
		partial void DeleteIngredient (Foundation.NSObject sender);

		[Action ("EditingBegins:")]
		partial void EditingBegins (Foundation.NSObject sender);

		[Action ("EditingQuantityEnded:")]
		partial void EditingQuantityEnded (Foundation.NSObject sender);
		
		void ReleaseDesignerOutlets ()
		{
			if (EditDeleteIngredientButton != null) {
				EditDeleteIngredientButton.Dispose ();
				EditDeleteIngredientButton = null;
			}

			if (EditIngedientLabel != null) {
				EditIngedientLabel.Dispose ();
				EditIngedientLabel = null;
			}

			if (EditQuantity != null) {
				EditQuantity.Dispose ();
				EditQuantity = null;
			}

			if (ShoppingIngredient != null) {
				ShoppingIngredient.Dispose ();
				ShoppingIngredient = null;
			}
		}
	}
}
