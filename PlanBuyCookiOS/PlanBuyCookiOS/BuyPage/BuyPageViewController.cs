using System;
using System.Collections.Generic;
using CoreGraphics;
using Foundation;
using UIKit;

namespace PlanBuyCookiOS
{
	public partial class BuyPageViewController : UIViewController
	{
		BuyPageMasterIngredientsListController mMasterIngredientsListController;
		BuyPageMasterIngredientsListLayout mMasterIngredientsListLayout;


        public BuyPagePlannerMealsSource GetMealPlannerSource
        {
            get
            {
                return PlannerMealList.Source as BuyPagePlannerMealsSource;
            }
        }

		public BuyPageViewController () : base ("BuyPageViewController", null)
		{
		}

		public override void DidReceiveMemoryWarning ()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning ();
			
			// Release any cached data, images, etc that aren't in use.
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			// Perform any additional setup after loading the view, typically from a nib.
			mMasterIngredientsListLayout = new BuyPageMasterIngredientsListLayout (MasterIngredientsListContainerView);
			mMasterIngredientsListController = new BuyPageMasterIngredientsListController (mMasterIngredientsListLayout ,MasterIngredientsListContainerView);

            PlannerMealList.Source = new BuyPagePlannerMealsSource (mMasterIngredientsListController);

			View.AddSubview (mMasterIngredientsListController.CollectionView);

			mMasterIngredientsListController.CollectionView.BackgroundColor = UIColor.White;
			mMasterIngredientsListController.CollectionView.ScrollEnabled = true;
		}

		public void SetMealPlannerDayList(List<ShoppingListMeal> mealPlannerDayList)
		{
			(PlannerMealList.Source as BuyPagePlannerMealsSource).SetShoppingListMeals (mealPlannerDayList);
			PlannerMealList.ScrollEnabled = true;
			PlannerMealList.ReloadData ();
            mMasterIngredientsListController.Refresh ();
		}

		partial void SwitchToCookPage (NSObject sender)
		{
			AppDelegate.SwitchToCookPage();
		}

		partial void SwitchToPlanPage (NSObject sender)
		{
			AppDelegate.SwitchToPlanPage();
		}

        //When editing of a quantity begins we may have to scroll the collection view into position
        public void EditingQuantityStarted()
        {
            //Note that we add 1 to the table index because the first table is "AddYourOwn"
            mMasterIngredientsListController.CollectionView.ScrollEnabled = true;
            mMasterIngredientsListController.CollectionView.ScrollToItem(NSIndexPath.FromItemSection(MasterShoppingList.EditedShoppingListItem.mTableIndex+1, 0), UICollectionViewScrollPosition.Top, false);
            mMasterIngredientsListController.CollectionView.ScrollEnabled = false;
        }

        public void EditingQuantityEnded()
        {
            //Note that we add 1 to the table index because the first table is "AddYourOwn"
            mMasterIngredientsListController.CollectionView.ScrollEnabled = true;
            mMasterIngredientsListController.CollectionView.ScrollToItem(NSIndexPath.FromItemSection(0, 0), UICollectionViewScrollPosition.Bottom, false);
            mMasterIngredientsListController.CollectionView.ScrollEnabled = false;
        }

        public void RemoveFromAddYourOwn(string food)
        {
            BuyPageIndividualIngredientsListCell cell = mMasterIngredientsListController.CollectionView.CellForItem(NSIndexPath.FromItemSection(0, 0)) as BuyPageIndividualIngredientsListCell;

            cell.RemoveIngredient(food);
        }

        //Force the new and old edited shopping lists to update
        public void RefreshEditedLists(string newListName, string oldListName)
        {
            MasterShoppingList.MasterShoppingListItem newList = MasterShoppingList.GetMasterShoppingListItem(newListName);

            if (newList != null)
            {

                //Note that we add 1 to the table index because the first table is "AddYourOwn"
                UICollectionViewCell cell = mMasterIngredientsListController.CollectionView.CellForItem(NSIndexPath.FromItemSection(newList.mTableIndex+1, 0));

                if (cell != null)
                {
                    if (oldListName == "AddYourOwn")
                        (cell as BuyPageIndividualIngredientsListCell).SetEditMode(false);
                    else
                        (cell as BuyPageIndividualIngredientsListCell).SetEditMode(true);

                    UITableView shoppingListTable = (cell as BuyPageIndividualIngredientsListCell).GetIngredientsList();

                    if (shoppingListTable != null)
                        shoppingListTable.ReloadData();
                }
            }

            if (oldListName != "AddYourOwn")
            {
                MasterShoppingList.MasterShoppingListItem oldList = MasterShoppingList.GetMasterShoppingListItem(oldListName);

                if (oldList != null)
                {
                    //Note that we add 1 to the table index because the first table is "AddYourOwn"
                    UICollectionViewCell cell = mMasterIngredientsListController.CollectionView.CellForItem(NSIndexPath.FromItemSection(oldList.mTableIndex + 1, 0));

                    if (cell != null)
                    {
                        (cell as BuyPageIndividualIngredientsListCell).SetEditMode(false);
                        UITableView shoppingListTable = (cell as BuyPageIndividualIngredientsListCell).GetIngredientsList();

                        if (shoppingListTable != null)
                            shoppingListTable.ReloadData();
                    }
                }
            }
        }

        partial void OpenSettings(NSObject sender)
        {
            AppDelegate.OpenSettings(this.View, sender as UIButton );
        }


        partial void EmailShoppingList(NSObject sender)
        {
            AppDelegate.EmailShoppingList(MasterShoppingList.CreateHTMLShoppingList());
        }

		public void RefreshShoppingList()
		{
			PlannerMealList.ReloadData();
		}

		partial void ClearShoppingList (NSObject sender)
		{
			MasterShoppingList.ClearShoppingList();
			(PlannerMealList.Source as BuyPagePlannerMealsSource).ClearMealsFromShoppingList();
			SetMealPlannerDayList (AppDelegate.PlanPageViewController.GetMealPlanForShoppingList ());
			mMasterIngredientsListController.CollectionView.ReloadData();
		}
	}
}

