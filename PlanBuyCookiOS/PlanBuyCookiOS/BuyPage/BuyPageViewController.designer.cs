// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace PlanBuyCookiOS
{
	[Register ("BuyPageViewController")]
	partial class BuyPageViewController
	{
		[Outlet]
		UIKit.UIView MasterIngredientsListContainerView { get; set; }

		[Outlet]
		UIKit.UITableView PlannerMealList { get; set; }

		[Outlet]
		UIKit.UIButton SettingsButton { get; set; }

		[Action ("ClearShoppingList:")]
		partial void ClearShoppingList (Foundation.NSObject sender);

		[Action ("EmailShoppingList:")]
		partial void EmailShoppingList (Foundation.NSObject sender);

		[Action ("OpenSettings:")]
		partial void OpenSettings (Foundation.NSObject sender);

		[Action ("SwitchToCookPage:")]
		partial void SwitchToCookPage (Foundation.NSObject sender);

		[Action ("SwitchToPlanPage:")]
		partial void SwitchToPlanPage (Foundation.NSObject sender);
		
		void ReleaseDesignerOutlets ()
		{
			if (MasterIngredientsListContainerView != null) {
				MasterIngredientsListContainerView.Dispose ();
				MasterIngredientsListContainerView = null;
			}

			if (PlannerMealList != null) {
				PlannerMealList.Dispose ();
				PlannerMealList = null;
			}

			if (SettingsButton != null) {
				SettingsButton.Dispose ();
				SettingsButton = null;
			}
		}
	}
}
