using System;
using System.Collections.Generic;
using CoreGraphics;
using System.Linq;
using Foundation;
using UIKit;
using ObjCRuntime;
using HtmlAgilityPack;
using System.Globalization;


namespace PlanBuyCookiOS
{
    public partial class MasterShoppingList
	{
        public class MasterShoppingListItem
        {
            public MasterShoppingListItem(string shoppingListName, int shoppingListIndex, List<ShoppingListIngredient> shoppingList)
            {
                mName = shoppingListName;
                mTableIndex = shoppingListIndex;
                mShoppingList = shoppingList;
            }

            public void PushIngredientToEndOfList(int ingredientIndex)
            {
                ShoppingListIngredient ingredient = mShoppingList.ElementAt(ingredientIndex);
                mShoppingList.RemoveAt(ingredientIndex);
                mShoppingList.Add(ingredient);
            }

            public void InsertIngredientBeforeFirstIgnoredItem(int ingredientIndex)
            {
                ShoppingListIngredient movedIngredient = mShoppingList.ElementAt(ingredientIndex);

                var ignored = mShoppingList.Where(sl => sl.mbIgnore);

                if (ignored.Count() > 0)
                {
                    mShoppingList.RemoveAt(ingredientIndex);

                    int insertAt = mShoppingList.IndexOf(ignored.First());

                    mShoppingList.Insert(insertAt, movedIngredient);
                }
            }

            public void DeleteEntry(int ingredientIndex)
            {
                mShoppingList.RemoveAt(ingredientIndex);
            }

            public void DeleteEntry(ShoppingListIngredient ingredient)
            {
                var toRemove = mShoppingList.Where(sli => sli.mFood == ingredient.mFood);

                if (toRemove.Count() > 0)
                    mShoppingList.Remove(toRemove.First());
            }

            public string HtmlList()
            {
                string entry = "<li><input type=\"checkbox\" id=\"{0}-{1}\"><label for=\"{0}-{1}\">{2}</label></li>";

                string htmlList = "";

                int itemIndex = 1;
                foreach (ShoppingListIngredient sli in mShoppingList)
                {
                    if (!sli.mbIgnore)
                        htmlList += string.Format(entry, mTableIndex, itemIndex++, FormattedIngredientString.IngredientsString(sli)) + "\n";
                }
                return htmlList;
            }

            public List<ShoppingListIngredient> mShoppingList;
            public string mName = "";
            public int mTableIndex = 0;
            public bool mbEditMode = false;
        }

        //Empty Template HTML Shopping List
        public static string sHtmlTemplate = "<!doctype html>  \n<style type=\"text/css\">\nheader{background-color:#FCD300;color:#FFF;font-weight:bold;padding:4px;margin:2px}\nfooter{background-color:#FCD300;color:#FFF;font-weight:bold;font-size:80%;padding:4px;margin:2px;}\na:link,a:visited{color:#FFF;}\nul{list-style:none;margin:0px;padding:0px;overflow: hidden;}\nul label+input[type=\"radio\"]{display:none;}\nul.items li{background-color:white;margin:0 0 0 3.2%;border-bottom:1px solid #DDD;float:left;width:22.6%;}\nul.items li:nth-of-type(4n+1){margin-left: 0;clear: left;}\nul.aisle label{background-color:#FFF;color:#000;font-weight:bold;display:block;padding:10px;margin:2px;border:2px solid #DDD;border-radius:4px;}\nul.items label{border:0;padding:0;display:inline-block;font-weight:normal;}\nul, label:hover{cursor:pointer;}\nul.aisle div.content{overflow:hidden;padding:0 10px;display:none;}\ninput[type=\"checkbox\"]{display:none;}\ninput[type=\"checkbox\"] + label{color:#000;text-decoration:normal;}\ninput[type=\"checkbox\"]:checked + label{color:#CCC;text-decoration:line-through;}\nul.aisle label+input[type=\"radio\"]:checked+div.content{display:block;}\nbody { -webkit-animation: bugfix infinite 1s; font: 85%/140% Arial, Helvetica, sans-serif;}\n@-webkit-keyframes bugfix{from {padding:0;} to {padding:0;} }\n@media screen and (max-width: 40em){ul.items li{width:31.2%;}ul.items li:nth-of-type(4n+1){margin-left: 3.2%;clear: none;}ul.items li:nth-of-type(3n+1){margin-left: 0;clear: left;}}\n@media screen and (max-width: 30em){ul.items li{width:48.4%;}ul.items li:nth-of-type(3n+1){clear: none;margin-left: 3.2%;}ul.items li:nth-of-type(2n+1){margin-left: 0;clear: left;}}\n@media screen and (max-width: 20em){ul.items li{width: 100% !important;margin-left: 0 !important;clear: none !important;}}\n@media print{ul.aisle div.content {overflow:hidden;padding:0 10px;display:block;}ul.items li{margin-left: 3.2%;clear: none;width:22.6%;}ul.items li:nth-of-type(4n+1){margin-left: 0;clear: left;}}\n</style>\n\n</head>\n<body>\n<header>MY SHOPPING LIST<span style=\"float:right\">$DATE</span></header>\n\n<ul class=\"aisle\">\n    <li>\n        <label for=\"c1\" onclick=\"\">FRUIT & VEG</label>\n        <input type=\"radio\" name=\"a\" id=\"c1\">\n        <div class=\"content\">\n        \t<ul class=\"items\" section=\"FRUIT & VEG\">\n            </ul>\n        </div>\n    </li>\n    \n    <li>\n        <label for=\"c2\">PANTRY & BAKERY</label>\n        <input type=\"radio\" name=\"a\" id=\"c2\">\n        <div class=\"content\">\n            <ul class=\"items\" section=\"PANTRY & BAKERY\">\n            </ul> \n       </div>\n    </li>\n    \n    <li>\n        <label for=\"c3\">MEAT & FISH</label>\n        <input type=\"radio\" name=\"a\" id=\"c3\">\n        <div class=\"content\">\n            <ul class=\"items\" section=\"MEAT & FISH\">\n            </ul> \n        </div>\n    </li>\n    \n    <li>\n        <label for=\"c4\">FRIDGE, FREEZER & DELI</label>\n        <input type=\"radio\" name=\"a\" id=\"c4\">\n        <div class=\"content\">\n\t\t\t<ul class=\"items\" section=\"FRIDGE, FREEZER & DELI\">\n            </ul>\n        </div>\n    </li>\n    \n        \n    <li>\n        <label for=\"c6\">HOUSEHOLD</label>\n        <input type=\"radio\" name=\"a\" id=\"c6\">\n        <div class=\"content\">\n            <ul class=\"items\" section=\"HOUSEHOLD\">\n            </ul>\n        </div>\n    </li>\n</ul>\n<footer><a href=\"http://planbuycook.com.au/\">&copy PlanBuyCook 2015</a></footer>\n</body>\n\n</html>";

        static Dictionary<string, MasterShoppingListItem > mMasterShoppingList = new Dictionary<string, MasterShoppingListItem> ();

        static string mCurrentEditedShoppingList="";

		public MasterShoppingList ()
		{
		}

        //Returns "" if no list being edited
        public static string EditedShoppingListName
        {
            set
            {
                if (mCurrentEditedShoppingList == value)
                {
                    SetEditMode(mCurrentEditedShoppingList, false);
                    mCurrentEditedShoppingList = "";
                }
                else if (mCurrentEditedShoppingList != "")
                {
                    SetEditMode(mCurrentEditedShoppingList, false);
                    SetEditMode(value, true);
                    mCurrentEditedShoppingList = value;
                }
                else
                {
                    SetEditMode(value, true);
                    mCurrentEditedShoppingList = value;
                }
            }
            get
            {
                return mCurrentEditedShoppingList;
            }
        }

        public static MasterShoppingListItem EditedShoppingListItem
        {
            get
            {
                if (EditedShoppingListName == "")
                    return null;

                MasterShoppingListItem masterShoppingListItem;

                mMasterShoppingList.TryGetValue(EditedShoppingListName, out masterShoppingListItem);

                return masterShoppingListItem;
            }
        }

		public static void ClearShoppingList()
		{
			foreach (MasterShoppingListItem shoppingListItem in mMasterShoppingList.Values) 
			{
				shoppingListItem.mShoppingList.Clear ();
			}
		}

        public static List<ShoppingListIngredient> GetShoppingList(int listIndex)
        {
            var list = mMasterShoppingList.Where(msl => msl.Value.mTableIndex == listIndex);

            if (list.Count() == 1)
            {
                return list.First().Value.mShoppingList;
            }
            return null;
        }


        public static MasterShoppingListItem GetMasterShoppingListItem(string shoppingListName)
        {
            MasterShoppingListItem masterShoppingListItem;

            mMasterShoppingList.TryGetValue(shoppingListName.ToLower(), out masterShoppingListItem);

            return masterShoppingListItem;
        }

		public static List<ShoppingListIngredient> GetShoppingList(string shoppingListName)
		{
            MasterShoppingListItem list;

            if (mMasterShoppingList.TryGetValue(shoppingListName, out list))
                return list.mShoppingList;
               
            return null;
		}

        public static void AddShoppingList(string shoppingListName, List<ShoppingListIngredient> shoppingList, int listIndex)
		{
            mMasterShoppingList.Add (shoppingListName, new MasterShoppingListItem(shoppingListName, listIndex, shoppingList));
		}

		public static void RemoveShoppingList(string shoppingListName)
		{
			mMasterShoppingList.Remove (shoppingListName);
		}

        public static void SetEditMode(string shoppingListName, bool bEditModeValue)
        {
            MasterShoppingListItem list;

            if (mMasterShoppingList.TryGetValue(shoppingListName, out list))
                list.mbEditMode = bEditModeValue;
        }

        public static bool GetEditMode(string shoppingListName)
        {
            MasterShoppingListItem list;

            if (mMasterShoppingList.TryGetValue(shoppingListName, out list))
                return list.mbEditMode;
            return false;
        }

        public static void AddIngredients(List<RecipeIngredient> ingredients, float ingredientMultiplier=1.0f)
		{
			foreach (var ingredient in ingredients)
			{
				MasterIngredientsList.Ingredient masterIngredient = MasterIngredientsList.FindIngredient(ingredient.mFood);

				if (masterIngredient != null)
				{
                    if (!ShoppingListIngredient.ShouldNotBeAddedToShoppingList(masterIngredient.Name))
                        AddIngredient(masterIngredient.Section, new ShoppingListIngredient(ingredient, ingredientMultiplier, masterIngredient.Aisle));
				}
				else
				{
                    AddIngredient("household", new ShoppingListIngredient(ingredient,ingredientMultiplier,""));
					Console.Error.WriteLine("Failed to find section name for ingredient: " + ingredient.mFood);				
				}
			}

            CheckForSaltAndIodisedSalt();
		}

        public static void RemoveIngredients(List<RecipeIngredient> ingredients, float ingredientMultiplier=1.0f)
		{
			foreach (var ingredient in ingredients)
			{
				MasterIngredientsList.Ingredient masterIngredient = MasterIngredientsList.FindIngredient(ingredient.mFood);

				if (masterIngredient != null)
				{
                    if (!ShoppingListIngredient.ShouldNotBeAddedToShoppingList(masterIngredient.Name))
                        RemoveIngredient(masterIngredient.Section, new ShoppingListIngredient(ingredient,ingredientMultiplier, masterIngredient.Aisle));
				}
				else
				{
                    RemoveIngredient("household", new ShoppingListIngredient(ingredient,ingredientMultiplier, ""));
					Console.Error.WriteLine("Failed to find section name for inrgedient: " + ingredient.mFood);				
				}
			}
		}


        public static void AddIngredient(string shoppingListName, ShoppingListIngredient ingredient)
		{
            MasterShoppingListItem masterShoppingListItem;

			if (mMasterShoppingList.TryGetValue (shoppingListName, out masterShoppingListItem)) 
			{
                ShoppingListIngredient ingredientInList = masterShoppingListItem.mShoppingList.Where (i => i.mFood == ingredient.mFood).FirstOrDefault ();

				if (ingredientInList != null) 
				{
					//Ingredient already exists so we're just upping the amount
                    if (!ingredientInList.Add(ingredient))
                        masterShoppingListItem.mShoppingList.Add (ingredient);
				} 
				else 
				{
                    masterShoppingListItem.mShoppingList.Add (ingredient);
				}

                masterShoppingListItem.mShoppingList.Sort((x, y) => x.mAisle.CompareTo(y.mAisle));
			}
		}

        public static void RemoveIngredient(string shoppingListName, ShoppingListIngredient ingredient)
		{
            MasterShoppingListItem masterShoppingListItem;

            if (mMasterShoppingList.TryGetValue (shoppingListName, out masterShoppingListItem)) 
			{
                ShoppingListIngredient ingredientInList = masterShoppingListItem.mShoppingList.Where (i => i.mFood == ingredient.mFood).FirstOrDefault ();

				if (ingredientInList != null) 
				{
                    ingredientInList.Remove(ingredient);

                    if (ingredientInList.Quantity == 0)
                        masterShoppingListItem.mShoppingList.Remove(ingredientInList);
				} 
				else 
				{
                    Console.Error.WriteLine(ingredient.mFood + "Trying to remove an ingredient that from the Master Shopping list that does not exist");
				}
			}
		}

        public static void CheckForSaltAndIodisedSalt()
        {
            var shoppingListItem = MasterShoppingList.GetMasterShoppingListItem("PANTRY & BAKERY");
                
            if (shoppingListItem == null)
                return;

            var shoppingList = shoppingListItem.mShoppingList;

            bool bContainsIodisedSalt = shoppingList.Find(i => i.mFood == "salt (iodised)") != null;
            var salt = shoppingList.Find(i => i.mFood == "salt");

            if (salt != null && bContainsIodisedSalt)
                shoppingList.Remove(salt);
        }
            
        static HtmlDocument mHtmlDocument; 

        public static void CreateEmptySectionTitle(string title)
        {
            var labelNode = mHtmlDocument.DocumentNode.SelectNodes ("//label").Where (label => label.InnerText == title).FirstOrDefault ();

            if (labelNode != null)
                labelNode.InnerHtml = title + " (EMPTY)";
        }

        public static string CreateHTMLShoppingList()
        {
            mHtmlDocument = new HtmlDocument();
            mHtmlDocument.LoadHtml(sHtmlTemplate);

//            foreach (var sectionString in MasterIngredientsList.Sections)
//            {
//                string upperSectionString = sectionString.ToUpper();
//
//                HtmlNode titleNode = mHtmlDocument.DocumentNode.SelectNodes ("//ul").Where (ul => ul.GetAttributeValue ("class", "") == "items" && ul.GetAttributeValue ("section", "") == upperSectionString).FirstOrDefault ();
//                string sectionList = MasterShoppingList.GetMasterShoppingListItem(upperSectionString).HtmlList();
//                if (sectionList != "" && titleNode != null)
//                    titleNode.InnerHtml = sectionList;
//                else
//                    CreateEmptySectionTitle(upperSectionString);
//            }

            HtmlNode dateNode = mHtmlDocument.DocumentNode.SelectNodes("//span").Where(span => span.InnerText == "$DATE").FirstOrDefault();
			string month = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(DateTime.Today.Month);
            string customDateString = DateTime.Today.Day + " " + month + ", " + DateTime.Today.Year; 
            dateNode.InnerHtml = customDateString;

            HtmlNode titleNode = mHtmlDocument.DocumentNode.SelectNodes ("//ul").Where (ul => ul.GetAttributeValue ("class", "") == "items" && ul.GetAttributeValue ("section", "") == "FRUIT & VEG").FirstOrDefault ();
            string sectionList = MasterShoppingList.GetMasterShoppingListItem("FRUIT & VEG").HtmlList();
            if (sectionList != "")
                titleNode.InnerHtml = sectionList;
            else
                CreateEmptySectionTitle("FRUIT & VEG");

            titleNode = mHtmlDocument.DocumentNode.SelectNodes ("//ul").Where (ul => ul.GetAttributeValue ("class", "") == "items" && ul.GetAttributeValue ("section", "") == "PANTRY & BAKERY").FirstOrDefault ();
            sectionList = MasterShoppingList.GetMasterShoppingListItem("PANTRY & BAKERY").HtmlList();
            if (sectionList != "")
                titleNode.InnerHtml = sectionList;
            else
                CreateEmptySectionTitle("PANTRY & BAKERY");

            titleNode = mHtmlDocument.DocumentNode.SelectNodes ("//ul").Where (ul => ul.GetAttributeValue ("class", "") == "items" && ul.GetAttributeValue ("section", "") == "MEAT & FISH").FirstOrDefault ();
            sectionList =  MasterShoppingList.GetMasterShoppingListItem("MEAT & FISH").HtmlList();
            if (sectionList != "")
                titleNode.InnerHtml = sectionList;
            else
                CreateEmptySectionTitle("MEAT & FISH");

            titleNode = mHtmlDocument.DocumentNode.SelectNodes ("//ul").Where (ul => ul.GetAttributeValue ("class", "") == "items" && ul.GetAttributeValue ("section", "") == "FRIDGE, FREEZER & DELI").FirstOrDefault ();
            sectionList = MasterShoppingList.GetMasterShoppingListItem("FRIDGE, FREEZER & DELI").HtmlList();
            if (sectionList != "")
                titleNode.InnerHtml = sectionList;
            else
                CreateEmptySectionTitle("FRIDGE, FREEZER & DELI");

            titleNode = mHtmlDocument.DocumentNode.SelectNodes ("//ul").Where (ul => ul.GetAttributeValue ("class", "") == "items" && ul.GetAttributeValue ("section", "") == "HOUSEHOLD").FirstOrDefault ();
            sectionList = MasterShoppingList.GetMasterShoppingListItem("HOUSEHOLD").HtmlList();
            if (sectionList != "")
                titleNode.InnerHtml = sectionList;
            else
                CreateEmptySectionTitle("HOUSEHOLD");

            return mHtmlDocument.DocumentNode.InnerHtml;
        }
	}
}

