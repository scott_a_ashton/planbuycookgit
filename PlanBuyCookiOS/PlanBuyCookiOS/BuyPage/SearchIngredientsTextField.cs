using System;
using UIKit;
using Foundation;
using CoreGraphics;

namespace PlanBuyCookiOS
{
    [Register("SearchIngredientsTextField")]
    public partial class SearchIngredientsTextField : UITextField
    {
        UIImageView mSearchIcon;

        public SearchIngredientsTextField ()
        {
        }

        public SearchIngredientsTextField(IntPtr handle) : base(handle)
        {
            Delegate = new SearchIngredientsDelegate();

          
            mSearchIcon = new UIImageView(UIImage.FromFile("SearchIcon.png"));
            mSearchIcon.Frame = new CGRect(0, 0, 20, 16);
            mSearchIcon.Alpha = 0.3f;
            LeftView = mSearchIcon;
            LeftViewMode = UITextFieldViewMode.Always;
        }
    }

    public class SearchIngredientsDelegate : UITextFieldDelegate
    {

        public override bool ShouldReturn(UITextField textField)
        {
            //            // NOTE: Don't call the base implementation on a Model class
            //            // see http://docs.xamarin.com/guides/ios/application_fundamentals/delegates,_protocols,_and_events
            //            Console.WriteLine("Trigger search");

            textField.ResignFirstResponder();
//            RecipeManager.Instance.Search(textField.Text);
//            AppDelegate.PlanPageViewController.RefreshMosaicView();
//            AppDelegate.PlanPageViewController.ScrollToTopOfMosaic();

            return true;
        }

        public override bool ShouldClear(UITextField textField)
        {
            return true;
        }

        public override bool ShouldEndEditing(UITextField textField)
        {
            // NOTE: Don't call the base implementation on a Model class
            // see http://docs.xamarin.com/guides/ios/application_fundamentals/delegates,_protocols,_and_events
            return true;
        }

        public override bool ShouldBeginEditing(UITextField textField)
        {
            return true;
        }
            
    }
}
