using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace PlanBuyCookiOS
{
	public class ShoppingListIngredient
	{
		public delegate void UnitConversion();

        public static MasterIngredientsList.Ingredient IodisedSalt;
        public static MasterIngredientsList.Ingredient BlackPepper;
        public static MasterIngredientsList.Ingredient Pepper;
        public static MasterIngredientsList.Ingredient Salt;
        public static MasterIngredientsList.Ingredient Water;
        public static List<MasterIngredientsList.Ingredient> mSpecialIngredients;
        public static List<MasterIngredientsList.Ingredient> mRemoveFromShoppingListIngredients;

		static Dictionary<Tuple<string,string>, UnitConversion> mUnitConversion = new Dictionary<Tuple<string, string>, UnitConversion>() ;

		public string mFood;
		float mQuantity; 
		public string mUnit;
        public string mAisle;

        public bool mbEditing = false;

        //Allow the ignoring of ingredients in the instance of strikethrough in the list
        public bool mbIgnore = false;


        public float Quantity
        {
            get
            {
                return mQuantity;
            }
        }

        public ShoppingListIngredient (string food, float quantity, string unit, string aisle)
        {
            mFood = food;
            mQuantity = quantity;
            mUnit = unit.Trim();
            mAisle = aisle;

            SpecialRules();
        }

        public ShoppingListIngredient (RecipeIngredient recipeIngredient, string aisle)
		{
			mFood = recipeIngredient.mFood;
			mQuantity = recipeIngredient.mQuantity;
            mUnit = recipeIngredient.mUnit.Trim();
            mAisle = aisle;

            SpecialRules();
		}

        public ShoppingListIngredient (RecipeIngredient recipeIngredient, float ingredientMultiplier, string aisle)
        {
            mFood = recipeIngredient.mFood;
            mQuantity = recipeIngredient.mQuantity * ingredientMultiplier;
            mUnit = recipeIngredient.mUnit.Trim();
            mAisle = aisle;

            SpecialRules();
        }
			
        public void SpecialRules()
        {
            if (mSpecialIngredients == null)
            {
                mSpecialIngredients = new List<MasterIngredientsList.Ingredient>();

                IodisedSalt = MasterIngredientsList.FindIngredient("salt (iodised)");
                mSpecialIngredients.Add(IodisedSalt);
                Debug.Assert(IodisedSalt != null);

                BlackPepper = MasterIngredientsList.FindIngredient("black pepper");
                mSpecialIngredients.Add(BlackPepper);
                Debug.Assert(BlackPepper != null);

                Pepper = MasterIngredientsList.FindIngredient("pepper");
                mSpecialIngredients.Add(BlackPepper);
                Debug.Assert(BlackPepper != null);

                Salt = MasterIngredientsList.FindIngredient("salt");
                mSpecialIngredients.Add(Salt);
                Debug.Assert(Salt != null);
            }
                
            if (!IsSpecialShoppingIngredient(mFood))
                return;

            //It's a special ingredient.  Zero out the quantity
            mQuantity = 0.0f;
            mUnit = "";
        }

        public static bool IsSpecialShoppingIngredient(string food)
        {
            string lowerFood = food.ToLower();
            return mSpecialIngredients.Find(i => i.Name == lowerFood) != null;
        }

        public static bool ShouldNotBeAddedToShoppingList(string food)
        {
            if (mRemoveFromShoppingListIngredients == null)
            {
                mRemoveFromShoppingListIngredients = new List<MasterIngredientsList.Ingredient>();

                Water =  MasterIngredientsList.FindIngredient("water");

                mRemoveFromShoppingListIngredients.Add(Water);
            }

            string lowerFood = food.ToLower();
            return mRemoveFromShoppingListIngredients.Find(i => i.Name == lowerFood) != null;
        }

        public bool Add(ShoppingListIngredient additionalIngredients)
		{
			Debug.Assert (additionalIngredients.mFood == mFood);

            //Is additionalIngredients convertable to mUnit
            if (MeasurementUnits.IsConvertableTo(mUnit, additionalIngredients.mUnit)) 
            {
                mQuantity += MeasurementUnits.GetConversion(mUnit, additionalIngredients.mUnit)(additionalIngredients.mQuantity);

                return true;
			} else {
				Console.Error.WriteLine ("Ingredients units need converting");
			}
            return false;
		}

        public void Remove(ShoppingListIngredient additionalIngredients)
		{
			Debug.Assert (additionalIngredients.mFood == mFood);

            if (MeasurementUnits.IsConvertableTo(mUnit, additionalIngredients.mUnit)) 
            {
                mQuantity -= MeasurementUnits.GetConversion(mUnit, additionalIngredients.mUnit)(additionalIngredients.mQuantity);
                if (mQuantity < 0.0f)
                    mQuantity = 0.0f;
			} else {
				Console.Error.WriteLine ("Ingredients units need converting");
			}
		}

        public void Modify(string editedUnit, float quantity)
        {
            if (MeasurementUnits.IsConvertableTo(mUnit, editedUnit)) 
            {
                mQuantity = MeasurementUnits.GetConversion(mUnit, editedUnit)(quantity);
                if (mQuantity < 0.0f)
                    mQuantity = 0.0f;
            } else {
                Console.Error.WriteLine ("Modifying a value with a non convertible unit");
            }
        }
	}
}

