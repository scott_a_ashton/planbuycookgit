using System;

namespace PlanBuyCookiOS
{
	public class ShoppingListMeal
	{
        public const int cMaxNumServes = 9;
        public const int cMinNumServes = 1;

        int         mMealIndex = 0;
        CalenderDay mCalendarDay;

        //TODO: Note that we don't need the index and the Calendar day but we'll leave so we don't break
        CalenderDay.Meal mCalendarDayMeal;

		public IRecipeImport mRecipe;

		public DateTime Date
		{
            get { return mCalendarDay.Date; }
		}
			
        public CalenderDay CalenderDay
        {
            get { return mCalendarDay; }
        }

        public CalenderDay.Meal CalenderDayMeal
        {
            get { return mCalendarDayMeal; }
        }

		public IRecipeImport Recipe
		{
            get 
            {
                return CalenderDayMeal.Recipe;
            }
		}

		public bool IsPartOfShoppingList
		{
			set;
			get;
		}
        public bool IsShoppedFor
        {
            set
            {
                CalenderDayMeal.IsShoppedFor = value;
            }
            get
            {
                return CalenderDayMeal.IsShoppedFor;
            }
        }

		public int NumServes
		{
			set;get;
		}

        public int OriginalNumServes
        {
            get { return CalenderDayMeal.Recipe.Serves; }
        }

        //Deprecated for now as you can't change numserves on the buy page
        public void IncrementNumServes()
        {
            NumServes++;

            NumServes = Utils.Clamp(NumServes, cMinNumServes, cMaxNumServes);
        }

        public void ResetNumServes()
        {
            NumServes = cMinNumServes;
        }

        public float ServesMulitplier
        {
            get
            {
                return (float)NumServes / (float)OriginalNumServes;
            }
        }

        public static bool IsEqualTo(ShoppingListMeal _this, ShoppingListMeal _that)
        {
            return (_this.CalenderDayMeal == _that.CalenderDayMeal);
        }

        public static int Comparer(ShoppingListMeal _this, ShoppingListMeal _that)
        {
            return DateTime.Compare(_this.Date, _that.Date);
        }

		public ShoppingListMeal (CalenderDay day, int index)
		{
            mMealIndex = index;
            mRecipe = day.mMeals [index].Recipe;
            NumServes = day.mMeals[index].Serves;
            mCalendarDay = day;
            mCalendarDayMeal = day.mMeals[index];
		}
	}
}

