using System;
using CoreGraphics;
using Foundation;
using UIKit;

namespace PlanBuyCookiOS
{
	public partial class CookPageMethodViewCell : UITableViewCell
	{

		public const float cMaxMethodFontSize = 20.0f;
		public const float cMinMethodFontSize = 12.0f;

		public static float mMethodFontSize = 15;
		public static float mSelectedMethodFontSize = 17;

        public int mRowIndex = 0;
        public float mTimer = 0.0f;

        public static UIImage sBlackClock;
        public static UIImage sRedClock;
        public static UIImage sGreenClock;



		public static readonly UINib Nib = UINib.FromName ("CookPageMethodViewCell", NSBundle.MainBundle);
		public static readonly NSString Key = new NSString ("CookPageMethodViewCell");

		public CookPageMethodViewCell (IntPtr handle) : base (handle)
		{

		}
			
        public static UIImage BlackClock
        {
            get
            {
                if (sBlackClock == null)
                    sBlackClock = UIImage.FromFile("MethodTimer_BlackClock.png");
                return sBlackClock;
            }
        }
        public static UIImage RedClock
        {
            get
            {
                if (sRedClock == null)
                    sRedClock = UIImage.FromFile("MethodTimer_RedClock.png");
                return sRedClock;
            }
        }
        public static UIImage GreenClock
        {
            get
            {
                if (sGreenClock == null)
                    sGreenClock = UIImage.FromFile("MethodTimer_GreenClock.png");
                return sGreenClock;
            }
        }

		public UITextView MethodTextView
		{
			get { return MethodText;}
		}

		public static CookPageMethodViewCell Create ()
		{
			CookPageMethodViewCell cell =  Nib.Instantiate (null, null)[0] as CookPageMethodViewCell;
			return cell;
		}

        public void InitialiseGestures(CookPageViewController.GestureDelegate gestureDelegate)
        {
            //if (InvisibleMethodTimer.GestureRecognizers == null)
            {
                UILongPressGestureRecognizer longPress = new UILongPressGestureRecognizer((UILongPressGestureRecognizer lp) => ResetTimer(lp));
                longPress.MinimumPressDuration = 0.5f;

                //longPress.NumberOfTouchesRequired = 1;
                //longPress.NumberOfTapsRequired = 1;
                longPress.Delegate = gestureDelegate;

                InvisibleMethodTimer.AddGestureRecognizer(longPress);
            }
        }

		public static void IncreaseFontSize ()
		{
			mMethodFontSize = Utils.Clamp (mMethodFontSize + 0.5f, cMinMethodFontSize, cMaxMethodFontSize);
			mSelectedMethodFontSize = mMethodFontSize + 2.0f;
		}

		public static void DecreaseFontSize()
		{
			mMethodFontSize = Utils.Clamp (mMethodFontSize - 0.5f, cMinMethodFontSize, cMaxMethodFontSize);
			mSelectedMethodFontSize = mMethodFontSize + 2.0f;
		}
            
		public static NSMutableAttributedString CreateMethodAttributedString(NSString newText, bool bIsSelected)
		{
			string fontName = bIsSelected ? "SceneStd-Black":"SceneStd-Regular";
            float fontSize = bIsSelected ? mSelectedMethodFontSize:mMethodFontSize;

			UIStringAttributes stringAttributes = new UIStringAttributes {
                Font = UIFont.FromName (fontName, fontSize),
				ForegroundColor = UIColor.Black,
                ParagraphStyle = new NSMutableParagraphStyle () { MinimumLineHeight = 20.0f, LineBreakMode = UILineBreakMode.WordWrap }
			};

			var AttributedText = new NSMutableAttributedString (newText);
			AttributedText.AddAttributes (stringAttributes, new NSRange (0, newText.Length));
			return AttributedText;
		}

        public void SetActiveClockGraphics()
        {
            MethodTimerImage.Image = GreenClock;
            Timer.TextColor = UIColor.Black;

            //Timer.TextColor = UIColor.FromRGB(16,167,153);
        }

        public void SetPausedClockGraphics()
        {
            MethodTimerImage.Image = RedClock;
            Timer.TextColor = UIColor.Red;
        }

        public void SetInactiveClockGraphics()
        {
            MethodTimerImage.Image = BlackClock;
            Timer.TextColor = UIColor.Black;
            Timer.Hidden = true;
        }

        public void SetTimer(MethodTimer timer)
        {
            if (timer.mbIsValid)
            {
                MethodTimerImage.Hidden = false;
                InvisibleMethodTimer.Hidden = false;

                if (timer.mbIsStarted)
                {
                    Timer.Hidden = false;

                    if (timer.mbPaused)
                        SetPausedClockGraphics();
                    else
                        SetActiveClockGraphics();

                    Timer.Text = timer.ToString();
                }
                else
                {
                    SetInactiveClockGraphics();
                    Timer.Hidden = true;
                }
            }
            else
            {
                MethodTimerImage.Hidden = true;
                Timer.Hidden = true;
                InvisibleMethodTimer.Hidden = true;
            }
        }

        public void SetMethod(int index, string method, MethodTimer timer)
        {
            mRowIndex = index;

            MethodIndex.Font = UIFont.FromName ("SceneStd-Black", mMethodFontSize);
            MethodIndex.Text = (index + 1).ToString();
            MethodIndex.TextColor = UIColor.FromRGB (253,206,49);

            MethodText.Font = UIFont.FromName ("SceneStd-Regular", mMethodFontSize);
            MethodText.AttributedText = CreateMethodAttributedString(new NSString(method), false);

            SetTimer(timer);
        }

        public void SetSelectedMethod(int index, string method, MethodTimer timer)
        {   
            mRowIndex = index;

			MethodText.Font = UIFont.FromName ("SceneStd-Black", mSelectedMethodFontSize);
			MethodText.TextContainer.LineBreakMode = UILineBreakMode.WordWrap;
            //MethodText.Text = method;
            MethodText.AttributedText = CreateMethodAttributedString(new NSString(method), true);
            //  MethodText.SizeToFit();

			MethodIndex.Text = (index + 1).ToString();
			MethodIndex.TextColor = UIColor.FromRGB (253,206,49);

            SetTimer(timer);
		}

        public void Select()
		{
			MethodText.Font = UIFont.FromName ("SceneStd-Black", mSelectedMethodFontSize);
			MethodText.TextContainer.LineBreakMode = UILineBreakMode.WordWrap;
		}

		public void Deselect()
		{
			MethodText.Font = UIFont.FromName ("SceneStd-Regular", mMethodFontSize);
			MethodText.TextContainer.LineBreakMode = UILineBreakMode.WordWrap;
		}

        partial void StartTimer(NSObject sender)
        {
            UIGestureRecognizer[] g = InvisibleMethodTimer.GestureRecognizers;
            CookPageMethodViewSource.StartTimer(CookPageMethodViewSource.TableView, mRowIndex);
        }

        partial void PauseTimer(NSObject sender)
        {
            Console.WriteLine("PauseTimer");
            CookPageMethodViewSource.TogglePause(CookPageMethodViewSource.TableView, mRowIndex);
        }

        partial void AddTimeToTimer(NSObject sender)
        {
            CookPageMethodViewSource.AddTimeToTimer(CookPageMethodViewSource.TableView, mRowIndex);
        }

        void ResetTimer(UILongPressGestureRecognizer gestureRecognizer)
        {
            //Make sure we only trigger the once
            if (gestureRecognizer.State == UIGestureRecognizerState.Began && !Timer.Hidden)
            {
                Console.WriteLine("Reset row " + mRowIndex);

                SetInactiveClockGraphics();
                CookPageMethodViewSource.ResetTimer(CookPageMethodViewSource.TableView, mRowIndex);
            }
        }
	}
}

