// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace PlanBuyCookiOS
{
	[Register ("CookPageMethodViewCell")]
	partial class CookPageMethodViewCell
	{
		[Outlet]
		UIKit.UIButton InvisibleMethodTimer { get; set; }

		[Outlet]
		UIKit.UILabel MethodIndex { get; set; }

		[Outlet]
		UIKit.UITextView MethodText { get; set; }

		[Outlet]
		UIKit.UIImageView MethodTimerImage { get; set; }

		[Outlet]
		UIKit.UILabel Timer { get; set; }

		[Action ("AddTimeToTimer:")]
		partial void AddTimeToTimer (Foundation.NSObject sender);

		[Action ("PauseTimer:")]
		partial void PauseTimer (Foundation.NSObject sender);

		[Action ("StartTimer:")]
		partial void StartTimer (Foundation.NSObject sender);
		
		void ReleaseDesignerOutlets ()
		{
			if (InvisibleMethodTimer != null) {
				InvisibleMethodTimer.Dispose ();
				InvisibleMethodTimer = null;
			}

			if (MethodIndex != null) {
				MethodIndex.Dispose ();
				MethodIndex = null;
			}

			if (MethodText != null) {
				MethodText.Dispose ();
				MethodText = null;
			}

			if (MethodTimerImage != null) {
				MethodTimerImage.Dispose ();
				MethodTimerImage = null;
			}

			if (Timer != null) {
				Timer.Dispose ();
				Timer = null;
			}
		}
	}
}
