using System;
using System.Collections.Generic;
using CoreGraphics;
using Foundation;
using UIKit;
using System.Linq;

namespace PlanBuyCookiOS
{
	public class CookPageMethodViewSource : UITableViewSource
	{

        static MethodTimer[] mTimers;

        static int mSelectedRow = 0;
        static IRecipeImport mRecipe;

        //I don't want to store this but it's getting to that stage of the project! 
        static UITableView mTableView;

        public CookPageMethodViewSource (UITableView tableView, IRecipeImport recipe)
		{
            mTableView = tableView;

            if (mRecipe != recipe)
            {
                mRecipe = recipe;
                mSelectedRow = 0;

                //Set mRecipe before call
                InitialiseTimers(recipe);
            }
		}
			
        public static UITableView TableView
        {
            get
            {
                return mTableView;
            }
        }

        public MethodTimer[] Timers
        {
            get
            {
                return mTimers;
            }
        }

        public void InitialiseTimers(IRecipeImport recipe)
        {
            List<RecipeStep> stepsList = recipe.StepsList;

            if (mTimers != null)
                InvalidateExistingTimers();

            mTimers = new MethodTimer[stepsList.Count];

            for (int stepIndex = 0; stepIndex < mTimers.Length; stepIndex++)
            {
                RecipeStep step = stepsList[stepIndex];

                if (step.mTimer > 0)
                    mTimers[stepIndex] = new MethodTimer(TimeSpan.FromMinutes(step.mTimer));
                else
                    mTimers[stepIndex] = new MethodTimer(TimeSpan.FromMinutes(0));
            }
        }



        public void InvalidateExistingTimers()
        {
            foreach (MethodTimer timer in mTimers)
            {
                timer.TogglePause();
            }
        }

		public override nint RowsInSection (UITableView tableview, nint section)
		{
            return mRecipe.StepsList.Count;
		}

		public override nint NumberOfSections (UITableView tableView)
		{
			// TODO: return the actual number of sections
			return 1;
		}

//		public override string TitleForHeader (UITableView tableView, int section)
//		{
//			return "Header";
//		}

//		public override string TitleForFooter (UITableView tableView, int section)
//		{
//			return "Footer";
//		}

		public override UITableViewCell GetCell (UITableView tableView, NSIndexPath indexPath)
		{
			var cell = tableView.DequeueReusableCell (CookPageMethodViewCell.Key) as CookPageMethodViewCell;

			if (cell == null)
				cell = CookPageMethodViewCell.Create ();

            //Add a long press recogniser if there is a valid timer on this method step
            if (mTimers[indexPath.Row]!=null && mTimers[indexPath.Row].mbIsValid)
            {
                CookPageViewController.GestureDelegate gDelegate = new CookPageViewController.GestureDelegate(AppDelegate.CookPageViewController);
                cell.InitialiseGestures(gDelegate);
            }

			string method = mRecipe.StepsList [indexPath.Row].mMethod;

			if (indexPath.Row == mSelectedRow)
                cell.SetSelectedMethod(indexPath.Row, method, mTimers[indexPath.Row]);
			else
                cell.SetMethod(indexPath.Row, method, mTimers[indexPath.Row]);
				
			return cell;
		}

		public override nfloat GetHeightForRow (UITableView tableView, NSIndexPath indexPath)
		{
			NSString methodText = new NSString(mRecipe.StepsList [indexPath.Row].mMethod);
			CGSize stringSize;
            float height = 0.0f;
            var ctxt = new NSStringDrawingContext();

            if (indexPath.Row == mSelectedRow)
            {
                NSMutableAttributedString text = CookPageMethodViewCell.CreateMethodAttributedString(methodText, true);

                stringSize = text.GetBoundingRect(new CGSize(514.0f, 99999), NSStringDrawingOptions.UsesFontLeading | NSStringDrawingOptions.UsesLineFragmentOrigin, ctxt).Size;
                //Make the minimum height of the line taller if there is a timer on the method

				height = (float)stringSize.Height + 12;

                if (mTimers[indexPath.Row].mbIsStarted && height < 50)
					height = (float)stringSize.Height + 32;
            }
            else
            {
                NSMutableAttributedString text = CookPageMethodViewCell.CreateMethodAttributedString(methodText, false);

                stringSize = text.GetBoundingRect(new CGSize(514.0f, 99999), NSStringDrawingOptions.UsesFontLeading | NSStringDrawingOptions.UsesLineFragmentOrigin, ctxt).Size;

				height = (float)stringSize.Height + 12;

                //Make the minimum height of the line taller if there is a timer on the method
                if (mTimers[indexPath.Row].mbIsStarted && height < 50)
					height = (float)stringSize.Height + 32;
            }

            return height;
		}

		public override void RowDeselected (UITableView tableView, NSIndexPath indexPath)
		{

		}

		public override void RowSelected (UITableView tableView, NSIndexPath indexPath)
		{
			int oldSelectedRow = mSelectedRow;
			mSelectedRow = indexPath.Row;

			if (oldSelectedRow != indexPath.Row) 
				tableView.ReloadSections(NSIndexSet.FromIndex(0), UITableViewRowAnimation.Automatic);
		}

		public void AdvanceSelected()
		{
			mSelectedRow++;
		}

        //Eek - Static method
        public static void StartTimer(UITableView table, int index)
        {
            mTimers[index].Start(mRecipe.Name, table, index);

            NSIndexPath[] rows = new NSIndexPath[]{ NSIndexPath.FromItemSection(index, 0) };
            table.ReloadRows(rows, UITableViewRowAnimation.None);
        }
            
        public void UpdateTimer (UITableView tableView, NSIndexPath indexPath)
        {
            tableView.CellAt(indexPath).SetNeedsDisplay();
        }

        public static void TogglePause(UITableView table, int index)
        {
            if (!mTimers[index].mbIsStarted)
                mTimers[index].Start(mRecipe.Name, table, index);
            else
                mTimers[index].TogglePause();

            NSIndexPath[] rows = new NSIndexPath[]{ NSIndexPath.FromItemSection(index, 0) };
            table.ReloadRows(rows, UITableViewRowAnimation.None);
        }

        public static void AddTimeToTimer(UITableView table, int index)
        {
            mTimers[index].AddTime(TimeSpan.FromSeconds(5*60));

            NSIndexPath[] rows = new NSIndexPath[]{ NSIndexPath.FromItemSection(index, 0) };
            table.ReloadRows(rows, UITableViewRowAnimation.None);
        }

        public static void ResetTimer(UITableView table, int index)
        {
            mTimers[index].Reset();

            NSIndexPath[] rows = new NSIndexPath[]{ NSIndexPath.FromItemSection(index, 0) };
            table.ReloadRows(rows, UITableViewRowAnimation.None);
        }
        public static bool HasTimer(int index)
        {
            if (mRecipe is RecipeWeb)
                return false;

            return mRecipe.StepsList[index].mTimer > 0;
        }

        public static bool TimerIsActive(int index)
        {
            if (mTimers[index] == null)
                return false;

            var timer = mTimers[index];

            return (mTimers[index] != null && mTimers[index].mbIsValid && mTimers[index].mbIsStarted);
        }        

        public static void UpdateLiveTimers()
        {
            if (mTimers == null)
                return;

            var liveTimers = mTimers.Where((t, i) => TimerIsActive(i));

            foreach (var timer in liveTimers)
                timer.Update();
        }

        public static void CancellAllTimers()
        {
            if (mTimers == null)
                return;

            var liveTimers = mTimers.Where((t, i) => TimerIsActive(i));

            foreach (var timer in liveTimers)
                timer.Reset();
        }
    }
}

