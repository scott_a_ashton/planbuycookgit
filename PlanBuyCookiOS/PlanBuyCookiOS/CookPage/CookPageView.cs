using System;
using System.Text;
using CoreGraphics;
using Foundation;
using UIKit;


namespace PlanBuyCookiOS
{
	[Register("CookPageView")]
	public partial class CookPageView : UIView
	{
		readonly float IngredientFontSize = 13.0f;
		readonly float IngredientsListTitleFontSize = 14.0f;
		readonly float RecipeTitleFontSize = 28.0f;
		readonly float RecipeInfoFontSize = 13.0f;
		readonly float TipFontSize = 15.0f;
        readonly float NutritionFontSize = 13.0f;

        static UIPopoverController sPopOver;

		IRecipeImport mDisplayedRecipe;

        //A serves adjustment to the current recipe - the actual recipe is not modified
        int mAdjustedServes = 0;

        public float ServesMultiplier
        {
            get
            {
                if (mDisplayedRecipe != null)
                    return (float)mAdjustedServes / (float)mDisplayedRecipe.Serves;
                return 1;
            }
        }

		public CookPageView ()
		{
		}

		public CookPageView(IntPtr handle) : base(handle)
		{
		}

		//helper method
		public void EnumerateFonts()
		{
			var fontList = new StringBuilder();
			var familyNames = UIFont.FamilyNames;
			foreach (var familyName in familyNames ){
				fontList.Append(String.Format("Family: {0}\n", familyName));
				Console.WriteLine("Family: {0}\n", familyName);
				var fontNames = UIFont.FontNamesForFamilyName(familyName);
				foreach (var fontName in fontNames ){
					Console.WriteLine("\tFont: {0}\n", fontName);
					fontList.Append(String.Format("\tFont: {0}\n", fontName));
				}
			};
			Console.WriteLine(fontList.ToString());
		}

		public void Initialise()
		{
			IngredientsList.AlwaysBounceHorizontal = false;
			IngredientsList.ContentInset = new UIEdgeInsets(5, 0, 5, 0);

			LoadFonts ();
		}

		void LoadFonts()
		{
			IngredientsListTitle.Font = UIFont.FromName ("SceneStd-Black", IngredientsListTitleFontSize);
			RecipeTitle.Font = UIFont.FromName ("Bernard MT Condensed", RecipeTitleFontSize);

			//Recipe Info
			PrepTextTitle.Font = UIFont.FromName ("SceneStd-Regular", RecipeInfoFontSize);
			PrepText.Font = UIFont.FromName ("SceneStd-Black", RecipeInfoFontSize);
			CookText.Font = UIFont.FromName ("SceneStd-Black", RecipeInfoFontSize);
			CookTextTitle.Font = UIFont.FromName ("SceneStd-Regular", RecipeInfoFontSize);
			ServesText.Font = UIFont.FromName ("SceneStd-Black", RecipeInfoFontSize);
			ServesTextTitle.Font = UIFont.FromName ("SceneStd-Regular", RecipeInfoFontSize);
			TipBox.Font = UIFont.FromName ("SegoePrint-Bold", TipFontSize);

            ProteinLabel.Font = UIFont.FromName ("SceneStd-Regular", RecipeInfoFontSize);
			ProteinValue.Font =  UIFont.FromName ("SceneStd-Black", RecipeInfoFontSize);
            FatLabel.Font = UIFont.FromName ("SceneStd-Regular", RecipeInfoFontSize);
			FatValue.Font =  UIFont.FromName ("SceneStd-Black", RecipeInfoFontSize);
            CarbsLabel.Font = UIFont.FromName ("SceneStd-Regular", RecipeInfoFontSize);
			CarbsValue.Font =  UIFont.FromName ("SceneStd-Black", RecipeInfoFontSize);
            FibreLabel.Font = UIFont.FromName ("SceneStd-Regular", RecipeInfoFontSize);
			FibreValue.Font =  UIFont.FromName ("SceneStd-Black", RecipeInfoFontSize);
            EnergyLabel.Font = UIFont.FromName ("SceneStd-Regular", NutritionFontSize);
            EnergyValueKj.Font =  UIFont.FromName ("SceneStd-Black", NutritionFontSize);
            EnergyValueCal.Font =  UIFont.FromName ("SceneStd-Black", NutritionFontSize);
		}
			
        static UIPopoverController ServesPopover
        {
            get
            {
                if (sPopOver == null)
                {
                    sPopOver = new UIPopoverController(new ServesPopoverViewController());
                    sPopOver.PopoverContentSize = sPopOver.ContentViewController.View.Frame.Size;
                    sPopOver.BackgroundColor = UIColor.White;
                    //sPopOver.DidDismiss += (object dismissSender, EventArgs e) => Console.WriteLine(( sPopOver.ContentViewController as ServesPopoverViewController).ServesSliderValue.ToString());
                }
                return sPopOver;
            }
        }

        //We've got here by double clicking on the meal in the planner (not the mosaic)
        public void UpdateRecipe (CalenderDay.Meal meal)
        {
            UpdateRecipe(meal.Recipe, meal.Serves);
        }

        public void UpdateRecipe (IRecipeImport recipeToDisplay, int adjustedServes = -1)
		{
            Console.WriteLine("Cook Page: " + recipeToDisplay.Name);

			if (recipeToDisplay == null)
				return;

            //Cancel all the current timers if we change recipe
            if (mDisplayedRecipe != recipeToDisplay)
                CookPageMethodViewSource.CancellAllTimers();

			mDisplayedRecipe = recipeToDisplay;

            RecipeTitle.Text = recipeToDisplay.Name;

			ServesButton.Enabled = true;
            PrepText.Text = recipeToDisplay.PrepTime.ToString () + " MINS";
            CookText.Text = recipeToDisplay.CookTime.ToString () + " MINS";

            if (adjustedServes != -1)
            {
                ServesText.Text = adjustedServes.ToString ();
                mAdjustedServes = adjustedServes;
            }
            else
            {
                ServesText.Text = recipeToDisplay.Serves.ToString ();
                mAdjustedServes = recipeToDisplay.Serves;
            }
   
            if (mDisplayedRecipe is RecipeWeb)
                IngredientsList.AttributedText = IngredientsString (mDisplayedRecipe as RecipeWeb);
            else
    			IngredientsList.AttributedText = IngredientsString (mDisplayedRecipe as RecipeSQL);

			IngredientsList.TextAlignment = UITextAlignment.Right;
			IngredientsList.Editable = false;

            CookPageMethodViewSource source = new CookPageMethodViewSource (MethodTable, recipeToDisplay);
			MethodTable.Source = source;
			MethodTable.ReloadData ();
            MethodTable.ScrollToRow(NSIndexPath.FromRowSection(0, 0), UITableViewScrollPosition.Top, false);

			TipButton.Enabled = recipeToDisplay.Notes.Length > 0;
			TipButton.Selected = false;
			TipBox.Hidden = true;
			TipBox.Text = recipeToDisplay.Notes;

            NutritionWindow.Hidden = true;
            EnergyValueKj.Text = recipeToDisplay.Energy.ToString() + "kj";
            EnergyValueCal.Text = (Math.Floor(recipeToDisplay.Energy/ 4.2)).ToString() + "kcal";
            FatValue.Text = recipeToDisplay.Fat.ToString() + "g";
            ProteinValue.Text = recipeToDisplay.Protein.ToString() + "g";
            FibreValue.Text = recipeToDisplay.Fibre.ToString() + "g";
            CarbsValue.Text = recipeToDisplay.Carbs.ToString() + "g";
            FatValue.Text = recipeToDisplay.Fat.ToString() + "g";

			NutritionButton.Enabled = true;
            NutritionButton.Selected = false;

            Snowflake.Hidden = !recipeToDisplay.CategoriesString.Contains("freezes");

            Favourite.Hidden = true;
            if (mDisplayedRecipe is RecipeSQL)
                Favourite.Hidden = !(mDisplayedRecipe as RecipeSQL).Favourite;

			//TODO: Implement Image cache
			byte[] encodedDataAsBytes = Convert.FromBase64String (recipeToDisplay.ImageString);
			RecipeImage.Image = UIImage.LoadFromData (NSData.FromArray (encodedDataAsBytes));

            //Disable some of the buttons for web recipes, and crop the image to square
            if (recipeToDisplay is RecipeWeb)
            {
                RecipeImage.Image = Utils.CropToSquareImage(RecipeImage.Image);

                NutritionButton.Enabled = false;
                ServesButton.Enabled = false;
            }
		}


        public NSMutableAttributedString IngredientsString(RecipeSQL recipe)
        {
            UIStringAttributes ingredientRegular = new UIStringAttributes {Font = UIFont.FromName("SceneStd-Regular", IngredientFontSize), ForegroundColor = UIColor.Black};
            UIStringAttributes ingredientBold = new UIStringAttributes {Font = UIFont.FromName("SceneStd-Black", IngredientFontSize), ForegroundColor = UIColor.Black};

            NSMutableAttributedString regular = new NSMutableAttributedString("");

            foreach (RecipeIngredient ingredient in recipe.mIngredientsList)
            {
				MasterIngredientsList.Ingredient masterIngredient = MasterIngredientsList.FindIngredient(ingredient.mFood);

				if (masterIngredient == null)
				{
					Console.Error.WriteLine(recipe.Name +": "+ingredient.mFood + " not found in master ingredients list");
					continue;
				}

				RoundingRules.RoundedAmount roundedAmount = RoundingRules.RoundingRule (masterIngredient.RoundingRule, ingredient, ServesMultiplier);

				float ingredientQuantity = roundedAmount.mQuantity;
                string quantityString = ingredientQuantity == 0 ? "" : FormattedIngredientString.CreateNumericalString(roundedAmount);
                string unitString = "";
                string foodString = ingredient.mFood;
                string methodString = ingredient.mPrep!=""? ", " + ingredient.mPrep : "";
                string ingredientString = "";

                RecipeUnit recipeUnit = MeasurementUnits.GetRecipeUnit((ingredient.mUnit != roundedAmount.mUnit) ?  roundedAmount.mUnit : ingredient.mUnit);

                if (recipeUnit == null)
                    recipeUnit = MeasurementUnits.GetRecipeUnit("");

                unitString = FormattedIngredientString.ConstructUnitString(ingredientQuantity, roundedAmount.mUnit, recipeUnit);
                    
                if (quantityString != "")
                    ingredientString += quantityString + " ";

                if (unitString != "")
                    ingredientString += unitString + " ";

                if (ingredientQuantity > 1 && masterIngredient.Plural != "")
                    foodString = masterIngredient.Plural;

                ingredientString += foodString + methodString + "\n\n";

				int positionOfIngredient = (int)regular.Length;

                regular.Append(new NSMutableAttributedString(ingredientString));

				int positionOfFood = (int)regular.Length - ((foodString+methodString).Length+2);

                regular.AddAttributes(ingredientRegular.Dictionary, new NSRange(positionOfIngredient, ingredientString.Length));
                regular.AddAttributes(ingredientBold.Dictionary, new NSRange(positionOfFood, foodString.Length));
            }
            return regular;
        }

        public NSMutableAttributedString IngredientsString(RecipeWeb recipe)
        {
            //string ingredientsString = "";

            //UIStringAttributes ingredientRegular = new UIStringAttributes {Font = UIFont.FromName("SceneStd-Regular", IngredientFontSize), ForegroundColor = UIColor.Black};
            //UIStringAttributes ingredientBold = new UIStringAttributes {Font = UIFont.FromName("SceneStd-Black", IngredientFontSize), ForegroundColor = UIColor.Black};

            NSMutableAttributedString regular = new NSMutableAttributedString("");

            foreach (RecipeIngredient ingredient in recipe.mIngredientsList)
                regular.Append(new NSMutableAttributedString(ingredient.mFood + "\n\n"));

            return regular;
        }

		partial void CookPageTip (NSObject sender)
		{
			(sender as UIButton).Selected = !(sender as UIButton).Selected;
			TipBox.Hidden = !TipBox.Hidden;

			MethodTable.SelectRow(NSIndexPath.FromRowSection(1,0),true, UITableViewScrollPosition.None);

            CaptureTouch.Hidden = TipBox.Hidden;
		}

		partial void NextRecipe (NSObject sender)
		{
			RecipeManager.Instance.NextRecipe();
            UpdateRecipe(RecipeManager.Instance.CurrentRecipe);
		}

		partial void PrevRecipe (NSObject sender)
		{
			RecipeManager.Instance.PrevRecipe();
			UpdateRecipe(RecipeManager.Instance.CurrentRecipe);
		}

		partial void SwitchToPlanPage (NSObject sender)
		{
			AppDelegate.SwitchToPlanPage();
		}

		partial void SwitchToBuyPage (NSObject sender)
		{
			AppDelegate.SwitchToBuyPage();
		}

		partial void DecreaseFontSize (NSObject sender)
		{
			CookPageMethodViewCell.DecreaseFontSize();
			MethodTable.ReloadSections(NSIndexSet.FromIndex(0), UITableViewRowAnimation.Automatic);
		}

		partial void IncreaseFontSize (NSObject sender)
		{
			CookPageMethodViewCell.IncreaseFontSize();
			MethodTable.ReloadSections(NSIndexSet.FromIndex(0), UITableViewRowAnimation.Automatic);
		}

		partial void RecipeTrashCan (NSObject sender)
		{
			//throw new System.NotImplementedException ();
		}

        partial void ToggleNutrition(NSObject sender)
        {
            NutritionWindow.Hidden = !NutritionWindow.Hidden;
            NutritionButton.Selected = !NutritionButton.Selected;

            CaptureTouch.Hidden = NutritionWindow.Hidden;
        }

        partial void ToggleFavourite(NSObject sender)
        {
            if (mDisplayedRecipe is RecipeSQL)
                (mDisplayedRecipe as RecipeSQL).Favourite = !(mDisplayedRecipe as RecipeSQL).Favourite;

            Favourite.Hidden = !(mDisplayedRecipe as RecipeSQL).Favourite;
            RecipeManager.Instance.UpdateRecipe(mDisplayedRecipe as RecipeSQL);
        }

        partial void AdjustServes(NSObject sender)
        {
            (ServesPopover.ContentViewController as ServesPopoverViewController).ServesSliderValue = (float)mAdjustedServes / 10.0f;
            (ServesPopover.ContentViewController as ServesPopoverViewController).SetUpdateCallback(UpdateServesText);

            ServesText.Text = mAdjustedServes.ToString();

            CGRect frame = ServesButton.Frame;
            //frame.Size = new SizeF(ServesButton.Frame.Width,ServesButton.Frame.Height * 2);
            ServesPopover.PresentFromRect(frame, ServesInfoView , UIPopoverArrowDirection.Up , true);
        }

        void UpdateServesText(float valueFromSlider)
        {
            mAdjustedServes = ((int)(1.0f + (9.0f * valueFromSlider)));
            ServesText.Text = mAdjustedServes.ToString();

            if (mDisplayedRecipe is RecipeWeb)
                IngredientsList.AttributedText = IngredientsString (mDisplayedRecipe as RecipeWeb);
            else
                IngredientsList.AttributedText = IngredientsString (mDisplayedRecipe as RecipeSQL);

            IngredientsList.TextAlignment = UITextAlignment.Right;
        }

        partial void CloseNutritionAndTip(NSObject sender)
        {
            if (!NutritionWindow.Hidden)
            {
                NutritionWindow.Hidden = true;
                NutritionButton.Selected = false;
            }

            if (!TipBox.Hidden)
            {
                TipBox.Hidden = true;
                TipButton.Selected = false;
            }

            CaptureTouch.Hidden = true;
        }

        partial void OpemPBCWebPage(NSObject sender)
        {
            if(UIApplication.SharedApplication.CanOpenUrl(NSUrl.FromString(AppDelegate.sPBCWebPage))) 
            {
                // Open in Safari instead
                UIApplication.SharedApplication.OpenUrl(NSUrl.FromString(AppDelegate.sPBCWebPage));
            }
        }
   	}
}

