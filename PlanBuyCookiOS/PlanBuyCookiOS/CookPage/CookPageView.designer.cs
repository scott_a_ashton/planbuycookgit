// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace PlanBuyCookiOS
{
	partial class CookPageView
	{
		[Outlet]
		UIKit.UIButton CaptureTouch { get; set; }

		[Outlet]
		UIKit.UILabel CarbsLabel { get; set; }

		[Outlet]
		UIKit.UILabel CarbsValue { get; set; }

		[Outlet]
		UIKit.UILabel CookText { get; set; }

		[Outlet]
		UIKit.UILabel CookTextTitle { get; set; }

		[Outlet]
		UIKit.UILabel EnergyLabel { get; set; }

		[Outlet]
		UIKit.UILabel EnergyValueCal { get; set; }

		[Outlet]
		UIKit.UILabel EnergyValueKj { get; set; }

		[Outlet]
		UIKit.UILabel FatLabel { get; set; }

		[Outlet]
		UIKit.UILabel FatValue { get; set; }

		[Outlet]
		UIKit.UIImageView Favourite { get; set; }

		[Outlet]
		UIKit.UILabel FibreLabel { get; set; }

		[Outlet]
		UIKit.UILabel FibreValue { get; set; }

		[Outlet]
		UIKit.UITextView IngredientsList { get; set; }

		[Outlet]
		UIKit.UILabel IngredientsListTitle { get; set; }

		[Outlet]
		UIKit.UITableView MethodTable { get; set; }

		[Outlet]
		UIKit.UIButton NutritionButton { get; set; }

		[Outlet]
		UIKit.UIView NutritionWindow { get; set; }

		[Outlet]
		UIKit.UILabel PrepText { get; set; }

		[Outlet]
		UIKit.UILabel PrepTextTitle { get; set; }

		[Outlet]
		UIKit.UILabel ProteinLabel { get; set; }

		[Outlet]
		UIKit.UILabel ProteinValue { get; set; }

		[Outlet]
		UIKit.UIImageView RecipeImage { get; set; }

		[Outlet]
		UIKit.UILabel RecipeTitle { get; set; }

		[Outlet]
		UIKit.UIButton ServesButton { get; set; }

		[Outlet]
		UIKit.UIView ServesInfoView { get; set; }

		[Outlet]
		UIKit.UILabel ServesText { get; set; }

		[Outlet]
		UIKit.UILabel ServesTextTitle { get; set; }

		[Outlet]
		UIKit.UIImageView Snowflake { get; set; }

		[Outlet]
		UIKit.UITextView TipBox { get; set; }

		[Outlet]
		UIKit.UIButton TipButton { get; set; }

		[Action ("AdjustServes:")]
		partial void AdjustServes (Foundation.NSObject sender);

		[Action ("CloseNutrition:")]
		partial void CloseNutrition (Foundation.NSObject sender);

		[Action ("CloseNutritionAndTip:")]
		partial void CloseNutritionAndTip (Foundation.NSObject sender);

		[Action ("CookPageTip:")]
		partial void CookPageTip (Foundation.NSObject sender);

		[Action ("DecreaseFontSize:")]
		partial void DecreaseFontSize (Foundation.NSObject sender);

		[Action ("IncreaseFontSize:")]
		partial void IncreaseFontSize (Foundation.NSObject sender);

		[Action ("NextRecipe:")]
		partial void NextRecipe (Foundation.NSObject sender);

		[Action ("OpemPBCWebPage:")]
		partial void OpemPBCWebPage (Foundation.NSObject sender);

		[Action ("PrevRecipe:")]
		partial void PrevRecipe (Foundation.NSObject sender);

		[Action ("RecipeTrashCan:")]
		partial void RecipeTrashCan (Foundation.NSObject sender);

		[Action ("SwitchToBuyPage:")]
		partial void SwitchToBuyPage (Foundation.NSObject sender);

		[Action ("SwitchToPlanPage:")]
		partial void SwitchToPlanPage (Foundation.NSObject sender);

		[Action ("ToggleFavourite:")]
		partial void ToggleFavourite (Foundation.NSObject sender);

		[Action ("ToggleNutrition:")]
		partial void ToggleNutrition (Foundation.NSObject sender);
		
		void ReleaseDesignerOutlets ()
		{
			if (CaptureTouch != null) {
				CaptureTouch.Dispose ();
				CaptureTouch = null;
			}

			if (CarbsLabel != null) {
				CarbsLabel.Dispose ();
				CarbsLabel = null;
			}

			if (CarbsValue != null) {
				CarbsValue.Dispose ();
				CarbsValue = null;
			}

			if (CookText != null) {
				CookText.Dispose ();
				CookText = null;
			}

			if (CookTextTitle != null) {
				CookTextTitle.Dispose ();
				CookTextTitle = null;
			}

			if (EnergyLabel != null) {
				EnergyLabel.Dispose ();
				EnergyLabel = null;
			}

			if (EnergyValueCal != null) {
				EnergyValueCal.Dispose ();
				EnergyValueCal = null;
			}

			if (EnergyValueKj != null) {
				EnergyValueKj.Dispose ();
				EnergyValueKj = null;
			}

			if (FatLabel != null) {
				FatLabel.Dispose ();
				FatLabel = null;
			}

			if (FatValue != null) {
				FatValue.Dispose ();
				FatValue = null;
			}

			if (Favourite != null) {
				Favourite.Dispose ();
				Favourite = null;
			}

			if (FibreLabel != null) {
				FibreLabel.Dispose ();
				FibreLabel = null;
			}

			if (FibreValue != null) {
				FibreValue.Dispose ();
				FibreValue = null;
			}

			if (IngredientsList != null) {
				IngredientsList.Dispose ();
				IngredientsList = null;
			}

			if (IngredientsListTitle != null) {
				IngredientsListTitle.Dispose ();
				IngredientsListTitle = null;
			}

			if (MethodTable != null) {
				MethodTable.Dispose ();
				MethodTable = null;
			}

			if (NutritionButton != null) {
				NutritionButton.Dispose ();
				NutritionButton = null;
			}

			if (NutritionWindow != null) {
				NutritionWindow.Dispose ();
				NutritionWindow = null;
			}

			if (PrepText != null) {
				PrepText.Dispose ();
				PrepText = null;
			}

			if (PrepTextTitle != null) {
				PrepTextTitle.Dispose ();
				PrepTextTitle = null;
			}

			if (ProteinLabel != null) {
				ProteinLabel.Dispose ();
				ProteinLabel = null;
			}

			if (ProteinValue != null) {
				ProteinValue.Dispose ();
				ProteinValue = null;
			}

			if (RecipeImage != null) {
				RecipeImage.Dispose ();
				RecipeImage = null;
			}

			if (RecipeTitle != null) {
				RecipeTitle.Dispose ();
				RecipeTitle = null;
			}

			if (ServesButton != null) {
				ServesButton.Dispose ();
				ServesButton = null;
			}

			if (ServesInfoView != null) {
				ServesInfoView.Dispose ();
				ServesInfoView = null;
			}

			if (ServesText != null) {
				ServesText.Dispose ();
				ServesText = null;
			}

			if (ServesTextTitle != null) {
				ServesTextTitle.Dispose ();
				ServesTextTitle = null;
			}

			if (Snowflake != null) {
				Snowflake.Dispose ();
				Snowflake = null;
			}

			if (TipBox != null) {
				TipBox.Dispose ();
				TipBox = null;
			}

			if (TipButton != null) {
				TipButton.Dispose ();
				TipButton = null;
			}
		}
	}
}
