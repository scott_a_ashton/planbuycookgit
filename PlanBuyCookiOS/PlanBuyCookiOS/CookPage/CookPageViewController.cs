using System;
using CoreGraphics;
using Foundation;
using UIKit;
using Parse;

namespace PlanBuyCookiOS
{
	public partial class CookPageViewController : UIViewController
	{
        //Handling gesture recognisers on the Cook Page
        public class GestureDelegate : UIGestureRecognizerDelegate
        {
            CookPageViewController controller;

            public GestureDelegate (CookPageViewController controller)
            {
                this.controller = controller;
            }

            public override bool ShouldBegin(UIGestureRecognizer recognizer)
            {
                if (recognizer is UILongPressGestureRecognizer)
                    return true;
                return false;
            }
            public override bool ShouldReceiveTouch(UIGestureRecognizer aRecogniser, UITouch aTouch)
            {
                if (aRecogniser is UILongPressGestureRecognizer)
                    return true;
                return false;
            }

            // Ensure that the pinch, pan and rotate gestures are all recognized simultaneously
            public override bool ShouldRecognizeSimultaneously (UIGestureRecognizer gestureRecognizer, UIGestureRecognizer otherGestureRecognizer)
            {
                //TODO: Check that the drag is on one of the recipe icons

                // if the gesture recognizers views differ, don't recognize
                if (gestureRecognizer.View != otherGestureRecognizer.View)
                    return false;

                if (!(gestureRecognizer is UILongPressGestureRecognizer))
                    return false;

                return false;
            }
        }

		public CookPageViewController () : base ("CookPageViewController", null)
		{
		}

		public override void DidReceiveMemoryWarning ()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning ();
			
			// Release any cached data, images, etc that aren't in use.
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			
			// Perform any additional setup after loading the view, typically from a nib.
			(View as CookPageView).Initialise();
		}

		public void InitialiseRecipe()
		{

		}

		public void UpdateRecipe(int recipeIndex)
		{
            CookPageMethodViewSource.UpdateLiveTimers();
			//Recipe index is -1 when the program is first run
			if (recipeIndex == -1)
                (View as CookPageView).UpdateRecipe (RecipeManager.Instance.GetRecipe (0), AppDelegate.Settings.Serves);
			else
                (View as CookPageView).UpdateRecipe (RecipeManager.Instance.GetRecipe (recipeIndex), AppDelegate.Settings.Serves);
		}

        public void UpdateRecipe(CalenderDay.Meal meal)
        {
            (View as CookPageView).UpdateRecipe (meal);
        }

		public void UpdateRecipe(IRecipeImport recipe)
		{
			(View as CookPageView).UpdateRecipe (recipe);
		}
         
	}
}

