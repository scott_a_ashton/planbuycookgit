using System;
using CoreGraphics;
using Foundation;
using UIKit;
using AudioToolbox;


namespace PlanBuyCookiOS
{
    public class MethodTimer
    {
        SystemSound mAlarmSound;



        public SystemSound AlarmSound
        {
            get
            {
                if (mAlarmSound == null)
                {
                    //mAlarmSound = SystemSound.FromFile("PrestoRing.aif");
                    mAlarmSound = SystemSound.FromFile("Beacon.aif");
                    mAlarmSound.AddSystemSoundCompletion(() => mAlarmSound.PlayAlertSound(), null);
                }
                return mAlarmSound;
            }
        }

        public MethodTimer(TimeSpan duration)
        {
            mResetTime = duration;
            mTimeLeft = duration;

            if (duration.Ticks > 0)
                mbIsValid = true;

            mbIsStarted = false;
            mbPaused = false;
            mbAlarm = false;

            mbAlarmTextFlash = false;
        }

        public void Start(string recipeName, UITableView table, int rowIndex)
        {
            mRecipeName = recipeName;
            mbIsStarted = true;
            mTableView = table;
            mRow = rowIndex;
            mbPaused = false;

            CreateLocalNotification();

            mTimerUpdate = NSTimer.CreateRepeatingScheduledTimer(1.0f, (NSTimer nst) => this.Update());

            Update();
        }

        public void TogglePause()
        {
            if (!mbAlarm && mbIsStarted)
            {
                if (!mbPaused)
                {
                    if (mTimerUpdate != null)
                        mTimerUpdate.Invalidate();

                    if (mTimeLeft.Ticks > 0)
                        mbPaused = true;
                }
                else
                {
                    mbPaused = false;
                    Start(mRecipeName, mTableView, mRow);
                }
            }
            else
            {
                ResetAlarm();
            }
        }

        public void Reset()
        {
            mbIsStarted = false;
            mbPaused = false;
            mTimeLeft = mResetTime;

            if (mNotification != null)
                CancelLocalNotification();

            if (mTimerUpdate != null)
            {
                mTimerUpdate.Invalidate();
                mTimerUpdate = null;
            }

            ResetAlarm();
        }

        public void ResetAlarm()
        {
            mbAlarm = false;
            mbAlarmTextFlash = false;
            AlarmSound.Close();
            mAlarmSound = null;

            if (mAlarmTextFlash != null)
            {
                mAlarmTextFlash.Invalidate();
                mAlarmTextFlash = null;
            }
        }

        public bool Alarm
        {
            get
            {
                return mbAlarm;
            }

            set
            {
                if (mbAlarm && !value)
                {
                    StopAlarm();
                }
                else if (!mbAlarm && value)
                {
                    StartAlarm();
                }
                mbAlarm = value;
            }
        }

        public void Update()
        {
            if (AppDelegate.IsOnCookPage())
            {
                NSIndexPath[] rows = new NSIndexPath[]{ NSIndexPath.FromRowSection(mRow, 0) };
                mTableView.ReloadRows(rows, UITableViewRowAnimation.None);
            }

            if (mNotification != null)
            {
                Console.WriteLine("Tick " + mTimeLeft.Seconds);
				mTimeLeft = mNotification.FireDate.NSDateToDateTime() - DateTime.UtcNow;
            }

            if (mTimeLeft.Ticks <= 0)
            {
                mTimeLeft = new TimeSpan(0);
                StartAlarm();
            }
        }

        public void AddTime(TimeSpan addTime)
        {
            if (mbIsValid)
            {
                mTimeLeft = mTimeLeft.Add(addTime);
				mNotification.FireDate = DateTime.Now.Add(mTimeLeft).DateTimeToNSDate();
            }
        }

        public override string ToString()
        {
            if (!mbAlarmTextFlash)
            {
                if (mTimeLeft.Hours > 0)
                    return String.Format("{0:D1}:{1:D2}:{2:D2}", mTimeLeft.Hours, mTimeLeft.Minutes, mTimeLeft.Seconds);
                else
                    return String.Format("{0:D2}:{1:D2}", mTimeLeft.Minutes, mTimeLeft.Seconds);
            }
            else
            {
                return "";
            }
        }

        public void StartAlarm()
        {
            AlarmSound.PlaySystemSound();
            mbAlarm = true;
			mAlarmTextFlash = NSTimer.CreateRepeatingScheduledTimer(0.1, (NSTimer nst) => FlashText() );

            if (mTimerUpdate != null)
            {
                mTimerUpdate.Invalidate();
            }

            if (HasNotification())
                CancelLocalNotification();
        }

        void FlashText()
        {
            mbAlarmTextFlash = !mbAlarmTextFlash;

            if (AppDelegate.IsOnCookPage())
            {
                NSIndexPath[] rows = new NSIndexPath[]{ NSIndexPath.FromRowSection(mRow, 0) };
                mTableView.ReloadRows(rows, UITableViewRowAnimation.None);
            }
        }

        public void StopAlarm()
        {
        }

        public void CreateLocalNotification()
        {
            Console.WriteLine("Create local notification");

            //---- create the notification
            var notification = new UILocalNotification();

            //---- set the fire date (the date time in which it will fire)
            notification.TimeZone = NSTimeZone.LocalTimeZone;
			notification.FireDate = DateTime.Now.Add(mTimeLeft).DateTimeToNSDate();

            //---- configure the alert stuff
            notification.AlertAction = "PlanBuyCook Timer";
            notification.AlertBody = string.Format("Timer for {0}, step {1}, has finished.", mRecipeName, mRow);

            //---- modify the badge
            notification.ApplicationIconBadgeNumber = 0;

            //---- set the sound to be the default sound
            notification.SoundName = UILocalNotification.DefaultSoundName;

            //---- schedule it
            UIApplication.SharedApplication.ScheduleLocalNotification(notification);

            mNotification = notification;
        }

        public void CancelLocalNotification()
        {
            Console.WriteLine("Cancel local notifcation");

            if (mNotification != null)
                UIApplication.SharedApplication.CancelLocalNotification(mNotification);

            mNotification = null;
        }

        public bool HasNotification()
        {
            return mNotification != null;
        }

        public string mRecipeName = "";
        public bool mbIsValid = false;
        public bool mbAlarm = false;
        public bool mbIsStarted = false;
        public bool mbPaused = false;
        public bool mbAlarmTextFlash = false;

        int mRow;
        UITableView mTableView;

        TimeSpan mResetTime;
        TimeSpan mTimeLeft;
        NSTimer mTimerUpdate;
        NSTimer mAlarmTextFlash;
        UILocalNotification mNotification;
    }
}

