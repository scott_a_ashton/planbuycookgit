using System;
using UIKit;
using Foundation;
using CoreGraphics;


[Register("GenericTextField")]
public partial class GenericTextField : UITextField
{
	public GenericTextField ()
	{
	}

	public GenericTextField(IntPtr handle) : base(handle)
	{
		Delegate = new GenericTextFieldDelegate();

	}
}

public class GenericTextFieldDelegate : UITextFieldDelegate
{
	//Gets rid of the keyboard
	public override bool ShouldReturn(UITextField textField)
	{
		// NOTE: Don't call the base implementation on a Model class
		// see http://docs.xamarin.com/guides/ios/application_fundamentals/delegates,_protocols,_and_events
		textField.ResignFirstResponder();

        return false;
	}

	public override bool ShouldEndEditing(UITextField textField)
	{
		// NOTE: Don't call the base implementation on a Model class
		// see http://docs.xamarin.com/guides/ios/application_fundamentals/delegates,_protocols,_and_events
		return true;
	}

	public override bool ShouldClear(UITextField textField)
	{
		return true;
	}

	public override bool ShouldBeginEditing(UITextField textField)
	{
		return true;
	}
}