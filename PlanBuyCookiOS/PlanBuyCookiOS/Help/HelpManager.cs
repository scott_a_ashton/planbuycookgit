using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using CoreGraphics;
using Foundation;
using UIKit;
using EventKit;
using MessageUI;
using Parse;


namespace PlanBuyCookiOS
{
	public class HelpManager
	{
		public enum HelpState
		{
			Null,
			InitialHelpMessages,
//            PlanBuyCookUsingTheseButtons,
			EnageWithMosaic,
//			HoldAndDragRecipesOntoPlanner,
			EnageWithCalendarMeal,
			TapOnLabelToEditText,
			Finished
		}

		public HelpManager ()
		{
		}

		static bool mbTutorialActive = true;
		static HelpState sState = HelpState.Null;
		static UIPopoverController sHelpPopoverController;
		static UIPopoverController sAltHelpPopoverController;

		public static bool Active
		{
            set { mbTutorialActive = value; }
			get { return mbTutorialActive; }
		}
		public static HelpState State
		{
			get { return sState; }
		}


		public static void Reset()
		{
			sState = HelpState.Null;

			AdvanceState ();
		}

		public static UIPopoverController HelpPopover
		{
			get
			{
				if (sHelpPopoverController == null)
				{
					sHelpPopoverController = new UIPopoverController(new SettingsHelpPopover());
					sHelpPopoverController.PopoverContentSize = sHelpPopoverController.ContentViewController.View.Frame.Size;
					sHelpPopoverController.BackgroundColor = UIColor.White;
					sHelpPopoverController.DidDismiss += (object sender, EventArgs e) => {HelpManager.AdvanceState();};
				}
				return sHelpPopoverController;
			}
		}

		public static UIPopoverController AltHelpPopover
		{
			get
			{
				if (sAltHelpPopoverController == null)
				{
					sAltHelpPopoverController = new UIPopoverController(new SettingsHelpPopover());
					sAltHelpPopoverController.PopoverContentSize = sAltHelpPopoverController.ContentViewController.View.Frame.Size;
					sAltHelpPopoverController.BackgroundColor = UIColor.White;
				}
				return sAltHelpPopoverController;
			}
		}
		public static void AdvanceState()
		{
			sState = (HelpState)(((int)sState) + 1);

			switch (State)
			{
			    case HelpState.InitialHelpMessages:
					(HelpPopover.ContentViewController as SettingsHelpPopover).HelpLabel.Text = "Tap to bring up SETTINGS and link to Tutorial";
					(AltHelpPopover.ContentViewController as SettingsHelpPopover).HelpLabel.Text = "PLAN, BUY, and COOK using these buttons";
					break;

				case HelpState.EnageWithMosaic:
					(HelpPopover.ContentViewController as SettingsHelpPopover).HelpLabel.Text = "DOUBLE TAP to view recipe";
					(AltHelpPopover.ContentViewController as SettingsHelpPopover).HelpLabel.Text = "HOLD and DRAG recipe onto the meal planner left";
					break;

				case HelpState.EnageWithCalendarMeal:
					(HelpPopover.ContentViewController as SettingsHelpPopover).HelpLabel.Text = "DOUBLE TAP on meal image to view scaled recipe";
					(AltHelpPopover.ContentViewController as SettingsHelpPopover).HelpLabel.Text = "TAP on number to change number of serves";
					break;

				case HelpState.TapOnLabelToEditText:
					(HelpPopover.ContentViewController as SettingsHelpPopover).HelpLabel.Text = "TAP on label to edit text";
					break;

				case HelpState.Finished:
					mbTutorialActive = false;
					break;
			}
		}

		public static void PresentPopover(CGRect anchor, UIView containingFrame)
		{
			if (mbTutorialActive) 
				HelpPopover.PresentFromRect (anchor, containingFrame, UIPopoverArrowDirection.Up, false);
		}
		public static void PresentAltPopover(CGRect anchor, UIView containingFrame, UIPopoverArrowDirection arrowDirection = UIPopoverArrowDirection.Up)
		{
			if (mbTutorialActive) 
				AltHelpPopover.PresentFromRect (anchor, containingFrame, arrowDirection, false);
		}

		public static void PresentBothPopovers(CGRect anchor, UIView containingFrame)
		{
			if (mbTutorialActive) 
			{
				HelpPopover.PresentFromRect (anchor, containingFrame, UIPopoverArrowDirection.Up, false);
				AltHelpPopover.PresentFromRect (anchor, containingFrame, UIPopoverArrowDirection.Left, false);
			}
		}
	}
}

