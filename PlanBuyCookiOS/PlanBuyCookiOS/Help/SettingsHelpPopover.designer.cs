// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace PlanBuyCookiOS
{
	[Register ("SettingsHelpPopover")]
	partial class SettingsHelpPopover
	{
		[Outlet]
		UIKit.UILabel HelpStringLabel { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (HelpStringLabel != null) {
				HelpStringLabel.Dispose ();
				HelpStringLabel = null;
			}
		}
	}
}
