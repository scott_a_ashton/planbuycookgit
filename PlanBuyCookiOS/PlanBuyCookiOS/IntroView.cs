using System;
using CoreGraphics;
using Foundation;
using UIKit;
using AVFoundation;

namespace PlanBuyCookiOS
{
    [Register("IntroView")]
    public class IntroView : UIView
    {
        AVPlayer _player;
        AVPlayerLayer _playerLayer;
        AVAsset _asset;
        AVPlayerItem _playerItem;

        NSObject mNotificationHandle;
        Action<NSNotification> mOnMovieComplete;

        public IntroView ()
        {
        }

        public IntroView(IntPtr handle) : base(handle)
        {

        }

        public void PlayMovie(Action<NSNotification> onMovieComplete)
        {
            _asset = AVAsset.FromUrl (NSUrl.FromFilename ("PBC-Intro_ipad.m4v"));
            _playerItem = new AVPlayerItem (_asset);   

            _player = new AVPlayer (_playerItem);  
            _player.ActionAtItemEnd = AVPlayerActionAtItemEnd.None;
            _playerLayer = AVPlayerLayer.FromPlayer (_player);
            Frame = new CGRect(0, 0, 1024, 768);
            _playerLayer.Frame = Frame;

            mOnMovieComplete = onMovieComplete;
            mNotificationHandle = NSNotificationCenter.DefaultCenter.AddObserver(AVPlayerItem.DidPlayToEndTimeNotification, NotifyAndDeregister);

            Layer.AddSublayer (_playerLayer);
            _player.Play();

       }

        public void NotifyAndDeregister(NSNotification notify)
        {
            if (mOnMovieComplete!=null)
                mOnMovieComplete(notify);

            if (mNotificationHandle!=null)
                NSNotificationCenter.DefaultCenter.RemoveObserver(mNotificationHandle);
        }
    }
}

