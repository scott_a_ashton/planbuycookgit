using System;
using CoreGraphics;
using System.Collections.Generic;
using System.Linq;
using Foundation;
using UIKit;

public class LoadingOverlay : UIView {
    // control declarations
    UIActivityIndicatorView activitySpinner;
    UILabel loadingLabel;

    public LoadingOverlay (CGRect frame, bool bLandscape) : base (frame)
    {
        // configurable bits
        AutoresizingMask = UIViewAutoresizing.FlexibleDimensions;

        nfloat centerX;
        nfloat centerY;

        if (!bLandscape)
        {
            // derive the center x and y
            centerX = Frame.Width / 2;
            centerY = Frame.Height - Frame.Height / 6;
        }
        else
        {
            centerX = Frame.Height / 2;
            centerY = Frame.Width - Frame.Width / 6;
        }
            
        // create the activity spinner, center it horizontall and put it 5 points above center x
        activitySpinner = new UIActivityIndicatorView(UIActivityIndicatorViewStyle.WhiteLarge);
        activitySpinner.Frame = new CGRect (
            centerX - (activitySpinner.Frame.Width / 2) ,
            centerY - activitySpinner.Frame.Height - 20 ,
            activitySpinner.Frame.Width ,
            activitySpinner.Frame.Height);
        activitySpinner.AutoresizingMask = UIViewAutoresizing.FlexibleMargins;
        AddSubview (activitySpinner);
        activitySpinner.StartAnimating ();
    }

    /// <summary>
    /// Fades out the control and then removes it from the super view
    /// </summary>
    public void Hide ()
    {
        UIView.Animate (
            0.5, // duration
            () => { Alpha = 0; },
            () => { RemoveFromSuperview(); }
        );
    }
};