using System;
using System.Linq;
using System.Collections.Generic;
using EventKit;
using Foundation;
using UIKit;
using System.Threading.Tasks;
using System.Threading;
using ObjCRuntime;


namespace PlanBuyCookiOS
{
	public class PBCCalendar
	{
        static bool mbAccessToCalendars = false;
        public static readonly string cPlanBuyCookCalendarTitle = "PlanBuyCook Calendar";

		static EKEventStore mEventStore;
		static EKCalendar[] mCalendars;
        static EKEvent[] mEvents;

        static EKCalendar mPBCCalender;
        static string mPBCCalendarIdentifier="";

        static DateTime mCurrenDate;
        static DateTime mStartDate;
        static DateTime mEndDate;

        static List<CalenderDay> mDays = new List<CalenderDay>();

        //Number of days before an after the current date we can look at
        static int mDateRange;

		public static EKEventStore EventStore
		{
			get
			{
				return mEventStore;
			}
		}

        public static bool AccessToCalendars
        {
            set { mbAccessToCalendars = value; }
            get { return mbAccessToCalendars; }
        }

		public PBCCalendar ()
		{
		    mEventStore = new EKEventStore ( );

            mDateRange = 60;
            mStartDate = new DateTime(DateTime.Now.AddDays(-DateRange).Year, DateTime.Now.AddDays(-DateRange).Month, DateTime.Now.AddDays(-DateRange).Day);
            mCurrenDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
            mEndDate = new DateTime(DateTime.Now.AddDays(DateRange).Year, DateTime.Now.AddDays(DateRange).Month, DateTime.Now.AddDays(DateRange).Day);

            int numberOfDays = (mEndDate - mStartDate).Days;

            mDays = Enumerable.Range(0, numberOfDays).Select (i => new CalenderDay (mStartDate.AddDays((double)i))).ToList ();
		}


        //See here for an explanation : http://stackoverflow.com/questions/15706969/how-to-create-and-save-ekcalendar-on-ios-6
        public static void SetCalendarSource()
        {
            if (!mbAccessToCalendars)
                return;

            foreach (EKSource source in EventStore.Sources)
            {
                if (source.SourceType == EKSourceType.CalDav &&
                    source.Title.ToLower().Contains("icloud"))
                {
                    mPBCCalender.Source = source;
                    break;
                }
            }

            if (mPBCCalender.Source == null)
            {
                foreach (EKSource source in EventStore.Sources)
                {
                    if (source.SourceType == EKSourceType.Local)
                    {
                        mPBCCalender.Source = source;
                        break;
                    }
                }
            }
        }

        public static DateTime StartDate
        {
            get { return mStartDate; }
        }

        public static DateTime CurrentDate
        {
            get { return mCurrenDate; }
        }

        public static DateTime EndDate
        {
            get { return mEndDate; }
        }

        public static int DateRange
        {
            get { return mDateRange; }
        }
        public static List<CalenderDay> Days
        {
            get { return mDays; }
        }

        public static EKCalendar GetPBCCalendar()
        {
            if (mbAccessToCalendars == false)
                return null;

            if (mPBCCalender == null)
            {
                try
                {
                    mPBCCalender = EKCalendar.Create(EKEntityType.Event, EventStore);
                    mPBCCalender.Title = cPlanBuyCookCalendarTitle;

                    //Hook calendar up to local source
                    SetCalendarSource();

                    //Save the new calendar back to the eventstore
                    NSError error;
                    bool success = mEventStore.SaveCalendar(mPBCCalender, true, out error);
                    if (success)
                    {
                        mPBCCalendarIdentifier = mPBCCalender.CalendarIdentifier;
                        Console.WriteLine("Successfully saved calendar " + mPBCCalendarIdentifier);
                        PBCCalendar.UpdateCalendar();

                        return mPBCCalender;
                    }
                    else
                    {
                        Console.Error.WriteLine(error.ToString());
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
            }
            return mPBCCalender;
        }



        public static void EnumerateCalendars()
		{
            if (!mbAccessToCalendars)
                return;

            mCalendars = mEventStore.GetCalendars (EKEntityType.Event);

            mPBCCalender = mCalendars.Where(cal => cal.Title == cPlanBuyCookCalendarTitle).FirstOrDefault();
		}

        public static void EnumerateAllEvents()
        {
            if (!mbAccessToCalendars)
                return;

			NSPredicate query = mEventStore.PredicateForEvents ( StartDate.DateTimeToNSDate(), EndDate.DateTimeToNSDate(), new EKCalendar[]{mPBCCalender} );
            mEvents = mEventStore.EventsMatching(query);

            if (mEvents == null)
                mEvents = new EKEvent[0];
        }

        readonly static string cShoppedString = "(shopped)";

        public static async Task SaveEventToCalendar(EKEvent newEvent, Action onComplete)
        {
            if (!mbAccessToCalendars)
                return;

            NSError error;
            if (!mEventStore.SaveEvent(newEvent, EKSpan.ThisEvent, true, out error))
                Console.Error.WriteLine(error.ToString());
            else
            {
                if (onComplete!=null)
                    onComplete();
            }
        }

        public static async Task RemoveEventFromCalendar(EKEvent removeEvent)
        {
            if (!mbAccessToCalendars)
                return;

            NSError error;
            if (!mEventStore.RemoveEvent(removeEvent, EKSpan.ThisEvent, true, out error))
                Console.Error.WriteLine(error.ToString());
        }

        static Object mLock = new object();

        static bool mbWritingToCalendar = false;
        public static bool WritingToCalendar
        {
            set
            {
                lock(mLock)
                {
                    mbWritingToCalendar = value;
                }
            }
            get
            {
                return mbWritingToCalendar;
            }
        }

        public static async Task PauseWhileWritingToCalendar()
        {
            if (!WritingToCalendar)
                WritingToCalendar = true;
            else
            {
                while (WritingToCalendar)
                {
                    await Task.Delay(32);
                }
                WritingToCalendar = true;
            }
        }

        public static async Task RemoveMeal(CalenderDay day, int mealIndex)
        {
            if (!mbAccessToCalendars)
                return;

            EKEvent ekEvent = null;
            CalenderDay.Meal meal = day.mMeals[mealIndex];

            if (meal.CalendarEventId != "")
                ekEvent = EventStore.EventFromIdentifier( meal.CalendarEventId );

            if (ekEvent != null)
            {
                await RemoveEventFromCalendar(ekEvent);
            }
        }

        public static async Task UpdateCalendar(CalenderDay day, int mealIndex)
        {
            if (!mbAccessToCalendars)
                return;
                
            EKEvent ekEvent = null;
            CalenderDay.Meal meal = day.mMeals[mealIndex];

            if (meal.CalendarEventId != "")
                ekEvent = EventStore.EventFromIdentifier( meal.CalendarEventId );

            if (meal.CalendarEventId == "" || (meal.CalendarEventId != "" && ekEvent == null))
                ekEvent = EKEvent.FromStore( EventStore );

            ekEvent.AllDay = true;
            ekEvent.Title = meal.Recipe.Name;
            ekEvent.Calendar = mPBCCalender;
			ekEvent.StartDate = (new DateTime(day.Date.Year, day.Date.Month, day.Date.Day, 12,0,0)).DateTimeToNSDate();
			ekEvent.EndDate = (new DateTime(day.Date.Year, day.Date.Month, day.Date.Day, 12,0,1)).DateTimeToNSDate();

            if (meal.Recipe is RecipeSpecialMeal)
            {
                ekEvent.Notes = (meal.Recipe as RecipeSpecialMeal).Description;

                await SaveEventToCalendar(ekEvent, () => meal.CalendarEventId = ekEvent.EventIdentifier);
            }
            else
            {
                //If this is a web recipe store the URL in the notes
                if (meal.Recipe is RecipeWeb)
                    ekEvent.Notes = meal.Recipe.Source;
                else
                    ekEvent.Notes = "Serves:" + meal.Serves;

                //Flag if this has been part of a shopping list
                if (meal.IsShoppedFor && !ekEvent.Notes.Contains(cShoppedString))
                    ekEvent.Notes += cShoppedString;

                await SaveEventToCalendar(ekEvent, () => meal.CalendarEventId = ekEvent.EventIdentifier);
            }
            WritingToCalendar = false;
        }


		public static async void PopulatePlannerFromCalendar()
        {
            if (!mbAccessToCalendars)
                return;

            foreach (EKEvent mealEvent in mEvents)
            {
				DateTime eventStart = mealEvent.EndDate.NSDateToDateTime();
                int dayIndex = eventStart.Subtract(mStartDate.ToUniversalTime()).Days;

                //First check if this is a special meal
                if (RecipeSpecialMeal.IsSpecialMealString(mealEvent.Title))
                {
                    mDays[dayIndex].AddRecipe(new RecipeSpecialMeal(mealEvent.Title, mealEvent.Notes));

                    mDays[dayIndex].mMeals.Last().CalendarEventId = mealEvent.EventIdentifier;
                }
                else
                {
                    //First check
                    string mealNotes = mealEvent.Notes;
                    bool bIsShoppedFor = mealEvent.Notes.Contains(cShoppedString);

                    if (bIsShoppedFor)
                        mealNotes = mealNotes.Replace(cShoppedString, "");

                    NSUrl mealUrl = NSUrl.FromString(mealNotes);

                    RecipeWeb.RecipeConstruct constructor = RecipeWeb.GetConstructor (mealUrl);

                    if (constructor != null) 
					{
                        RecipeWeb recipeWeb = constructor (mealNotes);

                        Console.WriteLine ("Background load " + mealNotes);

						if (await recipeWeb.LoadRecipeHtml ()) 
						{
							mDays [dayIndex].AddRecipe (recipeWeb);
                            mDays[dayIndex].mMeals.Last().IsShoppedFor = bIsShoppedFor;
                            mDays[dayIndex].mMeals.Last().CalendarEventId = mealEvent.EventIdentifier;

							if ((DateTime.UtcNow - mealEvent.EndDate.NSDateToDateTime()).Days < 8) {
								AppDelegate.PlanPageViewController.BackgroundRefreshCalendarView ();
							}
						}
						else
                            Console.WriteLine ("Failed to load " + mealNotes);
					} 
					else 
					{
						//This is a normal meal
						Task<IRecipeImport> recipe = RecipeManager.Instance.AsyncFindRecipe(mealEvent.Title);

						if (recipe.Result == null) {
							Console.Error.WriteLine ("Error reading recipe from Calendar:" + mealEvent.Title);
							continue;
						}

						mDays[dayIndex].AddRecipe(recipe.Result);
                        mDays[dayIndex].mMeals.Last().IsShoppedFor = bIsShoppedFor;
                        mDays[dayIndex].mMeals.Last().CalendarEventId = mealEvent.EventIdentifier;

						//Parse out the meal index and serves number
                        string[] servesString = mealNotes.Split(':');

						Console.WriteLine(recipe.Result.Name + eventStart.ToString() + " " + DateTime.Now);
						if (servesString.Count() >= 2)
						{
							int serves = 0;
							if (!int.TryParse(servesString[1], out serves))
								mDays[dayIndex].mMeals[mDays[dayIndex].MealCount-1].Serves = mDays[dayIndex].mMeals[mDays[dayIndex].MealCount-1].Recipe.Serves;
							else
								mDays[dayIndex].mMeals[mDays[dayIndex].MealCount-1].Serves = serves;
						}
						else
						{
							mDays[dayIndex].mMeals[mDays[dayIndex].MealCount-1].Serves = mDays[dayIndex].mMeals[mDays[dayIndex].MealCount-1].Recipe.Serves;
						}

						if ((DateTime.UtcNow - mealEvent.EndDate.NSDateToDateTime()).Days < 8) {
                            AppDelegate.PlanPageViewController.BackgroundRefreshCalendarView ();
                        }
					}
                }
            }
        }
            
        public static async void UpdateCalendar()
        {
            if (!mbAccessToCalendars)
                return;

            await PauseWhileWritingToCalendar();

            NSError error;
            if (!mEventStore.Commit(out error))
                Console.WriteLine(error.ToString());

            WritingToCalendar = false;
        }
	}
}

