using System;
using System.Collections.Generic;

namespace PlanBuyCookiOS
{
	public class CalenderDay
	{
		DateTime	mDate;

        public class Meal
        {
            int mServes = 1;
            IRecipeImport mRecipe;

            public Meal(IRecipeImport recipe, bool bShoppedFor = false) 
            {
                Recipe = recipe;
                Serves = AppDelegate.Settings.Serves;
                IsShoppedFor = bShoppedFor;
                CalendarEventId = "";
            }

            public int Serves
            {
                set;
                get;
            }

            public bool IsShoppedFor
            {
                set;
                get;
            }

            //Unique iCal Event Id
            public string CalendarEventId
            {
                set;
                get;
            }

            public IRecipeImport Recipe
            {
                set;
                get;
            }

            float ServesMultiplier
            {
                get { return (float)Serves / (float)Recipe.Serves; }
            }
        }

        public List<Meal> mMeals = new List<Meal>();

		public DateTime Date
		{
			get { return mDate; }
		}

		public int MealCount
		{
            get { return mMeals.Count; }
		}

		public CalenderDay (DateTime date)
		{
            mDate = new DateTime(date.Year, date.Month, date.Day);
		}
			
		public void AddRecipe(IRecipeImport recipe) 
		{
            if (mMeals.Count >= 3)
                return;

            mMeals.Add(new Meal(recipe));
		}

		public void RemoveRecipe(int index) 
		{
            mMeals.RemoveAt(index);
		}
	}
}

