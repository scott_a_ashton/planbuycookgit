using System;
using System.Collections.Generic;
using CoreGraphics;
using Foundation;
using UIKit;
using CoreData;


namespace PlanBuyCookiOS
{
	public class DropTarget
	{
		public PlanPagePlannerViewCell PlannerCell;
		public uint RecipeIndex;

		public UIButton Image;
		public UILabel Label;

		public CGPoint PosInMainView;

		public DropTarget (uint mealIndex, UIButton image, UILabel label, PlanPagePlannerViewCell plannerCell, UIView transformToView)
		{
			RecipeIndex = mealIndex;
			Image = image;
			Label = label;
			PlannerCell = plannerCell;

			PosInMainView = plannerCell.ConvertPointToView (image.Center, transformToView);
		}
	}
}

