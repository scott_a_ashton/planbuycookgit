using System;
using CoreGraphics;
using Foundation;
using UIKit;

namespace PlanBuyCookiOS
{
    public partial class EnterURLPopover : UIViewController
    {
		public delegate void ActionOnUrlReceipt(string url);
		public static ActionOnUrlReceipt sCreateWebRecipeOnEnterUrl;

        public EnterURLPopover() : base("EnterURLPopover", null)
        {
        }

        public override void DidReceiveMemoryWarning()
        {
            // Releases the view if it doesn't have a superview.
            base.DidReceiveMemoryWarning();
			
            // Release any cached data, images, etc that aren't in use.
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
			
            // Perform any additional setup after loading the view, typically from a nib.
        }

        public UITextField URLTextField
        {
            get { return WebRecipeURLTextField; }
        }

        partial void ViewRecipeOnCookPage(NSObject sender)
        {
			if (sCreateWebRecipeOnEnterUrl != null)
			{
				sCreateWebRecipeOnEnterUrl(URLTextField.Text);

				//PlanPagePlannerViewCell.sCreateWebRecipeOnEnterUrl = null;   
			}
        }
    }
}

