// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace PlanBuyCookiOS
{
	[Register ("EnterURLPopover")]
	partial class EnterURLPopover
	{
		[Outlet]
		UIKit.UIButton CookPageButton { get; set; }

		[Outlet]
		UIKit.UITextField WebRecipeURLTextField { get; set; }

		[Action ("ViewRecipeOnCookPage:")]
		partial void ViewRecipeOnCookPage (Foundation.NSObject sender);
		
		void ReleaseDesignerOutlets ()
		{
			if (CookPageButton != null) {
				CookPageButton.Dispose ();
				CookPageButton = null;
			}

			if (WebRecipeURLTextField != null) {
				WebRecipeURLTextField.Dispose ();
				WebRecipeURLTextField = null;
			}
		}
	}
}
