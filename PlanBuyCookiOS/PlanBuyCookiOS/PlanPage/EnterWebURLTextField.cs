using System;
using UIKit;
using Foundation;
using CoreGraphics;

namespace PlanBuyCookiOS
{
    [Register("EnterWebURLTextField")]
    public partial class EnterWebURLTextField : UITextField
    {
        UIImageView mWebIcon;

        public EnterWebURLTextField ()
        {
        }

        public EnterWebURLTextField(IntPtr handle) : base(handle)
        {
            Delegate = new WebURLDelegate();

            mWebIcon = new UIImageView(UIImage.FromFile("WebURL.png"));
            mWebIcon.Frame = new CGRect(0, 0, 16, 16);
            mWebIcon.Alpha = 0.3f;
            LeftView = mWebIcon;
            LeftViewMode = UITextFieldViewMode.Always;
        }

        public override CGRect LeftViewRect(CGRect forBounds)
        {
            CGRect baseRect = base.LeftViewRect(forBounds);
            return new CGRect(baseRect.Location.X + 5, baseRect.Location.Y, baseRect.Width, baseRect.Height);
        }
    }

    public class WebURLDelegate : UITextFieldDelegate
    {
        //Gets rid of the keyboard
        public override bool ShouldReturn(UITextField textField)
        {
            // NOTE: Don't call the base implementation on a Model class
            // see http://docs.xamarin.com/guides/ios/application_fundamentals/delegates,_protocols,_and_events
            textField.ResignFirstResponder();
            return true;
        }

        public override bool ShouldEndEditing(UITextField textField)
        {
            // NOTE: Don't call the base implementation on a Model class
            // see http://docs.xamarin.com/guides/ios/application_fundamentals/delegates,_protocols,_and_events
			if (EnterURLPopover.sCreateWebRecipeOnEnterUrl != null) {
				EnterURLPopover.sCreateWebRecipeOnEnterUrl (textField.Text);
			}

            return true;
        }

        public override bool ShouldClear(UITextField textField)
        {
            return true;
        }

        public override bool ShouldBeginEditing(UITextField textField)
        {
            return true;
        }
    }
}
