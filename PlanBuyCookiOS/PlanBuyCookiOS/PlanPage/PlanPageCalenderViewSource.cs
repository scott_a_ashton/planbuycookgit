using System;
using System.Collections.Generic;
using System.Linq;
using UIKit;
using Foundation;

namespace PlanBuyCookiOS
{

	public class PlanPageCalenderViewSource : UITableViewSource
	{
		string cellIdentifier = "PlanPageCalenderViewCell";

		int mSelectedRow = 0;
        public static bool mbIsScrollingToRow = false;

		public int SelectedRow
		{
			get {return mSelectedRow;}
		}

        //For now map this onto the PBCCalendar days
        public List<CalenderDay> Days
        {
            get{ return PBCCalendar.Days; }
        }

		public PlanPageCalenderViewSource()
		{
		} 
			
		public override nint RowsInSection (UITableView tableview, nint section)
		{
			return Days.Count();
		}

		public override nint NumberOfSections (UITableView tableView)
		{
			// TODO: return the actual number of sections
			return 1;
		}
            
        DateTime Today
        {
            get
            {
                DateTime today = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0,0,0);
                return today;
            }
        }
		//		public override string TitleForHeader (UITableView tableView, int section)
		//		{
		//			return "Header";
		//		}

		//		public override string TitleForFooter (UITableView tableView, int section)
		//		{
		//			return "Footer";
		//		}

		public override UITableViewCell GetCell (UITableView tableView, NSIndexPath indexPath)
		{
			PlanPagePlannerViewCell cell = (PlanPagePlannerViewCell)tableView.DequeueReusableCell ("PlanPagePlannerViewCell");

			if (cell == null) {
				cell = PlanPagePlannerViewCell.Create (this, tableView, indexPath.Row, Days[indexPath.Row]); 
			} else {
				cell.Initialise (this, tableView, indexPath.Row, Days[indexPath.Row]);
			}

            if (Days[indexPath.Row].Date < Today)
                cell.BackgroundColor = new UIColor(0.05f,0.05f,0.05f,0.05f);
            else
                cell.BackgroundColor = UIColor.White;

			return cell;
		}


        public static bool IsScrollingToRow
        {
            get
            {
                return mbIsScrollingToRow;
            }
        }

        public static void ScrollToRowStarted()
        {
            mbIsScrollingToRow = true;
        }

        public override void ScrollAnimationEnded(UIScrollView scrollView)
        {
            mbIsScrollingToRow = false;
        }

		public override nfloat GetHeightForRow (UITableView tableView, NSIndexPath indexPath)
		{
			if (Days [indexPath.Row].MealCount == 0 )
				return 109.0f;

			if (Days [indexPath.Row].MealCount == 1) 
			{
				if (mSelectedRow == indexPath.Row)
					return 174.0f;
				else
					return 109.0f;
			} 
			else if (Days [indexPath.Row].MealCount >= 2) 
			{
				if (Days [indexPath.Row].MealCount > 2)
					return 243.0f;
				else 
				{
					if (mSelectedRow == indexPath.Row)
						return 243.0f;
					else
						return 174.0f;
				}
			}
			return 109f;
		}
			
		public override void RowDeselected (UITableView tableView, NSIndexPath indexPath)
		{
			// NOTE: Don't call the base implementation on a Model class
			// see http://docs.xamarin.com/guides/ios/application_fundamentals/delegates,_protocols,_and_events
			Console.WriteLine ("Row deselected" + indexPath.Row);
		}
		public override void RowSelected (UITableView tableView, NSIndexPath indexPath)
		{
			int oldRow = mSelectedRow;
			mSelectedRow = indexPath.Row;

			//if (mDays [indexPath.Row].RecipeCount > 0)
			RefreshSelectedRow(tableView, mSelectedRow, oldRow);
		}

		public void SelectRow(UITableView tableView, int row)
		{
			int oldRow = mSelectedRow;
			mSelectedRow = row;

			tableView.SelectRow(NSIndexPath.FromRowSection (row, 0), true, UITableViewScrollPosition.None);
			RefreshSelectedRow (tableView, mSelectedRow, oldRow);
		}

		//Refresh 2 rows, the selected and the deselected
		public void RefreshSelectedRow(UITableView tableView, int selectedRow, int oldSelectedRow)
		{
			NSIndexPath ipSelectedRow = NSIndexPath.FromRowSection (selectedRow, 0);
			NSIndexPath ipDeselectedRow = NSIndexPath.FromRowSection (oldSelectedRow, 0);

			if (selectedRow != oldSelectedRow) 
			{
				NSIndexPath[] rows;

				if (ipDeselectedRow.Row < ipSelectedRow.Row)
					rows = new NSIndexPath[]{ ipDeselectedRow, ipSelectedRow};
				else
					rows = new NSIndexPath[]{ ipSelectedRow, ipDeselectedRow };

				tableView.ReloadRows (rows, UITableViewRowAnimation.Automatic);
			} 
			else 
			{
				NSIndexPath[] rows = new NSIndexPath[]{ ipSelectedRow };
				tableView.ReloadRows (rows, UITableViewRowAnimation.Automatic);
			}
		}

		public void AddRemoveRecipe(UITableView tableView, int row)
		{
			SelectRow(tableView, row);
		}

		public List<ShoppingListMeal> GetMealPlanForShoppingList()
		{
            var possibleDays = Days.Where (cd => cd.MealCount > 0 && ((cd.Date.Day==DateTime.Now.Day && cd.Date.Month==DateTime.Now.Month && cd.Date.Year==DateTime.Now.Year) || DateTime.Compare (cd.Date, DateTime.Now) > 0));
			var shoppingListMeals = new List<ShoppingListMeal> ();

			foreach(var day in possibleDays)
			{
                for (int recipeIndex = 0; recipeIndex < day.mMeals.Count; recipeIndex++)
				{
					if (!(day.mMeals [recipeIndex].Recipe is RecipeSpecialMeal)) {
						if (!day.mMeals [recipeIndex].IsShoppedFor)
							shoppingListMeals.Add (new ShoppingListMeal (day, recipeIndex));
					}
				}
			}
			return shoppingListMeals;
		}
	}
}

