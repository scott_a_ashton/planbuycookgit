using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.IO;
using System.Diagnostics;
using CoreGraphics;
using Foundation;
using UIKit;

namespace PlanBuyCookiOS
{
	[Register("PlanPageMosaicLayout")]
	public class PlanPageMosaicLayout : UICollectionViewLayout
	{
		//protected int mColumnCount = 1;
		protected int mCellCount = 1;
		string mFilename = "Data/pbc-box-pattern.xml";

		protected XDocument mXml = null;
		protected bool mbXmlLoaded = false;
		UIView mContainerView;

		List<CGRect> mLayoutRects = new List<CGRect>();

		//The rect for the entire layout
		CGRect mSinglePageLayoutRect;
		CGRect mGlobalLayoutRect;
		int mNumSpecialMealRects = 0;
		int mNumLayoutPages = 0;

		// Total is a function of the number of recipes and number of special meals
		int mTotalLayoutRects = 0; 

		public PlanPageMosaicLayout(IntPtr handle) : base (handle)
		{
			//RegisterClassForDecorationView (typeof(PlanPageMosaicRecipeListCell), PlanPageMosaicRecipeListCell.Key);
		}

		public PlanPageMosaicLayout (UIView containerView)
		{
			//RegisterClassForDecorationView (typeof(PlanPageMosaicRecipeListCell), PlanPageMosaicRecipeListCell.Key);
			mContainerView = containerView;
		}

		public override CGSize CollectionViewContentSize 
		{
			get 
			{
				CGSize size = mContainerView.Frame.Size;

                size.Height = mGlobalLayoutRect.Height;

				return size;
			}
		}

		public void LoadXml()
		{
			try
			{
				mXml = XDocument.Load (mFilename); 

				mbXmlLoaded = true;
			}
			catch (Exception e)
			{
				Console.WriteLine(e.Message);
			}
		}

		XDocument Xml
		{
			get
			{
				if (!mbXmlLoaded)
					LoadXml ();

				return mXml;
			}
		}

		public void ExtractLayout()
		{
			//First extract the screen dimensions
			try
			{
				{
					var polygons = Xml.Root.Elements ("polygon");

					mSinglePageLayoutRect = new CGRect(0,0,0,0);
					foreach (var poly in polygons) 
					{			
						string pointsList = poly.Attribute("points").Value;

						CGRect cellRect = ParseSVGPolygon(pointsList);

						if (IsSpecialMealRect(cellRect))
							mNumSpecialMealRects++;

						//Update to create a rect for the entire layout
						mSinglePageLayoutRect = CGRect.Union(mSinglePageLayoutRect, cellRect);

						mLayoutRects.Add(cellRect);
					}
					CalculateTotalLayoutRects();
				}
			}
			catch (Exception e) {
				Console.WriteLine(e.Message);
			}
		}


		void CalculateTotalLayoutRects()
		{
			int specialMealCount = 0;
			int recipeIndex = 0;

			mGlobalLayoutRect = CGRect.Empty;
			//At the end of this loop mTotalLayoutRects is the total number of layout rects which is the number of recipes plus special meals
			do
			{
				int page = (recipeIndex + specialMealCount) / mLayoutRects.Count;

				if (!IsSpecialMealRect(mLayoutRects[(recipeIndex + specialMealCount) % mLayoutRects.Count]))
					recipeIndex++;
				else 
					specialMealCount++;

				//Update the global rect
				CGRect rect = mLayoutRects[(recipeIndex + specialMealCount) % mLayoutRects.Count];
				rect.Location = new CGPoint(rect.Location.X, rect.Location.Y + mSinglePageLayoutRect.Height * page);

				mGlobalLayoutRect = CGRect.Union(mGlobalLayoutRect, rect);
				mTotalLayoutRects++;
			}
			while (RecipeManager.Instance.NumRecipes != recipeIndex);
		}


		CGRect ParseSVGPolygon(string pointsList)
		{
			char[] delimiter = new char[]{ ',' };
			string[] coordinates = pointsList.Split (delimiter, 8);

			CGPoint point1 = new CGPoint (float.Parse(coordinates [0]), float.Parse(coordinates [1]));
			CGPoint point2 = new CGPoint (float.Parse(coordinates [2]), float.Parse(coordinates [3]));
			CGPoint point3 = new CGPoint (float.Parse(coordinates [4]), float.Parse(coordinates [5]));
			CGPoint point4 = new CGPoint (float.Parse(coordinates [6]), float.Parse(coordinates [7]));

			float midX = (float)(point1.X + point2.X + point3.X + point4.X) / 4.0f;
			float midY = (float)(point1.Y + point2.Y + point3.Y + point4.Y) / 4.0f;

			double widthOver2 = Math.Abs (midX - point1.X);
			double heightOver2 = Math.Abs (midY - point1.Y);

			return new CGRect (new CGPoint(midX - widthOver2, midY - heightOver2), new CGSize(widthOver2*2, heightOver2*2));
		}

		public override void PrepareLayout ()
		{
			base.PrepareLayout ();
		}


		public override bool ShouldInvalidateLayoutForBoundsChange (CGRect newBounds)
		{
			return true;
		}

		public override UICollectionViewLayoutAttributes LayoutAttributesForItem (NSIndexPath path)
		{
			UICollectionViewLayoutAttributes attributes = UICollectionViewLayoutAttributes.CreateForCell (path);

			int rectIndex = (int)path.Item % mLayoutRects.Count;
			int layoutPage = (int)path.Item / mLayoutRects.Count;

			CGRect rect = mLayoutRects [rectIndex];
			attributes.Size = new CGSize(rect.Size.Width, rect.Size.Height);

			CGPoint rectOrigin = new CGPoint (rect.X, rect.Y + mSinglePageLayoutRect.Height * layoutPage);
			attributes.Center = new CGPoint((rectOrigin.X + rect.Width/2), (rectOrigin.Y + rect.Height/2));

			return attributes;
		}
			
		public override UICollectionViewLayoutAttributes LayoutAttributesForDecorationView (NSString kind, NSIndexPath indexPath)
		{
			return base.LayoutAttributesForDecorationView (kind, indexPath);
		}

		bool IsSpecialMealRect (CGRect rect)
		{
			return (rect.Width == rect.Height && rect.Width < 380.0f);
		}

		public override UICollectionViewLayoutAttributes[] LayoutAttributesForElementsInRect (CGRect rect)
		{
			//Create enough rects for all the recipes in memory at the moment.
			var attributes = new UICollectionViewLayoutAttributes [mTotalLayoutRects];

			for (int i = 0; i < mTotalLayoutRects; i++) 
			{
				NSIndexPath indexPath = NSIndexPath.FromItemSection (i, 0);
				attributes [i] = LayoutAttributesForItem (indexPath);
			}

			//var decorationAttribs = UICollectionViewLayoutAttributes.CreateForDecorationView (myDecorationViewId, NSIndexPath.FromItemSection (0, 0));
			//decorationAttribs.Size = CollectionView.Frame.Size;
			//decorationAttribs.Center = CollectionView.Center;
			//decorationAttribs.ZIndex = -1;
			//attributes [cellCount] = decorationAttribs;

			return attributes;
		}

		public int NumRectsInLayout()
		{
			return mTotalLayoutRects;
		}

	}
}
