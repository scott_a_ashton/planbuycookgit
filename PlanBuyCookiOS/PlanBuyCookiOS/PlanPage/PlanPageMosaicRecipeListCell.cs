using System;
using System.Linq;
using System.Collections.Generic;
using CoreGraphics;
using Foundation;
using UIKit;

namespace PlanBuyCookiOS
{
	public partial class PlanPageMosaicRecipeListCell : UICollectionViewCell
	{
		public static readonly UINib Nib = UINib.FromName ("PlanPageMosaicRecipeListCell", NSBundle.MainBundle);
		public static readonly NSString Key = new NSString ("PlanPageMosaicRecipeListCell");
		bool mbAddedGestureRecognizers = false;

		static List<DropTarget> mDropTargets = new List<DropTarget>();
		static bool mbDragging = false;
		static bool mbTouchAndHold = false;

		static UIImage mCircleImageMask;
		static UIImage mMaskedRecipeImage;
		static UIImageView mDraggableImageView;
		static UICircleOutlineView mCircleOutline;
		static PlanPageMosaicRecipeListController mMosaicController;
		public const float DraggableRecipeImageRadius = 32.0f;
        public const float cRecipeTitleFontSize = 15.0f;


		public const float cResizePortraitsTo = 256f;

		class GestureDelegate : UIGestureRecognizerDelegate
		{
			PlanPageMosaicRecipeListController controller;

			public GestureDelegate (PlanPageMosaicRecipeListController controller)
			{
				this.controller = controller;
			}

			public override bool ShouldReceiveTouch(UIGestureRecognizer aRecogniser, UITouch aTouch)
			{
                //Hacky interception of touches to trigger tutorial popovers
				if (HelpManager.Active) 
				{
					if (HelpManager.State == HelpManager.HelpState.EnageWithMosaic) 
					{
						HelpManager.PresentBothPopovers (aRecogniser.View.Frame, controller.View);
					}
				}

				return true;
			}

			// Ensure that the pinch, pan and rotate gestures are all recognized simultaneously
			public override bool ShouldRecognizeSimultaneously (UIGestureRecognizer gestureRecognizer, UIGestureRecognizer otherGestureRecognizer)
			{
				//TODO: Check that the drag is on one of the recipe icons

				// if the gesture recognizers views differ, don't recognize
				//				if (gestureRecognizer.View != otherGestureRecognizer.View)
				//					return false;

//				if (!(gestureRecognizer is UILongPressGestureRecognizer))
//					return false;
//
//				// if either of the gesture recognizers is a long press, don't recognize
				if ((gestureRecognizer is UILongPressGestureRecognizer && otherGestureRecognizer is UIPanGestureRecognizer))
					return false;

				return true;
			}
		}

		public PlanPageMosaicRecipeListCell (IntPtr handle) : base (handle)
		{
		}

		public static PlanPageMosaicRecipeListCell Create ()
		{
			PlanPageMosaicRecipeListCell cell = (PlanPageMosaicRecipeListCell)Nib.Instantiate (null, null) [0];

			cell.InitialiseFont ();

			return cell;			
		}
			
		public UIImageView ImageView
		{
			get { return RecipeImage; }
		}

        public string RecipeTitle
        {
            set
            {
                if (RecipeTitleHalfWidth.Hidden)
                    RecipeNameFullWidth.Text = value;
                else
                    RecipeTitleHalfWidth.Text = value;

                if (!RecipeTitleHalfWidth.Hidden)
                {
                    CGSize stringSize = (new NSString(value)).StringSize(UIFont.FromName("SceneStd-Black", cRecipeTitleFontSize), new CGSize(ImageView.Frame.Width, 99999), UILineBreakMode.WordWrap);

                    CGRect newFrame =new CGRect(ImageView.Frame.X + 5f, ImageView.Frame.Height - 10 - (stringSize.Height + 5), ImageView.Frame.Width - 10.0f, stringSize.Height + 5);

                    if (RecipeTitleHalfWidth.Frame != newFrame)
                        SetNeedsDisplay();
                }
            }
            get
            {
                if (RecipeTitleHalfWidth.Hidden)
                    return RecipeNameFullWidth.Text;
                return RecipeTitleHalfWidth.Text;
            }

        }

        public UIImageView FavouriteIcon
        {
            get
            {
                return Favourite;
            }
        }
        public UIImageView FreezableIcon
        {
            get
            {
                return Freezable;
            }
        }
		public UILabel RecipeTitleHalfWidth
		{
			get { return RecipeName1; }
		}

        public UILabel RecipeTitleFullWidth
        {
            get { return RecipeNameFullWidth; }
        }

		public NSString Text
		{
			set 
            { 
                RecipeName1.AttributedText = CreateRecipeString(value); 
             }
		}
			
		public static UIImage CircleImageMask
		{
			get
			{
				if (mCircleImageMask==null)
				{
					mCircleImageMask = UIImage.FromFile ("CircleMask.png");
				}
				return mCircleImageMask;
			}
		}

		public bool IsDraggingSource
		{
			get
			{
				return mbDragging;
			}
		}

        static UIFont sRecipeTitleFont;
        static UIFont sRecipeNameFullWidthFont;

		public void InitialiseFont()
		{
            if (sRecipeTitleFont == null)
                sRecipeTitleFont = UIFont.FromName("SceneStd-Black", cRecipeTitleFontSize);
            if (sRecipeNameFullWidthFont == null)
                sRecipeNameFullWidthFont = UIFont.FromName("SceneStd-Black", cRecipeTitleFontSize);

            RecipeName1.Font = sRecipeTitleFont;
            RecipeName1.TextColor = UIColor.White;
            RecipeName1.ShadowColor = UIColor.Black;

            RecipeNameFullWidth.Font = sRecipeNameFullWidthFont;
            RecipeNameFullWidth.TextColor = UIColor.White;
            RecipeNameFullWidth.ShadowColor = UIColor.Black;
        }

		public void UseSegoeFont()
		{
            RecipeName1.Font = UIFont.FromName ("SegoePrint-Bold", cRecipeTitleFontSize);
            RecipeName1.ShadowColor = UIColor.Clear;
		}

		public void AddGestureRecognizers(PlanPageMosaicRecipeListController controller)
		{
			if (mbAddedGestureRecognizers)
				return;

			mMosaicController = controller;
				
			var panGesture = new UIPanGestureRecognizer (PanImage);
			panGesture.MaximumNumberOfTouches = 2;
			panGesture.Delegate = new GestureDelegate (controller);

			var touchAndHoldGesture = new UILongPressGestureRecognizer (TouchAndHold);
            touchAndHoldGesture.MinimumPressDuration = 0.3f;
			touchAndHoldGesture.NumberOfTouchesRequired = 1;
			touchAndHoldGesture.NumberOfTapsRequired = 0;
			touchAndHoldGesture.AllowableMovement = 10.0f;
			touchAndHoldGesture.Delegate = new GestureDelegate (controller);

            var doubleTap = new UITapGestureRecognizer(DoubleTapRecipe);
            doubleTap.NumberOfTapsRequired = 2;
            doubleTap.NumberOfTouchesRequired = 1;

            AddGestureRecognizer (doubleTap);
			AddGestureRecognizer (panGesture);
			AddGestureRecognizer (touchAndHoldGesture);

			mbAddedGestureRecognizers = true;
		}

        public void RemoveGestureRecognisers()
        {
            mbAddedGestureRecognizers = false;

            if (GestureRecognizers != null && GestureRecognizers.Length > 0)
            {
                while (GestureRecognizers.Length > 0)
                    RemoveGestureRecognizer(GestureRecognizers[0]);
            }
        }
			
		public NSMutableAttributedString CreateRecipeString(NSString newText)
		{
			UIStringAttributes stringAttributes = new UIStringAttributes {
                Font = UIFont.FromName ("SceneStd-Black", 16),
                ForegroundColor = UIColor.White,
				//BackgroundColor = UIColor.FromWhiteAlpha(1.0f, 1.0f),

                //      ParagraphStyle = new NSMutableParagraphStyle () { MinimumLineHeight = 20.0f }
			};

			var AttributedText = new NSMutableAttributedString (newText);
			AttributedText.AddAttributes (stringAttributes, new NSRange (0, newText.Length));
			return AttributedText;
		}
			
        void DoubleTapRecipe(UITapGestureRecognizer gestureRecognizer)
        {
            //Check if the selected index has been initialised
            if (mMosaicController.SelectedItemIndex != -1)
            {
                if (!mMosaicController.IsSpecialMeal(mMosaicController.SelectedItemIndex) && mMosaicController.IsSearchHit(mMosaicController.SelectedItemIndex))
                {
                    AppDelegate.SwitchToCookPage();
                }
            }
        }


		void TouchAndHold(UILongPressGestureRecognizer gestureRecognizer)
		{
			if (gestureRecognizer.State == UIGestureRecognizerState.Began) 
			{
				mbTouchAndHold = true;
                mbDragging = true;


                mMaskedRecipeImage = Utils.CreateCircularRecipeImagePortrait(RecipeImage.Image);

				//Check if the image view has been created
				if (mDraggableImageView == null) 
				{
					mDraggableImageView = new UIImageView (mMaskedRecipeImage);	

					var location = gestureRecognizer.LocationInView (AppDelegate.PlanPage);
					location.X -= DraggableRecipeImageRadius;
					location.Y -= DraggableRecipeImageRadius;

					mDraggableImageView.Frame = new CGRect (location, new CGSize (DraggableRecipeImageRadius*2, DraggableRecipeImageRadius*2));

					AppDelegate.PlanPage.AddSubview (mDraggableImageView);

					mCircleOutline = new UICircleOutlineView (mDraggableImageView.Frame , DraggableRecipeImageRadius * 1.5f);
					AppDelegate.PlanPage.AddSubview (mCircleOutline);
				} 
				else 
				{
					mDraggableImageView.Hidden = false;
					mDraggableImageView.Alpha = 1.0f;

					var location = gestureRecognizer.LocationInView (AppDelegate.PlanPage);
					location.X -= DraggableRecipeImageRadius;
					location.Y -= DraggableRecipeImageRadius;

					mDraggableImageView.Frame = new CGRect (location, new CGSize (DraggableRecipeImageRadius * 2, DraggableRecipeImageRadius * 2));
					mDraggableImageView.Image = mMaskedRecipeImage;

					mCircleOutline.Hidden = false;
					mCircleOutline.UpdateFrame (mDraggableImageView.Frame);
				}

				AdjustAnchorPointForGestureRecognizer (gestureRecognizer);

				mDropTargets = AppDelegate.PlanPageViewController.CalculateDropTargets ();

				//Make sure the cell that we touch and hold is also selected
				mMosaicController.SelectItem (mMosaicController.CollectionView.IndexPathForCell (this));
				RecipeImage.Alpha = 0.5f;
			}
			else if (gestureRecognizer.State == UIGestureRecognizerState.Ended)
			{
				mDraggableImageView.Hidden = true;
                mCircleOutline.Hidden = true;
				mbTouchAndHold = false;

				if (mMosaicController.GetSelectedCell () == null)
					return;

				UIView.Animate (0.15f, 0, UIViewAnimationOptions.CurveLinear,
					() => {mMosaicController.GetSelectedCell ().ImageView.Alpha = 1.0f;},
					() => {mMosaicController.GetSelectedCell ().ImageView.Alpha = 1.0f;}
				);

				UIView.Animate (0.15f, 0, UIViewAnimationOptions.CurveLinear,
					() => {mDraggableImageView.Alpha = 1.0f;},
					() => {mDraggableImageView.Alpha = 0.0f; mDraggableImageView.Hidden = true;}
				);
			}
		}

		void PanImage (UIPanGestureRecognizer gestureRecognizer)
		{
			if (mDraggableImageView == null)
				return;
				
			//AdjustAnchorPointForGestureRecognizer (gestureRecognizer)

			var image = mDraggableImageView;

			if ((gestureRecognizer.State == UIGestureRecognizerState.Began || gestureRecognizer.State == UIGestureRecognizerState.Changed) && mbTouchAndHold) 
			{
				mbDragging = true;
				var translation = gestureRecognizer.TranslationInView (AppDelegate.PlanPage);
				image.Center = new CGPoint (image.Center.X + translation.X, image.Center.Y + translation.Y);
				// Reset the gesture recognizer's translation to {0, 0} - the next callback will get a delta from the current position.
				gestureRecognizer.SetTranslation (CGPoint.Empty, image);
			}
			//Update the outline around the image
			mCircleOutline.UpdateFrame (mDraggableImageView.Frame);

			//Check if the draggable is in range of any of the drop targets
			if (gestureRecognizer.State == UIGestureRecognizerState.Ended && mbDragging) 
			{
				mbDragging = false;
				mCircleOutline.Hidden = true;
                mbTouchAndHold = false;

				//Check if we're over any of the drop targets
				DropTarget potentialDropTarget = InRangeOfADroptarget ();

				if (potentialDropTarget != null) 
				{
                    if (!mMosaicController.IsSpecialMeal(mMosaicController.SelectedItemIndex))
                        potentialDropTarget.PlannerCell.SetRecipe(potentialDropTarget.RecipeIndex, mDraggableImageView.Image, RecipeManager.Instance.GetRecipe(mMosaicController.ItemToRecipeIndexMapping(mMosaicController.SelectedItemIndex)));
                    else
                        potentialDropTarget.PlannerCell.SetSpecialMeal(potentialDropTarget.RecipeIndex, mDraggableImageView.Image, RecipeManager.Instance.GetRecipe(mMosaicController.ItemToRecipeIndexMapping(mMosaicController.SelectedItemIndex)));
				
					if (HelpManager.Active) 
					{
						if (HelpManager.State == HelpManager.HelpState.EnageWithCalendarMeal && !mMosaicController.IsSpecialMeal(mMosaicController.SelectedItemIndex)) 
						{
							HelpManager.PresentPopover (potentialDropTarget.Image.Frame, potentialDropTarget.PlannerCell);
							HelpManager.PresentAltPopover (potentialDropTarget.PlannerCell.GetServesButton(potentialDropTarget.RecipeIndex).Frame, potentialDropTarget.PlannerCell, UIPopoverArrowDirection.Down);
						}
						else if (HelpManager.State == HelpManager.HelpState.TapOnLabelToEditText && mMosaicController.IsSpecialMeal(mMosaicController.SelectedItemIndex)) 
						{
							HelpManager.PresentPopover (potentialDropTarget.Label.Frame, potentialDropTarget.PlannerCell);
						}
					}
				}
					
				if (mMosaicController.GetSelectedCell () == null)
					return;

                if (mMosaicController.GetSelectedCell() != null)
                {
                    UIView.Animate(0.15f, 0, UIViewAnimationOptions.CurveLinear,
                        () =>
                        {
                            mMosaicController.GetSelectedCell().ImageView.Alpha = 1.0f;
                        },
                        () =>
                        {
                            mMosaicController.GetSelectedCell().ImageView.Alpha = 1.0f;
                        }
                    );
                }

				UIView.Animate (0.15f, 0, UIViewAnimationOptions.CurveLinear,
					() => {mDraggableImageView.Alpha = 1.0f;},
					() => {mDraggableImageView.Alpha = 0.0f; mDraggableImageView.Hidden = true;}
				);
			}
		}

		DropTarget InRangeOfADroptarget ()
		{
			if (mDropTargets == null || mDropTargets.Count == 0)
				return null;
				
			var targets = mDropTargets.Where(dt => (dt.PosInMainView.DistanceTo(mDraggableImageView.Center)) < DraggableRecipeImageRadius);

			if (targets != null && targets.Count() > 0)
				return targets.First ();

			return null;
		} 
			
		void AdjustAnchorPointForGestureRecognizer (UIGestureRecognizer gestureRecognizer)
		{
			if (gestureRecognizer.State == UIGestureRecognizerState.Began) 
			{
				var image = mDraggableImageView;
				var locationInView = gestureRecognizer.LocationInView (image);
				var locationInSuperview = gestureRecognizer.LocationInView (AppDelegate.PlanPage);

				image.Layer.AnchorPoint = new CGPoint (locationInView.X / image.Bounds.Size.Width, locationInView.Y / image.Bounds.Size.Height);
				image.Center = locationInSuperview;
			}
		}

        //Force the formatring of the strings on the redraw - THere has to be a better way than this!!!!!
        public override void Draw(CGRect rect)
        {
            if (!RecipeTitleHalfWidth.Hidden)
            {
                CGRect frame = RecipeTitleHalfWidth.Frame;
                NSString text = new NSString(RecipeTitleHalfWidth.Text);

                CGSize stringSize = text.StringSize(UIFont.FromName("SceneStd-Black", cRecipeTitleFontSize), new CGSize(RecipeTitleHalfWidth.Frame.Width, 99999), UILineBreakMode.WordWrap);

                RecipeTitleHalfWidth.Frame = new CGRect(RecipeTitleHalfWidth.Frame.X, ImageView.Frame.Height - (stringSize.Height + 5), RecipeTitleHalfWidth.Frame.Width, stringSize.Height + 5);

            }
            if (!RecipeTitleFullWidth.Hidden)
            {
                CGRect frame = RecipeTitleFullWidth.Frame;
                NSString text = new NSString(RecipeTitleFullWidth.Text);

                CGSize stringSize = text.StringSize(UIFont.FromName("SceneStd-Black", cRecipeTitleFontSize), new CGSize( ImageView.Frame.Width*0.7f-25.0f, 99999), UILineBreakMode.WordWrap);
                //
                RecipeTitleFullWidth.Frame = new CGRect(RecipeTitleFullWidth.Frame.X, ImageView.Frame.Height - (stringSize.Height + 5), RecipeTitleFullWidth.Frame.Width, stringSize.Height);
            }
        }

        partial void LookAtRecipeOnCookPage(NSObject sender)
        {
            Console.WriteLine("Cook Page");
            mMosaicController.SelectItem (mMosaicController.CollectionView.IndexPathForCell (this));
            AppDelegate.SwitchToCookPage();
        }

   	}
}

