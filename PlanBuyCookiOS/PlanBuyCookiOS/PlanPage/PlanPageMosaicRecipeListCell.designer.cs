// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace PlanBuyCookiOS
{
	[Register ("PlanPageMosaicRecipeListCell")]
	partial class PlanPageMosaicRecipeListCell
	{
		[Outlet]
		UIKit.UIImageView Favourite { get; set; }

		[Outlet]
		UIKit.UIImageView Freezable { get; set; }

		[Outlet]
		UIKit.UIImageView RecipeImage { get; set; }

		[Outlet]
		UIKit.UILabel RecipeName1 { get; set; }

		[Outlet]
		UIKit.UILabel RecipeNameFullWidth { get; set; }

		[Action ("LookAtRecipeOnCookPage:")]
		partial void LookAtRecipeOnCookPage (Foundation.NSObject sender);

		[Action ("OnLongPressSelect:")]
		partial void OnLongPressSelect (Foundation.NSObject sender);
		
		void ReleaseDesignerOutlets ()
		{
			if (Freezable != null) {
				Freezable.Dispose ();
				Freezable = null;
			}

			if (Favourite != null) {
				Favourite.Dispose ();
				Favourite = null;
			}

			if (RecipeImage != null) {
				RecipeImage.Dispose ();
				RecipeImage = null;
			}

			if (RecipeName1 != null) {
				RecipeName1.Dispose ();
				RecipeName1 = null;
			}

			if (RecipeNameFullWidth != null) {
				RecipeNameFullWidth.Dispose ();
				RecipeNameFullWidth = null;
			}
		}
	}
}
