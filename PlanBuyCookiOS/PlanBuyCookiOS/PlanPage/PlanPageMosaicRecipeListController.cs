using System;
using System.Collections.Generic;
using CoreGraphics;
using Foundation;
using UIKit;
using ObjCRuntime;

namespace PlanBuyCookiOS
{
	[Register("PlanPageMosaicRecipeListController")]
	public class PlanPageMosaicRecipeListController : UICollectionViewController
	{
		Dictionary<string, UIImage> mImageCache = new Dictionary<string, UIImage> ();

        UIImage[] mSpecialMealIcons = new UIImage[5];
		string[] mSpecialMealString = {"Takeaway","Freezer/Leftovers","Plan Your Own", "Web Recipe", "Dining Out"};
		int mSelectedIndex = -1;


		UIView mContainer;
		public PlanPageMosaicRecipeListController (UICollectionViewLayout layout, UIView container) : base (layout)
		{
			mContainer = container;
			mContainer.BackgroundColor = UIColor.White;
		}
			
		public int SelectedItemIndex
		{
			get { return mSelectedIndex; }
		}
		public int SelectedRecipeIndex
		{
			get 
			{ 
				if (SelectedItemIndex == -1)
					return 0;
				return mItemToRecipeIndexMapping[SelectedItemIndex]; 
			}
		}
		public override void DidReceiveMemoryWarning ()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning ();
			
			// Release any cached data, images, etc that aren't in use.
		}
			
		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			
			// Register any custom UICollectionViewCell classes
			//CollectionView.RegisterClassForCell (typeof(PlanPageMosaicRecipeListCell), PlanPageMosaicRecipeListCell.Key);
			
			// Note: If you use one of the Collection View Cell templates to create a new cell type,
			// you can register it using the RegisterNibForCell() method like this:
			//

			CollectionView.ScrollEnabled = true;
			//RectangleF frame = CollectionView.FrameForAlignmentRect;

            //Sometimes the superview is offset, and sometimes not??
            CollectionView.Frame = new CGRect(-CollectionView.Superview.Frame.X,0,765,693);
                
			CollectionView.BackgroundColor = UIColor.White;
			CollectionView.RegisterNibForCell (PlanPageMosaicRecipeListCell.Nib, PlanPageMosaicRecipeListCell.Key);

			mSpecialMealIcons [0] = UIImage.FromFile ("Takeaway.png"); 
			mSpecialMealIcons [1] = UIImage.FromFile ("Leftovers.png");
			mSpecialMealIcons [2] = UIImage.FromFile ("WriteYourOwn.png");
			mSpecialMealIcons [3] = UIImage.FromFile ("WWWRecipe.png");
            mSpecialMealIcons [4] = UIImage.FromFile ("DiningOut.png");

			//Create a mapping from a tile to a recipe index
			CacheItemToRecipeIndexMapping ();
            PrecacheMosaicImages();
		}
			

		public override nint NumberOfSections (UICollectionView collectionView)
		{
			// TODO: return the actual number of sections
			return 1;
		}
			
		public override nint GetItemsCount (UICollectionView collectionView, nint section)
		{
			// TODO: return the actual number of items in the section
			if (section == 0)
				return (Layout as PlanPageMosaicLayout).NumRectsInLayout ();
			return 0;
		}

		public override bool ShouldSelectItem (UICollectionView collectionView, NSIndexPath indexPath)
		{
			return true;
		}
            
		public override void ItemSelected (UICollectionView collectionView, NSIndexPath indexPath)
		{
			//Check if we've just selected the current item
			if (mSelectedIndex == indexPath.Item)
				return;

			mSelectedIndex = (int)indexPath.Item;
		}

        //NOTE: Do not delete.  This is being implcitly called internally somewhere and removing it causes a Selector crash
        public override void ItemDeselected(UICollectionView collectionView, NSIndexPath indexPath)
        {
        }

		//SelectItem and DeselectItem are for programmitically setting the selected item
		public void DeselectItem(NSIndexPath indexPath)
		{
			CollectionView.DeselectItem( indexPath, true);
		}

		public void SelectItem(NSIndexPath indexPath)
		{
            Console.WriteLine("{0} {1} ",indexPath.Item, mSelectedIndex);

			CollectionView.SelectItem (indexPath, false, UICollectionViewScrollPosition.None);

			if (mSelectedIndex != indexPath.Item && mSelectedIndex != -1)
				DeselectItem (NSIndexPath.FromItemSection (mSelectedIndex, 0));

			mSelectedIndex = (int)indexPath.Item;
		}

		Dictionary<int, int> mItemToRecipeIndexMapping = new Dictionary<int, int> ();

		public int ItemToRecipeIndexMapping(int item)
		{
			int tileIndex = item % mItemToRecipeIndexMapping.Count;

            return mItemToRecipeIndexMapping [tileIndex];
		}

		public void CacheItemToRecipeIndexMapping()
		{
			int recipeIndex = 0;
			int specialRecipeIndex = 0;
			for (int item = 0; item < (CollectionView.CollectionViewLayout as PlanPageMosaicLayout).NumRectsInLayout (); item++) 
			{
				NSIndexPath ip = NSIndexPath.FromItemSection(item, 0);

				UICollectionViewLayoutAttributes layout = CollectionView.CollectionViewLayout.LayoutAttributesForItem (ip);

				if (layout.Frame.Height == layout.Frame.Width && layout.Frame.Width < 380.0f) 
				{
					mItemToRecipeIndexMapping.Add (mItemToRecipeIndexMapping.Count, -(specialRecipeIndex%mSpecialMealIcons.Length + 1));
					specialRecipeIndex++;
				} else {
					mItemToRecipeIndexMapping.Add (mItemToRecipeIndexMapping.Count, recipeIndex);
					recipeIndex++;
				}
			}
		}


        public bool IsSpecialMeal(int cellIndex)
        {
            return ItemToRecipeIndexMapping(cellIndex) < 0;
        }
			
        public bool IsSearchHit(int cellIndex)
        {
            if (RecipeManager.Instance.SearchIsActive)
                return RecipeManager.Instance.IsSearchHit(ItemToRecipeIndexMapping(cellIndex));

            return true;
        }

		public PlanPageMosaicRecipeListCell GetSelectedCell()
		{
			NSIndexPath[] selected = CollectionView.GetIndexPathsForSelectedItems ();
			int selectedIndex = mSelectedIndex;

			if (selected.Length > 0) {
				return CollectionView.CellForItem (selected [0])  as PlanPageMosaicRecipeListCell;
			}

			Console.Error.WriteLine("Error, CollectionView.GetIndexPathsForSelectedItems was empty!");
			if (selectedIndex == -1)
				return CollectionView.CellForItem(NSIndexPath.FromItemSection(0,0)) as PlanPageMosaicRecipeListCell;

			return CollectionView.CellForItem(NSIndexPath.FromItemSection(selectedIndex,0)) as PlanPageMosaicRecipeListCell;
		}
			
        public void PrecacheMosaicImages()
        {
            for (int mosaicImageIndex = 0; mosaicImageIndex < mItemToRecipeIndexMapping.Count; mosaicImageIndex++)
            {
                UICollectionViewLayoutAttributes layout = CollectionView.CollectionViewLayout.LayoutAttributesForItem (NSIndexPath.FromItemSection(mosaicImageIndex, 0));

                //Check for special meal
                if (!(layout.Frame.Height == layout.Frame.Width && layout.Frame.Width < 380.0f))
                {
                    IRecipeImport recipe = RecipeManager.Instance.GetRecipe (ItemToRecipeIndexMapping(mosaicImageIndex)) as IRecipeImport;
                    UIImage cachedImage;

                    if (recipe != null) 
                    {
                        string imageAspect = "";
                        if (layout.Frame.Height > layout.Frame.Width)
                            imageAspect = "Portrait";
                        else if (layout.Frame.Height < layout.Frame.Width)
                            imageAspect = "Landscape";
                        else
                            imageAspect = "Square";

                        mImageCache.TryGetValue (recipe.Name+imageAspect, out cachedImage);

                        if (cachedImage == null) 
                        {
                            byte[] encodedDataAsBytes;

                            if (imageAspect=="Portrait")
                                encodedDataAsBytes = Convert.FromBase64String(recipe.ImagePortraitString);
                            else if (imageAspect=="Landscape")
                                encodedDataAsBytes = Convert.FromBase64String(recipe.ImageLandscapeString);
                            else
                                encodedDataAsBytes = Convert.FromBase64String(recipe.ImageString);

                            NSData data = NSData.FromArray (encodedDataAsBytes);                            

                            UIImage image = UIImage.LoadFromData (data);

							Console.WriteLine("Caching image mosaic " + recipe.Name + imageAspect);
                            mImageCache.Add (recipe.Name+imageAspect, image);
                        }
                    }
                }
            }
        }

		public override UICollectionViewCell GetCell(UICollectionView collectionView, NSIndexPath indexPath)
		{
			var cell = collectionView.DequeueReusableCell (PlanPageMosaicRecipeListCell.Key, indexPath) as PlanPageMosaicRecipeListCell;

            cell.Layer.ShouldRasterize = true;
            cell.Layer.RasterizationScale = UIScreen.MainScreen.Scale;

            cell.RecipeTitleHalfWidth.Hidden = cell.Frame.Width > 200.0f;
            cell.RecipeTitleFullWidth.Hidden = cell.Frame.Width <= 200.0f;

			//First check if it's a special meal cell
			if (IsSpecialMeal((int)indexPath.Item)) 
			{
				int meal = -mItemToRecipeIndexMapping[(int)indexPath.Item] - 1;

				cell.ImageView.Image = mSpecialMealIcons[meal];
				cell.ImageView.Alpha = 1.0f;
				cell.Text = new NSString(mSpecialMealString [meal]);
                cell.RecipeTitleHalfWidth.TextColor = UIColor.Black;
				cell.UseSegoeFont ();
                cell.FavouriteIcon.Hidden = true;
                cell.FreezableIcon.Hidden = true;

                if (AppDelegate.sbWebRecipesEnabled)
                    cell.AddGestureRecognizers(this);
                else
                {
                    if (meal == -((int)SpecialMealType.WWWRecipe + 1))
                    {
                        cell.RemoveGestureRecognisers();
                    }
                    else
                        cell.AddGestureRecognizers(this);
                }
			} 
			else 
			{
				IRecipeImport recipe = RecipeManager.Instance.GetRecipe (ItemToRecipeIndexMapping((int)indexPath.Item)) as IRecipeImport;

                cell.InitialiseFont ();
                cell.AddGestureRecognizers (this);

				UIImage cachedImage;

				if (recipe != null) 
				{
                    cell.FavouriteIcon.Hidden = !recipe.Favourite;
                    cell.FreezableIcon.Hidden = !recipe.CategoriesString.Contains("freezes");

                    string imageAspect = "";
                    if (cell.Frame.Height > cell.Frame.Width)
                        imageAspect = "Portrait";
                    else if (cell.Frame.Height < cell.Frame.Width)
                        imageAspect = "Landscape";
                    else
                        imageAspect = "Square";

                    mImageCache.TryGetValue (recipe.Name+imageAspect, out cachedImage);

					if (cachedImage != null) 
					{
						cell.ImageView.Image = cachedImage;
					} 
					else if (recipe.ImageString != "") 
					{
						cell.ImageView.ContentMode = UIViewContentMode.ScaleToFill;

						byte[] encodedDataAsBytes;

                        if (imageAspect=="Portrait")
                            encodedDataAsBytes = Convert.FromBase64String(recipe.ImagePortraitString);
                        else if (imageAspect=="Landscape")
                            encodedDataAsBytes = Convert.FromBase64String(recipe.ImageLandscapeString);
                        else
                            encodedDataAsBytes = Convert.FromBase64String(recipe.ImageString);

						NSData data = NSData.FromArray (encodedDataAsBytes);                            

						UIImage image = UIImage.LoadFromData (data);

                        Console.WriteLine("Creating image mosaic " + recipe.Name);
                        mImageCache.Add (recipe.Name+imageAspect, image);

						cell.ImageView.Image = image;
					}
                    cell.RecipeTitle = recipe.Name.ToUpper ();
				}

                //Display search results differently (if search is active)
                if (RecipeManager.Instance.SearchIsActive)
                {
					if (!RecipeManager.Instance.IsSearchHit(ItemToRecipeIndexMapping((int)indexPath.Item)))
                    {
                        cell.ImageView.Alpha = 0.3f;
                        cell.RecipeTitleFullWidth.Alpha = 0.3f;
                        cell.RecipeTitleHalfWidth.Alpha = 0.3f;
                    }
                    else
                    {
                        cell.ImageView.Alpha = 1.0f;
                        cell.RecipeTitleFullWidth.Alpha = 1.0f;
                        cell.RecipeTitleHalfWidth.Alpha = 1.0f;
                    }
                }
                else
                {
                    cell.ImageView.Alpha = 1.0f;
                    cell.RecipeTitleFullWidth.Alpha = 1.0f;
                    cell.RecipeTitleHalfWidth.Alpha = 1.0f;
                }
			}
                
			return cell;
		}
	}
}

