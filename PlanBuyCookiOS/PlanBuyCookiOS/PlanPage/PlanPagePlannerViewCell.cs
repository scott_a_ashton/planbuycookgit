using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CoreGraphics;
using Foundation;
using UIKit;

namespace PlanBuyCookiOS
{
	public partial class PlanPagePlannerViewCell : UITableViewCell
	{
		//Cache the circular portraits
		static Dictionary<string, UIImage> mImageCache = new Dictionary<string, UIImage> ();
        static UIPopoverController sPopOver;
        static UIPopoverController sEnterURLPopover;

		//Keep a record of the PlanPageFactory
		PlanPageCalenderViewSource mSource;
		UITableView mTableView;
		int mRow;
		CalenderDay mDay;

		readonly float cDayTitleFontSize = 15.0f;
		readonly float cRecipeNameFontSize = 12.0f;
		readonly float cDragYourRecipeFontSize = 12.0f;

        public delegate void ActionOnUrlReceipt(string url);
        public static ActionOnUrlReceipt sCreateWebRecipeOnEnterUrl;

		public static readonly UINib Nib = UINib.FromName ("PlanPagePlannerViewCell", NSBundle.MainBundle);
		public static readonly NSString Key = new NSString ("PlanPagePlannerViewCell");

		public PlanPagePlannerViewCell (IntPtr handle) : base (handle)
		{
		}

		public static PlanPagePlannerViewCell Create (PlanPageCalenderViewSource source, UITableView tableView, int row, CalenderDay day)
		{
			PlanPagePlannerViewCell cell = (PlanPagePlannerViewCell)Nib.Instantiate (null, null) [0];

			cell.InitialiseFonts ();
			cell.Initialise (source, tableView, row, day);

			return cell;
		}

		public UITableView TableView
		{
			get 
			{
				return mTableView;
			}
		}
			
		void InitialiseFonts()
		{
			DayTitle.Font = UIFont.FromName ("SceneStd-Black", cDayTitleFontSize);

			RecipeName1.Font = UIFont.FromName ("SceneStd-Regular", cRecipeNameFontSize);
			RecipeName2.Font = UIFont.FromName ("SceneStd-Regular", cRecipeNameFontSize);
			RecipeName3.Font = UIFont.FromName ("SceneStd-Regular", cRecipeNameFontSize);

			DragYourRecipes1.Font = UIFont.FromName ("SegoePrint-Bold", cDragYourRecipeFontSize);
			DragYourRecipes2.Font = UIFont.FromName ("SegoePrint-Bold", cDragYourRecipeFontSize);			
			DragYourRecipes3.Font = UIFont.FromName ("SegoePrint-Bold", cDragYourRecipeFontSize);

            Serves1Label.Font = UIFont.FromName ("SceneStd-Black", cRecipeNameFontSize);
            Serves2Label.Font = UIFont.FromName ("SceneStd-Black", cRecipeNameFontSize);
            Serves3Label.Font = UIFont.FromName ("SceneStd-Black", cRecipeNameFontSize);

            CustomEntry1.Font = UIFont.FromName ("SegoePrint-Bold", cDragYourRecipeFontSize);
		}
			

		public UIButton GetRecipeImage(uint index)
		{
			if (index == 0)
				return RecipeImage1;
			if (index == 1)
				return RecipeImage2;
			if (index == 2)
				return RecipeImage3;

			return null;
		}

		public UILabel GetRecipeLabel(uint index)
		{
			if (index == 0)
				return RecipeName1;
			if (index == 1)
				return RecipeName2;
			if (index == 2)
				return RecipeName3;

			return null;
		}

		public UIButton GetServesButton(uint index)
		{
			if (index == 0)
				return Serves1;
			if (index == 1)
				return Serves2;
			if (index == 2)
				return Serves3;

			return null;
		}

		public DropTarget GetDropTarget(uint index)
		{
			return new DropTarget (index, GetRecipeImage (index), GetRecipeLabel (index), this, AppDelegate.PlanPage);
		}

		//Initialise (this, tableView, indexPath.Row);
		public void Initialise(PlanPageCalenderViewSource source, UITableView tableView, int row, CalenderDay day)
        {
            string[] months = new string[]{ "JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC" };

            mRow = row;
            mTableView = tableView;
            mSource = source; 
            mDay = day;
            bool bIsSpecialMeal = false;
			bool bIsWebRecipe = false;
            string recipeName = "";

            CustomEntry1.EditingDidBegin += (object sender1, EventArgs e1) =>
            {
                //EditingDidBeing is triggered multiple times so need logic to stop ScrollToRow being called many times
                if (!PlanPageCalenderViewSource.IsScrollingToRow)
                {
                    Console.WriteLine("Scroll to row");
                    PlanPageCalenderViewSource.ScrollToRowStarted();
                    mTableView.ScrollToRow(mTableView.IndexPathForCell(this), UITableViewScrollPosition.Top, true);
                }
            };
            CustomEntry2.EditingDidBegin += (object sender2, EventArgs e2) =>
            {
                //EditingDidBeing is triggered multiple times so need logic to stop ScrollToRow being called many times
                if (!PlanPageCalenderViewSource.IsScrollingToRow)
                {
                    PlanPageCalenderViewSource.ScrollToRowStarted();
                    mTableView.ScrollToRow(mTableView.IndexPathForCell(this), UITableViewScrollPosition.Top, true);
                }
            };
            CustomEntry3.EditingDidBegin += (object sender3, EventArgs e3) =>
            {
                //EditingDidBeing is triggered multiple times so need logic to stop ScrollToRow being called many times
                if (!PlanPageCalenderViewSource.IsScrollingToRow)
                {
                    PlanPageCalenderViewSource.ScrollToRowStarted();
                    mTableView.ScrollToRow(mTableView.IndexPathForCell(this), UITableViewScrollPosition.Top, true);
                }
            };

            CustomEntry1.EditingDidEnd += (object _sender1, EventArgs _e1) =>
            {
                if (mDay.mMeals.Count >= 1 && (mDay.mMeals[0].Recipe is RecipeSpecialMeal))
                {
                    //We get multiple EditingDidEnd events so only update the calendar if we really need to
                    if ((mDay.mMeals[0].Recipe as RecipeSpecialMeal).Description != CustomEntry1.Text)
                    {
                        (mDay.mMeals[0].Recipe as RecipeSpecialMeal).Description = CustomEntry1.Text; 
                        PBCCalendar.UpdateCalendar(mDay, 0);
                    }
                }

            };
            CustomEntry2.EditingDidEnd += (object _sender2, EventArgs _e2) =>
            {
                if (mDay.mMeals.Count >= 2 && (mDay.mMeals[1].Recipe is RecipeSpecialMeal))
                {
                    //We get multiple EditingDidEnd events so only update the calendar if we really need to
                    if ((mDay.mMeals[1].Recipe as RecipeSpecialMeal).Description != CustomEntry2.Text)
                    {
                        (mDay.mMeals[1].Recipe as RecipeSpecialMeal).Description = CustomEntry2.Text; 
                        PBCCalendar.UpdateCalendar(day,1);
                    }
                }
            };      
            CustomEntry3.EditingDidEnd += (object _sender3, EventArgs _e3) =>
            {
                if (mDay.mMeals.Count >= 3 && (mDay.mMeals[2].Recipe is RecipeSpecialMeal))
                {
                    //We get multiple EditingDidEnd events so only update the calendar if we really need to
                    if ((mDay.mMeals[2].Recipe as RecipeSpecialMeal).Description != CustomEntry3.Text)
                    {
                        (mDay.mMeals[2].Recipe as RecipeSpecialMeal).Description = CustomEntry3.Text; 
                        PBCCalendar.UpdateCalendar(day,2);
                    }
                }
            };

			//Set the day string
			DayTitle.Text = day.Date.DayOfWeek.ToString ().ToUpper () + " " + day.Date.Day + " " + months[day.Date.Month-1];

			switch (day.MealCount) 
            {
				case 0:
					SetRecipeSlotInCell (0, "", GetCachedRecipePortrait ("emptyrecipe"), bImageHidden:false, bNameHidden:true, bAddButtonHidden:true, bAddButtonSelected:false, bDragYourRecipeHidden:false, bSpecialMeal:false, bIsWebRecipe:false);
					SetRecipeSlotInCell (1, "", GetCachedRecipePortrait ("emptyrecipe"), true, true, true, false, true, false, false);
					SetRecipeSlotInCell (2, "", GetCachedRecipePortrait ("emptyrecipe"), true, true, true, false, true, false, false);
					break;

			    case 1:
					bIsWebRecipe = day.mMeals [0].Recipe is RecipeWeb;
                    bIsSpecialMeal = (mDay.mMeals [0].Recipe is RecipeSpecialMeal);
                    recipeName = bIsSpecialMeal ? mDay.mMeals[0].Recipe.Description : mDay.mMeals[0].Recipe.Name;                

					SetRecipeSlotInCell (0, recipeName, GetCachedRecipePortrait (mDay.mMeals [0].Recipe), false, false, false, true, true, bIsSpecialMeal, bIsWebRecipe);

    				if (mSource.SelectedRow == row)
						SetRecipeSlotInCell (1, "", GetCachedRecipePortrait ("emptyrecipe"), false, true, true, false, false, false, false);
    				else
						SetRecipeSlotInCell (1, "", GetCachedRecipePortrait ("emptyrecipe"), true, true, true, false, true, false, false);

					SetRecipeSlotInCell (2, "", GetCachedRecipePortrait ("emptyrecipe"), true, true, true, false, true, false, false);
    				break;

			    case 2:
					bIsWebRecipe = day.mMeals [0].Recipe is RecipeWeb;
                    bIsSpecialMeal = (mDay.mMeals [0].Recipe is RecipeSpecialMeal);
                    recipeName = bIsSpecialMeal ? mDay.mMeals[0].Recipe.Description : mDay.mMeals[0].Recipe.Name;                
					SetRecipeSlotInCell (0, recipeName, GetCachedRecipePortrait (mDay.mMeals [0].Recipe), false, false, false, true, true, bIsSpecialMeal, bIsWebRecipe);

					bIsWebRecipe = day.mMeals [1].Recipe is RecipeWeb;
					bIsSpecialMeal = (mDay.mMeals [1].Recipe is RecipeSpecialMeal);
                    recipeName = bIsSpecialMeal ? mDay.mMeals[1].Recipe.Description : mDay.mMeals[1].Recipe.Name;                
					SetRecipeSlotInCell (1, recipeName, GetCachedRecipePortrait (mDay.mMeals [1].Recipe), false, false, false, true, true, bIsSpecialMeal, bIsWebRecipe);

    				if (mSource.SelectedRow == row)
					SetRecipeSlotInCell (2, "", GetCachedRecipePortrait ("emptyrecipe"), false, true, true, false, false, false,false);
    				else
					SetRecipeSlotInCell (2, "", GetCachedRecipePortrait ("emptyrecipe"), true, true, true, false, true, false, false);
					break;

                case 3:
					bIsWebRecipe = day.mMeals [0].Recipe is RecipeWeb;
                    bIsSpecialMeal = (mDay.mMeals [0].Recipe is RecipeSpecialMeal);
                    recipeName = bIsSpecialMeal ? mDay.mMeals[0].Recipe.Description : mDay.mMeals[0].Recipe.Name;                
					SetRecipeSlotInCell (0, recipeName, GetCachedRecipePortrait (mDay.mMeals [0].Recipe), false, false, false, true, true, bIsSpecialMeal, bIsWebRecipe);

					bIsWebRecipe = day.mMeals [1].Recipe is RecipeWeb;
                    bIsSpecialMeal = (mDay.mMeals [1].Recipe is RecipeSpecialMeal);
                    recipeName = bIsSpecialMeal ? mDay.mMeals[1].Recipe.Description : mDay.mMeals[1].Recipe.Name;                
					SetRecipeSlotInCell (1, recipeName, GetCachedRecipePortrait (mDay.mMeals [1].Recipe), false, false, false, true, true, bIsSpecialMeal, bIsWebRecipe);

					bIsWebRecipe = day.mMeals [2].Recipe is RecipeWeb;
                    bIsSpecialMeal = (mDay.mMeals [2].Recipe is RecipeSpecialMeal);
                    recipeName = bIsSpecialMeal ? mDay.mMeals[2].Recipe.Description : mDay.mMeals[2].Recipe.Name; 
					SetRecipeSlotInCell (2, recipeName, GetCachedRecipePortrait (mDay.mMeals [2].Recipe), false, false, false, true, true, bIsSpecialMeal, bIsWebRecipe);
					break;
			}
		}

		void SetRecipeSlotInCell(int slot, string recipeName, UIImage image, bool bImageHidden, bool bNameHidden, bool bAddButtonHidden, bool bAddButtonSelected, bool bDragYourRecipeHidden, bool bSpecialMeal, bool bIsWebRecipe)
		{
			UIButton recipeImage = null;
			UILabel recipeNameLabel = null;
			UIButton addRemoveButton = null;
			UILabel dragRecipeLabel = null;
            UILabel servesLabel = null;
            UIButton servesButton = null;
            UITextField customEntry = null;
            UIImageView shoppedFor = null;
            UIImageView personIcon = null;

            switch (slot) 
            {
                case 0:
                    recipeImage = RecipeImage1;
                    recipeNameLabel = RecipeName1;
                    addRemoveButton = AddRecipeButton1;
                    dragRecipeLabel = DragYourRecipes1;
                    servesLabel = Serves1Label;
                    servesButton = Serves1;
                    customEntry = CustomEntry1;
                    shoppedFor = ShoppedFor1;
                    personIcon = PersonIcon1;
				    break;
			    case 1:
    				recipeImage = RecipeImage2;
    				recipeNameLabel = RecipeName2;
    				addRemoveButton = AddRecipeButton2;
    				dragRecipeLabel = DragYourRecipes2;
                    servesLabel = Serves2Label;
                    servesButton = Serves2;
                    customEntry = CustomEntry2;
                    shoppedFor = ShoppedFor2;
                    personIcon = PersonIcon2;
    				break;
                case 2:
                    recipeImage = RecipeImage3;
                    recipeNameLabel = RecipeName3;
                    addRemoveButton = AddRecipeButton3;
                    dragRecipeLabel = DragYourRecipes3;
                    servesLabel = Serves3Label;
                    servesButton = Serves3;
                    customEntry = CustomEntry3;
                    shoppedFor = ShoppedFor3;
                    personIcon = PersonIcon3;
				    break;
			}
			servesButton.Enabled = !bIsWebRecipe;
			servesLabel.Enabled = !bIsWebRecipe;

			recipeImage.Hidden = bImageHidden;
			//This line doesn'twork as of 8.3
			//recipeImage.ImageView.Image = image;
			recipeImage.SetImage(image, UIControlState.Normal);

			recipeNameLabel.Hidden = bNameHidden;
            customEntry.Hidden = true;

            if (mDay.mMeals.Count > slot)
                shoppedFor.Hidden = !mDay.mMeals[slot].IsShoppedFor;
            else
                shoppedFor.Hidden = true;

            //If the name is set then we'll be displaying the num serves label/button
            servesLabel.Hidden = bNameHidden;
            servesButton.Hidden = bNameHidden;
            personIcon.Hidden = servesLabel.Hidden;

            if (!bNameHidden)
                servesLabel.Text = mDay.mMeals[slot].Serves.ToString();

			if (bSpecialMeal) 
            {
                customEntry.Hidden = false;
                customEntry.Text = recipeName;
                customEntry.Font = UIFont.FromName ("SegoePrint-Bold", cDragYourRecipeFontSize);
                recipeNameLabel.Hidden = true;

                servesButton.Hidden = true;
                servesLabel.Hidden = true;
                personIcon.Hidden = servesLabel.Hidden;
			} 
            else 
            {
                recipeNameLabel.Hidden = false;
				recipeNameLabel.Text = recipeName.ToUpper ();
				recipeNameLabel.Font = UIFont.FromName ("SceneStd-Regular", cRecipeNameFontSize);
			}

			addRemoveButton.Hidden = bAddButtonHidden;
			addRemoveButton.Selected = bAddButtonSelected;
			dragRecipeLabel.Hidden = bDragYourRecipeHidden;
		}


        public static UIImage GetCachedRecipePortrait(IRecipeImport recipe)
        {
            UIImage portrait = GetCachedRecipePortrait(recipe.Name);

            if (portrait == null)
            {
                Console.WriteLine("Creating image " + recipe.Name);
                portrait = Utils.CreateCircularRecipeImagePortrait(recipe);

                mImageCache.Add(recipe.Name, portrait);
            }
            return portrait;
        }


		public static UIImage GetCachedRecipePortrait(string name)
		{
			UIImage cachedImage;

			if (mImageCache.TryGetValue (name, out cachedImage))
				return cachedImage;
			else 
			{
                if (name == "emptyrecipe" || name == "Recipe Name")
                {
                    UIImage emptyRecipe = UIImage.FromFile("PlanPageCircleImageHolder.png");
                    mImageCache.Add("emptyrecipe", emptyRecipe);
                    return emptyRecipe;
                }
                else if (RecipeSpecialMeal.IsSpecialMealString(name))
                {
                    UIImage image = RecipeSpecialMeal.Image(name);
                    mImageCache.Add (name, image);
                    return image;
                }
			}
			return null;
		}

		public void SetSpecialMeal(uint index, UIImage image, IRecipeImport specialMeal)
		{
			//Cache the portrait image
			if (GetCachedRecipePortrait(specialMeal.Name) == null)
				mImageCache.Add (specialMeal.Name, image);

            if (index < mDay.mMeals.Count)
            {
                BuyPagePlannerMealsSource.RemoveMealFromPossibleShoppingList(mDay.mMeals[(int)index]);
                mDay.mMeals[(int)index].Recipe = specialMeal;
            }
			else {
				mDay.AddRecipe (specialMeal);
			}

            PBCCalendar.UpdateCalendar(mDay, (int)index);	
			mSource.AddRemoveRecipe(mTableView, mRow);
		}

		public void SetRecipe(uint index, UIImage image, IRecipeImport recipe)
		{
			//Cache the portrait image
			if (GetCachedRecipePortrait(recipe.Name) == null)
				mImageCache.Add (recipe.Name, image);

            if (index < mDay.mMeals.Count)
            {
                BuyPagePlannerMealsSource.RemoveMealFromPossibleShoppingList(mDay.mMeals[(int)index]);
                mDay.mMeals[(int)index].Recipe = recipe;
            }
			else {
				mDay.AddRecipe (recipe);
			}
            PBCCalendar.UpdateCalendar(mDay, (int)index);
			mSource.AddRemoveRecipe(mTableView, mRow);
		}

		partial void AddRemoveRecipe1 (NSObject sender)
		{
			if (!AddRecipeButton1.Selected)
            {
				mDay.AddRecipe(new RecipeSQL());
                PBCCalendar.UpdateCalendar(mDay, 0);
            }
			else
            {
                PBCCalendar.RemoveMeal(mDay, 0);
                BuyPagePlannerMealsSource.RemoveMealFromPossibleShoppingList(mDay.mMeals[0]);
				mDay.RemoveRecipe(0);
            }

			mSource.AddRemoveRecipe(mTableView, mRow);
		} 

		partial void AddRemoveRecipe2 (NSObject sender)
		{
			if (!AddRecipeButton2.Selected)
            {
				mDay.AddRecipe(new RecipeSQL());
                PBCCalendar.UpdateCalendar(mDay,1);
            }
			else
            {
                PBCCalendar.RemoveMeal(mDay, 1);
                BuyPagePlannerMealsSource.RemoveMealFromPossibleShoppingList(mDay.mMeals[1]);
                mDay.RemoveRecipe(1);
            }
			mSource.AddRemoveRecipe(mTableView, mRow);
		}

		partial void AddRemoveRecipe3 (NSObject sender)
		{
			if (!AddRecipeButton3.Selected)
            {
				mDay.AddRecipe(new RecipeSQL());
                PBCCalendar.UpdateCalendar(mDay,2);
            }
			else
            {
                PBCCalendar.RemoveMeal(mDay, 2);
                BuyPagePlannerMealsSource.RemoveMealFromPossibleShoppingList(mDay.mMeals[2]);
                mDay.RemoveRecipe(2);
            }

			mSource.AddRemoveRecipe(mTableView, mRow);
		}

        static UIPopoverController ServesPopover
        {
            get
            {
                if (sPopOver == null)
                {
                    sPopOver = new UIPopoverController(new ServesPopoverViewController());
                    sPopOver.PopoverContentSize = sPopOver.ContentViewController.View.Frame.Size;
                    sPopOver.BackgroundColor = UIColor.White;

                    sPopOver.DidDismiss += (object sender1, EventArgs e1) => 
                    {
                        Console.WriteLine("Did dismiss start");

                        if (sServesModifiedForDay != null)
                        {
                            if (sServesModifiedForMealIndex >= 0 && sServesModifiedForMealIndex < sServesModifiedForDay.MealCount)
                            {
                                PBCCalendar.UpdateCalendar(sServesModifiedForDay, sServesModifiedForMealIndex); 
                                BuyPagePlannerMealsSource.UpdateServesOnShoppingList(sServesModifiedForDay.mMeals[sServesModifiedForMealIndex]);
                            }
                        }
                        Console.WriteLine("Did dismiss end");
                    };
                }
                return sPopOver;
            }
        }
            

        static UIPopoverController RecipeURLPopover
        {
            get
            {
                if (sEnterURLPopover == null)
                {
                    sEnterURLPopover = new UIPopoverController(new EnterURLPopover());
                    sEnterURLPopover.PopoverContentSize = sEnterURLPopover.ContentViewController.View.Frame.Size;
                    sEnterURLPopover.BackgroundColor = UIColor.White;
                }
                return sEnterURLPopover;
            }
        }

        static int sServesModifiedForMealIndex = -1;
        static CalenderDay sServesModifiedForDay = null;

        partial void AdjustServes1(NSObject sender)
        {
            (ServesPopover.ContentViewController as ServesPopoverViewController).ServesSliderValue = (float)mDay.mMeals [0].Serves / 10.0f;
            (ServesPopover.ContentViewController as ServesPopoverViewController).SetUpdateCallback(UpdateServes1Value);

            Serves1Label.Hidden = false;
            Serves1Label.Text = mDay.mMeals [0].Serves.ToString();
            ServesPopover.PresentFromRect(Serves1.Frame, this, UIPopoverArrowDirection.Left , true);

            sServesModifiedForMealIndex = 0;
            sServesModifiedForDay = mDay;
        }

        partial void AdjustServes2(NSObject sender)
        {
            (ServesPopover.ContentViewController as ServesPopoverViewController).ServesSliderValue = (float)mDay.mMeals [1].Serves / 10.0f;
            (ServesPopover.ContentViewController as ServesPopoverViewController).SetUpdateCallback(UpdateServes2Value);

            Serves2Label.Hidden = false;
            Serves2Label.Text = mDay.mMeals [1].Serves.ToString();
            ServesPopover.PresentFromRect(Serves2.Frame, this, UIPopoverArrowDirection.Left , true);
        
            sServesModifiedForMealIndex = 1;
            sServesModifiedForDay = mDay;
        }
			
        partial void AdjustServes3(NSObject sender)
        {
            (ServesPopover.ContentViewController as ServesPopoverViewController).ServesSliderValue = (float)mDay.mMeals [2].Serves / 10.0f;
            (ServesPopover.ContentViewController as ServesPopoverViewController).SetUpdateCallback(UpdateServes3Value);

            Serves3Label.Hidden = false;
            Serves3Label.Text = mDay.mMeals [2].Serves.ToString();
            ServesPopover.PresentFromRect(Serves3.Frame, this, UIPopoverArrowDirection.Left , true);

            sServesModifiedForMealIndex = 2;
            sServesModifiedForDay = mDay;
        }

        void UpdateServes1Value(float valueFromSlider)
        {
            mDay.mMeals[0].Serves = ((int)(1.0f + (9.0f * valueFromSlider)));
            Serves1Label.Text = mDay.mMeals [0].Serves.ToString();
        }

        void UpdateServes2Value(float valueFromSlider)
        {
            mDay.mMeals[1].Serves = ((int)(1.0f + (9.0f * valueFromSlider)));
            Serves2Label.Text = mDay.mMeals [1].Serves.ToString();
        }

        void UpdateServes3Value(float valueFromSlider)
        {
            mDay.mMeals[2].Serves = ((int)(1.0f + (9.0f * valueFromSlider)));
            Serves3Label.Text = mDay.mMeals [2].Serves.ToString();
        }

		public void SendWebRecipeToCookPageWrapper(int mealIndex)
		{
			EnterURLPopover.sCreateWebRecipeOnEnterUrl =  (async (string url) => 
				{
                    RecipeWeb.RecipeConstruct constructor = RecipeWeb.GetConstructor(NSUrl.FromString(url));
					RecipeWeb webRecipe;

					if (constructor != null)
					{
						webRecipe = constructor(url);

						if (await webRecipe.LoadRecipeHtml ())
						{
							mDay.mMeals[mealIndex] = new CalenderDay.Meal(webRecipe);
                            PBCCalendar.UpdateCalendar(mDay, mealIndex);

							RecipeURLPopover.Dismiss(false);
							AppDelegate.SwitchToCookPage(mDay.mMeals[mealIndex]);
						}
						else
						{
							Console.Error.WriteLine("Failed to load Url :" + webRecipe.Source);
						}
					}
				});
		}

        partial void DoubleTapRecipeImage1(NSObject sender)
        {
            if (mDay.mMeals.Count >= 1 && mDay.mMeals[0].Recipe is IRecipeImport)
            {
                if ((mDay.mMeals[0].Recipe is RecipeSpecialMeal) && (mDay.mMeals[0].Recipe as RecipeSpecialMeal).Type == SpecialMealType.WWWRecipe)
                {
                    RecipeURLPopover.PresentFromRect(RecipeImage1.Frame, this, UIPopoverArrowDirection.Left , true);
					SendWebRecipeToCookPageWrapper(0);
                }
                else
                {
                    if (!(mDay.mMeals[0].Recipe is RecipeSpecialMeal))
                        AppDelegate.SwitchToCookPage(mDay.mMeals[0]);
                }
            }
        } 

        partial void DoubleTapRecipeImage2(NSObject sender)
        {
			if (mDay.mMeals.Count >= 2 && mDay.mMeals[1].Recipe is IRecipeImport)
			{
				if ((mDay.mMeals[1].Recipe is RecipeSpecialMeal) && (mDay.mMeals[1].Recipe as RecipeSpecialMeal).Type == SpecialMealType.WWWRecipe)
				{
					RecipeURLPopover.PresentFromRect(RecipeImage2.Frame, this, UIPopoverArrowDirection.Left , true);
					SendWebRecipeToCookPageWrapper(1);
				}
				else
				{
					if (!(mDay.mMeals[1].Recipe is RecipeSpecialMeal))
						AppDelegate.SwitchToCookPage(mDay.mMeals[1]);
				}
			}
        }

        partial void DoubleTapRecipeImage3(NSObject sender)
        {
			if (mDay.mMeals.Count >= 3 && mDay.mMeals[2].Recipe is IRecipeImport)
			{
				if ((mDay.mMeals[2].Recipe is RecipeSpecialMeal) && (mDay.mMeals[2].Recipe as RecipeSpecialMeal).Type == SpecialMealType.WWWRecipe)
				{
					RecipeURLPopover.PresentFromRect(RecipeImage3.Frame, this, UIPopoverArrowDirection.Left , true);
					SendWebRecipeToCookPageWrapper(2);
				}
				else
				{
					if (!(mDay.mMeals[2].Recipe is RecipeSpecialMeal))
						AppDelegate.SwitchToCookPage(mDay.mMeals[2]);
				}
			}
        }
	}
}

