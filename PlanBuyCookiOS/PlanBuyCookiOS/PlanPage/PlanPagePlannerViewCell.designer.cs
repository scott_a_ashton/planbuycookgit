// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace PlanBuyCookiOS
{
	[Register ("PlanPagePlannerViewCell")]
	partial class PlanPagePlannerViewCell
	{
		[Outlet]
		UIKit.UIButton AddRecipeButton1 { get; set; }

		[Outlet]
		UIKit.UIButton AddRecipeButton2 { get; set; }

		[Outlet]
		UIKit.UIButton AddRecipeButton3 { get; set; }

		[Outlet]
		UIKit.UITextField CustomEntry1 { get; set; }

		[Outlet]
		UIKit.UITextField CustomEntry2 { get; set; }

		[Outlet]
		UIKit.UITextField CustomEntry3 { get; set; }

		[Outlet]
		UIKit.UILabel DayDate { get; set; }

		[Outlet]
		UIKit.UILabel DayTitle { get; set; }

		[Outlet]
		UIKit.UILabel DragYourRecipes1 { get; set; }

		[Outlet]
		UIKit.UILabel DragYourRecipes2 { get; set; }

		[Outlet]
		UIKit.UILabel DragYourRecipes3 { get; set; }

		[Outlet]
		UIKit.UIImageView PersonIcon1 { get; set; }

		[Outlet]
		UIKit.UIImageView PersonIcon2 { get; set; }

		[Outlet]
		UIKit.UIImageView PersonIcon3 { get; set; }

		[Outlet]
		UIKit.UIButton RecipeImage1 { get; set; }

		[Outlet]
		UIKit.UIButton RecipeImage2 { get; set; }

		[Outlet]
		UIKit.UIButton RecipeImage3 { get; set; }

		[Outlet]
		UIKit.UILabel RecipeName1 { get; set; }

		[Outlet]
		UIKit.UILabel RecipeName2 { get; set; }

		[Outlet]
		UIKit.UILabel RecipeName3 { get; set; }

		[Outlet]
		UIKit.UIButton Serves1 { get; set; }

		[Outlet]
		UIKit.UILabel Serves1Label { get; set; }

		[Outlet]
		UIKit.UIButton Serves2 { get; set; }

		[Outlet]
		UIKit.UILabel Serves2Label { get; set; }

		[Outlet]
		UIKit.UIButton Serves3 { get; set; }

		[Outlet]
		UIKit.UILabel Serves3Label { get; set; }

		[Outlet]
		UIKit.UIImageView ShoppedFor1 { get; set; }

		[Outlet]
		UIKit.UIImageView ShoppedFor2 { get; set; }

		[Outlet]
		UIKit.UIImageView ShoppedFor3 { get; set; }

		[Action ("AddRemoveRecipe1:")]
		partial void AddRemoveRecipe1 (Foundation.NSObject sender);

		[Action ("AddRemoveRecipe2:")]
		partial void AddRemoveRecipe2 (Foundation.NSObject sender);

		[Action ("AddRemoveRecipe3:")]
		partial void AddRemoveRecipe3 (Foundation.NSObject sender);

		[Action ("AdjustServes1:")]
		partial void AdjustServes1 (Foundation.NSObject sender);

		[Action ("AdjustServes2:")]
		partial void AdjustServes2 (Foundation.NSObject sender);

		[Action ("AdjustServes3:")]
		partial void AdjustServes3 (Foundation.NSObject sender);

		[Action ("DoubleTapRecipeImage1:")]
		partial void DoubleTapRecipeImage1 (Foundation.NSObject sender);

		[Action ("DoubleTapRecipeImage2:")]
		partial void DoubleTapRecipeImage2 (Foundation.NSObject sender);

		[Action ("DoubleTapRecipeImage3:")]
		partial void DoubleTapRecipeImage3 (Foundation.NSObject sender);
		
		void ReleaseDesignerOutlets ()
		{
			if (PersonIcon1 != null) {
				PersonIcon1.Dispose ();
				PersonIcon1 = null;
			}

			if (PersonIcon2 != null) {
				PersonIcon2.Dispose ();
				PersonIcon2 = null;
			}

			if (PersonIcon3 != null) {
				PersonIcon3.Dispose ();
				PersonIcon3 = null;
			}

			if (AddRecipeButton1 != null) {
				AddRecipeButton1.Dispose ();
				AddRecipeButton1 = null;
			}

			if (AddRecipeButton2 != null) {
				AddRecipeButton2.Dispose ();
				AddRecipeButton2 = null;
			}

			if (AddRecipeButton3 != null) {
				AddRecipeButton3.Dispose ();
				AddRecipeButton3 = null;
			}

			if (CustomEntry1 != null) {
				CustomEntry1.Dispose ();
				CustomEntry1 = null;
			}

			if (CustomEntry2 != null) {
				CustomEntry2.Dispose ();
				CustomEntry2 = null;
			}

			if (CustomEntry3 != null) {
				CustomEntry3.Dispose ();
				CustomEntry3 = null;
			}

			if (DayDate != null) {
				DayDate.Dispose ();
				DayDate = null;
			}

			if (DayTitle != null) {
				DayTitle.Dispose ();
				DayTitle = null;
			}

			if (DragYourRecipes1 != null) {
				DragYourRecipes1.Dispose ();
				DragYourRecipes1 = null;
			}

			if (DragYourRecipes2 != null) {
				DragYourRecipes2.Dispose ();
				DragYourRecipes2 = null;
			}

			if (DragYourRecipes3 != null) {
				DragYourRecipes3.Dispose ();
				DragYourRecipes3 = null;
			}

			if (RecipeImage1 != null) {
				RecipeImage1.Dispose ();
				RecipeImage1 = null;
			}

			if (RecipeImage2 != null) {
				RecipeImage2.Dispose ();
				RecipeImage2 = null;
			}

			if (RecipeImage3 != null) {
				RecipeImage3.Dispose ();
				RecipeImage3 = null;
			}

			if (RecipeName1 != null) {
				RecipeName1.Dispose ();
				RecipeName1 = null;
			}

			if (RecipeName2 != null) {
				RecipeName2.Dispose ();
				RecipeName2 = null;
			}

			if (RecipeName3 != null) {
				RecipeName3.Dispose ();
				RecipeName3 = null;
			}

			if (Serves1 != null) {
				Serves1.Dispose ();
				Serves1 = null;
			}

			if (Serves1Label != null) {
				Serves1Label.Dispose ();
				Serves1Label = null;
			}

			if (Serves2 != null) {
				Serves2.Dispose ();
				Serves2 = null;
			}

			if (Serves2Label != null) {
				Serves2Label.Dispose ();
				Serves2Label = null;
			}

			if (Serves3 != null) {
				Serves3.Dispose ();
				Serves3 = null;
			}

			if (Serves3Label != null) {
				Serves3Label.Dispose ();
				Serves3Label = null;
			}

			if (ShoppedFor1 != null) {
				ShoppedFor1.Dispose ();
				ShoppedFor1 = null;
			}

			if (ShoppedFor2 != null) {
				ShoppedFor2.Dispose ();
				ShoppedFor2 = null;
			}

			if (ShoppedFor3 != null) {
				ShoppedFor3.Dispose ();
				ShoppedFor3 = null;
			}
		}
	}
}
