using System;
using System.Text;
using CoreGraphics;
using Foundation;
using UIKit;
using CoreImage;


namespace PlanBuyCookiOS
{
	[Register("PlanPageView")]
	public partial class PlanPageView : UIView
	{
		readonly float IngredientFontSize = 13.0f;
		readonly float IngredientsListTitleFontSize = 14.0f;
		readonly float RecipeTitleFontSize = 28.0f;
		readonly float RecipeInfoFontSize = 13.0f;
		readonly float TipFontSize = 15.0f;

		bool mPlannerOut = true;


		public PlanPageView ()
		{
		}

		public PlanPageView(IntPtr handle) : base(handle)
		{

		}

		public UIView RecipeListContainer
		{
			get { return PlanPageRecipeListContainer; }
		}

		public UITableView CalenderView
		{
			get { return PlanPageCalenderView; }
		}

		public UIButton GetSettingsButton
		{
			get { return SettingsButton; }
		}
		public UIButton GetCookButton
		{
			get { return CookButton; }
		}

		//The view that conains all the buttons at the top of the screen (PLAN,BUY,COOK, and Settings)
		public UIView GetTopButtonFrameView
		{
			get { return TopButtonFrame; }
		}

		//helper method
		public void EnumerateFonts()
		{
			var fontList = new StringBuilder();
			var familyNames = UIFont.FamilyNames;
			foreach (var familyName in familyNames ){
				fontList.Append(String.Format("Family: {0}\n", familyName));
				Console.WriteLine("Family: {0}\n", familyName);
				var fontNames = UIFont.FontNamesForFamilyName(familyName);
				foreach (var fontName in fontNames ){
					Console.WriteLine("\tFont: {0}\n", fontName);
					fontList.Append(String.Format("\tFont: {0}\n", fontName));
				}
			};
			Console.WriteLine(fontList.ToString());
		}

		public void LoadFonts()
		{
			MealSearchBox.LeftViewMode = UITextFieldViewMode.UnlessEditing;
		}

		public void InitialiseCalenderSource()
		{
			PlanPageCalenderView.Source = new PlanPageCalenderViewSource ();

            ResetCalendarView();
		}

        public void ResetCalendarView()
        {
            PlanPageCalenderView.SelectRow (NSIndexPath.FromRowSection (PBCCalendar.DateRange, 0), false, UITableViewScrollPosition.Top);
            PlanPageCalenderView.ReloadData ();
        }

		partial void TogglePlannerPanel (NSObject sender)
		{
			if (mPlannerOut)
			{
				UIView.Animate (0.3, 0, UIViewAnimationOptions.CurveEaseOut,
					() => { PlanPageCalenderView.Superview.Center = new CGPoint (UIScreen.MainScreen.Bounds.Left - PlanPageCalenderView.Superview.Frame.Width/2 + 32, PlanPageCalenderView.Superview.Center.Y);},
					() => {	TogglePlannerButton.Selected = !mPlannerOut;  }
				);
			}
			else
			{
				UIView.Animate (0.3, 0, UIViewAnimationOptions.CurveEaseOut,
					() => {	PlanPageCalenderView.Superview.Center = new CGPoint (UIScreen.MainScreen.Bounds.Left + PlanPageCalenderView.Superview.Frame.Width/2 + 32, PlanPageCalenderView.Superview.Center.Y);},
					() => {	TogglePlannerButton.Selected = !mPlannerOut;}
				);
			}
			mPlannerOut = !mPlannerOut;
		}

		partial void SwitchToCookPage (NSObject sender)
		{
			AppDelegate.SwitchToCookPage();
		}

		partial void SwitchToBuyPage (NSObject sender)
		{
			AppDelegate.SwitchToBuyPage();
		}
			
        partial void OpenSettings(NSObject sender)
        {
            AppDelegate.OpenSettings(this, sender as UIButton );
        }
	}
}

