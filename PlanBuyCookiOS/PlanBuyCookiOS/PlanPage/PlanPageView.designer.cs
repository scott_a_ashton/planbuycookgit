// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace PlanBuyCookiOS
{
	partial class PlanPageView
	{
		[Outlet]
		UIKit.UIButton CookButton { get; set; }

		[Outlet]
		UIKit.UITextField MealSearchBox { get; set; }

		[Outlet]
		UIKit.UITableView PlanPageCalenderView { get; set; }

		[Outlet]
		UIKit.UIView PlanPageRecipeListContainer { get; set; }

		[Outlet]
		UIKit.UIButton SettingsButton { get; set; }

		[Outlet]
		UIKit.UIButton TogglePlannerButton { get; set; }

		[Outlet]
		UIKit.UIView TopButtonFrame { get; set; }

		[Action ("OpenSettings:")]
		partial void OpenSettings (Foundation.NSObject sender);

		[Action ("SwitchToBuyPage:")]
		partial void SwitchToBuyPage (Foundation.NSObject sender);

		[Action ("SwitchToCookPage:")]
		partial void SwitchToCookPage (Foundation.NSObject sender);

		[Action ("TogglePlannerPanel:")]
		partial void TogglePlannerPanel (Foundation.NSObject sender);
		
		void ReleaseDesignerOutlets ()
		{
			if (MealSearchBox != null) {
				MealSearchBox.Dispose ();
				MealSearchBox = null;
			}

			if (PlanPageCalenderView != null) {
				PlanPageCalenderView.Dispose ();
				PlanPageCalenderView = null;
			}

			if (PlanPageRecipeListContainer != null) {
				PlanPageRecipeListContainer.Dispose ();
				PlanPageRecipeListContainer = null;
			}

			if (SettingsButton != null) {
				SettingsButton.Dispose ();
				SettingsButton = null;
			}

			if (TogglePlannerButton != null) {
				TogglePlannerButton.Dispose ();
				TogglePlannerButton = null;
			}

			if (TopButtonFrame != null) {
				TopButtonFrame.Dispose ();
				TopButtonFrame = null;
			}

			if (CookButton != null) {
				CookButton.Dispose ();
				CookButton = null;
			}
		}
	}
}
