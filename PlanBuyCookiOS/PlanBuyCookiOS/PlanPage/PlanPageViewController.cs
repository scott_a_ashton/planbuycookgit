using System;
using System.Collections.Generic;
using CoreGraphics;
using Foundation;
using UIKit;
using CoreData;


namespace PlanBuyCookiOS
{

	public partial class PlanPageViewController : UIViewController
	{
		class GestureDelegate : UIGestureRecognizerDelegate
		{
			PlanPageViewController controller;

			public GestureDelegate (PlanPageViewController controller)
			{
				this.controller = controller;
			}

			public override bool ShouldReceiveTouch(UIGestureRecognizer aRecogniser, UITouch aTouch)
			{
				return true;
			}

			// Ensure that the pinch, pan and rotate gestures are all recognized simultaneously
			public override bool ShouldRecognizeSimultaneously (UIGestureRecognizer gestureRecognizer, UIGestureRecognizer otherGestureRecognizer)
			{
				//TODO: Check that the drag is on one of the recipe icons

				// if the gesture recognizers views differ, don't recognize
				if (gestureRecognizer.View != otherGestureRecognizer.View)
					return false;

				if (!(gestureRecognizer is UILongPressGestureRecognizer))
					return false;

				// if either of the gesture recognizers is a long press, don't recognize
				if ((gestureRecognizer is UILongPressGestureRecognizer && otherGestureRecognizer is UIPanGestureRecognizer))
					return true;

				return true;
			}
		}
            
		PlanPageMosaicRecipeListController mMosaicCollectionViewController;
		PlanPageMosaicLayout mMosaicLayout;
        LoadingOverlay mBusyIndicator;

		public PlanPageViewController () : base ("PlanPageViewController", null)
		{
		}

		public override void DidReceiveMemoryWarning ()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning ();
		}

        public IntroView IntroView
        {
            get
            {
                return IntroMovieView;
            }
        }

		public PlanPageView PlanPageView
		{
			get
			{
				return View as PlanPageView;
			}
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();


		}

		public void CreateMosaicLayout()
		{
            (View as PlanPageView).CalenderView.Hidden = false;

            mMosaicLayout = new PlanPageMosaicLayout ((View as PlanPageView).RecipeListContainer);
            mMosaicLayout.LoadXml ();
            mMosaicLayout.ExtractLayout ();

			mMosaicCollectionViewController = new PlanPageMosaicRecipeListController (mMosaicLayout, (View as PlanPageView).RecipeListContainer);

			(View as PlanPageView).RecipeListContainer.AddSubview (mMosaicCollectionViewController.View);

            // Perform any additional setup after loading the view, typically from a nib.
            (View as PlanPageView).InitialiseCalenderSource ();
		}


		public void ShowTutorial()
		{
			HelpManager.Reset ();

			if (HelpManager.State == HelpManager.HelpState.InitialHelpMessages) 
			{
				HelpManager.PresentPopover (PlanPageView.GetSettingsButton.Frame, PlanPageView.GetTopButtonFrameView);
				HelpManager.PresentAltPopover (PlanPageView.GetCookButton.Frame, PlanPageView.GetTopButtonFrameView);
			}
		}

		public void RefreshMosaicView()
		{
			mMosaicCollectionViewController.CollectionView.ReloadData ();
		}

		public void RefreshCalendarView()
		{
			(View as PlanPageView).CalenderView.ReloadData ();
		}

        public void ResetCalendarViewPosition()
        {
            (View as PlanPageView).ResetCalendarView();
        }

		public void BackgroundRefreshCalendarView()
		{
			InvokeOnMainThread (() => {	AppDelegate.PlanPageViewController.RefreshCalendarView ();	});
		}

        public void ScrollToTopOfMosaic()
        {
            mMosaicCollectionViewController.CollectionView.ScrollToItem(NSIndexPath.FromItemSection(0, 0), UICollectionViewScrollPosition.Top, false);
        }

		public int GetSelectedRecipe()
		{
            if (mMosaicCollectionViewController.SelectedRecipeIndex < 0)
                return 0;

			return mMosaicCollectionViewController.SelectedRecipeIndex;
		}
			
		public List<DropTarget> CalculateDropTargets()
		{
			UITableViewCell[] visibleCells = (View as PlanPageView).CalenderView.VisibleCells;

			List<DropTarget> dropTargets = new List<DropTarget> ();

			foreach (UITableViewCell cell in visibleCells) 
			{
				PlanPagePlannerViewCell planPageCell = cell as PlanPagePlannerViewCell;

				if (!planPageCell.GetRecipeImage (0).Hidden)
					dropTargets.Add (planPageCell.GetDropTarget(0));
				if (!planPageCell.GetRecipeImage (1).Hidden)
					dropTargets.Add (planPageCell.GetDropTarget(1));
				if (!planPageCell.GetRecipeImage (2).Hidden)
					dropTargets.Add (planPageCell.GetDropTarget(2));
			}

			return dropTargets;
		}

		public List<ShoppingListMeal> GetMealPlanForShoppingList()
		{
			return ((View as PlanPageView).CalenderView.Source as PlanPageCalenderViewSource).GetMealPlanForShoppingList();
		}

        public void PlayMovie()
        {
            View.BringSubviewToFront(IntroMovieView);
            //(IntroMovieView as IntroView).OnMovieComplete(HideMovie);
            (IntroMovieView as IntroView).PlayMovie(HideMovie);
        }

        public void HideMovie(NSNotification movieHasEnded)
        {
            HideMovie();
        }

        public void HideMovie()
        {
            UIView.Animate (0.5f, 0, UIViewAnimationOptions.CurveEaseOut,
                () => {IntroView.Frame = new CGRect(-1024,0,1024,768);},
                () => {IntroView.Frame = new CGRect(-1024,0,1024,768);}
            );
            UIView.Animate (0.5f, 0, UIViewAnimationOptions.CurveEaseOut,
                () => {Splash.Frame = new CGRect(-1024,0,1024,768);},
				() => {Splash.Frame = new CGRect(-1024,0,1024,768); ShowTutorial();}
            );
        }
        public void PresentBusyIndicator()
        {
            mBusyIndicator = new LoadingOverlay(View.Frame, bLandscape: true);
            View.AddSubview(mBusyIndicator);
        }

        public void HideBusyIndicator()
        {
            mBusyIndicator.Hide();
        }
	}
}

