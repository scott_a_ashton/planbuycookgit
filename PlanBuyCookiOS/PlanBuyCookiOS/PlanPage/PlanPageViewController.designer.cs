// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace PlanBuyCookiOS
{
	[Register ("PlanPageViewController")]
	partial class PlanPageViewController
	{
		[Outlet]
		PlanBuyCookiOS.IntroView IntroMovieView { get; set; }

		[Outlet]
		UIKit.UIImageView Splash { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (IntroMovieView != null) {
				IntroMovieView.Dispose ();
				IntroMovieView = null;
			}

			if (Splash != null) {
				Splash.Dispose ();
				Splash = null;
			}
		}
	}
}
