using System;
using UIKit;
using Foundation;
using CoreGraphics;

namespace PlanBuyCookiOS
{
    [Register("SearchRecipeTextField")]
    public partial class SearchRecipeTextField : UITextField
    {
        UIImageView mSearchIcon;

        public SearchRecipeTextField ()
        {
        }

        public SearchRecipeTextField(IntPtr handle) : base(handle)
        {
            Delegate = new SearchRecipeDelegate();

            mSearchIcon = new UIImageView(UIImage.FromFile("SearchIcon.png"));
            mSearchIcon.Frame = new CGRect(0, 0, 16, 16);
            mSearchIcon.Alpha = 0.3f;
            LeftView = mSearchIcon;
            LeftViewMode = UITextFieldViewMode.Always;
        }

        public override CGRect LeftViewRect(CGRect forBounds)
        {
            CGRect baseRect = base.LeftViewRect(forBounds);
            return new CGRect(baseRect.Location.X + 5, baseRect.Location.Y, baseRect.Width, baseRect.Height);
        }
    }

    public class SearchRecipeDelegate : UITextFieldDelegate
    {
        //Gets rid of the keyboard
        public override bool ShouldReturn(UITextField textField)
        {
            // NOTE: Don't call the base implementation on a Model class
            // see http://docs.xamarin.com/guides/ios/application_fundamentals/delegates,_protocols,_and_events
            textField.ResignFirstResponder();
            RecipeManager.Instance.Search(textField.Text);
            AppDelegate.PlanPageViewController.RefreshMosaicView();
            AppDelegate.PlanPageViewController.ScrollToTopOfMosaic();

            return true;
        }

        public override bool ShouldEndEditing(UITextField textField)
        {
            // NOTE: Don't call the base implementation on a Model class
            // see http://docs.xamarin.com/guides/ios/application_fundamentals/delegates,_protocols,_and_events
            return true;
        }

        public override bool ShouldClear(UITextField textField)
        {
            RecipeManager.Instance.Search("");
            AppDelegate.PlanPageViewController.RefreshMosaicView();

            return true;
        }

        public override bool ShouldBeginEditing(UITextField textField)
        {
            return true;
        }
    }
}

