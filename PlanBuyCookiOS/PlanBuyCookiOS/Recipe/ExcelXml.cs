using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.IO;
using System.Diagnostics;

namespace PlanBuyCookiOS
{
	public class ExcelXml
	{
		public class WorkSheet
		{
			int mRowCount = 0;
			int mColumnCount = 0;
			string mName;
			string[,] mData;

			public int RowCount
			{
				get { return mRowCount; }
			}
			public int ColumnbCount
			{
				get { return mColumnCount; }
			}
			public string[,] Data 
			{
				get { return mData; }
			}

			public WorkSheet(string worksheet, string[,] data, int rowCount, int columnCount)
			{
				mName = worksheet;
				mData = data;
				mRowCount = rowCount;
				mColumnCount = columnCount;
			}

			public string[] GetRow(int rowNumber)
			{
				string[] row = new string[mColumnCount];

				for (int cell = 0; cell < mColumnCount; cell++)
					row[cell] = mData[cell, rowNumber];

				return row;
			}

			public string[] GetColumn(int columnNumber)
			{
				string[] column = new string[mRowCount];

				for (int rowIndex = 0; rowIndex < mRowCount; rowIndex++)
					column[rowIndex] = mData[columnNumber, rowIndex];

				return column;
			}

			public string[] GetColumn(int columnNumber, int minRow, int maxRow)
			{

				string[] column = new string[maxRow-minRow];

				for (int rowIndex = minRow; rowIndex < maxRow; rowIndex++)
					column[rowIndex-minRow] = mData[columnNumber, rowIndex];

				return column;
			}

			public string[] GetRow(int rowNumber, int minColum, int maxColumn)
			{
				string[] row = new string[maxColumn-minColum];

				for (int cellIndex = minColum; cellIndex < maxColumn; cellIndex++)
					row[cellIndex] = mData[cellIndex, rowNumber];

				return row;
			}
			public int SearchForStringInColumn(string searchString, int columnIndex)
			{
				return GetColumn(columnIndex).Select ((item, indexer) => new {str = item, index = indexer}).
											Where (item => item.str.ToLower () == searchString.ToLower ()).
											Select (item => item.index).
											FirstOrDefault ();
			}
		}

		protected string mFilename;

		public ExcelXml (string filename)
		{
			mFilename = filename;
		}

		protected XDocument mXml = null;
		protected bool mbXmlLoaded = false;

		public string XmlPath
		{
			get
			{ 
				return mFilename;
			}
		}

		public XDocument Xml
		{
			get
			{
				if (!mbXmlLoaded) 
					LoadXml();

				return mXml;
			}
		}

		void LoadXml()
		{
			try
			{
				mXml = XDocument.Load (XmlPath); 

				mbXmlLoaded = true;
			}
			catch (Exception e)
			{
				Console.WriteLine(e.Message);
			}
		}

		public bool IsInitialised
		{
			get
			{
				return mbXmlLoaded;
			}
		}

		public WorkSheet LoadWorksheet(string worksheetName)
		{
			try
			{
				XNamespace ns = Xml.Root.FirstAttribute.Value;
				XElement workbookElement = Xml.Element(ns+"Workbook");
				XElement worksheetElement = workbookElement.Elements(ns+"Worksheet").Where(sheet => sheet.Attribute(ns+"Name").Value==worksheetName).First();
				XElement table = worksheetElement.Elements(ns+"Table").First();

				//Read the column count from the Xml (min 1)
				int columnCount = 1;
				int.TryParse(table.Attribute(ns+"ExpandedColumnCount").Value, out columnCount);
				Console.WriteLine("column Count" + columnCount);

				//Read the row count from the Xml (min 1)
				int rowCount = 1;
				int.TryParse(table.Attribute(ns+"ExpandedRowCount").Value, out rowCount);

				//get all the rows
				XElement[] rows = table.Elements(ns+"Row").ToArray();

				//Create a Units string table to dump the Xcel values into
				string[,] worksheet = new string[columnCount,rowCount];

				//Initialise all the string elements
				for (int row=0; row < rowCount; row++)
					for (int cell=0; cell < columnCount; cell++)
						worksheet[cell, row] = "";

				int rowIndex = 1;
				int minColumnIndex = 0;
				int internalColumnCount = 0;

				foreach (XElement row in rows)
				{
					XElement[] cells = row.Elements(ns+"Cell").ToArray();

					//Read the row index if it's stored as an attribute
					if (row.Attributes(ns+"Index").Any())
					{
						if (!int.TryParse(row.Attribute(ns+"Index").Value, out rowIndex))
							Console.WriteLine ("Failed parsing row index ");
					}

					//Iterate through the cells in the Xml
					for (int index = 0; index < cells.Length; index++)
					{
						int cellIndex = internalColumnCount;

						//Grab data from the cell
						string cellValue = cells[index].Elements(ns+"Data").Any() ? cells[index].Element(ns+"Data").Value : "";

						if (cells[index].Attributes(ns+"Index").Any())
						{
							internalColumnCount = 0;
							if (int.TryParse(cells[index].Attribute(ns+"Index").Value, out cellIndex))
							{
								worksheet[cellIndex-1, rowIndex-1] = cellValue;
								minColumnIndex = cellIndex;
							}
						}
						else
						{
							worksheet[minColumnIndex + internalColumnCount, rowIndex-1] = cellValue;
							internalColumnCount++;
						}

					}
					rowIndex++;
				}
				return new WorkSheet(worksheetName, worksheet, rowCount, columnCount);
			}
			catch (Exception e)
			{
				Console.WriteLine ("Failed to load worksheet " + worksheetName);
				Console.WriteLine(e.Message);
			}

			return null;
		}		
	}
}

