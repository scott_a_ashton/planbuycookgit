using System;

namespace PlanBuyCookiOS
{
    public class FormattedIngredientString
    {
        public FormattedIngredientString()
        {
        }

        public static string CreateNumericalString(RoundingRules.RoundedAmount rounded)
        {
            //Unless we're displaying litres or kilograms then convert to fractions
            if (rounded.mUnit == "kg" || rounded.mUnit == "kilogram" || rounded.mUnit == "l" || rounded.mUnit == "litre")
            {
                return rounded.mQuantity.ToString();
            }

            float amount = rounded.mQuantity;
            float frac = amount - (float)Math.Floor(amount);
            string fracString = "";

            if (frac == 0.25f)
                fracString = "¼";
            else if (frac == 0.5f)
                fracString = "½";
            else if (frac == 0.75f)
                fracString = "¾";

            if (Math.Floor(amount) != 0.0f)
                return Math.Floor(amount).ToString() + fracString;
            return fracString;
        }


        public static string IngredientsString(string food, RoundingRules.RoundedAmount rounded)
        {
            float ingredientQuantity = rounded.mQuantity;
            string ingredientUnit = rounded.mUnit;

            string quantityString = ingredientQuantity == 0 ? "" : FormattedIngredientString.CreateNumericalString(rounded);
            string unitString = "";
            string foodString = food;
            string ingredientString = "";

            RecipeUnit recipeUnit = MeasurementUnits.GetRecipeUnit(rounded.mUnit);

            MasterIngredientsList.Ingredient masterIngredient = MasterIngredientsList.FindIngredient(food);
 
            if (recipeUnit == null)
                recipeUnit = MeasurementUnits.GetRecipeUnit("");

            unitString = ConstructUnitString(ingredientQuantity, ingredientUnit, recipeUnit);

            if (quantityString != "")
                ingredientString += quantityString + " ";

            if (unitString != "")
                ingredientString += unitString + " ";

            if (ingredientQuantity > 1 && masterIngredient!=null && masterIngredient.Plural != "")
                foodString = masterIngredient.Plural;

            ingredientString += foodString;

            return ingredientString;
        }

        public static string ConstructUnitString(float ingredientQuantity, string ingredientUnit, RecipeUnit recipeUnit)
        {
            string unitString = "";

            if (ingredientQuantity == 1 && ingredientUnit != "number" && recipeUnit.Unit != "")
                unitString = recipeUnit.Unit + " of";
            else if (ingredientQuantity > 1 && ingredientUnit != "number" && recipeUnit.Unit != "")
                unitString = recipeUnit.Unit + " of";
            else if (ingredientQuantity > 1 && ingredientUnit != "number" && recipeUnit.Unit == "" && recipeUnit.Plural != "")
                unitString = recipeUnit.Plural + " of";
            else if (ingredientQuantity >= 1 && (ingredientUnit == "number" || ingredientUnit == ""))
                unitString = "";
            else if (ingredientQuantity < 1 && (ingredientUnit == "number" || ingredientUnit == ""))
                unitString = "";
            else if (ingredientQuantity == 0 && (ingredientUnit == "number" || ingredientUnit == ""))
                unitString = "";
            else
                unitString = ingredientUnit + " of";

            return unitString;
        }

        public static string IngredientsStringNoQuantity(ShoppingListIngredient ingredient)
        {
            MasterIngredientsList.Ingredient masterIngredient = MasterIngredientsList.FindIngredient(ingredient.mFood);

            if (masterIngredient == null)
                Console.Error.WriteLine(ingredient.mFood + " not found in master ingredients list");

            RoundingRules.RoundedAmount roundedAmount = RoundingRules.RoundingRule (masterIngredient.RoundingRule, ingredient);

            float ingredientQuantity = roundedAmount.mQuantity;
            string quantityString = ingredientQuantity == 0 ? "" : FormattedIngredientString.CreateNumericalString(roundedAmount);
            string unitString = "";
            string foodString = ingredient.mFood;
            string ingredientString = "";

            RecipeUnit recipeUnit = MeasurementUnits.GetRecipeUnit((ingredient.mUnit != roundedAmount.mUnit) ?  roundedAmount.mUnit : ingredient.mUnit);

            if (recipeUnit == null)
                recipeUnit = MeasurementUnits.GetRecipeUnit("");

            unitString = FormattedIngredientString.ConstructUnitString(ingredientQuantity, roundedAmount.mUnit, recipeUnit);

//            if (quantityString != "")
//                ingredientString += quantityString + " ";

            if (unitString != "")
                ingredientString += unitString + " ";

            if (ingredientQuantity > 1 && (masterIngredient != null && masterIngredient.Plural != ""))
                foodString = masterIngredient.Plural;

            ingredientString += foodString;
            return ingredientString;
        }

        public static string IngredientsString(ShoppingListIngredient ingredient)
        {
            MasterIngredientsList.Ingredient masterIngredient = MasterIngredientsList.FindIngredient(ingredient.mFood);

            if (masterIngredient == null)
                Console.Error.WriteLine(ingredient.mFood + " not found in master ingredients list");

            RoundingRules.RoundedAmount roundedAmount = RoundingRules.RoundingRule ((masterIngredient==null)?0:masterIngredient.RoundingRule, ingredient);

            float ingredientQuantity = roundedAmount.mQuantity;
            string quantityString = ingredientQuantity == 0 ? "" : FormattedIngredientString.CreateNumericalString(roundedAmount);
            string unitString = "";
            string foodString = ingredient.mFood;
            string ingredientString = "";

            RecipeUnit recipeUnit = MeasurementUnits.GetRecipeUnit((ingredient.mUnit != roundedAmount.mUnit) ?  roundedAmount.mUnit : ingredient.mUnit);

            if (recipeUnit == null)
                recipeUnit = MeasurementUnits.GetRecipeUnit("");

            unitString = FormattedIngredientString.ConstructUnitString(ingredientQuantity, roundedAmount.mUnit, recipeUnit);

            if (quantityString != "")
                ingredientString += quantityString + " ";

            if (unitString != "")
                ingredientString += unitString + " ";

            if (ingredientQuantity > 1 && (masterIngredient != null && masterIngredient.Plural != ""))
                foodString = masterIngredient.Plural;

            ingredientString += foodString;
            return ingredientString;
        }
    }
}

