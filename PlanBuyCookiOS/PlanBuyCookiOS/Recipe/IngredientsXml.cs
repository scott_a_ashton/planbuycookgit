using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Diagnostics;

namespace PlanBuyCookiOS
{
	public class IngredientsXml : PlanBuyCookSharedData
	{
		public class Ingredient
		{
			public string mFood;
			public string mPlural;
			public string mCategory;
			public string mAisle;
			public string mEnergy;
			public string mPerUnit;
		}

		//we load the ingredients initially as a table before parsing the data into the ingredients dictionary
		string[,] 									mIngredientsTable;
		protected Dictionary<string, Ingredient> 	mIngredients = new Dictionary<string, Ingredient>();
		int mStartingRow = 2;
		int mTitleRow = 3;
		int mStartingColumn = 1;

		public IngredientsXml () : base()
		{ 

		} 

		public override void Extract()
		{
			mIngredientsTable = LoadWorksheet ("Ingredients");

			try
			{

				string[] rowString1 = GetRow(mTitleRow, mIngredientsTable);

				//Check the structure of the worksheet
				Debug.Assert (mIngredientsTable [mStartingColumn, mTitleRow] == "food", "Food Column has moved in ingredients worksheet" + mIngredientsTable [1, mTitleRow]);
				Debug.Assert (mIngredientsTable [mStartingColumn+1, mTitleRow] == "plural", "Column has moved in ingredients worksheet");
				Debug.Assert (mIngredientsTable [mStartingColumn+2, mTitleRow] == "category", "Column has moved in ingredients worksheet");
				Debug.Assert (mIngredientsTable [mStartingColumn+3, mTitleRow] == "shopping aisle", "Column has moved in ingredients worksheet");
				Debug.Assert (mIngredientsTable [mStartingColumn+4, mTitleRow] == "energy", "Column has moved in ingredients worksheet");
				Debug.Assert (mIngredientsTable [mStartingColumn+5, mTitleRow] == "per unit", "Column has moved in ingredients worksheet");

				for (int row = mStartingRow; row < mIngredientsTable.GetLength(1); row++) 
				{
					Ingredient ingredient = new Ingredient ();

					string[] rowString = GetRow(row, mIngredientsTable);

					foreach (string s in rowString)
						Console.Write(s+" ");

					Console.WriteLine("");

					ingredient.mFood = mIngredientsTable [mStartingColumn+0, row];
					ingredient.mPlural = mIngredientsTable [mStartingColumn+1, row];
					ingredient.mCategory = mIngredientsTable [mStartingColumn+2, row];
					ingredient.mAisle = mIngredientsTable [mStartingColumn+3, row];
					ingredient.mEnergy = mIngredientsTable [mStartingColumn+4, row];
					ingredient.mPerUnit = mIngredientsTable [mStartingColumn+5, row];

					//Ignore empty rows
					if (ingredient.mFood.Length == 0)
						continue;

					if (mIngredients.ContainsKey(ingredient.mFood))
						throw new Exception("Ingredients contains duplicate keys :" + ingredient.mFood);
					else
						mIngredients.Add(ingredient.mFood, ingredient);
				}
			}

			catch (Exception e) 
			{
				Console.WriteLine (e.Message);
			}

		}

	}
}
