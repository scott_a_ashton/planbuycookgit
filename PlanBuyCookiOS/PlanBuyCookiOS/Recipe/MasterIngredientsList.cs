using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Diagnostics;
using System.Threading.Tasks;
using SQLite;
using System.IO;
using Parse;


//This class is our Master Ingredients List
//The list cab be loaded either from the legacy Xml format or from Parse.  It's likely that the Xml will be totally phased out at some point
//
namespace PlanBuyCookiOS
{
	public class MasterIngredientsList : PlanBuyCookSharedData
	{
		public enum Territory
		{
			Australia,
			UnitedKingdom,
			UnitedStates
		}


		public class Ingredient
		{
			[PrimaryKey, AutoIncrement]
			public int Id { get; set; }
			
			[Unique]
			public string ParseId {set;get;}
			[Unique]
			public string Name { set; get; }
			public string Plural{ set; get; }
			public string Section{ set; get; }
			public int RoundingRule { set; get; }
			public string RoundingRuleWords { set; get; }
			public string Aisle{ set; get; }
		}

        private SQLiteAsyncConnection mIngredientsDb;

        public bool mGenerateNewDatabase = false;

        static MasterIngredientsList sInstance = null;

		//we load the ingredients initially as a table before parsing the data into the ingredients dictionary
		string[,] 										mIngredientsTableXml;
		public static Dictionary<string, Ingredient> 	mIngredients = new Dictionary<string, Ingredient>();

		public static string[] mSections;
		public static string[] mAisles;

		public delegate void OnFinishedLoadingIngredients();
		public static OnFinishedLoadingIngredients mOnFinishedLoadingIngredients;

		int mStartingRow = 1;
		int mTitleRow = 0;
		int mStartingColumn = 1;

		List<ParseObject> mAllParseObjects;

		public MasterIngredientsList () : base()
		{ 
		} 

		public static bool IsInitialised
		{
			set;
			get;
		}

        public static MasterIngredientsList Instance
        {
            get
            {
                if (sInstance == null)
                {
                    sInstance = new MasterIngredientsList();
                }
                return sInstance;
            }
        }

		public static List<Ingredient> Ingredients
		{
			get{ return mIngredients.Values.ToList (); }
		}

		public static Ingredient FindIngredient(string name)
		{
            string lowerTrimName = name.ToLower().Trim();
			Ingredient foundIngredient;

            if (!mIngredients.TryGetValue(lowerTrimName, out foundIngredient))
            {
                //Search plurals
                foreach (Ingredient ingredient in mIngredients.Values)
                {
                    if (ingredient.Plural == lowerTrimName)
                        return ingredient;
                }
            }

			return foundIngredient;
		}


		public static string[] Sections
		{
			get
			{ 
                if (mSections == null)
                {
                    mSections = new string[]{"fruit & veg", "pantry & bakery", "meat & fish", "fridge, freezer & deli", "household"};
                }

				return mSections;
			}
		}
		public static string[] Aisles
		{
			get
			{ 
				if (mAisles == null)
					mAisles = mIngredients.Values.ToList ().Select(mi => mi.Aisle).Distinct().ToArray(); 
				return mAisles;
			}
		}

		public static int GetSectionIndex(string name)
		{
			return Array.FindIndex (Sections, s => s == name);
		}


		public async Task Initialise()
		{
            if (mGenerateNewDatabase)
                mIngredientsDb = new SQLiteAsyncConnection (Path.Combine (Environment.GetFolderPath (Environment.SpecialFolder.Personal), "MasterIngredients.db"));
            else
                mIngredientsDb = new SQLiteAsyncConnection ("MasterIngredients.db");

			try
			{
				if (mIngredientsDb != null)
					mIngredientsDb.CreateTableAsync<Ingredient> ().Wait();

				//TODO: Check if the local copy of the database exists first, and if it contains anything
                //Extract();
                if (mGenerateNewDatabase)
                    ExtractFromParse();   

                await ExtractIngredientsFromDatabase();
			}
			catch (Exception e) {
				Console.Error.WriteLine ("Failed to connect to database");
			}
		}

		public async void ExtractFromParse()
		{
			if (IsInitialised)
				return;

			bool bHasDownloaded = await DownloadFromParse();

			if (bHasDownloaded)
				await CreateLocalMasterIngredientsDb ();
			else
				Console.Error.WriteLine ("Failed to download master ingredients list from Parse.");
		}


		public async Task ExtractIngredientsFromDatabase()
		{
			var ingredients = await mIngredientsDb.Table<Ingredient> ().ToListAsync ();

			foreach (var ingredient in ingredients)
			{
                mIngredients.Add (ingredient.Name, ingredient);
			}

			if (mOnFinishedLoadingIngredients != null)
				mOnFinishedLoadingIngredients ();

			IsInitialised = true;
		}


		void AddIngredientToDb (Ingredient ingredient)
		{
			try
			{
				mIngredientsDb.InsertAsync (ingredient);
			}
			catch (Exception e) 
			{
				Console.Error.WriteLine ("Failed to add ingredient " + ingredient.Name + " to database.");
			}
		}


		public override void Extract()
		{
			if (IsInitialised)
				return;

			mIngredientsTableXml = LoadWorksheet ("Ingredients");

			try
			{
				string[] rowString1 = GetRow(mTitleRow, mIngredientsTableXml);

				//Check the structure of the worksheet
				Debug.Assert (mIngredientsTableXml [mStartingColumn, mTitleRow] == "AU ingredients", "AU Ingredients Column has moved in ingredients worksheet" + mIngredientsTableXml [1, mTitleRow]);
				Debug.Assert (mIngredientsTableXml [mStartingColumn+1, mTitleRow] == "plural", "plural Column has moved in ingredients worksheet");
				Debug.Assert (mIngredientsTableXml [mStartingColumn+2, mTitleRow] == "AU section", "AU section Column has moved in ingredients worksheet");
				Debug.Assert (mIngredientsTableXml [mStartingColumn+3, mTitleRow] == "AU aisle", "AU isle Column has moved in ingredients worksheet");

				for (int row = mStartingRow; row < mIngredientsTableXml.GetLength(1); row++) 
				{
					Ingredient ingredient = new Ingredient ();

					string[] rowString = GetRow(row, mIngredientsTableXml);

					ingredient.Name = mIngredientsTableXml [mStartingColumn+0, row];
					ingredient.Plural = mIngredientsTableXml [mStartingColumn+1, row];
					ingredient.Section = mIngredientsTableXml [mStartingColumn+2, row];
					ingredient.Aisle = mIngredientsTableXml [mStartingColumn+3, row];

					//Ignore empty rows
					if (ingredient.Name.Length == 0)
						continue;

					if (mIngredients.ContainsKey(ingredient.Name))
					{
						Console.WriteLine("Duplicate ingredient: " + ingredient.Name);
						//throw new Exception("Ingredients contains duplicate keys :" + ingredient.mFood);
					}
					else
						mIngredients.Add(ingredient.Name, ingredient);
				}
			}

			catch (Exception e) 
			{
				Console.WriteLine (e.Message);
			}

		}

		public static string CheckIngredient(string ingredient)
		{
			//Check if this ingredient has already been flagged
			if (mIngredients.Count == 0 || ingredient == "" || ingredient [0] == '?')
				return ingredient;

			Ingredient masterIngredient;

			if (mIngredients.TryGetValue (ingredient, out masterIngredient)) {
				return ingredient;
			}
			return "?" + ingredient + "?";
		}

        public static List<Ingredient> GetSuggestions (string foodString)
		{
            string lowerFoodString = foodString.Trim().ToLower();
            List<Ingredient> results = new List<Ingredient>();

            foreach (Ingredient ingredient in mIngredients.Values)
            {
                string[] wordsInIngredients = ingredient.Name.Split(' ');

                if (ingredient.Name.StartsWith(lowerFoodString))
                {
                    results.Add(ingredient);
                    continue;
                }

                foreach (string word in wordsInIngredients)
                {
                    if (word.StartsWith(lowerFoodString))
                    {
                        results.Add(ingredient);
                        break;
                    }
                }

            }
            return results;
		}


		//--------------- Parse Functionality ---------------

		public async Task CreateLocalMasterIngredientsDb()
		{
			if (mIngredientsDb == null)
				return;

			int index = 0;
			foreach (ParseObject parseIngredient in mAllParseObjects) 
			{
				Ingredient ingredient = new Ingredient();

				ingredient.Id = index++;
				ingredient.ParseId = parseIngredient.ObjectId;
				ingredient.Name = parseIngredient.Get<string>("name");
				ingredient.Aisle = parseIngredient.Get<string>("aisle");
				ingredient.Plural = parseIngredient.Get<string>("plural");
				ingredient.Section = parseIngredient.Get<string>("section");
				ingredient.RoundingRule = parseIngredient.Get<int>("roundingRule");
				ingredient.RoundingRuleWords = parseIngredient.Get<string> ("roundingRuleWords");

				AddIngredientToDb (ingredient);

				Console.WriteLine (ingredient.Id.ToString() + "adding " + ingredient.Name + " to database");
			}
		}

		bool RecipeListReady
		{
			get;
			set;
		}

		public async Task<bool> DownloadFromParse()
		{
			RecipeListReady = false;

			ParseClient.Initialize (ParseManager.mParseApplicationID_PBC, ParseManager.mParseDotnet_PBC);

			ParseQuery<ParseObject> query = ParseObject.GetQuery("MasterIngredient");

			mAllParseObjects = (await query.Limit(1000).FindAsync()).ToList();

			if (mAllParseObjects.Count == 0)
				return false;

			return true;
		}
	}
}

