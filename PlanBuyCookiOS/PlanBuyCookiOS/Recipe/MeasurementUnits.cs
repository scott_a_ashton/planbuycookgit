using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace PlanBuyCookiOS
{
    public class StringPair
    {
        public string s1;
        public string s2;
        public int S1Hash=0;
        public int S2Hash=0;

        public StringPair(string _s1, string _s2)
        {
            s1 = _s1;
            s2 = _s2;

            S1Hash = HashString(s1);
            S2Hash = HashString(s2);
        }

        public int HashString(string text)
        {
            unchecked
            {
                int hash = 23;
                foreach (char c in text)
                {
                    hash = hash * 31 + c;
                }
                return hash;
            }
        }

        public int Hashcode
        {
            get
            {
                return GetHashCode();
            }
        }

        public override int GetHashCode()
        {
            return S1Hash + (S2Hash >> 1);
        }

        public override bool Equals(object other)   //When Running this entire code, put a break-point on both the Equals() and GetHashCode() methods, and see the execution flow.
        {
            StringPair _other = (StringPair)other;

            return (s1 == _other.s1) && (s2 == _other.s2);
        }
    }

	public class MeasurementUnits : PlanBuyCookSharedData
	{
        static string[,] 							        mUnitsTable;
        protected static Dictionary<string, RecipeUnit> 	mUnits;

        static int mStartingRow = 2;

        public delegate float Conversion(float inValue);
        public static Dictionary< Tuple<string, string>, Conversion> mConvertable = new Dictionary<Tuple<string, string>, Conversion>();


        public static void InitConvertableMatrix()
        {

            foreach (KeyValuePair<string, RecipeUnit> kvp in mUnits)
            {
                RecipeUnit unit = kvp.Value;

                if (unit.Unit != "")
                {
                    mConvertable[Converts(unit.Unit, unit.Unit)] = (float x) => {return x;};
                }
                if (unit.Name != "")
                {
                    mConvertable[Converts(unit.Name, unit.Name)] = (float x) => {return x;};
                }
                if (unit.Plural != "")
                {
                    mConvertable[Converts(unit.Plural, unit.Plural)] = (float x) => {return x;};
                }

                if (unit.Unit != "" && unit.Name != "")
                {
                    mConvertable[Converts(unit.Unit, unit.Name)] = (float x) => {return x;};
                    mConvertable[Converts(unit.Name, unit.Unit)] = (float x) => {return x;};
                }
                if (unit.Unit != "" && unit.Plural != "")
                {
                    mConvertable[Converts(unit.Unit, unit.Plural)] = (float x) => {return x;};
                    mConvertable[Converts(unit.Plural, unit.Unit)] = (float x) => {return x;};
                }
                if (unit.Name != "" && unit.Plural != "")
                {
                    mConvertable[Converts(unit.Name, unit.Plural)] = (float x) => {return x;};
                    mConvertable[Converts(unit.Plural, unit.Name)] = (float x) => {return x;};
                }
            }

            mConvertable[Converts("","")] = (float x) => x;

            //GRAMS TO KG
            mConvertable[Converts("g","kg")] = (float x) => x*1000.0f;
            mConvertable[Converts("g","kilogram")] = (float x) => x*1000.0f;
            mConvertable[Converts("g","kilograms")] = (float x) => x*1000.0f;

            mConvertable[Converts("gram","kg")] = (float x) => x*1000.0f;
            mConvertable[Converts("gram","kilogram")] = (float x) => x*1000.0f;
            mConvertable[Converts("gram","kilograms")] = (float x) => x*1000.0f;

            mConvertable[Converts("grams","kg")] = (float x) => x*1000.0f;
            mConvertable[Converts("grams","kilogram")] = (float x) => x*1000.0f;
            mConvertable[Converts("grams","kilograms")] = (float x) => x*1000.0f;

            //GRAMS TO KG
            mConvertable[Converts("kg","g")] = (float x) => x/1000.0f;
            mConvertable[Converts("kilogram", "g")] = (float x) => x/1000.0f;
            mConvertable[Converts("kilograms", "g")] = (float x) => x/1000.0f;

            mConvertable[Converts("kg", "gram")] = (float x) => x/1000.0f;
            mConvertable[Converts("kilogram", "gram")] = (float x) => x/1000.0f;
            mConvertable[Converts("kilograms", "gram")] = (float x) => x/1000.0f;

            mConvertable[Converts("kg", "grams")] = (float x) => x/1000.0f;
            mConvertable[Converts("kilogram", "grams")] = (float x) => x/1000.0f;
            mConvertable[Converts("kilograms", "grams")] = (float x) => x/1000.0f;

            //ML TO LITRES
            mConvertable[Converts("ml","l")] = (float x) => x*1000.0f;
            mConvertable[Converts("ml","litre")] = (float x) => x*1000.0f;
            mConvertable[Converts("ml","litres")] = (float x) => x*1000.0f;

            mConvertable[Converts("millilitre","l")] = (float x) => x*1000.0f;
            mConvertable[Converts("millilitre","litre")] = (float x) => x*1000.0f;
            mConvertable[Converts("millilitre","litres")] = (float x) => x*1000.0f;

            mConvertable[Converts("millilitres","l")] = (float x) => x*1000.0f;
            mConvertable[Converts("millilitres","litre")] = (float x) => x*1000.0f;
            mConvertable[Converts("millilitres","litres")] = (float x) => x*1000.0f;

            //LITRES TO ML
            mConvertable[Converts("l","ml")] = (float x) => x/1000.0f;
            mConvertable[Converts("litre","ml")] = (float x) => x/1000.0f;
            mConvertable[Converts("litres","ml")] = (float x) => x/1000.0f;

            mConvertable[Converts("l","millilitre")] = (float x) => x/1000.0f;
            mConvertable[Converts("litre","millilitre")] = (float x) => x/1000.0f;
            mConvertable[Converts("litres","millilitre")] = (float x) => x/1000.0f;

            mConvertable[Converts("l","millilitres")] = (float x) => x/1000.0f;
            mConvertable[Converts("litre","millilitres")] = (float x) => x/1000.0f;
            mConvertable[Converts("litres","millilitres")] = (float x) => x/1000.0f;

            //BUNCHES AND SPRIGS
            mConvertable[Converts("sprig","bunch")] = (float x) => x*20.0f;
            mConvertable[Converts("bunch","sprig")] = (float x) => x/20.0f;

            //DebugPrintHashes();
        }

//        public static void DebugPrintHashes()
//        {
//            foreach (var v in mConvertable)
//            {
//                Console.WriteLine("{0} {1} {2}", v.Key.Hashcode, v.Key.s1, v.Key.s2);
//            }
//        }
//
        public static Tuple<string, string> Converts(string a, string b)
        {
            return new Tuple<string, string>(a, b);
        }

        public static bool IsConvertableTo(string _a, string _b)
        {
            string a = _a.ToLower();
            string b = _b.ToLower();

            if (a == b)
                return true;

            Conversion conversion;
            return mConvertable.TryGetValue(Converts(a, b), out conversion);
        }

        public static Conversion GetConversion(string a, string b)
        {
            if (a == b)
                return mConvertable[Converts("", "")];

            return mConvertable[Converts(a, b)];
        }

        public MeasurementUnits (bool bXmlInitialise) : base()
		{ 
            if (bXmlInitialise)
                Extract();
            else
                InitialiseUnits();
		}

        public static RecipeUnit GetRecipeUnit(string unitName)
        {
            RecipeUnit unit;
            string unitLower = unitName.ToLower().TrimEnd();

            if (!mUnits.TryGetValue(unitLower, out unit))
            {
                unit = mUnits.Values.Where(unit1 => unit1.Unit == unitLower).FirstOrDefault();

                if (unit == null)
                {
                    unit = mUnits.Values.Where(unit2 => unit2.Plural == unitLower).FirstOrDefault();
                }
            }

            return unit;
        }

        public  void InitialiseUnits()
        {
            if (mUnits == null) 
            {
                mUnits = new Dictionary<string, RecipeUnit>();

                mUnits.Add("", new RecipeUnit("","",""));
                mUnits.Add("bag",new RecipeUnit("bag","bags",""));
                mUnits.Add("barrel",new RecipeUnit("barrel","barrels",""));
                mUnits.Add("bottle", new RecipeUnit("bottle","bottles",""));
                mUnits.Add("box", new RecipeUnit("box","boxes",""));
                mUnits.Add("bunch", new RecipeUnit("bunch","bunches",""));
                mUnits.Add("bushel", new RecipeUnit("bushel","bushels",""));
                mUnits.Add("can", new RecipeUnit("can","cans",""));
                mUnits.Add("case", new RecipeUnit("case","cases",""));
                mUnits.Add("clove", new RecipeUnit("clove","cloves",""));
                mUnits.Add("cloves", new RecipeUnit("clove","cloves",""));
                mUnits.Add("cup", new RecipeUnit("cup","cups",""));
                mUnits.Add("dash", new RecipeUnit("dash","dashes",""));
                mUnits.Add("drum", new RecipeUnit("drum","drums",""));
                mUnits.Add("ear", new RecipeUnit("ear","ears",""));
                mUnits.Add("gallon", new RecipeUnit("gallon","gallons","gal"));
                mUnits.Add("gram", new RecipeUnit("gram","grams","g"));
                mUnits.Add("grams", new RecipeUnit("gram","grams","g"));
                mUnits.Add("half", new RecipeUnit("half","halves",""));
                mUnits.Add("handful", new RecipeUnit("handful","handfuls",""));
                mUnits.Add("jar", new RecipeUnit("jar","jars",""));
                mUnits.Add("kilogram", new RecipeUnit("kilogram","kilograms","kg"));
                mUnits.Add("large", new RecipeUnit("large","",""));
                mUnits.Add("litre", new RecipeUnit("litre","litres","l"));
                mUnits.Add("rasher", new RecipeUnit("rasher","rashers",""));
                mUnits.Add("whole sheet", new RecipeUnit("whole sheet","whole sheets",""));
                mUnits.Add("medium", new RecipeUnit("medium","",""));
                mUnits.Add("millilitre", new RecipeUnit("millilitre","millilitres","ml"));
                mUnits.Add("millilitres", new RecipeUnit("millilitre","millilitres","ml"));
                mUnits.Add("number", new RecipeUnit("number","",""));
                mUnits.Add("ounce", new RecipeUnit("ounce","ounces","oz"));
                mUnits.Add("package", new RecipeUnit("package","packages",""));
                mUnits.Add("part", new RecipeUnit("part","parts",""));
                mUnits.Add("peck", new RecipeUnit("peck","pecks",""));
                mUnits.Add("piece", new RecipeUnit("piece","pieces",""));
                mUnits.Add("pinch", new RecipeUnit("pinch","pinches",""));
                mUnits.Add("pint", new RecipeUnit("pint","pints","pt"));
                mUnits.Add("pound", new RecipeUnit("pound","pounds","lb"));
                mUnits.Add("quart", new RecipeUnit("quart","quarts","qt"));
                mUnits.Add("shot", new RecipeUnit("shot","shots",""));
                mUnits.Add("slice", new RecipeUnit("slice","slices",""));
                mUnits.Add("small", new RecipeUnit("small","",""));
                mUnits.Add("splash", new RecipeUnit("splash","splashes",""));
                mUnits.Add("sprig", new RecipeUnit("sprig","sprigs",""));
                mUnits.Add("sprinkle", new RecipeUnit("sprinkle","sprinkles",""));
                mUnits.Add("stick", new RecipeUnit("stick","sticks",""));
                mUnits.Add("tablespoon", new RecipeUnit("tablespoon","tablespoons","tbsp"));
                mUnits.Add("teaspoon", new RecipeUnit("teaspoon","teaspoons","tsp"));
                mUnits.Add("teaspoons", new RecipeUnit("teaspoon","teaspoons","tsp"));
                mUnits.Add("tin", new RecipeUnit("tin","tins",""));
            }

            InitConvertableMatrix();
        }

		public void Extract()
		{
			mUnitsTable = LoadWorksheet ("Units");

            //TODO: REwrite according to the new worksheet format
			try
			{
//				for (int row = mStartingRow; row < mUnitsTable.GetLength(1); row++) 
//				{
//					RecipeUnit unit = new RecipeUnit ();
//
//					unit.mName = mUnitsTable [0, row];
//					unit.mPlural = mUnitsTable [1, row];
//					unit.mType = mUnitsTable [2, row];
//					unit.mUnit = mUnitsTable [3, row];
//					unit.mLocale = mUnitsTable [4, row];
//
//					//Ignore empty rows
//					if (unit.mName.Length == 0)
//						continue;
//
//					if (mUnits.ContainsKey(unit.mName))
//						throw new Exception("Unit of measure contains duplicate keys :" + unit.mName);
//					else
//						mUnits.Add(unit.mName, unit);
//				}
			}

			catch (Exception e) 
			{
				Console.WriteLine (e.Message);
			}
		}
	}
}

