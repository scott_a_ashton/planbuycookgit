using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.IO;


namespace PlanBuyCookiOS
{
	public class PlanBuyCookSharedData
	{
		protected int mColumnCount = 1;
		protected int mRowCount = 1;

		public PlanBuyCookSharedData ()
		{
			if (!mbXmlLoaded) {
				LoadSharedXml();
			}
		}

		protected static XDocument mSharedXml = null;
		protected static bool mbXmlLoaded = false;

		public static string SharedXmlPath
		{
			get
			{ 
				var filename = "Data/PlanBuyCookMaster.xml";

				#if __ANDROID__
				string libraryPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal); ;
				#else
				// we need to put in /Library/ on iOS5.1 to meet Apple's iCloud terms
				// (they don't want non-user-generated data in Documents)
				string documentsPath = Environment.GetFolderPath (Environment.SpecialFolder.LocalApplicationData); // Documents folder
				//string libraryPath = Path.Combine (documentsPath, "..", "Library");

				var path = Path.Combine ("", filename);
				#endif
				return path;
			}
		}

		public static XDocument SharedXml
		{
			get
			{
				if (!mbXmlLoaded) 
					LoadSharedXml();

				return mSharedXml;
			}
		}

		public static void LoadSharedXml()
		{
			try
			{
				mSharedXml = XDocument.Load (SharedXmlPath); 

				mbXmlLoaded = true;
			}
			catch (Exception e)
			{
				Console.WriteLine(e.Message);
			}
		}

		public static bool IsInitialised
		{
			get
			{
				return mbXmlLoaded;
			}
		}


		public virtual void Extract()
		{
		}

		protected string[] GetRow(int rowNumber, string[,] table)
		{
			string[] row = new string[mColumnCount];

			for (int cell = 0; cell < mColumnCount; cell++)
				row[cell] = table[cell, rowNumber];

			return row;
		}

		protected string[] GetColumn(int columnNumber, string[,] table)
		{
			string[] column = new string[mRowCount];

			for (int rowIndex = 0; rowIndex < mRowCount; rowIndex++)
				column[rowIndex] = table[columnNumber, rowIndex];

			return column;
		}

		protected int SearchForStringInColumn(string searchString, int column, string[,] table)
		{
			return GetColumn(column, table).Where(item => searchString.ToLower() == item.ToLower()).Select((item, indexer) => indexer).FirstOrDefault();
		}

		public string[,] LoadWorksheet(string worksheetName)
		{
			try
			{
				XNamespace ns = SharedXml.Root.FirstAttribute.Value;
				XElement workbookElement = SharedXml.Element(ns+"Workbook");
				XElement worksheetElement = workbookElement.Elements(ns+"Worksheet").Where(sheet => sheet.Attribute(ns+"Name").Value==worksheetName).First();
				XElement table = worksheetElement.Elements(ns+"Table").First();

				//Read the column count from the Xml (min 1)
				int.TryParse(table.Attribute(ns+"ExpandedColumnCount").Value, out mColumnCount);
				Console.WriteLine("column Count" + mColumnCount);

				//Read the row count from the Xml (min 1)
				int.TryParse(table.Attribute(ns+"ExpandedRowCount").Value, out mRowCount);

				//get all the rows
				XElement[] rows = table.Elements(ns+"Row").ToArray();

				//Create a Units string table to dump the Xcel values into
				string[,] worksheet = new string[mColumnCount,mRowCount];

				//Initialise all the string elements
				for (int row=0; row < mRowCount; row++)
					for (int cell=0; cell < mColumnCount; cell++)
						worksheet[cell, row] = "";

				int rowIndex = 1;
				int minColumnIndex = 0;
				int internalColumnCount = 0;

				foreach (XElement row in rows)
				{
					XElement[] cells = row.Elements(ns+"Cell").ToArray();

					//Read the row index if it's stored as an attribute
					if (row.Attributes(ns+"Index").Any())
					{
						if (!int.TryParse(row.Attribute(ns+"Index").Value, out rowIndex))
							Console.WriteLine ("Failed parsing row index ");
					}

					//Iterate through the cells in the Xml
					for (int index = 0; index < cells.Length; index++)
					{
						int cellIndex = internalColumnCount;

						//Grab data from the cell
						string cellValue = cells[index].Elements(ns+"Data").Any() ? cells[index].Element(ns+"Data").Value : "";

						if (cells[index].Attributes(ns+"Index").Any())
						{
							internalColumnCount = 0;
							if (int.TryParse(cells[index].Attribute(ns+"Index").Value, out cellIndex))
							{
								worksheet[cellIndex-1, rowIndex-1] = cellValue;
								minColumnIndex = cellIndex;
							}
						}
						else
						{
							worksheet[minColumnIndex + internalColumnCount, rowIndex-1] = cellValue;
							internalColumnCount++;
						}
					}
					rowIndex++;
				}
				return worksheet;
			}
			catch (Exception e)
			{
				Console.WriteLine ("Failed to load worksheet " + worksheetName);
				Console.WriteLine(e.Message);
			}

			return null;
		}		
	}
}

