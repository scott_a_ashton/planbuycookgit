using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace PlanBuyCookiOS
{
	public class Preparation : PlanBuyCookSharedData
	{
		public class Prep
		{
			public string mName;
			public string mPlural;
			public string mType;
			public string mUnit;
			public string mLocale;
		}

		//we load the ingredients initially as a table before parsing the data into the ingredients dictionary
		string[,] 							mPreparationTable;
		protected Dictionary<string, Prep> 	mPreparation = new Dictionary<string, Prep>();

		public Preparation () : base()
		{ 

		}

		public override void Extract()
		{
			mPreparationTable = LoadWorksheet ("Ingredients");

		}

	}
}
