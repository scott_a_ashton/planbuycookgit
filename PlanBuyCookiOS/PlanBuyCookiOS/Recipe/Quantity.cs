using System;
using System.Collections.Generic;
using UnitPair = System.Tuple<PlanBuyCookiOS.Quantity.Unit, PlanBuyCookiOS.Quantity.Unit>;

namespace PlanBuyCookiOS
{
	public class Quantity
	{
		public enum Unit
		{
			Kilogram

		}

		public delegate float Conversion (UnitPair UnitPair, float from);

			Dictionary<UnitPair, Conversion> mUnitConversionTable;

		public Quantity ()
		{
		}
	}
}

