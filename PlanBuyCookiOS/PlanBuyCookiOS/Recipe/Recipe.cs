using System;
using System.Collections.Generic;
using System.Net;
using Foundation;
using System.Security.Cryptography.X509Certificates;
using System.Diagnostics;
using System.Threading.Tasks;
using System.IO;
using System.Text;
using HtmlAgilityPack;
using System.Linq;

using RecipeIngredient = System.Tuple<PlanBuyCookiOS.Quantity, PlanBuyCookiOS.Ingredient>;
using RecipeStep = System.Tuple<string, float>; 
using Serves = System.Tuple<int, int>;
using Category = System.String;

namespace PlanBuyCookiOS
{
	public interface IRecipeImport
	{
		bool ParsedOK { get; }
		string Name { get; }
		string Description { get; }
		int PrepTime { get; }
		int CookTime { get; }
		int Serves { get; }
		string Notes { get; }
        int Energy{ get; }
        int Fat{ get; }
        int Fibre { get; }
        int Carbs { get; }
        int Protein { get; }
        bool Favourite { get; }

		string StepsString { get; }
		string IngredientsString { get; }
		string CategoriesString { get; }
        string TypeString { get; }
        string Source {get;}

		List<RecipeStep> StepsList { get; }
		List<RecipeIngredient> IngredientsList { get; }

		string ImageString { get; }
		string ImagePortraitString { get; }
		string ImageLandscapeString { get; }

        bool Search(string ingredientString, bool isLastWord=false);
	}

	public class Recipe 
	{
		public struct Ingredient
		{
			public Ingredient(string[] ingredient)
			{
				//TODO: implement this properly, just to get things to compile for now
				mQuantity = ingredient[0];
				mUnit = ingredient[1];
				mFood = ingredient[2];
				mPrep = ingredient[3];
			}

			public string mQuantity;
			public string mUnit;
			public string mFood;
			public string mPrep;
		};

		public struct Step
		{
			public string mNumber;
			public string mTimer; //min
			public string mMethod;
		}

		public enum Type
		{
			Meat, Beef, Main
		};

		public enum SourceType
		{
			Invalid,
			Xml,
			Url,
		};
//		public enum Category
//		{
//			Default,
//		};

        public delegate RecipeWeb RecipeConstruct (string url);

		IRecipeImport 				mInternalRecipe;

		string						mRecipeAddress;
		SourceType					mSource = SourceType.Invalid;
		bool						mbIsValidRecipe = false;
		bool						mbIsValidAddress = false;

		string 						mName;
		string 						mDescription;
		int 						mPrepTime;			//Minutes
		int 						mCookTime; 			//Minutes
		Serves						mNumServes; 		//eg 4, 4-6, 8
		List<Category> 				mCategories;
		RecipeIngredient[]			mIngredientsList;
		RecipeStep 					mSteps;

		//we load the ingredients initially as a table before parsing the data into the ingredients dictionary
		string[,] 					mRecipeTable;
		int 						mStartingColumn = 1;
		int 						mTitleRow = 1;

		static bool					mbStaticInitialise = false;
		static Dictionary<string, RecipeConstruct>	mSupportedWebsites = new Dictionary<string, RecipeConstruct>();	

		public static void StaticInitialise()
		{
			//Register supported websites
            mSupportedWebsites.Add ("allrecipes.com.au", (string url) => new RecipeWeb_allrecipescomau(url));
            mSupportedWebsites.Add ("www.goodfood.com.au", (string url) => new RecipeWeb_goodfoodcomau(url));
            mSupportedWebsites.Add ("www.taste.com.au", (string url) => new RecipeWeb_tastecomau(url));

			mbStaticInitialise = true;
		}

		public Recipe(string recipe)
		{
			if (!mbStaticInitialise)
				StaticInitialise ();

			mRecipeAddress = recipe;

			NSUrl url = NSUrl.FromString (mRecipeAddress);

			if (url != null && url.Host != null) 
			{
				mSource = SourceType.Url;
				mbIsValidAddress = true;
			} 
			else if (mRecipeAddress.ToLower ().Contains (".xml")) 
			{
				string fullPath = mRecipeAddress; 
				if (File.Exists (fullPath))
					mSource = SourceType.Xml;

				mbIsValidAddress = true;
			} 
		}

		public async void Extract()
		{
			if (!mbIsValidAddress)
				Console.Error.Write ("Invalid asddress");

			try
			{
				switch (mSource) 
				{
					case SourceType.Url:
						NSUrl url = NSUrl.FromString (mRecipeAddress);
						
						if (url != null && url.Host!=null) 
						{
							RecipeConstruct constructor;

							if (mSupportedWebsites.TryGetValue (url.Host, out constructor)) 
							{
                                RecipeWeb webRecipe = constructor (mRecipeAddress);

                                mbIsValidRecipe = webRecipe.LoadRecipeHtml ().Result;
								if (mbIsValidRecipe)
									InitialiseFromWeb (webRecipe);
							}
						}
						mbIsValidRecipe = true;
						break;

					case SourceType.Xml:
						RecipeXml xmlRecipe = new RecipeXml(mRecipeAddress);
						xmlRecipe.Extract();
						mbIsValidRecipe = xmlRecipe.HasParsedOK;
						
						if (mbIsValidRecipe)
							InitialiseFromXml(xmlRecipe);
						break;
				}
			}
			catch (Exception e) 
			{
                Console.WriteLine ("Failed to load recipe:" + e.Message);
			}
		}

		void InitialiseFromWeb(RecipeWeb webRecipe)
		{
			Debug.Assert (webRecipe.ParsedOK);

			mName = webRecipe.Name;
		}

		void InitialiseFromXml(RecipeXml xmlRecipe)
		{
			Debug.Assert (xmlRecipe.HasParsedOK);
		}

		void InitialiseFromParse(RecipeParse parseRecipe)
		{
			//Debug.Assert (xmlRecipe.HasParsedOK);
		}


	}
}
