using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace PlanBuyCookiOS
{
	public class RecipeIngredient
	{
		public int mIndex=0;
		public float mQuantity=0.0f;
		public string mUnit="";
		public string mFood="";
		public string mPrep="";
        public float mMax = 0.0f;


		public RecipeIngredient (IList<object> parseIngredient)
		{
			mIndex = Convert.ToInt32(parseIngredient[0]);
			mQuantity = Convert.ToSingle(parseIngredient[1]);
			mUnit = Convert.ToString(parseIngredient[2]);
			mFood = Convert.ToString(parseIngredient[3]);
			mPrep = Convert.ToString(parseIngredient[4]);
		}

		public RecipeIngredient (int index, float quantity, string unit, string food, string prep)
		{
			mIndex = index;
			mQuantity = quantity;
			mUnit = unit;
			mFood = food;
            mPrep = StripPrepOfMaxAmount(prep, out mMax);
		}
			
        string StripPrepOfMaxAmount(string prep, out float maxAmount)
        {
            maxAmount = 0.0f;

            if (prep.ToLower().Contains("max"))
            {
                int openBracketIndex = prep.IndexOf('[');
                int closedBracketIndex = prep.IndexOf(']');

                if (openBracketIndex == -1 || closedBracketIndex == -1)
                {
                    maxAmount = 0.0f;
                    return prep;
                }
                string expression = prep.Substring(openBracketIndex+1, closedBracketIndex - openBracketIndex-1);
                string prepMinusExpression = prep.Substring(0, openBracketIndex);

                int equalsIndex = expression.IndexOf('=');
                string value = expression.Substring(equalsIndex+1, expression.Length - equalsIndex-1);

                float.TryParse(value, out maxAmount);

                return prepMinusExpression;
            }
            else
                return prep;
        }

		public override string ToString ()
		{
			string ingredientString = mIndex.ToString () + RecipeManager.cFieldSeparator +
									mQuantity.ToString () + RecipeManager.cFieldSeparator +
									mUnit + RecipeManager.cFieldSeparator +
									mFood + RecipeManager.cFieldSeparator +
									mPrep;

			return ingredientString;
		}

		static public bool Parse(string ingredientString, out RecipeIngredient newIngredient)
		{
			string[] fields = ingredientString.Split (new char[]{ RecipeManager.cFieldSeparator });

			Debug.Assert (fields.Length == 5);

			int index = 0;
			bool bParsed = int.TryParse(fields [0], out index);
			float quantity = 0.0f;
			bParsed |= float.TryParse (fields [1], out quantity);
			string unit = fields [2];
			string food = fields [3];			
			string prep = fields [4];

			if (bParsed)
				newIngredient = new RecipeIngredient (index,quantity,unit,food,prep);
			else
				newIngredient = null;

			return bParsed;
		}

	}
}

