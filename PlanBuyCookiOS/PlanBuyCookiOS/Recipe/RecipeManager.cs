using System;
using System.Collections.Generic;
using Parse;
using System.Threading.Tasks;
using System.Linq;
using System.IO;
using SQLite;
using Foundation;

namespace PlanBuyCookiOS
{
	public class RecipeManager
	{
        //private readonly SQLiteAsyncConnection mRecipeDb = new SQLiteAsyncConnection (Path.Combine (Environment.GetFolderPath (Environment.SpecialFolder.Personal), "Recipe.db"));
        private SQLiteAsyncConnection mRecipeDb;
        bool mbGenerateNewDatabase = false;

		public static readonly char cFieldSeparator = (char)249;
		public static readonly char cRecordSeparator = (char)250;

		public static RecipeManager Instance = null;

		static List<ParseObject> mAllParseObjects;
		static List<IRecipeImport> mRecipes = new List<IRecipeImport> ();

		public delegate void OnFinishedLoadingRecipes();
		public OnFinishedLoadingRecipes mOnFinishedLoadingRecipes;
        bool mbRecipesLoaded = false;

		int mCurrentRecipeIndex = 0;

		public IRecipeImport CurrentRecipe
		{
			get
			{
				if (mRecipes.Count == 0 || mCurrentRecipeIndex >= mRecipes.Count)
					return null;

				return mRecipes.ElementAt(mCurrentRecipeIndex);
			}
		}

        readonly string  dbFilename = "Recipe.db";
        public string DatabasePath
        {
            get 
            {
                var documents = Environment.GetFolderPath (Environment.SpecialFolder.MyDocuments);
                var library = Path.Combine (documents, "..", "Library");
                return Path.Combine (library, dbFilename);
            }
        }
            
		public SQLiteAsyncConnection RecipeDb
		{
			get 
            {
                if (mRecipeDb == null)
                {
                    if (mbGenerateNewDatabase)
                        mRecipeDb = new SQLiteAsyncConnection(DatabasePath);
                    else
                    {
                        if (!File.Exists(DatabasePath))  
                            CopyFileInBundleToDocumentsFolder(dbFilename);
                        else
                        {
                            FileInfo storedDb = new FileInfo(DatabasePath);
                            FileInfo bundledDb = new FileInfo(dbFilename);

                            //Copy the bundled database if it's newer than the stored database
                            if (bundledDb.CreationTime > storedDb.CreationTime)
                                CopyFileInBundleToDocumentsFolder(dbFilename);
                        }

                        mRecipeDb = new SQLiteAsyncConnection(DatabasePath);
                    }
                }
                return mRecipeDb; 
            }
		}
        bool RecipeListLoaded
        {
            get { return mbRecipesLoaded; }
        }


		public RecipeManager ()
		{
            RecipeDb.CreateTableAsync<RecipeSQL> ();

			Instance = this;
		}


        public void TestMasterShoppingList()
        {
            Console.WriteLine("Testing Master Ingredients List");
            for (int recipe = 0; recipe < RecipeManager.mRecipes.Count(); recipe++)
            {
                Console.WriteLine("\nTesting {0}", RecipeManager.Instance.GetRecipe(recipe).Name);

                MasterShoppingList.AddIngredients(RecipeManager.Instance.GetRecipe(recipe).IngredientsList, 2.0f);
                MasterShoppingList.RemoveIngredients(RecipeManager.Instance.GetRecipe(recipe).IngredientsList, 2.0f);
            }
        }

        public void TestRecipeUnits()
        {
            for (int recipe = 0; recipe < RecipeManager.mRecipes.Count(); recipe++)
            {
                AppDelegate.SwitchToCookPage(RecipeManager.Instance.GetRecipe(recipe));
                Console.WriteLine("\n");
            }
        }

        private void CopyFileInBundleToDocumentsFolder(String filename)
        {
            //---path to Documents folder---
            var documentsFolderPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);

            //---destination path for file in the Documents folder
            var destinationPath = DatabasePath;

            //---path of source file---
            var sourcePath = Path.Combine(NSBundle.MainBundle.BundlePath, filename);

            //---print for verfications---
            Console.WriteLine(destinationPath);
            Console.WriteLine(sourcePath);

            try 
            {
                //---copy only if file does not exist---
                if (!File.Exists(destinationPath))
                {
                    File.Copy(sourcePath, destinationPath);

                    //Do not iCloud Backup
                    NSFileManager.SetSkipBackupAttribute (destinationPath, true); 
                }  
                else 
                {
                    File.Copy(sourcePath, destinationPath, true);

                    //Do not iCloud Backup
                    NSFileManager.SetSkipBackupAttribute (destinationPath, true); 

                    Console.WriteLine("File already exists. Overwriting: " + destinationPath);
                }
            }  
            catch (Exception e) 
            {
                Console.WriteLine(e.Message);
            }
        }

		public int NumRecipes
		{
			get { return mRecipes.Count; }
		}

		public async Task Initialise()
		{
			//ParseClient.Initialize (mParseApplicationID_LITM, mParseDotNet_LITM);
			ParseClient.Initialize (ParseManager.mParseApplicationID_PBC, ParseManager.mParseDotnet_PBC);

            if (mbGenerateNewDatabase)
                await ExtractRecipeDataFromParse ();
            else
                await ExtractRecipesFromDatabase ();
		}


		public async Task<bool> GetRecipeListFromParse()
		{
			ParseQuery<ParseObject> query = ParseObject.GetQuery("Recipe");

			mAllParseObjects = (await query.FindAsync()).ToList();

			if (mAllParseObjects.Count == 0)
				return false;

			return true;
		}

		public async Task ExtractRecipesFromDatabase()
		{
            var recipesDB = await mRecipeDb.Table<RecipeSQL> ().ToListAsync ();

            List<IRecipeImport> recipes = new List<IRecipeImport>();

			foreach (var recipe in recipesDB)
			{
                recipes.Add (recipe);
			}

            //Make sure that side dishes are at the end of the recipes
            var sideDishes = recipes.Where(r => r.TypeString.ToLower().Contains("side") || r.TypeString.ToLower().Contains("condiment"));

            Random rand = new Random(DateTime.UtcNow.Second);
            mRecipes = recipes.Except(sideDishes).OrderBy(r => rand.NextDouble()).ToList();
            mRecipes.AddRange(sideDishes.OrderBy(s => rand.NextDouble()));

            mbRecipesLoaded = true;

			if (mOnFinishedLoadingRecipes != null)
				mOnFinishedLoadingRecipes ();
		}

		public async Task ExtractRecipeDataFromParse ()
		{
            List<RecipeParse> parseRecipes = new List<RecipeParse>();

			if (await GetRecipeListFromParse ()) 
			{
                foreach (ParseObject parseRecipeObject in mAllParseObjects) 
				{
                    try
                    {
                        RecipeParse parseRecipe = new RecipeParse (parseRecipeObject);

                        if (parseRecipe.ParsedOK)
                        {
                            Console.WriteLine ("Loading Parse Recipe: " + parseRecipe.Name);

                            Console.WriteLine (parseRecipe.Name + ":Loading Parse Recipe Image");
                            parseRecipe.GetImageData();

                            Console.WriteLine (parseRecipe.Name + ":Loading Parse Portrait Image");
                            parseRecipe.GetImagePortraitData();

                            Console.WriteLine (parseRecipe.Name + ":Loading Parse Landscaoe Image");
                            parseRecipe.GetImageLandscapeData();

                            Console.WriteLine (parseRecipes.Count().ToString() + " - Loaded " + parseRecipe.Name);

                            parseRecipes.Add(parseRecipe);
                            AddRecipeToDb(parseRecipes.Last());
                        }
                        else
                        {
                           Console.WriteLine ("Failed to load " + parseRecipe.Name);
                        }
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                    }
				}
				mCurrentRecipeIndex = 0;
			}
            Console.WriteLine("Finished building database");
		}

        public void NextRecipe()
		{
			if (mRecipes.Count > 0)
				mCurrentRecipeIndex = (mCurrentRecipeIndex + 1) % mRecipes.Count;
		}
		public void PrevRecipe()
		{
			if (mRecipes.Count == 0)
				return;

			mCurrentRecipeIndex = (mCurrentRecipeIndex - 1);

			if (mCurrentRecipeIndex < 0)
				mCurrentRecipeIndex += mRecipes.Count;
		}
			
		public async Task LoadAllRecipeImages()
		{
			foreach (RecipeParse recipe in mRecipes) 
			{
				await recipe.GetImageDataAsync ();
			}
		}

        //This is being called by the Calendar module that is initialising at the same time
        //It may not be ready when called so wait till it is.
        public async Task<IRecipeImport> AsyncFindRecipe(string recipeName)
        {
            while (!mbRecipesLoaded)
                await Task.Delay(1);

            return mRecipes.Where (recipe => recipe.Name.ToLower() == recipeName.ToLower()).FirstOrDefault ();
        }

		public IRecipeImport FindRecipe(string recipeName)
		{
			return mRecipes.Where (recipe => recipe.Name.ToLower() == recipeName.ToLower()).FirstOrDefault ();
		}
		

        public List<IRecipeImport> Recipes
        {
            get
            {
                if (mbActiveSearch)
                    return mActiveSearchResults;
                else
                    return mRecipes;
            }
        }

		public IRecipeImport GetRecipe(int recipeIndex)
		{
            if (recipeIndex >= 0 && recipeIndex < Recipes.Count) 
			{
                return Recipes [recipeIndex];
			} 
			else 
			{
                return new RecipeSpecialMeal((SpecialMealType)recipeIndex);
			}
			return null;
		}

        bool mbActiveSearch = false;
        int mNumHits = 0;

        string mActiveSearchString = "";
        List<IRecipeImport> mActiveSearchResults = new List<IRecipeImport>();

        public bool SearchIsActive
        {
            get { return mbActiveSearch; }
        }
            
        public bool IsSearchHit(int searchRecipeIndex)
        {
            return searchRecipeIndex < mNumHits;
        }

        public bool RecipeNameContainsWord(IRecipeImport recipe, string singleWord)
        {
            //Split the name into multiple elements
            string[] nameElements = recipe.Name.Split(new char[]{' '}, StringSplitOptions.RemoveEmptyEntries);

            if (recipe.Name.ToLower().StartsWith(singleWord))
                return true;

            foreach (var word in nameElements)
            {
                if (word.ToLower().StartsWith(singleWord))
                    return true;
            }
            return false;
        }

        public bool RecipeNameMultipleWords(IRecipeImport recipe, string[] words)
        {
            foreach (var word in words)
            {
                if (RecipeNameContainsWord(recipe, word))
                    return true;
            }
            return false;
        }

        public bool RecipeTypeContainsWord(IRecipeImport recipe, string singleWord)
        {
            if (recipe is RecipeSQL)
            {
                RecipeSQL recipesql = recipe as RecipeSQL;

                foreach (var type in recipesql.mType)
                {
                    if (type.ToLower().StartsWith(singleWord))
                        return true;
                }
            }
            return false;
        }
        public bool RecipeCategoriesContainsWord(IRecipeImport recipe, string singleWord)
        {
            if (recipe is RecipeSQL)
            {
                RecipeSQL recipesql = recipe as RecipeSQL;

                foreach (var category in recipesql.mCategories)
                {
                    if (category.StartsWith(singleWord))
                        return true;
                }
            }
            return false;
        }
        public bool RecipeIngredientsContainsWord(IRecipeImport recipe, string singleWord)
        {
            if (recipe is RecipeSQL)
            {
                RecipeSQL recipesql = recipe as RecipeSQL;

                foreach (var ingredient in recipesql.mIngredientsList)
                {
                    if (ingredient.mFood.ToLower().StartsWith(singleWord))
                        return true;
                }
            }
            return false;
        }
        public bool RecipeContainsWord(IRecipeImport recipe, string searchString, bool isBeingTyped)
        {
            string[] searchStringElements = searchString.Split(new char[]{' '}, StringSplitOptions.RemoveEmptyEntries);

            if (searchStringElements.Length > 1)
                searchString = string.Concat(searchStringElements.Select(s => s + " ")).TrimEnd();

            if (RecipeNameContainsWord(recipe, searchString))
                return true;
            if (RecipeTypeContainsWord(recipe, searchString))
                return true;
            if (RecipeCategoriesContainsWord(recipe, searchString))
                return true;
            if (RecipeIngredientsContainsWord(recipe, searchString))
                return true;

            return false;
        }


        public void Search(string searchString)
        {
            if (searchString == "")
            {
                ClearSearch();
                return;
            }

            string lSearchString = searchString.ToLower().Trim();

            IEnumerable<IRecipeImport> searchSpace = mRecipes;

            var hits = searchSpace.Where(r => RecipeContainsWord(r, lSearchString, false)).ToList();
            searchSpace = searchSpace.Except(hits);

            mActiveSearchResults = hits.Concat(searchSpace).ToList();
            mNumHits = hits.Count();
            mbActiveSearch = true;
        }

        public void ClearSearch()
        {
            mbActiveSearch = false;
            mNumHits = 0;
        }

		void AddRecipeToDb (RecipeParse parseRecipe)
		{
			try
			{
				RecipeSQL recipeSQL = new RecipeSQL (parseRecipe);
                mRecipeDb.InsertAsync (recipeSQL);
			}
			catch (Exception e) 
			{
                Console.Error.WriteLine(e.Message);
				Console.Error.WriteLine ("Failed to add recipe " + parseRecipe.Name + " to database.");
			}
		}

        public void UpdateRecipe(RecipeSQL recipe)
        {
            try
            {
                mRecipeDb.UpdateAsync (recipe);
            }
            catch (Exception e) 
            {
                Console.Error.WriteLine(e.Message);
                Console.Error.WriteLine ("Failed to update recipe " + recipe.Name + " to database.");
            }
        }
	}
}

