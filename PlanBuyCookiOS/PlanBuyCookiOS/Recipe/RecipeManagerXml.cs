using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using System.IO;
using System.Diagnostics;
using Foundation;


namespace PlanBuyCookiOS
{
	public class RecipeManagerXml
	{
		IngredientsXml mIngredients = new IngredientsXml ();
        MeasurementUnits mUnits = new MeasurementUnits(bXmlInitialise:false);

		XDocument mRecipeListXml;
		bool mbRecipeListLoaded = false;

		List<RecipeXml> mRecipes = new List<RecipeXml> ();

		public MeasurementUnits Units
		{
			get
			{
				return mUnits;
			}
		}

		public RecipeManagerXml ()
		{
		}

		public void RegisterRecipeSites()
		{
		}

		public void LoadLocalRecipeList()
		{
			try
			{
				mRecipeListXml = XDocument.Load ("Data/Recipe/Recipes.xml"); 

				mbRecipeListLoaded = true;
			}
			catch (Exception e)
			{
				Console.WriteLine(e.Message);
				return;
			}

			//Get the list of receipes
			var recipesXml = mRecipeListXml.Element ("Recipes");

			if (!recipesXml.IsEmpty) 
			{
				//Extract the individual recipes
				var recipeElements = recipesXml.Elements ("Recipe");

				Console.WriteLine ("There are " + recipeElements.Count() + "recipes found");

				foreach (XElement xe in recipeElements) 
				{
					string recipeName = xe.Attribute ("name").Value;

					try
					{
						RecipeXml recipe = new RecipeXml("Data/Recipe/"+recipeName+".xml");

						recipe.Extract();

						mRecipes.Add(recipe);

						if (!recipe.HasParsedOK)
							Console.WriteLine ("Error loading recipe: " + recipeName);
					}
					catch (Exception e) 
					{
						Console.WriteLine (e.Message);
					}
				}
			}
		}

		public void Initialise()
		{
			LoadLocalRecipeList ();

//			RecipeWeb wr = new RecipeWeb_tastecomau ();
//			wr.LoadRecipeHtml ("http://www.taste.com.au/recipes/26964/chicken+tikka+masala+with+coriander+raita");
//
//			RecipeWeb rall = new RecipeWeb_allrecipescomau ();
//			rall.LoadRecipeHtml ("http://allrecipes.com.au/recipe/14086/chocolate-layer-cake-with-yoghurt-icing.aspx");
//
//			RecipeWeb rgf = new RecipeWeb_goodfoodcomau();
//			rgf.LoadRecipeHtml ("http://www.goodfood.com.au/good-food/cook/recipe/thai-red-duck-curry-with-pineapple-20130807-2rfz8.html");


			//Recipe recipe = new Recipe ("http://www.goodfood.com.au/good-food/cook/recipe/thai-red-duck-curry-with-pineapple-20130807-2rfz8.html");
			//recipe.Extract ();

//			Recipe recipe1 = new Recipe ("http://allrecipes.com.au/recipe/14086/chocolate-layer-cake-with-yoghurt-icing.aspx");
//			Recipe recipe2 = new Recipe ("http://www.taste.com.au/recipes/26964/chicken+tikka+masala+with+coriander+raita");
//
//			Recipe r = new Recipe ("htt p://www.taste.com.au/recipes/26964/chicken+tikka+masala+with+coriander+raita");
			//Recipe x = new Recipe ("Data/Recipe/GreenBeansAndPotato.xml");
			//x.Extract ();
			//mUnits.Extract ();
			//mIngredients.Extract ();
			//mRecipe.Extract ();
		}
	}
}

