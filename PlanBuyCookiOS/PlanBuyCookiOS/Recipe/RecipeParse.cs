using System;
using System.Collections.Generic;
using System.Net;
using Foundation;
using System.Security.Cryptography.X509Certificates;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Net.Http;
using System.IO;
using System.Text;
using HtmlAgilityPack;
using System.Linq;
using System.Xml.XPath;
using System.Xml;
using Parse;



namespace PlanBuyCookiOS
{


	public class RecipeParse : IRecipeImport
	{
		public bool mbExtractedOK = false;
		public string mParseId;

		public string mName;
		public string mDescription;
		public int mPrepTime = 0;
		public int mCookTime = 0;
		public int mServes = 0;

        public int mEnergy = 0;
        public int mFat = 0;
        public int mFibre = 0;
        public int mCarbs = 0;
        public int mProtein = 0;

		public string mAuthor;
		public string mSource;
		public string mNotes;

		public string[] mCategories;
        public DateTime mLastUpdated;

		//public string mCategories = "";
        public string[] mType;
		public List<RecipeStep> mStepsList = new List<RecipeStep>();
		public List<RecipeIngredient> mIngredientsList = new List<RecipeIngredient>();

		public System.Uri mImageUrl;
		public System.Uri mImagePortraitUrl;
		public System.Uri mImageLandscapeUrl;

		public NSData mImageData;
		public NSData mImagePortraitData;
		public NSData mImageLandscapeData;

		private ParseObject mParseObject;

		public bool mbImageLoaded = false;

		public bool ParsedOK
		{
			get { return mbExtractedOK;}
		}

		public string Id
		{
			get { return mParseId;}
		}	

		public string Name
		{
			get { return mName;}
		}	
		public string Description
		{
			get { return mDescription;}
		}
        public bool Favourite
        {
            get { return false;}
        }

        public string TypeString
        {
            get
            {
                string type  = string.Concat(mType.Select (t => RecipeManager.cRecordSeparator + t.ToString () ));

                if (type.Length > 1)
                    return type.Substring(1, type.Length - 1);
                else
                    return "";

            }
            set
            {
                mType = value.Split (new char[]{ RecipeManager.cRecordSeparator },StringSplitOptions.RemoveEmptyEntries);
            }
        }
		public string StepsString
		{
			get
			{
				return string.Concat(mStepsList.Select (s => RecipeManager.cRecordSeparator + s.ToString () ));
			}
			set
			{
				string[] recipeSteps = value.Split (new char[]{ RecipeManager.cRecordSeparator },StringSplitOptions.RemoveEmptyEntries);
				List<RecipeStep> newStepsList = new List<RecipeStep> ();

				foreach (string stepString in recipeSteps) 
				{
					RecipeStep newStep;

					if (RecipeStep.Parse(stepString, out newStep))
					{
						newStepsList.Add (newStep);
					}
				}
				mStepsList = newStepsList;
			}
		}
		public string IngredientsString
		{
			get
			{
				return string.Concat(mIngredientsList.Where(i => i.ToString()!= "").Select (i => RecipeManager.cRecordSeparator + i.ToString ()));
			}
			set
			{
				string[] recipeIngredients = value.Split (new char[]{ RecipeManager.cRecordSeparator }, StringSplitOptions.RemoveEmptyEntries);
				List<RecipeIngredient> newIngredientsList = new List<RecipeIngredient> ();

				foreach (string ingredientString in recipeIngredients) 
				{
					RecipeIngredient newIngredient;

					if (RecipeIngredient.Parse(ingredientString, out newIngredient))
					{
						newIngredientsList.Add (newIngredient);
					}
				}
				mIngredientsList = newIngredientsList;
			}
		}
		public List<RecipeStep> StepsList { get { return mStepsList; } }
		public List<RecipeIngredient> IngredientsList { get { return mIngredientsList; } }


        public string Source
        {
            get
            {
                return mSource;
            }
        }
		public int PrepTime
		{
			get { return mPrepTime;}
		}
		public int CookTime
		{
			get { return mCookTime;}
		}

		public string CategoriesString
		{
			set
			{

				if (value.Length > 0)
					mCategories = value.Split (new char[]{ ',' }, StringSplitOptions.RemoveEmptyEntries);
			}
			get
			{
				if (mCategories.Length == 0)
					return "";
				return string.Concat (mCategories.Select (c => c + ','));
			}
		}
		public string Notes
		{
			get { return mNotes;}
		}
		public NSData Image
		{
			get	{return mImageData;}
		}
		public NSData ImagePortrait
		{
			get	{return mImageData;}
		}
		public NSData ImageLandscape
		{
			get	{return mImageData;}
		}
		public int Serves
		{
			get {return mServes;}
		}
        public int Energy
        {
            get { return mEnergy; }
        }
        public int Fat
        {
            get { return mFat; }
        }
        public int Fibre
        {
            get { return mFibre; }
        }
        public int Carbs
        {
            get { return mCarbs; }
        }
        public int Protein
        {
            get { return mProtein; }
        }
		//Convert to base64 string
		public string ImageString
		{
			get
			{
				if (mImageData != null) {
					return mImageData.GetBase64EncodedString (NSDataBase64EncodingOptions.None);
				}
				return "";
			}
		}
		public string ImagePortraitString
		{
			get
			{
				if (mImagePortraitData != null) {
					return mImagePortraitData.GetBase64EncodedString (NSDataBase64EncodingOptions.None);
				}
				return "";
			}
		}
		public string ImageLandscapeString
		{
			get
			{
				if (mImageLandscapeData != null) {
					return mImageLandscapeData.GetBase64EncodedString (NSDataBase64EncodingOptions.None);
				}
				return "";
			}
		}
		public RecipeParse(ParseObject parseObject)
		{
			mParseObject = parseObject;
			ExtractDataFromParseObject ();
		}
			
		void  ExtractDataFromParseObject()
		{
            mParseId = mParseObject.ObjectId;
            mLastUpdated = mParseObject.UpdatedAt.Value;

			mName = mParseObject.Get<string> ("Name");

            if (mName.ToLower().Contains("grecian"))
                Console.WriteLine("");

			mDescription = mParseObject.Get<string> ("Description");
			mPrepTime = mParseObject.Get<int> ("PrepTime");
			mCookTime = mParseObject.Get<int> ("CookTime");
			mServes = mParseObject.Get<int> ("ServesMin");
			mCategories = new string[]{mParseObject.Get<string> ("Categories")};
			mDescription = mParseObject.Get<string> ("Description");
			mAuthor = mParseObject.Get<string> ("Author");
			mSource = mParseObject.Get<string> ("Source");
			mNotes = mParseObject.Get<string> ("Notes");
            TypeString = mParseObject.Get<string> ("Type");

            mEnergy = mParseObject.Get<int> ("Energy");
            mFat = mParseObject.Get<int> ("Fat");
            mFibre = mParseObject.Get<int> ("Fibre");
            mCarbs = mParseObject.Get<int> ("Carbs");
            mProtein  = mParseObject.Get<int> ("Protein");

			try
			{
				//Ingredients List
				IList<object> parseIngredients = mParseObject.Get<IList<object> > ("Ingredients");
				mIngredientsList = parseIngredients.Select (pi => new RecipeIngredient (pi as IList<object>)).ToList ();

				//Recipe Steps
				IList<object> parseSteps = mParseObject.Get<IList<object>>("Steps");
				mStepsList = parseSteps.Select(step => new RecipeStep(step as IList<object>)).ToList();

				ParseFile imageFile = mParseObject.Get<ParseFile> ("Image");
				mImageUrl = imageFile.Url;

				ParseFile portraitFile = mParseObject.Get<ParseFile> ("ImagePortrait");
				mImagePortraitUrl = portraitFile.Url;

				ParseFile landscapeFile = mParseObject.Get<ParseFile> ("ImageLandscape");
				mImageLandscapeUrl = landscapeFile.Url;

			}
			catch (Exception e) {
				Console.WriteLine (e.Message);
				mbExtractedOK = false;
				return;
			}
			mbExtractedOK = true;
		}

		public async Task<NSData> GetImageDataFromUrlAysnc (System.Uri nsDataUrl)
		{
			if (nsDataUrl.ToString() != "") 
			{
				var httpClient = new HttpClient ();

                byte[] contentsTask = await httpClient.GetByteArrayAsync (nsDataUrl);

				// await! control returns to the caller and the task continues to run on another thread
				NSData nsData = NSData.FromArray (contentsTask);

				// load from bytes
				return nsData;
			}
			return null;
		}
			

		public async Task<NSData> GetImageDataAsync()
		{
			if (mImageData != null)
				return mImageData;

            return (mImageData = await GetImageDataFromUrlAysnc (mImageUrl));
		}

		public NSData GetImageData ()
		{
			if (mImageData != null)
				return mImageData;

			mImageData = NSData.FromUrl(new NSUrl(mImageUrl.ToString()));

			return mImageData;
		}

		public NSData GetImagePortraitData ()
		{
			if (mImagePortraitData != null)
				return mImagePortraitData;

			mImagePortraitData = NSData.FromUrl(new NSUrl(mImagePortraitUrl.ToString()));

			return mImagePortraitData;
		}

		public NSData GetImageLandscapeData ()
		{
			if (mImageLandscapeData != null)
				return mImageLandscapeData;

			mImageLandscapeData = NSData.FromUrl(new NSUrl(mImageLandscapeUrl.ToString()));

			return mImageLandscapeData;
		}

		public async Task<NSData> GetImagePortraitDataAsync ()
		{
			if (mImagePortraitData != null)
				return mImagePortraitData;

			mImagePortraitData = await GetImageDataFromUrlAysnc (mImagePortraitUrl);

			return mImagePortraitData;
		}

		public async Task<NSData> GetImageLandscapeDataAsync ()
		{
			if (mImageLandscapeData != null)
				return mImageLandscapeData;

			mImageLandscapeData = await GetImageDataFromUrlAysnc (mImageLandscapeUrl);

			return mImageLandscapeData;
		}

        public bool Search(string ingredientString, bool isFinalWord= false)
        {
            string lowerIngredientString = ingredientString.ToLower();

            if (!isFinalWord)
            {
                var hits = mIngredientsList.Where(i => i.mFood.ToLower() == lowerIngredientString);
                return hits.Count() > 0;
            }
            else
            {
                var hits = mIngredientsList.Where(i => i.mFood.ToLower().StartsWith(lowerIngredientString));
                return hits.Count() > 0;
            }
        }
	}
}

