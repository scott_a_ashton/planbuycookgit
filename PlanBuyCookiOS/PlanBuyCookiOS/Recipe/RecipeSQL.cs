using System;
using System.Collections.Generic;
using SQLite;
using System.Linq;

namespace PlanBuyCookiOS
{

	public class Note
	{
		[PrimaryKey, AutoIncrement]
		public int Id { get; set; }
		public string Message { get; set; }
	}

	public class RecipeSQL : IRecipeImport
	{
		[Ignore]
		public bool ParsedOK
		{
			get { return mbInitialised;}
		}


		[PrimaryKey, AutoIncrement]
		public int Id { get; set; }

		[Unique]
		public string ParseId
		{
			get;
			set;
		}
		public string Name
		{
			get;
			set;
		}
		public string Description
		{
			get;
			set;
		}
        public bool Favourite
        {
            get;
            set;
        }
		public int PrepTime
		{
			get;
			set;
		}
		public int CookTime
		{
			get;
			set;
		}

		public int Serves
		{
			get;
			set;
		}
		public string Author
		{
			get;
			set;
		}
		public string Source
		{
			get;
			set;
		}
		public string Notes
		{
			get;
			set;
		}
        public string LastUpdated
        {
            get
            {
                return mLastUpdated.ToLongDateString();
            }
            set
            {
                DateTime.TryParse(value, out mLastUpdated);
            }
        }
        public int Energy
        {
            get;
            set;
        }
        public int Fat
        {
            get;
            set;
        }
        public int Fibre
        {
            get;
            set;
        }
        public int Carbs
        {
            get;
            set;
        }
        public int Protein
        {
            get;
            set;
        }

        public string TypeString
        {
            set
            {
                if (value.Length > 0)
                    mType = value.Split (new char[]{ ',' },StringSplitOptions.RemoveEmptyEntries).Select(s => s.Trim()).ToArray();
            }
            get
            {
                string type  = string.Concat(mType.Select (t => t.ToString () + ',' ));

                if (type.Length > 1)
                    return type.Substring(0, type.Length - 1);
                else
                    return "";

            }
        }

		public string CategoriesString
		{
			set
			{
                if (value.Length > 0)
                    mCategories = value.Split(new char[]{ ',' }, StringSplitOptions.RemoveEmptyEntries).Select(s => s.Trim()).ToArray();
			}
			get
			{
				if (mCategories.Length == 0)
					return "";
				return string.Concat (mCategories.Select (c => c + ','));
			}
		}
		public string ImageString
		{
			get;set;
		}
		public string ImagePortraitString
		{
			get;set;
		}
		public string ImageLandscapeString
		{
			get;set;
		}
		public string StepsString
		{
			get
			{
				return string.Concat(mStepsList.Select (s => RecipeManager.cRecordSeparator + s.ToString () ));
			}
			set
			{
				string[] recipeSteps = value.Split (new char[]{ RecipeManager.cRecordSeparator },StringSplitOptions.RemoveEmptyEntries);
				List<RecipeStep> newStepsList = new List<RecipeStep> ();

				foreach (string stepString in recipeSteps) 
				{
					RecipeStep newStep;

					if (RecipeStep.Parse(stepString, out newStep))
					{
						newStepsList.Add (newStep);
					}
				}
				mStepsList = newStepsList;
			}
		}
		public string IngredientsString
		{
			get
			{
				return string.Concat(mIngredientsList.Where(i => i.ToString()!= "").Select (i => RecipeManager.cRecordSeparator + i.ToString ()));
			}
			set
			{
				string[] recipeIngredients = value.Split (new char[]{ RecipeManager.cRecordSeparator }, StringSplitOptions.RemoveEmptyEntries);
				List<RecipeIngredient> newIngredientsList = new List<RecipeIngredient> ();

				foreach (string ingredientString in recipeIngredients) 
				{
					RecipeIngredient newIngredient;

					if (RecipeIngredient.Parse(ingredientString, out newIngredient))
					{
						newIngredientsList.Add (newIngredient);
					}
				}
				mIngredientsList = newIngredientsList;
			}
		}

		public List<RecipeStep> StepsList { get { return mStepsList; } }
		public List<RecipeIngredient> IngredientsList { get { return mIngredientsList; } }

		public bool mbInitialised = false;

        public DateTime mLastUpdated;
        public string[] mType;
		public string[] mCategories;
		public List<RecipeStep> mStepsList = new List<RecipeStep>();
		public List<RecipeIngredient> mIngredientsList = new List<RecipeIngredient>();

		public RecipeSQL (RecipeParse recipeParse)
		{
			if (recipeParse.ParsedOK) 
			{
				ParseId = recipeParse.mParseId;
				Name = recipeParse.Name;
                Description = recipeParse.Description;
                Favourite = false;
				PrepTime = recipeParse.PrepTime;
				CookTime = recipeParse.CookTime;
				Serves = recipeParse.mServes;
				Author = recipeParse.mAuthor;
				Source = recipeParse.mSource;
				Notes = recipeParse.mNotes;

                Energy = recipeParse.mEnergy;
                Fat = recipeParse.mFat;
                Fibre = recipeParse.mFibre;
                Carbs = recipeParse.mCarbs;
                Protein = recipeParse.mProtein;

                mType = recipeParse.mType;
                mCategories = recipeParse.mCategories;
                mLastUpdated = recipeParse.mLastUpdated;

				ImageString = recipeParse.ImageString;
				ImagePortraitString = recipeParse.ImagePortraitString;
				ImageLandscapeString = recipeParse.ImageLandscapeString;

				mStepsList = new List<RecipeStep> (recipeParse.mStepsList);
				mIngredientsList = new List<RecipeIngredient> (recipeParse.mIngredientsList);

				mbInitialised = true;
			}
		}

		//Default constructor : make sure there are no null string values
		public RecipeSQL()
		{
			ParseId = "";
			Name = "Recipe Name";
			PrepTime = 0;
			CookTime = 0;
			Serves = 0;
			Author = "";
			Source = "";
			Notes = "";
            TypeString = "";
            Energy = 0;
            Fat = 0;
            Fibre = 0;
            Carbs = 0;
            Protein = 0;
			mCategories = new string[0];

			ImageString = "";
			ImagePortraitString = null;
			ImageLandscapeString = null;

            StepsString = "";
			mStepsList = null;
			mIngredientsList = null;

			mbInitialised = true;
		}

        public bool Search(string ingredientString, bool isFinalWord= false)
        {
            string lowerIngredientString = ingredientString.ToLower();

            if (!isFinalWord)
            {
                var hits = mIngredientsList.Where(i => i.mFood.ToLower().Contains(lowerIngredientString));
                return hits.Count() > 0;
            }
            else
            {
                var hits = mIngredientsList.Where(i => i.mFood.ToLower().StartsWith(lowerIngredientString));
                return hits.Count() > 0;
            }
        }
	}
}

