using System;
using System.Collections.Generic;
using UIKit;

namespace PlanBuyCookiOS
{
	public enum SpecialMealType
	{
		Takeaway = -1,
		FreezerLeftovers = -2,
		Writeyourown = -3,
        WWWRecipe = -4,
        DiningOut = -5,
        Unknown = -6
	}

	public class RecipeSpecialMeal : IRecipeImport
	{
        public SpecialMealType Type { private set; get;}

        public RecipeSpecialMeal (string specialMealTypeString, string modifiedName="")
        {
            if (specialMealTypeString == RecipeSpecialMeal.MealTypeString(SpecialMealType.FreezerLeftovers))
                Type = SpecialMealType.FreezerLeftovers;
            else if (specialMealTypeString == RecipeSpecialMeal.MealTypeString(SpecialMealType.Takeaway))
                Type = SpecialMealType.Takeaway;
            else if (specialMealTypeString == RecipeSpecialMeal.MealTypeString(SpecialMealType.Writeyourown))
                Type = SpecialMealType.Writeyourown;
            else if (specialMealTypeString == RecipeSpecialMeal.MealTypeString(SpecialMealType.WWWRecipe))
                Type = SpecialMealType.WWWRecipe;
            else if (specialMealTypeString == RecipeSpecialMeal.MealTypeString(SpecialMealType.DiningOut))
                Type = SpecialMealType.DiningOut;
            else
                Type = SpecialMealType.Unknown;

            //Use Name for the mealType
            if (modifiedName != "")
                Description = modifiedName;
            else if (Type != SpecialMealType.Unknown)
                Description =  specialMealTypeString;
        }

        public RecipeSpecialMeal (SpecialMealType specialMealType)
		{
			//Use Name for the mealType
            Type = specialMealType;
            Description =  MealTypeString(specialMealType);
		}

        public static bool IsSpecialMealString(string s)
        {
            if (s == RecipeSpecialMeal.MealTypeString(SpecialMealType.FreezerLeftovers))
                return true;
            else if (s == RecipeSpecialMeal.MealTypeString(SpecialMealType.Takeaway))
                return true;
            else if (s == RecipeSpecialMeal.MealTypeString(SpecialMealType.Writeyourown))
                return true;
            else if (s == RecipeSpecialMeal.MealTypeString(SpecialMealType.WWWRecipe))
                return true;
            else if (s == RecipeSpecialMeal.MealTypeString(SpecialMealType.DiningOut))
                return true;
            else
                return false;
        }
            
        public static string MealTypeString(SpecialMealType specialMealType)
        {
            if (specialMealType == SpecialMealType.FreezerLeftovers) {
                return "Freezer/Leftovers";
            } else if (specialMealType == SpecialMealType.Takeaway) {
                return "Takeaway";
            } else if (specialMealType == SpecialMealType.Writeyourown) {
                return "Plan Your Own Meal";
            } else if (specialMealType == SpecialMealType.WWWRecipe) {
                return "Web Recipe";
            } else if (specialMealType == SpecialMealType.DiningOut) {
                return "Dining Out";
            }
            Console.Error.WriteLine("Invalid meal type when creating special meal");
            return "Special Meal";
        }

        public static UIImage Image(SpecialMealType specialMealType)
        {
            UIImage recipeImage = null;

            switch (specialMealType)
            {
                case SpecialMealType.Takeaway:
                    recipeImage = UIImage.FromFile("TakeawayPortrait.png");
                    break;
                case SpecialMealType.FreezerLeftovers:
                    recipeImage = UIImage.FromFile("LeftoversPortrait.png");
                    break;
                case SpecialMealType.Writeyourown:
                    recipeImage = UIImage.FromFile("WriteYourOwnPortrait.png");
                    break;
                case SpecialMealType.WWWRecipe:
                    recipeImage = UIImage.FromFile("WWWRecipePortrait.png");
                    break;
                case SpecialMealType.DiningOut:
                    recipeImage = UIImage.FromFile("DiningOutPortrait.png");
                    break;
            }

            return recipeImage;
        }

        public static UIImage Image(string specialMealTypeString)
        {
            if (specialMealTypeString == MealTypeString(SpecialMealType.Takeaway))
                return Image(SpecialMealType.Takeaway);
            else if (specialMealTypeString == MealTypeString(SpecialMealType.FreezerLeftovers))
                return Image(SpecialMealType.FreezerLeftovers);
            else if (specialMealTypeString == MealTypeString(SpecialMealType.Writeyourown))
                return Image(SpecialMealType.Writeyourown);
            else if (specialMealTypeString == MealTypeString(SpecialMealType.WWWRecipe))
                return Image(SpecialMealType.WWWRecipe);
            else if (specialMealTypeString == MealTypeString(SpecialMealType.DiningOut))
                return Image(SpecialMealType.DiningOut);

            Console.Error.WriteLine("Invalid special meal type in Image()");
            return null;
        }
		public bool ParsedOK { get {return true; } }
		public string Name 
        { 
            //Ignore setter
            set { }
            get
            {
                return MealTypeString(Type);
            } 
        }
		public string Description { set; get; }
        public bool Favourite { get { return false; } }
		public int PrepTime { get{return 0; } }
		public int CookTime { get{return 0; } }
		public int Serves { get{return 0; } }
		public string Notes { get{return ""; } }
        public int Energy { get{ return 0; }}
        public int Fat { get{ return 0; }}
        public int Fibre { get{ return 0; }}
        public int Carbs { get{ return 0; }}
        public int Protein { get{ return 0; }}

        public string TypeString  { get{return ""; } }
		public string StepsString { get{return ""; } }
		public string IngredientsString { get{return ""; } }
		public string CategoriesString { get{return ""; } }
        public string Source { get{return ""; }}

		public string ImageString { get{return ""; } }
		public string ImagePortraitString { get{return ""; } }
		public string ImageLandscapeString { get{return ""; } }
        public bool Search(string ingredientsString, bool isFinalWord=false) {return false;}

		public List<RecipeStep> StepsList { get { throw new NotImplementedException (); return new List<RecipeStep>(); } }
		public List<RecipeIngredient> IngredientsList { get { throw new NotImplementedException (); return new List<RecipeIngredient>(); } }

	}
}

