using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace PlanBuyCookiOS
{
	public class RecipeStep
	{
		public int mIndex=0;
		public int mTimer=0; //min
		public string mMethod="";

		public RecipeStep (IList<object> parseStep)
		{
			mIndex = Convert.ToInt32(parseStep[0]);
			mTimer = Convert.ToInt32(parseStep[1]);
			mMethod = Convert.ToString(parseStep[2]);
		}

		public RecipeStep (int index, int timer, string method)
		{
			mIndex = index;
			mTimer = timer;
			mMethod = method;
		}

		public override string ToString ()
		{
			string recipeString = mIndex.ToString () + RecipeManager.cFieldSeparator +
			                      mTimer.ToString () + RecipeManager.cFieldSeparator +
			                      mMethod;
			return recipeString;
		}

		static public bool Parse(string recipeString, out RecipeStep recipeStep)
		{
			string[] fields = recipeString.Split (new char[]{ RecipeManager.cFieldSeparator });

			if (fields.Length != 3) {
				recipeStep = null;
				return false;
			}

			//Debug.Assert (fields.Length == 3);

			int index = 0;
			int timer = 0;
			string method = "";

			bool bParsed = int.TryParse (fields [0], out index);
			bParsed |= int.TryParse (fields [1], out timer);
			method = fields [2];

			if (bParsed)
				recipeStep = new RecipeStep (index, timer, method);
			else
				recipeStep = null;

			return bParsed;
		}
	}
}

