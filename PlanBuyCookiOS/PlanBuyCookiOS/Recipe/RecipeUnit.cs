using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace PlanBuyCookiOS
{
    public class RecipeUnit
    {
        public string Name { set; get;}
        public string Plural { set; get;}
        public string Unit { set; get;}



        public RecipeUnit (string name, string plural, string unit)
        {
            Name = name;
            Plural = plural;
            Unit = unit;
        }
    }
}

