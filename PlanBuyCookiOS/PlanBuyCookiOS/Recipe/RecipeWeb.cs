using System;
using System.Collections.Generic;
using System.Net;
using Foundation;
using System.Security.Cryptography.X509Certificates;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Net.Http;
using System.IO;
using System.Text;
using HtmlAgilityPack;
using System.Linq;
using System.Xml.XPath;
using System.Xml;

namespace PlanBuyCookiOS
{

    public class RecipeWeb : IRecipeImport
	{
		protected bool mbParsedOK = false;
		protected HtmlDocument mHtmlDocument = new HtmlDocument();

		protected string mName;
		protected string mDescription;
		protected int mPrepTime = 0;
		protected int mCookTime = 0;

		protected string[] mRecipeSteps;
		protected string[] mIngredients;
		protected string[] mCategories;

		protected string mImageUrl;
		protected string mNotes="";
        protected string mSource="";
		protected NSData mImageData;
		protected int mServings;

        public List<RecipeStep> mStepsList = new List<RecipeStep>();
        public List<RecipeIngredient> mIngredientsList = new List<RecipeIngredient>();

		public bool mbImageLoaded = false;

		public bool ParsedOK
		{
			get { return mbParsedOK;}
		}
		public string Description
		{
			get { return mDescription;}
		}
		public string Name
		{
			get { return mName;}
		}		
        public bool Favourite
        {
            get { return false;}
        }       

		public string[] RecipeSteps
		{
			get	{return mRecipeSteps;}
		}
		public string[] Ingredients
		{
			get	{return mIngredients;}
		}
		public int PrepTime
		{
			get { return mPrepTime;}
		}
		public int CookTime
		{
			get { return mCookTime;}
		}

        public string TypeString 
        { 
            get
            {
                return "";
            }
        }

        public string StepsString
        {
            get
            {
                return string.Concat(mStepsList.Select (s => RecipeManager.cRecordSeparator + s.ToString () ));
            }
            set
            {
//                string[] recipeSteps = value.Split (new char[]{ RecipeManager.cRecordSeparator },StringSplitOptions.RemoveEmptyEntries);
//                List<RecipeStep> newStepsList = new List<RecipeStep> ();
//
//                for (int stepIndex = 0; stepIndex < mRecipeSteps; stepIndex++)
//                {
//                    newStepsList.Add(new RecipeStep(stepIndex + 1, 0, recipeSteps[stepIndex]);
//                }
            }
        }

        public string IngredientsString
        {
            get
            {
                return string.Concat(mIngredientsList.Where(i => i.ToString()!= "").Select (i => RecipeManager.cRecordSeparator + i.ToString ()));
            }
            set
            {
                string[] recipeIngredients = value.Split (new char[]{ RecipeManager.cRecordSeparator }, StringSplitOptions.RemoveEmptyEntries);
                List<RecipeIngredient> newIngredientsList = new List<RecipeIngredient> ();

                foreach (string ingredientString in recipeIngredients) 
                {
                    RecipeIngredient newIngredient;

                    if (RecipeIngredient.Parse(ingredientString, out newIngredient))
                    {
                        newIngredientsList.Add (newIngredient);
                    }
                }
                mIngredientsList = newIngredientsList;
            }
        }
        public string CategoriesString
        {
            set
            {
				if (value.Length > 0)
					mCategories = value.Split (new char[]{ ',' }, StringSplitOptions.RemoveEmptyEntries);
				else
					mCategories = new string[]{ };
            }
            get
            {
				if (mCategories == null)
					mCategories = new string[]{ };

                if (mCategories.Length == 0)
                    return "";

                return string.Concat (mCategories.Select (c => c + ','));
            }
        }


		public List<RecipeStep> StepsList { get { return mStepsList; } }
		public List<RecipeIngredient> IngredientsList { get { return mIngredientsList; } }

        public string Source
        {
            get
            {
                return mSource;
            }
        }

		public string Notes
		{
			get { return mNotes;}
		}
		public NSData Image
		{
			get	{return mImageData;}
		}
		public NSData ImagePortrait
		{
			get	{return mImageData;}
		}
		public NSData ImageLandscape
		{
			get	{return mImageData;}
		}
        public string ImageString
        {
            get
            {
                if (mImageData != null) {
                    return mImageData.GetBase64EncodedString (NSDataBase64EncodingOptions.None);
                }
                return "";
            }
        }
        public string ImagePortraitString
        {
            get
            {
                if (mImageData != null) {
                    return mImageData.GetBase64EncodedString (NSDataBase64EncodingOptions.None);
                }
                return "";
            }
        }
        public string ImageLandscapeString
        {
            get
            {
                if (mImageData != null) {
                    return mImageData.GetBase64EncodedString (NSDataBase64EncodingOptions.None);
                }
                return "";
            }
        }

        //-------WEB Recipes don't need to support nutritional information ------
        public int Energy
        {
            get { return 0; }
        }
        public int Fat
        {
            get { return 0; }
        }
        public int Fibre
        {
            get { return 0; }
        }
        public int Carbs
        {
            get { return 0; }
        }
        public int Protein
        {
            get { return 0; }
        }
        //-------WEB Recipes don't need to support nutritional information ------

		public int Serves
		{
			get {return mServings;}
		}

		public virtual string HostUrl
		{
			get { return "";}
		}

        bool mbIsValidAddress = false;

        public delegate RecipeWeb RecipeConstruct (string url);

        static bool                 mbStaticInitialise = false;
        static Dictionary<string, RecipeConstruct>  mSupportedWebsites = new Dictionary<string, RecipeConstruct>(); 

        public static void SupportedWebsites()
        {
            //Register supported websites
            mSupportedWebsites.Add ("allrecipes.com.au", (string url) => new RecipeWeb_allrecipescomau(url));
            mSupportedWebsites.Add ("www.goodfood.com.au", (string url) => new RecipeWeb_goodfoodcomau(url));
            mSupportedWebsites.Add ("www.taste.com.au", (string url) => new RecipeWeb_tastecomau(url));

            mbStaticInitialise = true;
        }


        public static RecipeConstruct GetConstructor(NSUrl url )
        {
            if (!mbStaticInitialise)
                SupportedWebsites();

            if (url != null && url.Host!=null) 
            {
                RecipeConstruct constructor;

                if (mSupportedWebsites.TryGetValue (url.Host, out constructor)) 
                {
                    return constructor;
                }
            }
            return null;
        }

        public RecipeWeb(string recipeUrl)
        {
            mSource = recipeUrl;

            NSUrl url = NSUrl.FromString (mSource);

            if (url != null && url.Host != null) 
            {
                mbIsValidAddress = true;
            } 
        }

        public virtual void CreateRecipeStepsFromWebSteps(string[] recipeSteps)
        {
            mStepsList = new List<RecipeStep>();

            if (recipeSteps == null)
                return;
            if (recipeSteps.Length == 0)
                return;
                
            for (int stepIndex = 0; stepIndex < recipeSteps.Length; stepIndex++)
            {
                mStepsList.Add(new RecipeStep(stepIndex + 1, 0, recipeSteps[stepIndex]));
            }
        }

        public virtual void CreateIngredientsFromWebIngredients(string[] recipeIngredients)
        {
            mIngredientsList = new List<RecipeIngredient>();

            if (recipeIngredients == null)
                return;
            if (recipeIngredients.Length == 0)
                return;

            for (int ingIndex = 0; ingIndex < recipeIngredients.Length; ingIndex++)
            {
                mIngredientsList.Add(new RecipeIngredient(ingIndex, 0.0f, "", recipeIngredients[ingIndex],""));
            }
        }

        public async Task<bool> LoadRecipeHtml()
		{
            if (!mbIsValidAddress)
                return false;

			try
			{
                HttpClient client = new HttpClient ();

                byte[] urlContentsTask = await client.GetByteArrayAsync(mSource);

				mHtmlDocument = new HtmlDocument ();
                mHtmlDocument.LoadHtml (Encoding.ASCII.GetString (urlContentsTask));

				if (urlContentsTask.Length > 0)
				{
					mName = ExtractName();

	                mRecipeSteps = ExtractRecipeSteps ();
	                CreateRecipeStepsFromWebSteps(mRecipeSteps);

					mIngredients = ExtractIngredients ();
	                CreateIngredientsFromWebIngredients(mIngredients);
	                //IngredientString = string.Concat( mIngredients.Select(i=>RecipeManager.cRecordSeparator + i);

	                //StepsString  = string.Concat( mIngredients.Select(s=>RecipeManager.cRecordSeparator + s);

					mServings = ExtractNumberOfServings ().Item1;
					mPrepTime = ExtractPrepTime ();
					mCookTime = ExtractCookTime ();
	                mImageData = await GetImage();

					mbParsedOK = true;
				}
				else
				{
					mbParsedOK = false;
				}
			}
			catch (Exception e) 
			{
                Console.WriteLine (e.Message);
			}
			return mbParsedOK;
		}

		public virtual string ExtractName()
		{
			return "";
		}
		public virtual string ExtractDescription()
		{
			return "";
		}

		public virtual async Task<NSData> GetImage()
		{
			return new NSData ();
		}
		public virtual string[] ExtractIngredients()
		{
			return new string[0];
		}
		public virtual string[] ExtractRecipeSteps()
		{
			return new string[0];
		}
		public virtual int ExtractCookTime()
		{
			return 0;
		}
		public virtual int ExtractPrepTime()
		{
			return 0;
		}
		//number of servings returns a number range
		public virtual Tuple<int,int> ExtractNumberOfServings()
		{
			return new Tuple<int, int>(1,1);
		}

        public bool Search(string ingredientString, bool isFinalWord= false)
        {
            string lowerIngredientString = ingredientString.ToLower();

            if (!isFinalWord)
            {
                var hits = mIngredientsList.Where(i => i.mFood.ToLower() == lowerIngredientString);
                return hits.Count() > 0;
            }
            else
            {
                var hits = mIngredientsList.Where(i => i.mFood.ToLower().StartsWith(lowerIngredientString));
                return hits.Count() > 0;
            }
        }
	}
}

