using System;
using System.Collections.Generic;
using System.Net;
using Foundation;
using UIKit;
using System.Security.Cryptography.X509Certificates;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Net.Http;
using System.IO;
using System.Text;
using HtmlAgilityPack;
using System.Linq;


namespace PlanBuyCookiOS
{
	public class RecipeWeb_allrecipescomau : RecipeWeb
	{
        public RecipeWeb_allrecipescomau(string address) : base(address)
        {
        }

		public override string ExtractName()
		{
			return string.Format ("[RecipeWeb_allrecipescomau]");
		}

		public override async Task<NSData> GetImage()
		{
			HttpClient client = new HttpClient();
			HtmlNode imageNode = mHtmlDocument.DocumentNode.SelectNodes ("//meta").Where (img => img.GetAttributeValue ("property", "") == "og:image").FirstOrDefault ();

			mImageUrl = imageNode.GetAttributeValue ("content", "");

			if (mImageUrl != "") 
			{
				byte[] bytes = await client.GetByteArrayAsync (mImageUrl);
				mImageData = NSData.FromArray (bytes);

				mbImageLoaded = true;
			}
			return mImageData;
		}

		public override string[] ExtractIngredients()
		{
			return mHtmlDocument.DocumentNode.SelectNodes ("//li").Where (e => e.GetAttributeValue ("class", "") == "ingredient").Select (node => node.InnerText.Trim()).ToArray ();
		}

		public override string[] ExtractRecipeSteps()
		{
			return mHtmlDocument.DocumentNode.SelectNodes ("//li").Where (e => e.GetAttributeValue ("class", "") == "directionsSteps").Select (node => node.FirstChild.InnerText.Trim()).ToArray();
		}

		public override int ExtractCookTime ()
		{
			HtmlNode cookTimeWrapper = mHtmlDocument.DocumentNode.SelectNodes ("//span").Where (e => e.GetAttributeValue ("class", "") == "cookTime").First();

			var spanElements = cookTimeWrapper.Descendants ().Where (element => element.NodeType == HtmlNodeType.Element && element.Name == "span");

			HtmlNode cookTimeElement = spanElements.Where(e => e.GetAttributeValue ("class", "") == "accent").First();

			int time = 0;
			int.TryParse (cookTimeElement.InnerText, out time);

			return time;
		}

		public override int ExtractPrepTime ()
		{
			HtmlNode prepTimeWrapper = mHtmlDocument.DocumentNode.SelectNodes ("//span").Where (e => e.GetAttributeValue ("class", "") == "prepTime").First();

			var spanElements = prepTimeWrapper.Descendants ().Where (element => element.NodeType == HtmlNodeType.Element && element.Name == "span");

			HtmlNode prepTimeElement = spanElements.Where(e => e.GetAttributeValue ("class", "") == "accent").First();

			int time = 0;
			int.TryParse (prepTimeElement.InnerText, out time);

			return time;		
		}

		public override Tuple<int,int> ExtractNumberOfServings()
		{
			string numServingsString = mHtmlDocument.DocumentNode.SelectNodes ("//h3").Where(e => e.GetAttributeValue ("class", "") == "yield").Select(node => node.InnerText).FirstOrDefault();

			numServingsString = numServingsString.Replace ("&nbsp;", "").Replace ("Serves:", "");

			int numServes = 1;

			if (!int.TryParse (numServingsString, out numServes)) 
			{
				Console.WriteLine ("Error parsing servings: " + numServingsString);
				return new Tuple<int, int> (1, 1);
			}
			else
				return new Tuple<int, int> (numServes, numServes);
		}
	}
}

