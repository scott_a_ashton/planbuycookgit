using System;
using System.Collections.Generic;
using System.Net;
using Foundation;
using System.Security.Cryptography.X509Certificates;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Net.Http;
using System.IO;
using System.Text;
using HtmlAgilityPack;
using System.Linq;


namespace PlanBuyCookiOS
{
	public class RecipeWeb_goodfoodcomau : RecipeWeb
	{

        public RecipeWeb_goodfoodcomau(string address) : base(address)
        {
        }


		public override async Task<NSData> GetImage()
		{
			HttpClient client = new HttpClient();
			HtmlNode imageNode = mHtmlDocument.DocumentNode.SelectNodes ("//img").Where (img => img.GetAttributeValue ("rel", "") == "v:photo").FirstOrDefault ();

			mImageUrl = imageNode.GetAttributeValue ("src", "");

			if (mImageUrl != "") 
			{
				byte[] bytes = await client.GetByteArrayAsync (mImageUrl);
				mImageData = NSData.FromArray (bytes);
				mbImageLoaded = true;
			}
			return mImageData;
		}
		public override string[] ExtractIngredients()
		{
			HtmlNode ingredientsWrapper = mHtmlDocument.DocumentNode.SelectNodes ("//div").Where(node => node.GetAttributeValue("class","") == "cN-simpleList listIngredients").FirstOrDefault();

			string[] ingredients= new string[0];

			if (ingredientsWrapper != null) {
				ingredients = ingredientsWrapper.Descendants().Where(element => element.NodeType == HtmlNodeType.Element && element.Name=="li").
												 Select (ingredient => ingredient.InnerText).
												 ToArray();
			}

			return ingredients;
		}
		public override string[] ExtractRecipeSteps()
		{
			HtmlNode steps = mHtmlDocument.DocumentNode.SelectNodes ("//div").Where(node => node.GetAttributeValue("property","") == "v:instructions").FirstOrDefault();

			string[] ssteps = steps.Elements("p").Select (step => step.InnerText).ToArray ();

			return ssteps;
		}

		public override int ExtractCookTime ()
		{
			HtmlNode servesNodeWrapper = mHtmlDocument.DocumentNode.SelectNodes ("//dl").Where (node => node.GetAttributeValue ("class", "") == @"cS-rateMetadata metaCuisine metaCuisineEasy").FirstOrDefault ();

			string metaData = servesNodeWrapper.InnerText;

			//Less than 30 minutes
			if (metaData.Contains("< 30 mins"))
			    return 30;
		    if (metaData.Contains("30 mins - 1 hour"))
			    return 45;
			if (metaData.Contains("1-2 hours"))
				return 90;
			if (metaData.Contains("2 hours +"))
				return 120;

			return 0;
		}

		public override int ExtractPrepTime ()
		{
			//there is no prep time in good food
			return base.ExtractPrepTime ();
		}

		public override Tuple<int,int> ExtractNumberOfServings()
		{
			HtmlNode servesNodeWrapper = mHtmlDocument.DocumentNode.SelectNodes ("//dl").Where (node => node.GetAttributeValue ("class", "") == @"cS-rateMetadata metaCuisine metaCuisineEasy").FirstOrDefault ();

			HtmlNode serves = servesNodeWrapper.ChildNodes.Where (dd => dd.Name == "dd" && 
																		dd.GetAttributeValue ("class", "") == "" &&
																		(dd.ChildNodes.Count == 1 && dd.ChildNodes [0].NodeType == HtmlNodeType.Text)).FirstOrDefault ();

			string servingsString = serves.InnerText;

			string[] servings = servingsString.Split ('-');

			int min = 1;
			int max = 1;

			int.TryParse (servings[0], out min);

			if (servings.Count() > 1)
				int.TryParse (servings [1], out max);
			else
				max = min;

			return new Tuple<int, int> (min, max);
		}
	}
}

