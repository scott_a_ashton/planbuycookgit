using System;
using System.Globalization;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using Foundation;
using System.Security.Cryptography.X509Certificates;
using System.Diagnostics;
using System.Threading.Tasks;
using System.IO;
using System.Text;
using HtmlAgilityPack;
using System.Linq;

namespace PlanBuyCookiOS
{
	public class RecipeWeb_tastecomau : RecipeWeb
	{

        public RecipeWeb_tastecomau(string address) : base(address)
        {
        }

        //<meta property="og:title" content="Baked Apples With Sultana Crumble Recipe" />
        public override string ExtractName()
        {
            HtmlNode titleNode = mHtmlDocument.DocumentNode.SelectNodes ("//meta").Where (img => img.GetAttributeValue ("property", "") == "og:title").FirstOrDefault ();

            string title = titleNode.GetAttributeValue("content","").ToLower();
            if (title.Length > 0)
            {
                title = string.Concat(CultureInfo.CurrentCulture.TextInfo.ToTitleCase(title.Substring(0,1))) + title.Substring(1, title.Length -1);
                title = title.Replace("recipe", "");
				title = title.Replace ("&amp;", "&");

            }
            return title;
        }

		public override async Task<NSData> GetImage()
		{
			HttpClient client = new HttpClient();
			HtmlNode imageNode = mHtmlDocument.DocumentNode.SelectNodes ("//img").Where (img => img.GetAttributeValue ("itemprop", "") == "photo").FirstOrDefault ();

			mImageUrl = imageNode.GetAttributeValue ("src", "");

			if (mImageUrl != "") 
			{
				byte[] bytes = await client.GetByteArrayAsync (mImageUrl);
				mImageData = NSData.FromArray (bytes);
				mbImageLoaded = true;
			}
			return mImageData;
		}

		public override string[] ExtractIngredients()
		{
			HtmlNode ingredientsWrapper = mHtmlDocument.DocumentNode.SelectNodes ("//ul").Where(e => e.GetAttributeValue ("class", "") == "ingredient-table").FirstOrDefault();

			string[] ingredients= new string[0];
			if (ingredientsWrapper != null) {
				ingredients = ingredientsWrapper.SelectNodes("//span").Where (span => span.GetAttributeValue ("class", "") == "element").Select (ingredientElement => ingredientElement.InnerText).ToArray();
			}

			return ingredients;
		}

		public override string[] ExtractRecipeSteps()
		{
			HtmlNode[] steps = mHtmlDocument.DocumentNode.SelectNodes ("//li").Where (e => e.GetAttributeValue ("class", "") == "methods").ToArray();

			string[] ssteps = steps.Select (step => step.Element("p").InnerText).ToArray ();

            for (int s = 0; s < steps.Length; s++)
            {
                ssteps[s] = ReplaceSpecialStrings(ssteps[s]);
            }

			return ssteps;
		}

		public override int ExtractCookTime ()
		{
			HtmlNode numServingsWrapper = mHtmlDocument.DocumentNode.SelectNodes ("//td").Where(e => e.GetAttributeValue ("class", "") == "cookTime").FirstOrDefault();

			string timeString = numServingsWrapper.SelectNodes ("./em").Select (el => el.InnerText).FirstOrDefault();

			string[] timeSplit = timeString.Split (':');

			int hours = 0;
			int.TryParse (timeSplit [0], out hours);

			int minutes = 0;
			int.TryParse (timeSplit [1], out minutes);

			return hours * 60 + minutes;
		}

		public override int ExtractPrepTime ()
		{
			HtmlNode numServingsWrapper = mHtmlDocument.DocumentNode.SelectNodes ("//td").Where(e => e.GetAttributeValue ("class", "") == "prepTime").FirstOrDefault();

			string timeString = numServingsWrapper.SelectNodes ("./em").Select (el => el.InnerText).FirstOrDefault();

			string[] timeSplit = timeString.Split (':');

			int hours = 0;
			int.TryParse (timeSplit [0], out hours);

			int minutes = 0;
			int.TryParse (timeSplit [1], out minutes);

			return hours * 60 + minutes;
		}

		public override Tuple<int,int> ExtractNumberOfServings()
		{
			HtmlNode numServingsWrapper = mHtmlDocument.DocumentNode.SelectNodes ("//td").Where(e => e.GetAttributeValue ("class", "") == "servings").FirstOrDefault();

			string servingsString = numServingsWrapper.SelectNodes ("./em").Select (el => el.InnerText).FirstOrDefault();

			int servings = 1;
			int.TryParse (servingsString, out servings);

			return new Tuple<int, int> (servings, servings);
		}

        public override void CreateRecipeStepsFromWebSteps(string[] recipeSteps)
        {
            mStepsList = new List<RecipeStep>();

            if (recipeSteps == null)
                return;
            if (recipeSteps.Length == 0)
                return;

            for (int stepIndex = 0; stepIndex < recipeSteps.Length; stepIndex++)
            {
                mStepsList.Add(new RecipeStep(stepIndex + 1, 0, recipeSteps[stepIndex]));
            }
        }

        const string cColourIndexString = "";
        public override void CreateIngredientsFromWebIngredients(string[] recipeIngredients)
        {
            mIngredientsList = new List<RecipeIngredient>();

            if (recipeIngredients == null)
                return;
            if (recipeIngredients.Length == 0)
                return;

            for (int ingIndex = 0; ingIndex < recipeIngredients.Length; ingIndex++)
            {
                mIngredientsList.Add(new RecipeIngredient(ingIndex, 0.0f, "", recipeIngredients[ingIndex],""));
            }
        }


        public string ReplaceSpecialStrings(string s)
        {
			s = s.Replace("&deg;", "°");
			s = s.Replace ("&amp;", "&");
            return s;
        }
	}
}

