using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Diagnostics;
using Foundation;

using RecipeIngredient = System.Tuple<PlanBuyCookiOS.Quantity, PlanBuyCookiOS.Ingredient>;
using RecipeStep = System.Tuple<string, float>; 
using Serves = System.Tuple<int, int>;
using Category = System.String;

namespace PlanBuyCookiOS
{
	public class RecipeXml : ExcelXml
	{
		public enum Type
		{
			Meat, Beef, Main
		};

		string 						mName;
		string 						mDescription;
		string 						mPrepTime;			//Minutes
		string 						mCookTime; 			//Minutes
		int				 			mNumServes; 		//eg 4, 4-6, 8
		string[]					mCategories;
		List<string>				mType;
		string[]					mSteps;
		string[]					mIngredients;
		string 						mAuthor;
		string 						mSource;

		//we load the ingredients initially as a table before parsing the data into the ingredients dictionary
		WorkSheet					mRecipeTable;
		int 						mStartingColumn = 1;
		int 						mTitleRow = 1;
		List<Recipe.Ingredient> 	mIngredientsList = new List<Recipe.Ingredient> ();
		List<Recipe.Step> 			mRecipeSteps = new List<Recipe.Step> ();
		string 						mRecipeNotes="";
		NSData						mImageData;

		bool						mbHasParsed = false;

		public bool ParsedOK
		{
			get { return mbHasParsed;}
		}
		public string Description
		{
			get { return mDescription;}
		}
		public string Name
		{
			get { return mName;}
		}		
		public string[] Ingredients
		{
			get	{return mIngredients;}
		}
		public int PrepTime
		{
			get 
			{ 
				int parsedPrepTime=0;
				int.TryParse(mPrepTime, out parsedPrepTime);

				return parsedPrepTime;
			}
		}
		public int CookTime
		{
			get 
			{ 
				int parsedCookTime=0;
				int.TryParse(mCookTime, out parsedCookTime);

				return parsedCookTime;
			}
		}
		public string[] StepsString
		{
			get { return mSteps;}
		}
		public string[] Categories
		{
			get { return mCategories;}
		}

		public NSData Image
		{
			get	{return mImageData;}
		}
		public NSData ImagePortrait
		{
			get	{return mImageData;}
		}
		public NSData ImageLandscape
		{
			get	{return mImageData;}
		}

		public int NumServes
		{
			get {return mNumServes;}
		}

		public string Notes
		{
			get { return mRecipeNotes;}
		}	

		public bool HasParsedOK
		{
			get { return mbHasParsed; }
		}

		public RecipeXml (string xmlRecipeFilename) : base(xmlRecipeFilename)
		{
		}

		bool TryParseNumServes(string serves, out Serves parsedNumServes)
		{
			string[] servings = serves.Split ('-');

			int min = 1;
			int max = 1;

			bool bServesMinOK = int.TryParse (servings[0], out min);

			bool bServingsMaxOK = true;
			if (servings.Count () > 1) 
			{
				bServingsMaxOK = int.TryParse (servings [1], out max);
			}
			else
				max = min;

			parsedNumServes = new Serves(min, max);

			return bServesMinOK && bServingsMaxOK;
		}

		public void Extract()
		{
			try
			{
				mRecipeTable = LoadWorksheet ("Recipe");

				//Check the structure of the worksheet
				int recipeNameRow = mRecipeTable.SearchForStringInColumn ("Recipe_Name", 1);
				int recipeDescriptionRow = mRecipeTable.SearchForStringInColumn ("Recipe_Description", 1);
				int recipePrepTimeRow = mRecipeTable.SearchForStringInColumn ("Recipe_prep_time", 1);
				int recipeCookTimeRow = mRecipeTable.SearchForStringInColumn ("Recipe_cook_time", 1);
				int recipeNumServesRow = mRecipeTable.SearchForStringInColumn ("Recipe_num_serves", 1);
				int recipeCategoriesRow = mRecipeTable.SearchForStringInColumn ("Recipe_categories", 1);
				int recipeTypeRow = mRecipeTable.SearchForStringInColumn ("Recipe_type", 1);
				int recipeIngredientsRow = mRecipeTable.SearchForStringInColumn ("Recipe_Ingredients", 1);
				int recipeStepsRow = mRecipeTable.SearchForStringInColumn ("Recipe_steps", 1);
				int recipeNotesRow = mRecipeTable.SearchForStringInColumn ("Recipe_Notes", 1);
				int recipeAuthorRow = mRecipeTable.SearchForStringInColumn ("Recipe_Author", 1);
				int recipeSourceRow = mRecipeTable.SearchForStringInColumn ("Recipe_source", 1);

				//Make sure we have all the headings present in the file
				Debug.Assert(recipeNameRow != 0 && recipeDescriptionRow != 0 && recipePrepTimeRow != 0 && recipeCookTimeRow != 0 &&
				             recipeNumServesRow != 0 && recipeCategoriesRow != 0 && recipeIngredientsRow != 0 && recipeStepsRow != 0 && recipeNotesRow != 0 &&
				             recipeAuthorRow != 0 && recipeSourceRow != 0, "Structure or recipe Xml has changed : " + XmlPath);

				//Recipe Name
				mName = mRecipeTable.Data[2,recipeNameRow];

				//Recipe Description
				mDescription = mRecipeTable.Data[2, recipeDescriptionRow];

				mPrepTime = mRecipeTable.Data[2, recipePrepTimeRow];
				mCookTime = mRecipeTable.Data[2, recipeCookTimeRow];

				Serves minMaxServes = new Serves(1,1);
				bool bServesParsedOK = TryParseNumServes(mRecipeTable.Data[2, recipeNumServesRow], out minMaxServes);
				Debug.Assert(bServesParsedOK);
				mNumServes = minMaxServes.Item1;

				mCategories = mRecipeTable.Data[2, recipeCategoriesRow].Split(',');
				mType = mRecipeTable.Data[2, recipeTypeRow].Split(',').ToList();

				//Extract the ingredients list. Extract the food column and count the number of non empty rows.
				int numIngredients = mRecipeTable.GetColumn(4, recipeIngredientsRow+1, recipeStepsRow-1).Where(item => item != "").Count();

				for (int ing=0; ing < numIngredients; ing++)
				{
					int rowIndex = recipeIngredientsRow+1+ing;

					Recipe.Ingredient ingredient = new Recipe.Ingredient {mQuantity= mRecipeTable.Data[2,rowIndex], 
																			mUnit = mRecipeTable.Data[3,rowIndex], 
																			mFood = mRecipeTable.Data[4,rowIndex], 
																			mPrep = mRecipeTable.Data[5,rowIndex]}; 
					mIngredientsList.Add(ingredient);
				}

				//Extract Recipe Steps
				int numRecipeSteps = mRecipeTable.GetColumn(4, recipeStepsRow+1, recipeNotesRow-1).Where(item => item != "").Count();

				for (int stepIndex=0; stepIndex < numRecipeSteps; stepIndex++)
				{
					int rowIndex = recipeStepsRow+1+stepIndex;

					Recipe.Step step = new Recipe.Step {mNumber = mRecipeTable.Data[2,rowIndex], 
														mTimer = mRecipeTable.Data[3,rowIndex], 
														mMethod = mRecipeTable.Data[4,rowIndex]}; 
					mRecipeSteps.Add(step);
				}

				//Extract Recipe Notes
				int numNotes = mRecipeTable.GetColumn(3, recipeNotesRow, recipeAuthorRow-1).Where(item => item != "").Count();

				for (int noteIndex=0; noteIndex < numNotes; noteIndex++)
				{
					string note = mRecipeTable.Data[3,noteIndex + recipeNotesRow];
					mRecipeNotes += note;
				}

				//Exrtact Author
				mAuthor = mRecipeTable.Data[2,recipeAuthorRow];

				//Exrtact Source
				mSource = mRecipeTable.Data[2,recipeSourceRow];

				mbHasParsed = true;
			}

			catch (Exception e) 
			{
				Console.WriteLine (e.Message);
			}
		}
	}
}
