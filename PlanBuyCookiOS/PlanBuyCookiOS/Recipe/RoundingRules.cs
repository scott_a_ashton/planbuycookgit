using System;

namespace PlanBuyCookiOS
{
	public class RoundingRules
	{
        public delegate RoundedAmount RoundingRuleFn(RecipeIngredient ingredient, float servingsMultiplier);

		public struct RoundedAmount
		{
			public float mQuantity;
			public string mUnit;
		}

		public RoundingRules ()
		{
		}

        public static RoundedAmount RoundingRule(int ruleIndex, ShoppingListIngredient ingredient)
        {
            return RoundingRule(ruleIndex, new RecipeIngredient(0, ingredient.Quantity, ingredient.mUnit, ingredient.mFood, ""), 1.0f);
        }

		public static RoundedAmount RoundingRule(int ruleIndex, RecipeIngredient ingredient, float servingsMultiplier)
		{
            RoundingRuleFn[] rules =
            {
                RoundingRule1,
                RoundingRule2,
                RoundingRule3,
                RoundingRule4,
                RoundingRule5,
                RoundingRule6,
                RoundingRule7,
                RoundingRule8,
                RoundingRule9,
                RoundingRule10,
                RoundingRule11,
                RoundingRule12,
                RoundingRule13,
                RoundingRule14,
                RoundingRule15,
                RoundingRule16,
                RoundingRule17,
                RoundingRule18,
            };
            RoundedAmount rounded;

            float quantityBeforeRuleApplication = ingredient.mQuantity * servingsMultiplier;

            if ((ruleIndex -1) >= 0 && (ruleIndex -1) < rules.Length)
                rounded = rules[ruleIndex-1](ingredient, servingsMultiplier);
            else
                rounded = DefaultRoundingRule (ingredient, servingsMultiplier);

            //Always prefer the shortened versions
            if (rounded.mUnit == "gram")
                rounded.mUnit = "g";
            if (rounded.mUnit == "kilogram")
                rounded.mUnit = "kg";
            if (rounded.mUnit == "litre")
                rounded.mUnit = "l";
            if (rounded.mUnit == "millilitre")
                rounded.mUnit = "ml";
            if (rounded.mUnit == "tablespoon")
                rounded.mUnit = "tbsp";
            if (rounded.mUnit == "teaspoon")
                rounded.mUnit = "tsp";

            //We have a custom max quantity that we use for Water - apply before any conversion rules are applied
            if (ingredient.mMax > 0.0f && rounded.mQuantity > ingredient.mMax)
                rounded.mQuantity = ingredient.mMax;

            //Convert grams to kg if we're over 1000
            if (IsGram(rounded.mUnit) && rounded.mQuantity >= 1000)
            {
                rounded.mUnit = "kg";
                rounded.mQuantity = rounded.mQuantity / 1000.0f;
            }

            //Convert ml to litres if we're over 1000
            if (IsMillilitre(rounded.mUnit) && rounded.mQuantity >= 1000)
            {
                rounded.mUnit = "l";
                rounded.mQuantity = rounded.mQuantity / 1000.0f;
            }
            if (IsSprig(rounded.mUnit) && rounded.mQuantity >= 16)
            {
                rounded.mUnit = "bunch";
                rounded.mQuantity = rounded.mQuantity / 16.0f;
            }
            if (IsTeaspoon(rounded.mUnit) && rounded.mQuantity >= 4)
            {
                rounded.mUnit = "tbsp";
                rounded.mQuantity = rounded.mQuantity / 4.0f;
            }
            //If the value was zero going in it should be zero going out.  Some rules have a minimum amount
            if (quantityBeforeRuleApplication== 0.0f)
                rounded.mQuantity = 0.0f;

            return rounded;
		}

        public static bool IsGram(string unitString)
        {
            string lowerUnitString = unitString.TrimEnd().ToLower();

            return (lowerUnitString == "g" || lowerUnitString == "gram");
        }

        public static bool IsKilogram(string unitString)
        {
            string lowerUnitString = unitString.TrimEnd().ToLower();

            return (lowerUnitString == "kg" || lowerUnitString == "kilogram");
        }

        public static bool IsMillilitre(string unitString)
        {
            string lowerUnitString = unitString.TrimEnd().ToLower();

            return (lowerUnitString == "ml" || lowerUnitString == "millilitre");
        }

        public static bool IsLitre(string unitString)
        {
            string lowerUnitString = unitString.TrimEnd().ToLower();

            return (lowerUnitString == "l" || lowerUnitString == "litre");
        }
        public static void ConvertFromKgIfRequired(ref RecipeIngredient ingredient)
        {
            if (IsKilogram(ingredient.mUnit))
            {
                ingredient.mUnit = "g";
                ingredient.mQuantity *= 1000f;
            }
        }

        public static void ConvertFromLitreIfRequired(ref RecipeIngredient ingredient)
        {
            if (IsLitre(ingredient.mUnit))
            {
                ingredient.mUnit = "ml";
                ingredient.mQuantity *= 1000f;
            }
        }
        public static bool IsTeaspoon(string unitString)
        {
            string lowerUnitString = unitString.TrimEnd().ToLower();

            return (lowerUnitString == "tsp" || lowerUnitString == "teaspoon");
        }
        public static bool IsTablespoon(string unitString)
        {
            string lowerUnitString = unitString.TrimEnd().ToLower();

            return (lowerUnitString == "tbsp" || lowerUnitString == "tablespoon");
        }

        public static bool IsCup(string unitString)
        {
            string lowerUnitString = unitString.TrimEnd().ToLower();

            return (lowerUnitString == "cup");
        }
        public static bool IsBunch(string unitString)
        {
            string lowerUnitString = unitString.TrimEnd().ToLower();

            return (lowerUnitString == "bunch");
        }
        public static bool IsSprig(string unitString)
        {
            string lowerUnitString = unitString.TrimEnd().ToLower();

            return (lowerUnitString == "sprig");
        }
        public static bool IsPinch(string unitString)
        {
            string lowerUnitString = unitString.TrimEnd().ToLower();

            return (lowerUnitString == "pinch");
        }
        public static bool IsUnit(string unitString, string testAgainst)
        {
            string lowerUnitString = testAgainst.TrimEnd().ToLower();

            return (lowerUnitString == unitString);
        }

        public static void RuleError(int ruleIndex, string food, string usingUnits, string correctUnits)
        {
            Console.Error.WriteLine("{0}: {1}: Using: ({2}), Should be using: ({3})", ruleIndex, food, usingUnits, correctUnits);
        }

		public static RoundedAmount DefaultRoundingRule(RecipeIngredient ingredient, float servingsMultiplier)
		{
			RoundedAmount rounded = new RoundedAmount();
			float scaledAmount = ingredient.mQuantity * servingsMultiplier;

			rounded.mUnit = ingredient.mUnit;
            rounded.mQuantity = scaledAmount;
			return rounded;
		}

        //Rounding rule for floats
        public static RoundedAmount GeneralRoundingRule(int ruleIndex, RecipeIngredient ingredient, float servingsMultiplier, float multiple, float min, string enforceUnit="")
        {
            RoundedAmount rounded = new RoundedAmount ();
            float scaledAmount = ingredient.mQuantity * servingsMultiplier;

            if (scaledAmount < min)
                rounded.mQuantity = min;
            else {
                rounded.mQuantity = (float)Math.Round(scaledAmount / multiple) * multiple;
            }       

            if (enforceUnit !="" && ingredient.mUnit != enforceUnit)
                RuleError(ruleIndex, food:ingredient.mFood, usingUnits:ingredient.mUnit, correctUnits:enforceUnit);

            rounded.mUnit = ingredient.mUnit;

            return rounded;     
        }

		//	RULE 1 - Round to nearest 50g. Min 100g.
		//	•	All meat, fish, tinned fish, chorizo (cured):
		//	•	Pumpkin
		//	•	Cannellini beans
		//	•	Polenta (fine/instant)
		public static RoundedAmount RoundingRule1(RecipeIngredient ingredient, float servingsMultiplier)
		{
            //If in kg then convert to grams (it will get converted back later)
            ConvertFromKgIfRequired(ref ingredient);

            if (IsGram(ingredient.mUnit))
                return GeneralRoundingRule(1, ingredient, servingsMultiplier, multiple: 50, min: 100);
            else
                if (ingredient.mQuantity > 0)
                    RuleError(ruleIndex:1, food:ingredient.mFood, usingUnits:ingredient.mUnit, correctUnits:"g or kg");

			return DefaultRoundingRule(ingredient, servingsMultiplier);
		}

		//	RULE 2 - Nearest 50g. Min 50g.
		//	•	Green beans
		//	•	Mushrooms
		//	•	Roasted red capsicum (in jar)
		//	•	Frozen peas
		//	•	Baby spinach
		//
		public static RoundedAmount RoundingRule2(RecipeIngredient ingredient, float servingsMultiplier)
		{
            //If in kg then convert to grams (it will get converted back later)
            ConvertFromKgIfRequired(ref ingredient);

            if (IsGram(ingredient.mUnit))
                return GeneralRoundingRule(2, ingredient, servingsMultiplier, multiple: 50, min: 50);
            else
                if (ingredient.mQuantity > 0)
                    RuleError(ruleIndex:2, food:ingredient.mFood, usingUnits:ingredient.mUnit, correctUnits:"g or kg");

            return DefaultRoundingRule(ingredient, servingsMultiplier);
		}

		//	RULE 3 - Round to nearest 30g. Min 30g
		//	•	Soba noodles
		public static RoundedAmount RoundingRule3(RecipeIngredient ingredient, float servingsMultiplier)
		{
            //If in kg then convert to grams (it will get converted back later)
            ConvertFromKgIfRequired(ref ingredient);

            if (IsGram(ingredient.mUnit))
                return GeneralRoundingRule (3,ingredient, servingsMultiplier, multiple: 30, min: 30);
            else
                if (ingredient.mQuantity > 0)
                    RuleError(ruleIndex:3, food:ingredient.mFood, usingUnits:ingredient.mUnit, correctUnits:"g or kg");

			return DefaultRoundingRule(ingredient, servingsMultiplier);
		}

		//		RULE 4 - Nearest 10g. Min 10g
		//		•	Plain flour
		//		•	Tomato paste
		//		•	Caster sugar
		//		•	Almond meal
		//		•	Cracked wheat (burghul)
		//		•	Parmesan
		//		•	Corn (tinned or frozen)
        //      •   Natural yoghurt
		public static RoundedAmount RoundingRule4(RecipeIngredient ingredient, float servingsMultiplier)
		{
            //If in kg then convert to grams (it will get converted back later)
            ConvertFromKgIfRequired(ref ingredient);

            if (IsGram(ingredient.mUnit))
                return GeneralRoundingRule(4,ingredient, servingsMultiplier, multiple: 10, min: 10);
            else
                if (ingredient.mQuantity > 0)
                    RuleError(ruleIndex:4, food:ingredient.mFood, usingUnits:ingredient.mUnit, correctUnits:"g or kg");

			return DefaultRoundingRule(ingredient, servingsMultiplier);
		}

		//		RULE 5 - Nearest 5g. Min 5g.
		//		•	Sambal oelek
		//		•	Red curry paste
		//		•	Crispy fried shallots
		public static RoundedAmount RoundingRule5(RecipeIngredient ingredient, float servingsMultiplier)
		{
            //If in kg then convert to grams (it will get converted back later)
            ConvertFromKgIfRequired(ref ingredient);

            if (IsGram(ingredient.mUnit))
                return GeneralRoundingRule(5, ingredient, servingsMultiplier, multiple: 5, min: 5);
            else
                if (ingredient.mQuantity > 0)
                    RuleError(ruleIndex:5, food:ingredient.mFood, usingUnits:ingredient.mUnit, correctUnits:"g or kg");

			return DefaultRoundingRule(ingredient, servingsMultiplier);
		}

		//		RULE 6 - Round to nearest 10ml. Min 10ml
		//		•	Liquids (milk, olive oil, extra virgin olive oil, oyster sauce, soy sauce, fish sauce, sweet chilli sauce, evaporated milk 97% far free, all vinegars, water, mirin, rice vinegar, red wine, white wine)
		//		•	Sour cream
		public static RoundedAmount RoundingRule6(RecipeIngredient ingredient, float servingsMultiplier)
		{
            ConvertFromLitreIfRequired(ref ingredient);

            if (ingredient.mUnit == "ml" || ingredient.mUnit == "millilitre")
            {
                if ((ingredient.mQuantity * servingsMultiplier) <= 300.0f)
                    return GeneralRoundingRule(6, ingredient, servingsMultiplier, multiple: 10, min: 10);
                else
                    return GeneralRoundingRule(6, ingredient, servingsMultiplier, multiple: 100, min: 300);
            }
            else
                if (ingredient.mQuantity > 0)
                    RuleError(ruleIndex:6, food:ingredient.mFood, usingUnits:ingredient.mUnit, correctUnits:"ml or litre");

			return DefaultRoundingRule(ingredient, servingsMultiplier);
		}

		//		RULE 7 - Nearest 100ml. Min 100ml
		//		•	Stock
		//		•	Tomato passata puree
		//		•	Tinned diced tomatoes
		public static RoundedAmount RoundingRule7(RecipeIngredient ingredient, float servingsMultiplier)
		{
            ConvertFromLitreIfRequired(ref ingredient);

            if (ingredient.mUnit == "ml" || ingredient.mUnit == "millilitre")
                return GeneralRoundingRule(7, ingredient, servingsMultiplier, multiple: 100, min: 100);
            else
            {
                if (ingredient.mQuantity > 0)
                    RuleError(ruleIndex:7, food:ingredient.mFood, usingUnits:ingredient.mUnit, correctUnits:"ml or litre");
            }

			return DefaultRoundingRule(ingredient, servingsMultiplier);
		}

		//		RULE 8 - Whole numbers only. Min 1 
		//		•	Eggs
		//		•	Bacon rashers
		//		•	Corn cobs
		//		•	Cloves
		//		•	Black peppercorns
		//		•	Cinnamon sticks
		//		•	Kaffir lime leaves
		//		•	Smoked ham hock
		//		•	Garlic cloves
		//		•	Bay leaf
		//		•	Lemongrass
		//		•	Fresh herbs (when measured in sprigs)
		public static RoundedAmount RoundingRule8(RecipeIngredient ingredient, float servingsMultiplier)
		{
            RoundedAmount rounded = GeneralRoundingRule (8, ingredient, servingsMultiplier, multiple: 1, min: 1);

			//There are no preferred units
			rounded.mUnit = ingredient.mUnit;

			return rounded;
		}

		//		RULE 9 - Where measurement is in pinches, min 1 pinch. Whole pinches only
		//		•	Pepper, salt
		//		•	Dried chilli flakes
		public static RoundedAmount RoundingRule9(RecipeIngredient ingredient, float servingsMultiplier)
		{
			if (ingredient.mUnit == "pinch") 
                return GeneralRoundingRule (9, ingredient, servingsMultiplier, multiple: 1, min: 1);

            RuleError(ruleIndex:9, food:ingredient.mFood, usingUnits:ingredient.mUnit, correctUnits:"pinch");

			return DefaultRoundingRule(ingredient, servingsMultiplier);
		}

        //    RULE 10
        //    below ¼ teaspoon make a dash. Min 1 dash. Up in ¼ teaspoon increments until 0.5. Round to nearest 0.5 teaspoon – with bias towards rounding down
        //    •   Sciricacha (hot): 
        public static RoundedAmount RoundingRule10(RecipeIngredient ingredient, float servingsMultiplier)
        {
            if (ingredient.mUnit == "teaspoon" || ingredient.mUnit == "tsp" || ingredient.mUnit == "dash")
            {
                RoundedAmount rounded = new RoundedAmount();

                rounded.mQuantity = ingredient.mQuantity * servingsMultiplier;
                rounded.mUnit = "tsp";

                if (rounded.mQuantity < 0.25f)
                {
                    rounded.mQuantity = 1.0f;
                    rounded.mUnit = "dash";

                    return rounded;
                }
                else if (rounded.mQuantity <= 0.5f)
                {
                    rounded.mQuantity = (float)Math.Floor(rounded.mQuantity / 0.25f) * 0.25f;

                    return rounded;
                }
                    
                rounded.mQuantity = (float)Math.Floor(rounded.mQuantity / 0.5f) * 0.5f;

                return rounded;
            }
            else
                RuleError(ruleIndex:10, food:ingredient.mFood, usingUnits:ingredient.mUnit, correctUnits:"tsp or dash");

            return DefaultRoundingRule(ingredient, servingsMultiplier);
        }


		//	RULE 11 - Under 1.0 make nearest ½ . Min ½. Round down until ¾ then round up. Whole numbers above 1.0
		//	•	Onions
		//	•	Celery
		//	•	Lebanese cucumber
		//	•	Red capsicum
		//	•	Spring onion
		//	•	Potatoes (desiree and others)
		//	•	Zucchini
		//	•	Eggplant
		//	•	Red onion
		public static RoundedAmount RoundingRule11(RecipeIngredient ingredient, float servingsMultiplier)
		{
			RoundedAmount rounded = new RoundedAmount ();

			rounded.mQuantity = ingredient.mQuantity * servingsMultiplier;

			if (rounded.mQuantity < 0.5f)
				rounded.mQuantity = 0.5f;
			else if (rounded.mQuantity < 1.0f) {
				rounded.mQuantity = (rounded.mQuantity < 0.75f) ? 0.5f : 1.0f;
			} else {
                return GeneralRoundingRule (11, ingredient, servingsMultiplier, multiple: 1, min: 1);
			}

			rounded.mUnit = ingredient.mUnit;
			return rounded;
		}

        //    RULE 12 - Min ¼. Round to nearest ¼ up to half.  Round to nearest ½ after ½.
        //    Over 2, whole numbers only, rounding down under 0.75 round down
        //    •   Wombok cabbage
        //    •   Savoy cabbage
        //    •   Iceberg lettuce
        //    •   Lime
        //    •   Lemon

        public static RoundedAmount RoundingRule12(RecipeIngredient ingredient, float servingsMultiplier)
        {
            RoundedAmount rounded = new RoundedAmount ();

            rounded.mQuantity = ingredient.mQuantity * servingsMultiplier;
            rounded.mUnit = ingredient.mUnit;

            if (rounded.mQuantity < 0.25f)
                rounded.mQuantity = 0.25f;
            else if (rounded.mQuantity <= 0.5f)
            {
                rounded.mQuantity = (float)Math.Round(rounded.mQuantity / 0.25f) * 0.25f;
            }
            else if (rounded.mQuantity > 0.5f && rounded.mQuantity <= 2.0f)
            {
                rounded.mQuantity = (float)Math.Round(rounded.mQuantity / 0.5f) * 0.5f;
            }
            else if (rounded.mQuantity > 2.0f)
            {
                float fraction = rounded.mQuantity - (float)Math.Round(rounded.mQuantity);

                //Round up after 0.75
                if (fraction < 0.75f)
                    rounded.mQuantity = (float)Math.Round(rounded.mQuantity);
                else
                    rounded.mQuantity = (float)Math.Ceiling(rounded.mQuantity);
            }

            if (!IsUnit("number", ingredient.mUnit))
                RuleError(ruleIndex:12, food:ingredient.mFood, usingUnits:ingredient.mUnit, correctUnits:"number");
                
            return rounded;
        }


        //    RULE 13
        //    Round to nearest whole number when measured in sprigs. Min 3 sprigs.  (TODO: IS THIS CORRECT)
        //    In teaspoons, round to nearest teaspoon.
        //    In cups, round to nearest 0.5 cup.
        //    •   All fresh herbs

        //RULE 13 TO READ: Measured in sprigs up to 10. 
        // Whole numbers only. 
        // 10 sprigs = 0.5 bunch, 
        // 20 sprigs = 1 bunch. 
        // Anything in between should be 0.75. 
        // Over 1 bunch only in .5 and whole bunches (no .25 or .75) when higher than 1 bunch
        public static RoundedAmount RoundingRule13(RecipeIngredient ingredient, float servingsMultiplier)
        {
            RoundedAmount rounded = new RoundedAmount ();

            rounded.mQuantity = ingredient.mQuantity * servingsMultiplier;
            rounded.mUnit = ingredient.mUnit;

            if (IsUnit("sprig", rounded.mUnit))
            {
                //10 sprigs = 1/2 bunch  
                if (rounded.mQuantity >= 10)
                {
                    rounded.mUnit = "bunch";

                    rounded.mQuantity = rounded.mQuantity / 20f;

                    if (rounded.mQuantity > 0.5f && rounded.mQuantity < 1.0f)
                        rounded.mQuantity = 0.75f;
                    else if (rounded.mQuantity > 1.0f)
                    {
                        //round to the nearest half
                        rounded.mQuantity = (float)Math.Round(rounded.mQuantity / 0.5f) * 0.5f;
                    }
                }
                else
                {
                    //Round to nearest sprig, with a min of 1
                    return GeneralRoundingRule (13, ingredient, servingsMultiplier, multiple: 1, min: 1);
                }
            }
            else if (IsBunch(rounded.mUnit))
            {
                //Round to nearest half bunch
                if (rounded.mQuantity < 0.5f)
                {
                    rounded.mUnit = "sprig";
                    rounded.mQuantity = (float)Math.Round(rounded.mQuantity * 20.0f);

                    //Enforce a minimum of 1 sprig
                    if (rounded.mQuantity < 1.0f)
                        rounded.mQuantity = 1.0f;
                }
                else if (rounded.mQuantity > 0.5f && rounded.mQuantity < 1.0f)
                {
                    rounded.mQuantity = 0.75f;
                }
                else if (rounded.mQuantity > 1.0f)
                {
                    //round to the nearest half
                    rounded.mQuantity = (float)Math.Round(rounded.mQuantity / 0.5f) * 0.5f;
                }
            }
            else
            {
                RuleError(ruleIndex: 13, food: ingredient.mFood, usingUnits: ingredient.mUnit, correctUnits: "bunches or sprigs");
            }

            return rounded;
        }
            
//        RULE 14 Under ¼ of a teaspoon make a pinch. 
//        4 pinches = 0.25 teaspoon. 
//        Round to nearest ¼ teaspoon up to ½ . 
//        Above ½ round up to full teaspoon. 
//        Fractions of teaspoons at 0.5 and below round down. No ¾ = round up. 
//        3 teaspoons = 1 tablespoon in ground spices. 
//        4 teaspoons keep at 1 tablespoon. but 5 to 1.5 tbsp, 
//        6 tsp= 2 tbsp. Bias round down to nearest .5 tbsp

        public static RoundedAmount RoundingRule14(RecipeIngredient ingredient, float servingsMultiplier)
        {
            RoundedAmount rounded = new RoundedAmount();

            rounded.mQuantity = ingredient.mQuantity * servingsMultiplier;
            rounded.mUnit = ingredient.mUnit;

            if (IsPinch(rounded.mUnit))
            {
                if (rounded.mQuantity >= 4.0f)
                {
                    rounded.mQuantity = rounded.mQuantity / 4.0f;
                    rounded.mUnit = "tsp";
                }
                else if (rounded.mQuantity < 1.0f)
                {
                    //Minimum of 1 pinch
                    rounded.mQuantity = 1.0f;
                }
                else
                {
                    //Whole pinches only
                    rounded.mQuantity = (float)Math.Round(rounded.mQuantity);
                }
            }

            if (IsTeaspoon(rounded.mUnit))
            {
                if (rounded.mQuantity < 0.25f)
                {
                    rounded.mQuantity = 1;
                    rounded.mUnit = "pinch";
                }
                else if (rounded.mQuantity <= 0.5f)
                {
                    rounded.mQuantity = (float)Math.Round(rounded.mQuantity / 0.25f) * 0.25f;
                    rounded.mUnit = "tsp";
                }
                else if (rounded.mQuantity > 0.5f && rounded.mQuantity <= 1.0f)
                {
                    rounded.mQuantity = 1;
                    rounded.mUnit = "tsp";
                }
                else if (rounded.mQuantity > 1.0f)
                {
                    if (rounded.mQuantity >= 3 && rounded.mQuantity < 5)
                    {
                        rounded.mQuantity = 1;
                        rounded.mUnit = "tbsp";
                    }
                    else if (rounded.mQuantity >= 5.0f && rounded.mQuantity < 6)
                    {
                        rounded.mQuantity = 1.5f;
                        rounded.mUnit = "tbsp";
                    }
                    else if (rounded.mQuantity >= 6.0f)
                    {
                        rounded.mQuantity = rounded.mQuantity / 3.0f;
                        rounded.mQuantity = (float)Math.Round(rounded.mQuantity / 0.5f) * 0.5f;
                        rounded.mUnit = "tbsp";
                    }
                    else
                    {
                        //Round to nearest 1/2 teaspoon between >1 && <3
                        rounded.mQuantity = (float)Math.Round(rounded.mQuantity / 0.5f) * 0.5f;
                        rounded.mUnit = "tsp";
                    }
                }
            }
            return rounded;
        }


    
        //    RULE 15 - Min ¼ teaspoon. After ½ teaspoon round to nearest teaspoon. Whole numbers after 1
        //    •   Ginger
        //    •   Sesame seeds
        //    •   Sesame oil
        //    •   Salt
        public static RoundedAmount RoundingRule15(RecipeIngredient ingredient, float servingsMultiplier)
        {
            RoundedAmount rounded = DefaultRoundingRule(ingredient, servingsMultiplier);

            if (IsTeaspoon(ingredient.mUnit))
            {
                if (rounded.mQuantity < 0.25f)
                    rounded.mQuantity = 0.25f;
                else if (rounded.mQuantity <= 0.5f)
                    rounded.mQuantity = (float)Math.Round(rounded.mQuantity / 0.25f) * 0.25f;
                else
                    return GeneralRoundingRule(15, ingredient, servingsMultiplier, multiple: 1, min: 1);
            }
            else
                RuleError(ruleIndex:15, food:ingredient.mFood, usingUnits:ingredient.mUnit, correctUnits:"tsp");

            rounded.mUnit = ingredient.mUnit;

            return rounded;
        }

        //    RULE 16 - Min ¼ cup. Round to nearest ¼ cup up to ½ . Above ½round to nearest ½ cup
        //    •   Quinoa
        //    •   Currants
        //    •   Fresh herbs (when measured in cups)
        //
        public static RoundedAmount RoundingRule16(RecipeIngredient ingredient, float servingsMultiplier)
        {
            RoundedAmount rounded = DefaultRoundingRule(ingredient, servingsMultiplier);

            if (IsCup(ingredient.mUnit))
            {
                //Min ¼ cup. Round to nearest ¼ cup up to ½ 
                if (rounded.mQuantity <= 0.5f)
                    return GeneralRoundingRule(16, ingredient, servingsMultiplier, multiple: 0.25f, min: 0.25f);

                //Above ½round to nearest ½ cup
                return GeneralRoundingRule(16, ingredient, servingsMultiplier, multiple: 0.5f, min: 0.5f);
            }
            else
                RuleError(ruleIndex:16, food:ingredient.mFood, usingUnits:ingredient.mUnit, correctUnits:"cups");

            return rounded;
        }

        //    RULE 17 - Round to nearest ½ bunch.
        //    •   English spinach
        //
        public static RoundedAmount RoundingRule17(RecipeIngredient ingredient, float servingsMultiplier)
        {
            if (IsBunch(ingredient.mUnit))
            {
                //Round to nearest half cup
                return GeneralRoundingRule(17, ingredient, servingsMultiplier, multiple: 0.5f, min: 0.5f);
            }

            RuleError(ruleIndex:17, food:ingredient.mFood, usingUnits:ingredient.mUnit, correctUnits:"cups");

            return DefaultRoundingRule(ingredient, servingsMultiplier);
        }

        //RULE 18 - 
        // Min 1 tablespoon. 
        // 3 tablespoons = 0.25 cup. 
        // Nearest 0.25 cup after 3 tablespoons. 
        // After 1 cup, measure in .5 or whole numbers only. 

        public static RoundedAmount RoundingRule18(RecipeIngredient ingredient, float servingsMultiplier)
        {
            RoundedAmount rounded = new RoundedAmount();

            rounded.mQuantity = ingredient.mQuantity * servingsMultiplier;
            rounded.mUnit = ingredient.mUnit;

            if (IsTablespoon(rounded.mUnit))
            {
                if (rounded.mQuantity < 1.0f)
                {
                    rounded.mQuantity = 1.0f;
                    return rounded;
                }
                else if (rounded.mQuantity <= 3.0f)
                {
                    return GeneralRoundingRule(18, ingredient, servingsMultiplier, multiple: 1, min: 1);
                }
                else if (rounded.mQuantity > 3.0f)
                {
                    rounded.mQuantity = rounded.mQuantity / 12.0f;
                    rounded.mUnit = "cup";
                }
            }
            else if (!IsCup(rounded.mUnit))
            {
                RuleError(ruleIndex:18, food:ingredient.mFood, usingUnits:ingredient.mUnit, correctUnits:"tbsp or cups");
            }

            if (IsCup(rounded.mUnit))
            {
                if (rounded.mQuantity <= 1.0f)
                {
                    rounded.mQuantity = (float)Math.Round(rounded.mQuantity / 0.25f) * 0.25f;
                }
                else if (rounded.mQuantity > 1.0f)
                {
                    rounded.mQuantity = (float)Math.Round(rounded.mQuantity / 0.5f) * 0.5f;
                }
                return rounded;
            }
            else
            {
                RuleError(ruleIndex:18, food:ingredient.mFood, usingUnits:ingredient.mUnit, correctUnits:"cups");
            }

            return DefaultRoundingRule(ingredient, servingsMultiplier);
        }
            
	}
}

