using System;
using System.Collections.Generic;
using CoreGraphics;
using Foundation;
using UIKit;

namespace PlanBuyCookiOS
{
    public partial class ServesPopoverViewController: UIViewController
    {
        public delegate void UpdateCallbackDelegate(float sliderValue);
        UpdateCallbackDelegate mUpdateCallback;

        public ServesPopoverViewController() : base("ServesPopoverViewController",null)
        {
        }


        public override void DidReceiveMemoryWarning ()
        {
            // Releases the view if it doesn't have a superview.
            base.DidReceiveMemoryWarning ();

            // Release any cached data, images, etc that aren't in use.
        }

        public override void ViewDidLoad ()
        {
            base.ViewDidLoad();
        }


        partial void ServesSliderChanges(NSObject sender)
        {
            if (mUpdateCallback!=null)
                mUpdateCallback(ServesSlider.Value);
        }

        public float ServesSliderValue
        {
            set { ServesSlider.Value = value; }
            get { return ServesSlider.Value; }
        }

        public void SetUpdateCallback(UpdateCallbackDelegate updateCallback)
        {
            mUpdateCallback = updateCallback;;
        }
    }
        
}

