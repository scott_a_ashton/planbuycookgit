// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace PlanBuyCookiOS
{
    [Register("ServesPopoverViewController")]
	partial class ServesPopoverViewController
	{
		[Outlet]
		UIKit.UISlider ServesSlider { get; set; }

		[Action ("ServesSliderChanges:")]
		partial void ServesSliderChanges (Foundation.NSObject sender);
		
		void ReleaseDesignerOutlets ()
		{
			if (ServesSlider != null) {
				ServesSlider.Dispose ();
				ServesSlider = null;
			}
		}
	}
}
