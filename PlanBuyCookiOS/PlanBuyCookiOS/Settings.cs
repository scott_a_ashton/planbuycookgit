using System;
using System.Xml;
using System.Xml.Serialization;
using System.IO;


namespace PlanBuyCookiOS
{
	[Serializable]
	public class Settings
	{
        public static readonly int cMaxServes = 10;
        public static readonly int cMinServes = 1;
        public static readonly int cDefaultServes = 4;

		[XmlAttribute]
		public bool SkipIntro = false;

		[XmlAttribute]
		public bool mTutorial = true;

		[XmlAttribute]
        public int Serves = Settings.cDefaultServes;

        public readonly static string cDefaultEmail = "chef@somewhere.com";
		[XmlAttribute]
        public string EMail = cDefaultEmail;

		public Settings ()
		{

		}

        public bool Tutorial
        {
            set
            {
                mTutorial = value;
                HelpManager.Active = value;
            }
            get
            {
                return mTutorial;
            }
        }
		public void Initialise()
		{
			if (!File.Exists (SettingsPath))
				Save ();
			else 
			{
				try
				{
					Load ();
				}
				catch (Exception e) 
				{
					//We've an exception when loading the xml file, save out a new file just in case the structure has changed
					Save ();
				}
			}
		}

        public bool HasEditedEmail
        {
            get
            {
                return EMail != cDefaultEmail;
            }
        }
		public string SettingsPath
		{
			get 
			{
				return Path.Combine (Environment.GetFolderPath (Environment.SpecialFolder.MyDocuments), "Settings.xml");
			}
		}

		public void Save()
		{
			XmlSerializer serializer = new XmlSerializer(typeof(Settings));

			TextWriter writer = new StreamWriter(SettingsPath);

			serializer.Serialize(writer, this);
			writer.Close ();
		}

		public void Load()
		{
			// Construct an instance of the XmlSerializer with the type
			// of object that is being deserialized.
			XmlSerializer settingsSerializer = new XmlSerializer(typeof(Settings));

			FileStream fileStream = new FileStream(SettingsPath, FileMode.Open);

			// Call the Deserialize method and cast to the object type.
			Settings settings = (Settings)settingsSerializer.Deserialize (fileStream);

			SkipIntro = settings.SkipIntro;
			Serves = settings.Serves;
			EMail = settings.EMail;
			Tutorial = settings.Tutorial;
		}
	}
}

