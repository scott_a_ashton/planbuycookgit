using System;
using CoreGraphics;
using Foundation;
using UIKit;

namespace PlanBuyCookiOS
{
    public partial class SettingsPopoverViewController : UIViewController
    {
        public SettingsPopoverViewController() : base("SettingsPopoverViewController", null)
        {
        }

        public override void DidReceiveMemoryWarning()
        {
            // Releases the view if it doesn't have a superview.
            base.DidReceiveMemoryWarning();
			
            // Release any cached data, images, etc that aren't in use.
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
			
            // Perform any additional setup after loading the view, typically from a nib.
			SkipIntro.On = AppDelegate.Settings.SkipIntro;
            Tutorial.On = AppDelegate.Settings.Tutorial;
            if (AppDelegate.Settings.HasEditedEmail)
                EMail.Text = AppDelegate.Settings.EMail;
			Serves.Text = AppDelegate.Settings.Serves.ToString ();

            Serves.EditingDidEnd += (object sender, EventArgs e) => ErrorCheckServes();
        }

		public void UpdateSettings()
		{
            AppDelegate.Settings.Tutorial = Tutorial.On;
			AppDelegate.Settings.SkipIntro = SkipIntro.On;
			AppDelegate.Settings.EMail = EMail.Text;
			int.TryParse (Serves.Text, out AppDelegate.Settings.Serves);

			AppDelegate.Settings.Save ();
		}

        void ErrorCheckServes()
        {
            int serves = 0;
            if (!int.TryParse(Serves.Text, out serves))
            {
                AppDelegate.Settings.Serves = Settings.cDefaultServes;
                Serves.Text = Settings.cDefaultServes.ToString();
            }
            else
            {
                AppDelegate.Settings.Serves = Utils.Clamp(serves, Settings.cMinServes, Settings.cMaxServes);
                Serves.Text = AppDelegate.Settings.Serves.ToString();
            }
        }

        partial void OpenPBCTutorial(NSObject sender)
        {
            if(UIApplication.SharedApplication.CanOpenUrl(NSUrl.FromString(AppDelegate.sPBCWebPage))) 
            {
                // Open in Safari instead
                UIApplication.SharedApplication.OpenUrl(NSUrl.FromString(AppDelegate.sPBCWebPage));
            }
        }
    }
}

