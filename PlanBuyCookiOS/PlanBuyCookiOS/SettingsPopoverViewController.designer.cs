// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace PlanBuyCookiOS
{
	[Register ("SettingsPopoverViewController")]
	partial class SettingsPopoverViewController
	{
		[Outlet]
		UIKit.UITextField EMail { get; set; }

		[Outlet]
		UIKit.UITextField Serves { get; set; }

		[Outlet]
		UIKit.UISwitch SkipIntro { get; set; }

		[Outlet]
		UIKit.UISwitch SyncsWithiCal { get; set; }

		[Outlet]
		UIKit.UISwitch Tutorial { get; set; }

		[Action ("OpenPBCTutorial:")]
		partial void OpenPBCTutorial (Foundation.NSObject sender);
		
		void ReleaseDesignerOutlets ()
		{
			if (EMail != null) {
				EMail.Dispose ();
				EMail = null;
			}

			if (Serves != null) {
				Serves.Dispose ();
				Serves = null;
			}

			if (SkipIntro != null) {
				SkipIntro.Dispose ();
				SkipIntro = null;
			}

			if (SyncsWithiCal != null) {
				SyncsWithiCal.Dispose ();
				SyncsWithiCal = null;
			}

			if (Tutorial != null) {
				Tutorial.Dispose ();
				Tutorial = null;
			}
		}
	}
}
