using System;
using CoreData;
using UIKit;
using Foundation;
using CoreGraphics;

namespace PlanBuyCookiOS
{
	public class UICircleOutlineView : UIView
	{
		UIImage mCircularImage;
		float mRadius;

		public UICircleOutlineView (CGRect circleImageRect, float radius) : base()
		{
			mRadius = radius;
			UpdateFrame (circleImageRect);

			ClearsContextBeforeDrawing = true;
			BackgroundColor = UIColor.Clear;
		}
			
		public void UpdateFrame(CGRect circleImageRect)
		{
			Frame = new CGRect (new CGPoint (circleImageRect.GetMidX ()-mRadius, circleImageRect.GetMidY ()-mRadius), new CGSize (mRadius*2, mRadius * 2));
		}

		public override void Draw (CGRect rect)
		{
			//UIImage *originalImage = [UIImage imageNamed:[NSString stringWithFormat:@"monk2.png"]];
//			UIImage originalImage = UIImage.FromFile ("CircleMask.png");
//
//			float oImageWidth = originalImage.Size.Width;
//			float oImageHeight = originalImage.Size.Height;
//
//			// Draw the original image at the origin
//			RectangleF oRect = new RectangleF(0, 0, oImageWidth, oImageHeight);
//			originalImage.DrawAsPatternInRect (oRect);
//
//			// Set the newRect to half the size of the original image 
//			RectangleF newRect = new RectangleF (0.0f, 0.0f, oImageWidth/2, oImageHeight/2);
//			UIImage newImage = CircularScaleNCrop (originalImage, newRect);
//
//			float nImageWidth = newImage.Size.Width;
//			float nImageHeight = newImage.Size.Height;
//
//			//Draw the scaled and cropped image
//			RectangleF thisRect = new RectangleF(oImageWidth+10.0f, 0.0f, nImageWidth, nImageHeight);
//			newImage.DrawAsPatternInRect (thisRect);
//
			//base.Draw (rect);

			CGPoint circleCentre = new CGPoint (rect.Location.X + rect.Width / 2, rect.Location.Y + rect.Height / 2);

			CGContext context = UIGraphics.GetCurrentContext ();
			context.SetStrokeColor (0.5f, 0.5f, 0.5f, 0.5f);
			context.SetLineWidth (5.0f);

			context.BeginPath ();
			context.AddArc(circleCentre.X, circleCentre.Y, mRadius-5, 0.0f, (float)(2.0*Math.PI), false);
			context.DrawPath (CGPathDrawingMode.Stroke);

			//context.ClosePath ();
			//context.Clip ();

			//mCircularImage.DrawAsPatternInRect (rect);
		}
	}
		
}

