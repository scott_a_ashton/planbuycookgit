using System;
using System.Linq;
using System.Collections.Generic;
using CoreGraphics;
using Foundation;
using UIKit;

namespace PlanBuyCookiOS
{
	public static class DateTimeNSDate
	{
		//After upgrading to the Unified API we now need this explicit conversion between NSDate and DateTime
		public static DateTime NSDateToDateTime(this NSDate date)
		{
			// NSDate has a wider range than DateTime, so clip
			// the converted date to DateTime.Min|MaxValue.
			double secs = date.SecondsSinceReferenceDate;
			if (secs < -63113904000)
				return DateTime.MinValue;
			if (secs > 252423993599)
				return DateTime.MaxValue;
			return (DateTime) date;
		}

		public static NSDate DateTimeToNSDate(this DateTime date)
		{
			if (date.Kind == DateTimeKind.Unspecified)
				date = DateTime.SpecifyKind (date, DateTimeKind.Local /* DateTimeKind.Local or DateTimeKind.Utc, this depends on each app */);
			return (NSDate) date;
		}
	}

	public static class Utils
	{
		public static double DistanceTo(this CGPoint point1, CGPoint point2)
		{
			var a = (point2.X - point1.X);
			var b = (point2.Y - point1.Y);

			return (float)Math.Sqrt(a * a + b * b);
		}

		public static float Clamp( float value, float min, float max )
		{
			return (value < min) ? min : (value > max) ? max : value;
		}

        public static int Clamp( int value, int min, int max )
        {
            return (value < min) ? min : (value > max) ? max : value;
        }

		// resize the image to be contained within a maximum width and height, keeping aspect ratio
		public static UIImage MaxResizeImage(UIImage sourceImage, float maxWidth, float maxHeight)
		{
			var sourceSize = sourceImage.Size;
			var maxResizeFactor = Math.Max(maxWidth / sourceSize.Width, maxHeight / sourceSize.Height);

			if (maxResizeFactor > 1) 
				return sourceImage;

			var width = maxResizeFactor * sourceSize.Width;
			var height = maxResizeFactor * sourceSize.Height;

			UIGraphics.BeginImageContext(new CGSize(width, height));
			sourceImage.Draw(new CGRect(0, 0, width, height));
			var resultImage = UIGraphics.GetImageFromCurrentImageContext();
			UIGraphics.EndImageContext();

			return resultImage;
		}

		// resize the image (without trying to maintain aspect ratio)
		public static UIImage ResizeImage(UIImage sourceImage, float width, float height)
		{
			UIGraphics.BeginImageContext(new CGSize(width, height));
			sourceImage.Draw(new CGRect(0, 0, width, height));
			var resultImage = UIGraphics.GetImageFromCurrentImageContext();
			UIGraphics.EndImageContext();

			return resultImage;
		}

        public static UIImage CropToSquareImage(UIImage sourceImage)
        {
            if (sourceImage.CGImage.Width == sourceImage.CGImage.Height)
                return sourceImage;

            if (sourceImage.CGImage.Width > sourceImage.CGImage.Height)
            {
                nint diff = sourceImage.CGImage.Width - sourceImage.CGImage.Height;
				return CropImage(sourceImage, (int)(diff/2), 0, (int)sourceImage.CGImage.Height, (int)sourceImage.CGImage.Height);
            }
            else
            {
                nint diff = sourceImage.CGImage.Width - sourceImage.CGImage.Height;
				return CropImage(sourceImage, 0, (int)diff/2, (int)sourceImage.CGImage.Width, (int)sourceImage.CGImage.Width);
            }
        }

		// crop the image, without resizing
		public static UIImage CropImage(UIImage sourceImage, int crop_x, int crop_y, int width, int height)
		{
			var imgSize = sourceImage.Size;

			UIGraphics.BeginImageContext(new CGSize(width, height));
			var context = UIGraphics.GetCurrentContext();

			var clippedRect = new CGRect(0, 0, width, height);
			context.ClipToRect(clippedRect);
			var drawRect = new CGRect(-crop_x, -crop_y, imgSize.Width, imgSize.Height);
			sourceImage.Draw(drawRect);
			var modifiedImage = UIGraphics.GetImageFromCurrentImageContext();

			UIGraphics.EndImageContext();

			return modifiedImage;
		}
            
        static readonly float cResizePortraitsTo = UIScreen.MainScreen.Scale > 1.0f ? 128f : 64f;

        public static UIImage CreateCircularRecipeImagePortrait(UIImage recipeImage)
        {
            UIImage circleMaskImage = UIImage.FromFile("CircleMask.png");

            //If this is a non square image then crop first before resizing and masking
            UIImage tempImage = recipeImage;

            if (recipeImage.CGImage.Width > recipeImage.CGImage.Height)
				tempImage = Utils.CropImage(tempImage, (int)(recipeImage.CGImage.Width/4), 0, (int)recipeImage.CGImage.Height, (int)recipeImage.CGImage.Height);
            else if (recipeImage.CGImage.Width < recipeImage.CGImage.Height)
				tempImage = Utils.CropImage(tempImage,0, (int)(recipeImage.CGImage.Height/4), (int)recipeImage.CGImage.Width, (int)recipeImage.CGImage.Width);

            CGImage recipeCGImage = tempImage.CGImage;

            CGImage circleCGMask = circleMaskImage.CGImage;
			CGImage mask = CGImage.CreateMask ((int)circleCGMask.Width, (int)circleCGMask.Height, (int)circleCGMask.BitsPerComponent, (int)circleCGMask.BitsPerPixel, (int)circleCGMask.BytesPerRow, circleCGMask.DataProvider, null, false);
            CGImage maskedImage = recipeCGImage.WithMask (mask);

            UIImage maskedRecipeImage = new UIImage (maskedImage);

            //Resize the image
            return Utils.MaxResizeImage (maskedRecipeImage, cResizePortraitsTo, cResizePortraitsTo);
        }


        public static UIImage CreateCircularRecipeImagePortrait(IRecipeImport recipe)
        {
            NSData imageData = NSData.FromArray(Convert.FromBase64String (recipe.ImageString));
            UIImage recipeImage = UIImage.LoadFromData(imageData);

            return CreateCircularRecipeImagePortrait(recipeImage);
        }
	}
}

